<?php
final class Core_WebhookRequestWrapper {
    public $model;
    public $method;
    public $token;
    public $time;
    public $data;
}