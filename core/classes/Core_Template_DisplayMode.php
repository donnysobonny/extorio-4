<?php
final class Core_Template_DisplayMode {
	const basic_with_messages = "basic with messages";
	const basic_no_messages = "basic without messages";
	const full = "full";

}