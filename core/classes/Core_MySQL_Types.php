<?php
final class Core_MySQL_Types {
	const _BIGINT = "BIGINT";
	const _BLOB = "BLOB";
	const _CHAR = "CHAR";
	const _DATE = "DATE";
	const _DATETIME = "DATETIME";
	const _DECIMAL = "DECIMAL";
	const _DOUBLE = "DOUBLE";
	const _ENUM = "ENUM";
	const _FLOAT = "FLOAT";
	const _INT = "INT";
	const _LONGBLOB = "LONGBLOB";
	const _LONGTEXT = "LONGTEXT";
	const _MEDIUMBLOB = "MEDIUMBLOB";
	const _MEDIUMINT = "MEDIUMINT";
	const _MEDIUMTEXT = "MEDIUMTEXT";
	const _SMALLINT = "SMALLINT";
	const _TEXT = "TEXT";
	const _TIME = "TIME";
	const _TIMESTAMP = "TIMESTAMP";
	const _TINYBLOB = "TINYBLOB";
	const _TINYINT = "TINYINT";
	const _TINYTEXT = "TINYTEXT";
	const _VARCHAR = "VARCHAR";
	const _YEAR = "YEAR";

}