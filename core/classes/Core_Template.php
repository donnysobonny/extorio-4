<?php

class Core_Template {

public $name;
public $extensionName;
public $extensionRoot;

public $title;

public $area_1;
public $area_2;
public $area_3;
public $area_4;
public $area_5;
public $area_6;
public $area_7;
public $area_8;
public $area_9;
public $area_10;
public $area_11;
public $area_12;
public $area_13;
public $area_14;
public $area_15;
public $area_16;
public $area_17;
public $area_18;
public $area_19;
public $area_20;
public $area_21;
public $area_22;
public $area_23;
public $area_24;
public $area_25;
public $area_26;
public $area_27;
public $area_28;

public $pageContent;

public $errorMessages = array();
public $warningMessages = array();
public $infoMessages = array();
public $successMessages = array();

function __construct($name, $extensionName, $extensionRoot) {
    $this->name = $name;
    $this->extensionName = $extensionName;
    $this->extensionRoot = $extensionRoot;
}

/**
 * @param Core_Content $content
 */
final public function beforeServe() {
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $this->title ?></title>
    <!-- JS FILES -->
    <script type="text/javascript" src="/core/assets/js/jquery.js"></script>
    <script type="text/javascript" src="/core/assets/js/jquery-ui.js"></script>
    <script type="text/javascript" src="/core/assets/js/jquery-cookie.js"></script>
    <script type="text/javascript" src="/core/assets/js/jquery.datetimepicker.js"></script>

    <script type="text/javascript" src="/core/assets/js/bootstrap.js"></script>

    <script type="text/javascript" src="/core/assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/core/assets/js/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="/core/assets/js/froala_editor.min.js"></script>
    <script type="text/javascript" src="/core/assets/js/plugins/lists.min.js"></script>
    <script type="text/javascript" src="/core/assets/js/plugins/colors.min.js"></script>
    <script type="text/javascript" src="/core/assets/js/plugins/font_family.min.js"></script>
    <script type="text/javascript" src="/core/assets/js/plugins/font_size.min.js"></script>
    <script type="text/javascript" src="/core/assets/js/plugins/tables.min.js"></script>
    <script type="text/javascript" src="/core/assets/js/plugins/video.min.js"></script>

    <script type="text/javascript" src="/core/assets/js/Core_Extorio.js"></script>
    <script type="text/javascript" src="/core/assets/js/Core_Orm_ComplexModel.js"></script>
    <!-- CSS FILES -->
    <link rel="stylesheet" href="/core/assets/css/jquery-ui.css"/>
    <link rel="stylesheet" href="/core/assets/css/jquery.datetimepicker.css"/>

    <link rel="stylesheet" href="/core/assets/css/bootstrap.css"/>

    <link rel="stylesheet" href="/core/assets/css/jquery.dataTables.css"/>
    <link rel="stylesheet" href="/core/assets/css/dataTables.bootstrap.css"/>

    <link href="/core/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

    <link href="/core/assets/css/froala_editor.min.css" rel="stylesheet" type="text/css"/>
    <link href="/core/assets/css/froala_style.min.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="/core/assets/css/Core_Extorio.css"/>
</head>
<body>
<?php


}

public function onStart() {

}

public function onServe() {

}

final public function afterServe() {
?>
<script>
    $(function () {
        $('.froala_editable_non_inline').editable({inlineMode: false});
        $('.froala_editable_inline').editable({inlineMode: true});
    });
</script>
</body>
</html>
    <?php
}
}