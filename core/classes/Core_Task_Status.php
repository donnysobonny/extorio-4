<?php
final class Core_Task_Status {
	const completed = "completed";
	const failed = "failed";
	const killed = "killed";
	const pending = "pending";
	const running = "running";

}