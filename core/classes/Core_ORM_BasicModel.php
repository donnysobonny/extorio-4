<?php

/**
 * The basic data model. Extending this class allows for simple serialization of objects with the database.
 *
 * IMPORTANT! Basic and complex models cannot be mixed together.
 *
 * Basic Model Rules:
 * 1 - Your properties can only be of the following types: string, integer, float,
 * double boolean. Properties that are not one of these types will be ignored when pushed to the database. For use of
 * objects and nested objects, see the Core_ORM_ComplexModel
 * 2 - All basic model objects *must* have an "id" property.
 * 3 - All basic model objects *must* have a matching table already created in the database. The table name should
 * exactly match the class name, and the field names should exactly match the property names.
 * 4 - The "id" field in the database should be the primary key, and should auto_increment.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Classes
 *
 * Class Core_ORM_BasicModel
 */
class Core_ORM_BasicModel {

    /**
     * All objects MUST have an id
     *
     * @var int
     */
    public $id;

    /**
     * The accepted data types for the basic model properties
     *
     * @var array
     */
    protected $internal_acceptedTypes = array(Core_PHP_Types::_STRING, Core_PHP_Types::_BOOLEAN, Core_PHP_Types::_FLOAT, Core_PHP_Types::_DOUBLE, Core_PHP_Types::_INTEGER, Core_PHP_Types::_NULL);

    /**
     * Before the object is updated, a cache of the object's currently stored state is stored in this property. Otherwise this is always null.
     *
     * @var $internal_old $this
     */
    protected $internal_old;

    /**
     * Excluded properties
     *
     * @var array
     */
    protected $internal_excludedProperties = array("internal_excludedProperties", "internal_acceptedTypes", "internal_old");

    /**
     * Construct an empty version of this object
     *
     * @return $this
     */
    final public static function constructEmpty() {
        $thisClass = get_called_class();
        return new $thisClass();
    }

    /**
     * Construct this object from an associative array.
     *
     * All keys of the array must match the property names of the object. Any mismatches wil
     * be ignored.
     *
     * @param array $array
     *
     * @throws Core_MVC_Exception
     * @return $this
     *
     */
    final public static function constructFromArray($array) {
        if(gettype($array) == Core_PHP_Types::_ARRAY && !Core_Utils_Array::valuesEmpty($array)) {
            $thisClass = get_called_class();
            /** @var Core_ORM_BasicModel $baseObject */
            $baseObject = new $thisClass();

            //if the id is set, get the object first
            if(isset($array["id"])) {
                $baseObject->id = $array["id"];
                $baseObject->pullThis();
            }

            foreach($array as $key => $value) {
                if(in_array(gettype($value),array(
                    Core_PHP_Types::_STRING,
                    Core_PHP_Types::_BOOLEAN,
                    Core_PHP_Types::_INTEGER,
                    Core_PHP_Types::_NULL,
                    Core_PHP_Types::_FLOAT,
                    Core_PHP_Types::_DOUBLE
                ))) {
                    //overwrite the value
                    $baseObject->$key = $value;
                }
            }

            return $baseObject;
        } else {
            return false;
        }
    }

    /**
     * Construct the object from a json string.
     *
     * All keys of the array must match the property names of the object and it's child objects. Any mismatches wil
     * be ignored.
     *
     * @param string $jsonString
     *
     * @throws Core_MVC_Exception
     * @return $this
     */
    final public static function constructFromJson($jsonString) {
        $array = (array)json_decode($jsonString,true);
        if($array != null) {
            return self::constructFromArray($array);
        } else {
            return false;
        }
    }

    /**
     * Find the numeric count of all instances of this model using the orm finder
     *
     * @param Core_ORM_Finder $finder
     *
     * @return int
     * @throws Core_Db_Exception
     */
    final public static function findCount(Core_ORM_Finder $finder) {

        if(gettype($finder) == Core_PHP_Types::_OBJECT) {
            if(get_class($finder) != "Core_ORM_Finder") {
                $finder = new Core_ORM_Finder();
            }
        } else {
            $finder = new Core_ORM_Finder();
        }

        $db = Core_Extorio::get()->getDbInstanceDefault();
        $class = get_called_class();

        $results = array();

        $ssql = "SELECT count(base.id) FROM `" . $class . "` base ";
        if (strlen($finder->getWhere())) {
            $ssql .= "WHERE " . $finder->getWhere() . " ";
        }

        if (strlen($finder->getOrderBy()) && strlen($finder->getOrderDir())) {
            $ssql .= "ORDER BY " . $finder->getOrderBy() . " " . $finder->getOrderDir() . " ";
        }

        if ($finder->getLimitStart() || $finder->getLimitResults()) {
            if ($finder->getLimitStart()) {
                $ssql .= "LIMIT " . $finder->getLimitStart() . ", " . $finder->getLimitResults() . " ";
            } else {
                $ssql .= "LIMIT " . $finder->getLimitResults() . " ";
            }
        }

        $row = $db->getRowIndices($ssql);

        return $row[0];
    }

    /**
     * Find all instances of this model using the orm finder
     *
     * @param Core_ORM_Finder $finder
     *
     * @return $this[]
     * @throws Core_Db_Exception
     */
    final public static function findAll(Core_ORM_Finder $finder) {

        if(gettype($finder) == Core_PHP_Types::_OBJECT) {
            if(get_class($finder) != "Core_ORM_Finder") {
                $finder = new Core_ORM_Finder();
            }
        } else {
            $finder = new Core_ORM_Finder();
        }

        $db = Core_Extorio::get()->getDbInstanceDefault();
        $class = get_called_class();

        $results = array();

        $ssql = "SELECT base.id FROM `" . $class . "` base ";
        if (strlen($finder->getWhere())) {
            $ssql .= "WHERE " . $finder->getWhere() . " ";
        }

        if (strlen($finder->getOrderBy()) && strlen($finder->getOrderDir())) {
            $ssql .= "ORDER BY " . $finder->getOrderBy() . " " . $finder->getOrderDir() . " ";
        }

        if ($finder->getLimitStart() || $finder->getLimitResults()) {
            if ($finder->getLimitStart()) {
                $ssql .= "LIMIT " . $finder->getLimitStart() . ", " . $finder->getLimitResults() . " ";
            } else {
                $ssql .= "LIMIT " . $finder->getLimitResults() . " ";
            }
        }

        $rows = $db->getRowsAssociative($ssql);
        if ($rows) {
            foreach ($rows as $row) {
                $obj = new $class();
                $obj->id = $row["id"];
                $obj->pullThis();

                $results[] = $obj;
            }
        }

        return $results;
    }

    /**
     * Fine one instance of this model using the orm finder
     *
     * @param Core_ORM_Finder $finder
     *
     * @return $this
     */
    final public static function findOne(Core_ORM_Finder $finder) {

        if(gettype($finder) == Core_PHP_Types::_OBJECT) {
            if(get_class($finder) != "Core_ORM_Finder") {
                $finder = new Core_ORM_Finder();
            }
        } else {
            $finder = new Core_ORM_Finder();
        }

        $finder->limitResults(1);
        $results = self::findAll($finder);
        if (count($results)) {
            return $results[0];
        } else {
            return false;
        }
    }

    /**
     * Find one instance of this model by id, using the orm finder
     *
     * @param $id
     *
     * @return $this
     */
    final public static function findById($id) {
        return self::findOne(Core_ORM_Finder::newInstance()->where("base.id = ".$id));
    }

    /**
     * Push this object to the database.
     *
     * @return $this
     */
    final public function pushThis() {
        $class = get_class($this);
        $db = Core_Extorio::get()->getDbInstanceDefault();
        //check whether we are inserting/updating
        if ($this->id) {
            //check whether we are creating/updating
            $ssql = "SELECT id FROM `" . $class . "` WHERE id = " . $this->id . " LIMIT 1";
            if ($db->getRowIndices($ssql)) {
                return $this->updateThis();
            } else {
                return $this->createThis();
            }
        } else {
            return $this->createThis();
        }
    }

    /**
     * Pull this object from the database (based on the id)
     *
     * @return bool
     */
    final public function pullThis() {

        $this->beforeRetrieve();

        $class = get_class($this);
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $ssql = "SELECT * FROM `" . $class . "` WHERE id = " . $this->id . " LIMIT 1";
        $row = $db->getRowAssociative($ssql);
        if ($row) {
            foreach ($row as $key => $value) {
                $this->$key = $value;
            }

            $this->afterRetrieve();

            return true;
        } else {

            $this->afterRetrieve();

            return false;
        }
    }

    /**
     * Remove this object from the database (based on the id)
     */
    final public function removeThis() {

        $this->beforeDelete();

        $class = get_class($this);
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $ssql = "DELETE FROM `" . $class . "` WHERE id = " . $this->id . " LIMIT 1";
        $db->delete($ssql);

        $this->afterDelete();
    }

    /**
     * Return this object as an array
     *
     * @return mixed
     */
    final public function toArray() {
        return json_decode(json_encode($this), true);
    }

    /**
     * Return this object as a standard object
     *
     * @return mixed
     */
    final public function toStdObject() {
        return json_decode(json_encode($this));
    }

    /**
     * Return this object as a json string
     *
     * @return string
     */
    final public function toJson() {
        return json_encode($this);
    }

    /**
     * Display this object in a readable fashion.
     *
     */
    final public function print_r() {
        echo "<pre>";
        print_r($this->toStdObject());
        echo "</pre>";
    }

    /**
     * Event method. This event is triggered before an object is created in the database.
     */
    protected function beforeCreate() {

    }

    /**
     * Event method. This event is triggered after an object is created in the database.
     */
    protected function afterCreate() {

    }

    /**
     * Event method. This event is triggered before an object is updated in the database.
     */
    protected function beforeUpdate() {

    }

    /**
     * Event method. This event is triggered after an object is created in the database.
     */
    protected function afterUpdate() {

    }

    /**
     * Event method. This event is triggered before an object is deleted in the database.
     */
    protected function beforeDelete() {

    }

    /**
     * Event method. This event is triggered after an object is deleted in the database.
     */
    protected function afterDelete() {

    }

    /**
     * Event method. This event is triggered before the object is pulled from the database
     */
    protected function beforeRetrieve() {

    }

    /**
     * Event method. This event is triggered after the object is pulled from the database
     */
    protected function afterRetrieve() {

    }

    /**
     * Create this object in the database.
     *
     * @return $this
     */
    final private function createThis() {

        $this->beforeCreate();

        $class = get_class($this);
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $ssql = "INSERT INTO `" . $class . "` SET ";
        $ssql .= $this->createSetForSql() . " ";

        $this->id = $db->insert($ssql);

        $this->afterCreate();

        return $this;
    }

    /**
     * Update this object in the database.
     *
     * @return $this
     */
    final private function updateThis() {
        $old = self::findById($this->id);
        //only update if something has been updated
        if ($this != $old) {
            $this->internal_old = $old;

            $this->beforeUpdate();

            $class = get_class($this);
            $db = Core_Extorio::get()->getDbInstanceDefault();
            $ssql = "UPDATE `" . $class . "` SET ";
            $ssql .= $this->createSetForSql() . " ";
            $ssql .= "WHERE id = " . $this->id . " LIMIT 1";

            $db->update($ssql);

            $this->afterUpdate();

            return $this;
        } else {
            return false;
        }
    }

    /**
     * Create the "set" part of the sql
     *
     * @throws Core_Extorio_Exception
     * @return string
     */
    final private function createSetForSql() {
        $set = "";
        $db = Core_Extorio::get()->getDbInstanceDefault();
        foreach ($this as $key => $value) {
            if(!in_array($key, $this->internal_excludedProperties)) {
                if(!strlen($value)) {
                    $value = null;
                }
                $type = gettype($value);
                if (in_array($type, $this->internal_acceptedTypes)
                ) {
                    switch ($type) {
                        case Core_PHP_Types::_STRING :
                            $set .= "`" . $key . "` = ('" . $db->clean($value) . "'), ";
                            break;
                        case Core_PHP_Types::_BOOLEAN :
                            $bit = $value ? 1 : 0;
                            $set .= "`" . $key . "` = " . $bit . ", ";
                            break;
                        case Core_PHP_Types::_INTEGER :
                            $set .= "`" . $key . "` = " . $value . ", ";
                            break;
                        case Core_PHP_Types::_FLOAT :
                            $set .= "`" . $key . "` = " . $value . ", ";
                            break;
                        case Core_PHP_Types::_DOUBLE :
                            $set .= "`" . $key . "` = " . $value . ", ";
                            break;
                        case Core_PHP_Types::_NULL :
                            if($key != "id") {
                                $set .= "`" . $key . "` = (''), ";
                            }
                            break;
                        default :
                            throw new Core_Extorio_Exception("The class " . get_class($this) . " is a basic model and the property
                        " . $key . " is not an accepted data type.");
                    }
                }
            }
        }
        $set = substr($set, 0, strlen($set) - 2);

        if (!strlen($set)) {
            $set = "`id` = NULL ";
        }

        return $set;
    }
}
