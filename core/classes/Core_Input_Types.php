<?php
final class Core_Input_Types {
	const _checkbox = "checkbox";
	const _date = "date";
	const _datetime = "datetime";
	const _decimalfield = "decimalfield";
	const _dropdown = "dropdown";
	const _html = "html";
	const _model = "model";
	const _model_array = "model_array";
	const _numberfield = "numberfield";
	const _textarea = "textarea";
	const _textfield = "textfield";
	const _time = "time";

}