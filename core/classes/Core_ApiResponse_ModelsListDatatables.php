<?php
final class Core_ApiResponse_ModelsListDatatables extends Core_ApiResponse {
    public $draw;
    public $recordsTotal;
    public $recordsFiltered;
    public $data = array();
    public $error = "";

    /**
     * @param Exception $exception
     */
    public function on_exception_action($exception) {
        $this->error = $exception->getMessage();
        $this->display();
    }
}