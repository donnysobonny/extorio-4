<?php
final class Core_Messager {
    public static function addErrorMessage($message) {
        if(is_array($message)) {
            foreach($message as $msg) {
                $_SESSION["extorio_error_messages"][] = $msg;
            }
        } else {
            $_SESSION["extorio_error_messages"][] = $message;
        }
    }

    public static function addWarningMessage($message) {
        if(is_array($message)) {
            foreach($message as $msg) {
                $_SESSION["extorio_warning_messages"][] = $msg;
            }
        } else {
            $_SESSION["extorio_warning_messages"][] = $message;
        }
    }

    public static function addInfoMessage($message) {
        if(is_array($message)) {
            foreach($message as $msg) {
                $_SESSION["extorio_info_messages"][] = $msg;
            }
        } else {
            $_SESSION["extorio_info_messages"][] = $message;
        }
    }

    public static function addSuccessMessage($message) {
        if(is_array($message)) {
            foreach($message as $msg) {
                $_SESSION["extorio_success_messages"][] = $msg;
            }
        } else {
            $_SESSION["extorio_success_messages"][] = $message;
        }
    }

    public static function fetchErrorMessages() {
        $messages = array();

        if(isset($_SESSION["extorio_error_messages"])) {
            $messages = $_SESSION["extorio_error_messages"];
            unset($_SESSION["extorio_error_messages"]);
        }

        return $messages;
    }

    public static function fetchWarningMessages() {
        $messages = array();

        if(isset($_SESSION["extorio_warning_messages"])) {
            $messages = $_SESSION["extorio_warning_messages"];
            unset($_SESSION["extorio_warning_messages"]);
        }

        return $messages;
    }

    public static function fetchInfoMessages() {
        $messages = array();

        if(isset($_SESSION["extorio_info_messages"])) {
            $messages = $_SESSION["extorio_info_messages"];
            unset($_SESSION["extorio_info_messages"]);
        }

        return $messages;
    }

    public static function fetchSuccessMessages() {
        $messages = array();

        if(isset($_SESSION["extorio_success_messages"])) {
            $messages = $_SESSION["extorio_success_messages"];
            unset($_SESSION["extorio_success_messages"]);
        }

        return $messages;
    }
}