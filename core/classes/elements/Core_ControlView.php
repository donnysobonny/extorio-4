<?php
class Core_ControlView extends Core_Element {

    public function onLoad() {

    }

    public function onDefault() {

    }

    public function onComplete() {

    }

    public function canView() {

    }

    public function canEdit() {

    }

    final protected function redirectToMethod($method=false,$urlParams=array(),$queryParams=array()) {
        header("Location: ".$this->getUrlToMethod($method,$urlParams,$queryParams));
        exit;
    }

    final protected function getUrlToMethod($method=false,$urlParams=array(),$queryParams=array()) {
        $url = $this->getUrlBase();
        if($method) {
            $url .= "/".$method;
        }
        return $this->getUrl($url,$urlParams,$queryParams);
    }

    final protected function getUrlBase() {
        $page = Core_Page::findByController($this->name);
        if(!$page) {
            $page = Core_Page::findByView($this->name);
        }
        $url = $page->requestAddress;
        $url = Core_Utils_String::notEndsWith($url,"/");
        return $url;
    }
}