<?php
class Core_Api extends Core_Element {

    /**
     * @var Core_ApiResponse
     */
    public $output;
    public $output_type = "json";

    public $httpMethod = "GET";

    public $urlFields = array();
    public $urlMethod = false;
    public $urlParams = array();

    public $postParams = array();

    public $internal_properties = array(
        "output",
        "httpMethod",
        "urlFields",
        "urlMethod",
        "urlParams",
        "postParams",
        "internal_properties"
    );

    public function canAccess() {
        //can access by default
        return true;
    }

    final public function throwError($message,$code=0,$statusCode=500,$exceptionType="Core_Api_Exception") {
        $this->output->setError($message,$code);
        $this->output->setStatusCode($statusCode);
        throw new $exceptionType($message,$code);
    }

    final protected function validateHttpMethod($validMethod,$throwError=false) {
        $validMethod = strtoupper($validMethod);
        if($this->httpMethod != $validMethod) {
            if($throwError) {
                $this->throwError("This method is not allowed on this api endpoint",0,405);
            } else {
                return false;
            }
        }
        return true;
    }

    final protected function validateLoggedInUser($userType,$accessLevel=false,$throwError=false) {
        //make sure $userType is a valid user model
        $class = Core_Class::findOne(Core_ORM_Finder::newInstance()
            ->where("base.name = ('".$userType."') AND base.type = ('user')")
        );
        if(!$class) {
            $this->throwError("Trying to validate the user type ".$userType." but it is not a valid user model.");
        }

        /** @var Core_BaseModel_type_User $type */
        $type = $userType;

        $check = false;
        if($accessLevel) {
            $check = $type::authenticateAsAccessLevel($accessLevel);
        } else {
            $check = $type::authenticate();
        }

        if(!$check && $throwError) {
            $this->throwError("Unauthorized",0,401);
        }

        return $check;
    }

    final protected function getLoggedInUser($userType) {
        /** @var Core_BaseModel_type_User $type */
        $type = $userType;

        return $type::getLoggedInUser();
    }

    /**
     * @param Exception $exception
     */
    final public function api_on_exception($exception) {
        if(!$this->output->error) {
            $this->output->setError($exception->getMessage(),$exception->getCode());
        }

        if($this->output->status_code == 200) {
            $this->output->setStatusCode(Core_HTTPStatusCodes::_500);
        }

        $this->output->display($this->output_type);
    }
}