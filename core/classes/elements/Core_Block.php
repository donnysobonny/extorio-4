<?php

class Core_Block extends Core_Element {

    public $id;
    public $title;
    public $panelMode;
    public $options;

    public function onStart() {

    }

    public function onLoad() {

    }

    public function onDefault() {

    }

    public function options() {

    }

    public function onComplete() {

    }

    public function canView() {

    }

    public function canEdit() {

    }

    final protected function fetchOptions() {
        $liveBlock = Core_LiveBlock::findById($this->id);
        if($liveBlock) {
            $this->options = json_decode($liveBlock->optionsJson,true);
        }
    }

    final protected function pushOptions() {
        $liveBlock = Core_LiveBlock::findById($this->id);
        if($liveBlock) {
            $liveBlock->optionsJson = json_encode($this->options);
            $liveBlock->pushThis();
        }
    }

    final protected function redirectToMethod($method=false,$urlParams=array(),$queryParams=array()) {
        header("Location: ".$this->getUrlToMethod($method,$urlParams,$queryParams));
        exit;
    }

    final protected function getUrlToMethod($method=false,$urlParams=array(),$queryParams=array()) {
        $url = $this->getUrlBase();
        if($method) {
            $url .= "/".$method;
        }
        return $this->getUrl($url,$urlParams,$queryParams);
    }

    final protected function getUrlBase() {
        $fields = Core_Utils_Server::getRequestURLFields();
        return "/" . $fields[0] . "/" . $fields[1] . "/" . $fields[2] . "/" . $fields[3];
    }

    final protected function setDisplayType($displayType) {
        $this->displayType = $displayType;
    }
}