<?php
final class Core_PropertyHierarchy {
    public $index;
    public $leftClassName;
    public $rightClassName;
    public $leftToRightProperty;
    public $parentPropertyPathLong;
    public $propertyPathShort;
    public $propertyPathLong;
}