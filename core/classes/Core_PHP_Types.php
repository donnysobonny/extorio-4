<?php
final class Core_PHP_Types {
	const _ARRAY = "array";
	const _BOOLEAN = "boolean";
	const _DOUBLE = "double";
	const _FLOAT = "float";
	const _INTEGER = "integer";
	const _NULL = "NULL";
	const _OBJECT = "object";
	const _RESOURCE = "resource";
	const _STRING = "string";
	const _UNKNOWN = "unknown type";
}