<?php
class Core_Extension {

    public $name;
    public $root;

    //the autoload classes
    protected $classDirs = array(
        "classes",
        "utilities",
        "exceptions",
        "models",
        "helpers",
        "extension"
    );

    protected $elementTypes = array(
        "controllers",
        "views",
        "templates",
        "blocks",
        "tasks",
        "interfaces"
    );

    final protected function Extorio() {
        return Core_Extorio::get();
    }

    final public function __construct($extensionName,$extensionRoot) {
        //make sure this was constructed only by Core_Extorio
        $trace = debug_backtrace();
        if(
            count($trace) < 2 ||
            !isset($trace[1]["class"]) ||
            $trace[1]["class"] !== "Core_Extorio"
        ) {
            throw new Exception("An extension object can only be constructed by Core_Extorio");
        }

        $this->name = $extensionName;
        $this->root = $extensionRoot;
    }

    final protected function addClassDir($classDir) {
        $this->classDirs[] = $classDir;
    }

    final public function getClassDirs() {
        return $this->classDirs;
    }

    final protected function addElementType($type) {
        $this->elementTypes[] = $type;
    }

    /**
     * @return Core_InstalledExtension
     */
    final protected function getInstalledExtension() {
        return Core_InstalledExtension::findOne(Core_ORM_Finder::newInstance()
            ->where("base.name = ('".$this->name."')")
        );
    }

    final public function getElementTypes() {
        return $this->elementTypes;
    }

    /**
     * Find a controller that is owned by this extension
     *
     * @param string $controllerName
     *
     * @return Core_ControlView
     */
    final public function constructController($controllerName) {
        return $this->constructElement("controllers",$controllerName);
    }

    /**
     * Find all controllers owned by this extension
     *
     * @return Core_ControlView[]
     */
    final public function constructAllControllers() {
        return $this->constructElements("contollers");
    }

    /**
     * Find a view owned by this extension
     *
     * @param string $viewName
     *
     * @return Core_ControlView
     */
    final public function constructView($viewName) {
        return $this->constructElement("views",$viewName);
    }

    /**
     * Find all views owned by this controller
     *
     * @return Core_ControlView[]
     */
    final public function constructAllViews() {
        return $this->constructElements("views");
    }

    /**
     * Find a template owned by this extension
     *
     * @param string $templateName
     *
     * @return Core_Template
     */
    final public function constructTemplate($templateName) {
        return $this->constructElement("templates",$templateName);
    }

    /**
     * Find all templates owned by this extension
     *
     * @return Core_Template[]
     */
    final public function constructAllTemplates() {
        return $this->constructElements("templates");
    }

    /**
     * Find a block owned by this extension
     *
     * @param string $blockName
     *
     * @return Core_Block
     */
    final public function constructBlock($blockName) {
        return $this->constructElement("blocks",$blockName);
    }

    /**
     * Find all blocks owned by this extension
     *
     * @return Core_Block[]
     */
    final public function constructAllBlocks() {
        return $this->constructElements("blocks");
    }

    /**
     * @param $taskName
     *
     * @return Core_Task
     */
    final public function constructTask($taskName) {
        return $this->constructElement("tasks",$taskName);
    }

    /**
     * @return Core_Task[]
     */
    final public function constructAllTasks() {
        return $this->constructElements("tasks");
    }

    final public function constructApi($apiName) {
        return $this->constructElement("apis",$apiName);
    }

    final public function constructAllApis() {
        return $this->constructElements("apis");
    }

    /**
     * Construct all elements by type
     *
     * @param $type
     *
     * @return Core_Element[]
     */
    final public function constructElements($type) {
        $files = $this->findFilesInDir($type);
        $classes = array();
        foreach($files as $file) {
            $type = $file->fileName;
            if(!class_exists($type)) {
                include_once($file->path);
            }
            $classes[] = new $type($type,$this->name,$this->root);
        }
        return $classes;
    }

    /**
     * Construct an element by type and name.
     *
     * @param $type
     * @param $className
     *
     * @return Core_Element
     *
     * @throws Core_Extension_Exception
     */
    final public function constructElement($type,$className) {
        $file = $this->findClassFileInDir($type,$className);
        if($file) {
            $type = $file->fileName;
            if(!class_exists($type)) {
                include_once($file->path);
            }
            return new $type($type,$this->name,$this->root);
        } else {
            return false;
        }
    }

    final public function findClassFile($className) {
        //go through the class dirs to find the class file
        foreach($this->classDirs as $dir) {
            $file = $this->findClassFileInDir($dir,$className);
            if($file) {
                return $file;
            }
        }
        //try the element types too just incase
        foreach($this->elementTypes as $dir) {
            $file = $this->findClassFileInDir($dir,$className);
            if($file) {
                return $file;
            }
        }
        return false;
    }

    final public function findClassFileInDir($dir,$className) {
        return $this->findFileInDir($dir,$className.".php");
    }

    final public function findFileInDir($dir,$fileName) {
        $dir = Core_Utils_String::startsWith($dir,"/");
        if(file_exists($this->root.$dir)) {
            $files = Core_Utils_File::findFilesInFolderRecursively($this->root.$dir);
            foreach($files as $file) {
                if($file->baseName == $fileName) {
                    return $file;
                }
            }
        }
        return false;
    }

    final public function findFilesInDir($dir,$extensions=array("php")) {
        $dir = Core_Utils_String::startsWith($dir,"/");
        $foundFiles = array();
        if(file_exists($this->root.$dir)) {
            $files = Core_Utils_File::findFilesInFolderRecursively($this->root.$dir);
            foreach($files as $file) {
                if(!empty($extensions)) {
                    if(in_array($file->extension,$extensions)) {
                        $foundFiles[] = $file;
                    }
                } else {
                    $foundFiles[] = $file;
                }
            }
        }

        return $foundFiles;
    }

    final public function getConfigAll() {
        $settings = array();
        //get the config first
        $file = $this->findFileInDir("config","config.php");
        if($file) {
            include($file->path);
            if(is_array($config)) {
                foreach($config as $key => $value) {
                    $settings[$key] = $value;
                }
            }
        }
        //get the override
        $file = $this->findFileInDir("config","config_override.php");
        if($file) {
            include($file->path);
            if(is_array($config)) {
                foreach($config as $key => $value) {
                    $settings[$key] = $value;
                }
            }
        }
        return $settings;
    }

    final public function getConfigOverride() {
        $settings = array();
        //get the override
        $file = $this->findFileInDir("config","config_override.php");
        if($file) {
            include($file->path);
            if(is_array($config)) {
                foreach($config as $key => $value) {
                    $settings[$key] = $value;
                }
            }
        }
        return $settings;
    }

    final public function addToConfigOverride($key,$value) {
        $this->setConfigOverride(array($key => $value));
    }

    final public function setConfigOverride($settings) {
        $current = $this->getConfigOverride();
        foreach($settings as $key => $value) {
            $current[$key] = $value;
        }
        //get the config_override file
        $file = Core_Utils_File::createFile($this->root."/config/config_override.php");
        if($file) {
            $file->open("w");
            $file->write('<?php
$config = '.var_export($current,true).';
            ');
            $file->close();
        }
        //update in extorio
        $this->Extorio()->updateStoredConfig($current);
    }

    final public function setLoadPriority($priority) {
        $installed = $this->getInstalledExtension();
        if($installed) {
            $installed->loadPriority = intval($priority);
            $installed->pushThis();
        }
    }

    final public function enable() {

        $this->onEnable();

        //find the installed extension and enable it
        $installed = $this->getInstalledExtension();
        if($installed) {
            $installed->isEnabled = true;
            $installed->pushThis();
        }
    }

    final public function disable() {

        $this->onDisable();

        //find the extension and disable it
        $installed = $this->getInstalledExtension();
        if($installed) {
            $installed->isEnabled = false;
            $installed->pushThis();
        }
    }

    public function onStart() {

    }

    public function onEnable() {

    }

    public function onDisable() {

    }

    public function onStartController(&$controller) {

    }

    public function onStartView(&$view) {

    }

    public function onStartTemplate(&$template) {

    }

    public function onStartBlock(&$block) {

    }

    public function onStartTask(&$task) {

    }

    public function onStartApi(&$api) {}
}