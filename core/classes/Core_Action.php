<?php

/**
 * A core action. This is a function/method that is bound to an event. When an event is run,
 * it runs all bound actions in order of priority
 *
 * The first action that is found with overrideOtherActions = true will be returned on it's own,
 * ignoring all other actios.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Classes
 *
 * Class Core_Action
 */
final class Core_Action {
    /**
     * The event name this action is bound to
     *
     * @var string
     */
    public $eventName;
    /**
     * The target object that contains the callable method
     *
     * @var string
     */
    public $targetObject;
    /**
     * The target method of the object to call
     *
     * @var string
     */
    public $targetMethod;
    /**
     * The priority of this action. The lower the number, the higher the priority
     *
     * @var
     */
    public $priority;
    /**
     * The first action that is found with this property = true is returned on it's own and all other actions bound
     * to the event are ignored
     *
     * @var bool
     */
    public $overrideOtherActions;

    /**
     * @param string $eventName
     * @param string $overrideOtherActions
     * @param int $priority
     * @param string $targetMethod
     * @param object $targetObject
     */
    function __construct($eventName, $overrideOtherActions, $priority, $targetMethod, $targetObject) {
        $this->eventName = $eventName;
        $this->overrideOtherActions = $overrideOtherActions;
        $this->priority = $priority;
        $this->targetMethod = $targetMethod;
        $this->targetObject = $targetObject;
    }

    /**
     * Run this action and return the response of the action. Returns NULL if the action didn't run or didn't return
     * anything.
     *
     * @param array $params an optional array of params to send to the target method.
     *
     * @return mixed|null
     */
    public function run($params=array()) {
        $result = null;
        if(is_callable(array($this->targetObject,$this->targetMethod))) {
            $result = call_user_func_array(array(&$this->targetObject, $this->targetMethod),$params);
        }
        return $result;
    }
}