<?php

/**
 * The complex model expands upon the functionality of the basic model. Extending thie class allows for far more
 * functionality than the basic model.
 *
 * IMPORTANT!
 * - basic and complex models cannot be mixed together.
 * - when you create models via the /extensions/EORM4/settings/modelmanager interface, the class files are created for
 * you automatically, as well as required data such as the class data and properties data. Therefore,
 * it is not recommended to simply extend this class, but rather to construct your models in the modelmanager and
 * make use of the class files it creates.
 *
 * Basic model rules:
 * 1 your properties can only be of the following types: string, boolean, integer, float,
 * object (must be another model), array (must be an array of the same model)
 * 2 all models *must* have an id property
 * 3 all models *must* have the corresponding table set up, along with data stored on the class and it's properties.
 * If you create the model via the modelmanager interface this will be done for you.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Models
 *
 * Class Core_ORM_ComplexModel
 */
class Core_ORM_ComplexModel {
    /**
     * The id
     *
     * @var
     */
    public $id;

    /**
     * The date this object was created. All objects have this property
     *
     * @var string
     */
    public $dateCreated;

    /**
     * The date this object was updated. All objects have this property.
     *
     * @var string
     */
    public $dateUpdated;

    /**
     * Internal property to tell us whether the object is persistent or not.
     *
     * Persistent objects will remain even when their parent object is removed. This is because the object *can*
     * exist as a child of another object and therefore is independant/persistent.
     *
     * Non-persistent objects will be removed when their parent object is removed. This is because the object can is
     * only a child of the parent object, and is dependant on that parent object. The id of a non-persistent object will
     * change regularly.
     *
     * @var bool
     */
    protected $internal_isPersistent = true;

    /**
     * The current Core_Db instance
     *
     * @var Core_Db
     */
    protected $internal_dbConnection = false;

    /**
     * This array is populated by the modelmanager, and specifies the property => type. Properties will always be
     * forced to be the type specified.
     *
     * @var array
     */
    protected $internal_propertyTypes = array();

    /**
     * This array is populated by the modelmanager, and specifies the property => classType. Properties will always
     * be converted to this class type if they are not already this class type.
     *
     * @var array
     */
    protected $internal_objectTypes = array();

    /**
     * A cache of the properties that are simple on the current object.
     *
     * @var array
     */
    protected $internal_SimpleProperties = array();
    /**
     * A cache of the properties that are complex (array/object) on the current object
     *
     * @var array
     */
    protected $internal_ComplexProperties = array();

    /**
     * Before the object is updated, a cache of the object's currently stored state is stored in this property. Otherwise this is always null.
     *
     * @var $internal_old $this
     */
    protected $internal_old;

    private $internal_accessChecksEnabled = true;

    private $internal_webhooksEnabled = true;

    /**
     * Accepted types for properties. Properties that are not of these types will be ignored.
     *
     * @var array
     */
    private $internal_acceptedTypes = array(
        Core_PHP_Types::_STRING,
        Core_PHP_Types::_INTEGER,
        Core_PHP_Types::_BOOLEAN,
        Core_PHP_Types::_FLOAT,
        Core_PHP_Types::_OBJECT,
        Core_PHP_Types::_ARRAY
    );

    /**
     * Get the property description of this model
     *
     * @return Core_PropertyDescription[]
     */
    final public static function getDescription_Array() {
        $thisClass = get_called_class();
        $propertyDescriptions = array();

        //get the properties of this object
        $class = Core_Class::findByName($thisClass);
        $properties = Core_Property::findAllByOwnerClassId($class->id);

        //build the property descriptions
        foreach($properties as $property) {
            $propertyDescriptions[$property->name] = Core_PropertyDescription::constructFromProperty($property);
        }

        return $propertyDescriptions;
    }

    final public static function getDescription_Html() {
        $thisClass = get_called_class();
        $propertyDescriptions = self::getDescription_Array();

        $html = '<table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
    <thead>
        <tr>
            <th style="background: none repeat scroll 0% 0% grey; border: 1px solid black; color: rgb(255, 255, 255); padding: 4px; font-size: 24px;" colspan="10">'.$thisClass.'</th>
        </tr>
        <tr>
            <th style="background: none repeat scroll 0% 0% grey; color: white; border: 1px solid black; padding: 3px; font-size: 18px;" >name</th>
            <th style="background: none repeat scroll 0% 0% grey; color: white; border: 1px solid black; padding: 3px; font-size: 18px;" >input type</th>
            <th style="background: none repeat scroll 0% 0% grey; color: white; border: 1px solid black; padding: 3px; font-size: 18px;" >PHP type</th>
            <th style="background: none repeat scroll 0% 0% grey; color: white; border: 1px solid black; padding: 3px; font-size: 18px;" >MySQL type</th>
            <th style="background: none repeat scroll 0% 0% grey; color: white; border: 1px solid black; padding: 3px; font-size: 18px;" >max length</th>
            <th style="background: none repeat scroll 0% 0% grey; color: white; border: 1px solid black; padding: 3px; font-size: 18px;" >model type</th>
            <th style="background: none repeat scroll 0% 0% grey; color: white; border: 1px solid black; padding: 3px; font-size: 18px;" >dropdown type</th>
            <th style="background: none repeat scroll 0% 0% grey; color: white; border: 1px solid black; padding: 3px; font-size: 18px;" >primary</th>
            <th style="background: none repeat scroll 0% 0% grey; color: white; border: 1px solid black; padding: 3px; font-size: 18px;" >indexed</th>
            <th style="background: none repeat scroll 0% 0% grey; color: white; border: 1px solid black; padding: 3px; font-size: 18px;" >unique</th>
        </tr>
    </thead>
    <tbody>
        ';
        $even = true;
        $style = "background: none repeat scroll 0% 0% rgb(243, 243, 243);";
        foreach($propertyDescriptions as $description) {
            $html .= '
        <tr style="';
            if($even) {
                $html .= $style;
            }
            $even  = !$even;
            $html .= '">
            <td style="padding: 2px 4px; font-size: 15px;">'.$description->name.'</td>
            <td style="padding: 2px 4px; font-size: 15px;">'.$description->inputType.'</td>
            <td style="padding: 2px 4px; font-size: 15px;">'.$description->PHPType.'</td>
            <td style="padding: 2px 4px; font-size: 15px;">'.$description->mysqlType.'</td>
            <td style="padding: 2px 4px; font-size: 15px;">'.$description->maxLength.'</td>
            <td style="padding: 2px 4px; font-size: 15px;">'.$description->classType.'</td>
            <td style="padding: 2px 4px; font-size: 15px;">'.$description->dropdownType.'</td>
            <td style="padding: 2px 4px; font-size: 15px;">'; if($description->isPrimary) $html .= "yes"; else $html .= "no"; $html .= '</td>
            <td style="padding: 2px 4px; font-size: 15px;">'; if($description->isIndex) $html .= "yes"; else $html .= "no"; $html .= '</td>
            <td style="padding: 2px 4px; font-size: 15px;">'; if($description->isUnique) $html .= "yes"; else $html .= "no"; $html .= '</td>
        </tr>
            ';
        }
        $html .= '</tbody>
</table>';

        return $html;
    }

    final public static function getStructure($indent = "") {
        $thisClass = get_called_class();

        $structure = "";

        //get the properties of this object
        $class = Core_Class::findByName($thisClass);
        $properties = Core_Property::findAllByOwnerClassId($class->id);

        foreach($properties as $property) {
            if($property->inputType == Core_Input_Types::_model) {
                $childClass = Core_Class::findById($property->childClassId);
                $childClassType = $childClass->name;
                $structure .= $indent."<strong>".$property->name."</strong> <span style='color: blue;'>model(<strong>".$childClass->name."</strong>)</span><br />";
                $structure .= $childClassType::getStructure($indent."&nbsp;&nbsp;&nbsp;&nbsp;");
            } elseif($property->inputType == Core_Input_Types::_model_array) {
                $childClass = Core_Class::findById($property->childClassId);
                $childClassType = $childClass->name;
                $structure .= $indent."<strong>".$property->name."</strong> <span style='color: blue;'>model_array(<strong>".$childClass->name."[]</strong>)</span><br />";
                $structure .= $childClassType::getStructure($indent."&nbsp;&nbsp;&nbsp;&nbsp;");
            } elseif($property->inputType == Core_Input_Types::_dropdown) {
                $dropdown = Core_Dropdown::findById($property->dropDownId);
                $elements = Core_DropdownElement::findByOwnerId($dropdown->id);
                $structure .= $indent."<strong>".$property->name."</strong> <span style='color: blue;'>dropdown(<strong>".$dropdown->name."</strong>)</span><br />";
                foreach($elements as $element) {
                    $structure .= $indent."&nbsp;&nbsp;&nbsp;&nbsp;[".$element->value."] ".$element->name."<br />";
                }
            } else {
                $structure .= $indent."<strong>".$property->name."</strong> <span style='color: blue;'>".$property->inputType."(<strong>".$property->phpType."</strong>)</span><br />";
            }
        }

        return $structure;
    }

    /**
     * Construct an empty version of this object
     *
     * @return $this
     */
    final public static function constructEmpty() {
        $thisClass = get_called_class();
        return new $thisClass();
    }

    /**
     * Construct this object from an associative array.
     *
     * All keys of the array must match the property names of the object and it's child objects. Any mismatches wil
     * be ignored.
     *
     * @param array $array
     *
     * @throws Core_MVC_Exception
     * @return $this
     *
     */
    final public static function constructFromArray($array) {
        if(gettype($array) == Core_PHP_Types::_ARRAY && !Core_Utils_Array::valuesEmpty($array)) {
            $thisClass = get_called_class();
            /** @var Core_ORM_ComplexModel $baseObject */
            $baseObject = new $thisClass();
            $baseObject->getSimpleAndCompexProperties();

            //if the id is set, get the object first
            if(isset($array["id"])) {
                $baseObject->id = $array["id"];
                //pull the object without access checks to pull it exactly as it is!
                $baseObject->pullThis(true);
            }

            //go through simple properties and assign them from the array
            foreach($baseObject->internal_SimpleProperties as $property) {
                if(isset($array[$property])) {
                    switch($baseObject->internal_propertyTypes[$property]) {
                        case Core_PHP_Types::_STRING :
                            $baseObject->$property = strval($array[$property]);
                            break;
                        case Core_PHP_Types::_BOOLEAN :
                            $baseObject->$property = (bool)$array[$property];
                            break;
                        case Core_PHP_Types::_INTEGER :
                            $baseObject->$property = intval($array[$property]);
                            break;
                        case Core_PHP_Types::_FLOAT :
                            $baseObject->$property = floatval($array[$property]);
                            break;
                    }
                }
            }

            //go through the complex properties and assign them from the array
            foreach($baseObject->internal_ComplexProperties as $property) {

                $arrayValuesEmpty = Core_Utils_Array::valuesEmpty($array[$property]);

                switch($baseObject->internal_propertyTypes[$property]) {
                    case Core_PHP_Types::_ARRAY :
                        if(isset($array[$property]) && !$arrayValuesEmpty) {
                            $childClassType = $baseObject->internal_objectTypes[$property];
                            $childObjects = array();
                            foreach($array[$property] as $childArray) {
                                if(gettype($childArray) == Core_PHP_Types::_ARRAY) {
                                    $childObjects[] = $childClassType::constructFromArray($childArray);
                                }
                            }
                            $baseObject->$property = $childObjects;
                        } elseif(isset($array[$property]) && $arrayValuesEmpty) {
                            //because the property was set and has has no values, clear it.
                            $baseObject->$property = array();
                        } //otherwise, leave this property as is!
                        break;
                    case Core_PHP_Types::_OBJECT :
                        if(isset($array[$property]) && !Core_Utils_Array::valuesEmpty($array[$property])) {
                            $childClassType = $baseObject->internal_objectTypes[$property];
                            $baseObject->$property = $childClassType::constructFromArray($array[$property]);
                        } elseif(isset($array[$property]) && $arrayValuesEmpty) {
                            //because the property was set and has has no values, clear it.
                            $baseObject->$property = null;
                        } //otherwise, leave this property as is!
                        break;
                }
            }

            return $baseObject;
        }

        return false;
    }

    /**
     * Construct the object from a json string.
     *
     * All keys of the array must match the property names of the object and it's child objects. Any mismatches wil
     * be ignored.
     *
     * @param string $jsonString
     *
     * @throws Core_MVC_Exception
     * @return $this
     */
    final public static function constructFromJson($jsonString) {
        $array = (array)json_decode($jsonString,true);
        if($array != null) {
            return self::constructFromArray($array);
        } else {
            return false;
        }
    }

    /**
     * Construct this object form XML. This is currently not implemented!
     *
     * @throws Core_MVC_Exception
     */
    final public static function constructFromXML() {
        throw new Core_MVC_Exception("Constructing from XML currently not supported");
    }

    /**
     * Find all instances of this model using the orm finder
     *
     * @param Core_ORM_Finder $finder
     *
     * @return $this[]
     * @throws Core_Db_Exception
     */
    final public static function findAll(Core_ORM_Finder $finder) {

        if(gettype($finder) == Core_PHP_Types::_OBJECT) {
            if(get_class($finder) != "Core_ORM_Finder") {
                $finder = new Core_ORM_Finder();
            }
        } else {
            $finder = new Core_ORM_Finder();
        }

        $db = Core_Extorio::get()->getDbInstanceDefault();

        $thisClass = get_called_class();
        $ssql = "SELECT DISTINCT base.id FROM `".$thisClass."` base ";

        if(strlen($finder->getWhere())) {
            $ssql = "SELECT DISTINCT base.id FROM `".$thisClass."` base LEFT JOIN Core_Relationship CR ON 1=1 ";

            $outAliasesLong = array();
            $outAliasesShort = array();

            $hierarchies = Core_Utils_Model::getPropertyHierarchyByClassName($thisClass,"","",$index=1,
                $outAliasesLong,$outAliasesShort);

            //replace the shorts with longs
            $where = $finder->getWhere();
            foreach($outAliasesShort as $key => $value) {
                //if the $where has $value, replace with the long version
                if(strpos($finder->getWhere(),$value) !== false) {
                    $where = preg_replace("/".$value."([\W\.])/",$outAliasesLong[$key]."$1",$where);
                }
            }

            $joins = "";
            foreach($hierarchies as $hierarchy) {
                $joins .= "

-- create the ".$hierarchy->leftClassName." to ".$hierarchy->rightClassName." join on the property ".$hierarchy->leftToRightProperty."
LEFT JOIN `".$hierarchy->rightClassName."` ".$hierarchy->propertyPathLong." ON
	CR.leftInstanceId = ".$hierarchy->parentPropertyPathLong.".id AND
	CR.leftClassName = ('".$hierarchy->leftClassName."') AND
	CR.rightClassName = ('".$hierarchy->rightClassName."') AND
	CR.leftToRightProperty = ('".$hierarchy->leftToRightProperty."') AND
	".$hierarchy->propertyPathLong.".id = CR.rightInstanceId
            ";
            }

            $ssql .= $joins;

            $ssql .= " WHERE ".$where." ";
        }

        if(strlen($finder->getOrderBy()) && strlen($finder->getOrderDir())) {
            $ssql .= " ORDER BY ".$finder->getOrderBy()." ".$finder->getOrderDir()." ";
        }

        if(strlen($finder->getLimitResults())) {
            $ssql .= "LIMIT ";
            if(strlen($finder->getLimitStart())) {
                $ssql .= $finder->getLimitStart().", ";
            }
            $ssql .= $finder->getLimitResults()." ";
        }

        $results = array();

        $rows = $db->getRowsAssociative($ssql);
        if($rows) {
            foreach($rows as $row) {
                $obj = new $thisClass();
                $obj->id = $row["id"];
                if($obj->pullThis($finder->getIgnoreAccessChecks(),$finder->getDepth(),0)) {
                    $results[] = $obj;
                }
            }
        }

        return $results;
    }

    /**
     * Find the numeric count of all instances of this model using the finder
     *
     * @param Core_ORM_Finder $finder
     *
     * @return int
     * @throws Core_Db_Exception
     */
    final public static function findCount(Core_ORM_Finder $finder) {

        if(gettype($finder) == Core_PHP_Types::_OBJECT) {
            if(get_class($finder) != "Core_ORM_Finder") {
                $finder = new Core_ORM_Finder();
            }
        } else {
            $finder = new Core_ORM_Finder();
        }

        $db = Core_Extorio::get()->getDbInstanceDefault();

        $thisClass = get_called_class();
        $ssql = "SELECT count(DISTINCT base.id) FROM `".$thisClass."` base ";

        if(strlen($finder->getWhere())) {
            $ssql = "SELECT count(DISTINCT base.id) FROM `".$thisClass."` base LEFT JOIN Core_Relationship CR ON 1=1 ";

            $outAliasesLong = array();
            $outAliasesShort = array();

            $hierarchies = Core_Utils_Model::getPropertyHierarchyByClassName($thisClass,"","",$index=1,
                $outAliasesLong,$outAliasesShort);

            //replace the shorts with longs
            $where = $finder->getWhere();
            foreach($outAliasesShort as $key => $value) {
                //if the $where has $value, replace with the long version
                if(strpos($finder->getWhere(),$value) !== false) {
                    $where = preg_replace("/".$value."([\W\.])/",$outAliasesLong[$key]."$1",$where);
                }
            }

            $joins = "";
            foreach($hierarchies as $hierarchy) {
                $joins .= "

-- create the ".$hierarchy->leftClassName." to ".$hierarchy->rightClassName." join on the property ".$hierarchy->leftToRightProperty."
LEFT JOIN `".$hierarchy->rightClassName."` ".$hierarchy->propertyPathLong." ON
	CR.leftInstanceId = ".$hierarchy->parentPropertyPathLong.".id AND
	CR.leftClassName = ('".$hierarchy->leftClassName."') AND
	CR.rightClassName = ('".$hierarchy->rightClassName."') AND
	CR.leftToRightProperty = ('".$hierarchy->leftToRightProperty."') AND
	".$hierarchy->propertyPathLong.".id = CR.rightInstanceId
            ";
            }

            $ssql .= $joins;

            $ssql .= " WHERE ".$where." ";
        }

        if(strlen($finder->getOrderBy()) && strlen($finder->getOrderDir())) {
            $ssql .= " ORDER BY ".$finder->getOrderBy()." ".$finder->getOrderDir()." ";
        }

        if(strlen($finder->getLimitResults())) {
            $ssql .= "LIMIT ";
            if(strlen($finder->getLimitStart())) {
                $ssql .= $finder->getLimitStart().", ";
            }
            $ssql .= $finder->getLimitResults()." ";
        }

        $row = $db->getRowIndices($ssql);
        if($row) {
            return $row[0];
        } else {
            return 0;
        }
    }

    /**
     * Find one instance of this model using the finder
     *
     * @param Core_ORM_Finder $finder
     *
     * @return $this
     * @throws Core_Db_Exception
     * @throws Core_MVC_Exception
     */
    final static public function findOne(Core_ORM_Finder $finder) {

        if(gettype($finder) == Core_PHP_Types::_OBJECT) {
            if(get_class($finder) != "Core_ORM_Finder") {
                $finder = new Core_ORM_Finder();
            }
        } else {
            $finder = new Core_ORM_Finder();
        }

        if(strlen($finder->getWhere())) {
            $db = Core_Extorio::get()->getDbInstanceDefault();

            $thisClass = get_called_class();
            $ssql = "SELECT DISTINCT base.id FROM `".$thisClass."` base LEFT JOIN Core_Relationship CR ON 1=1 ";

            $outAliasesLong = array();
            $outAliasesShort = array();

            $hierarchies = Core_Utils_Model::getPropertyHierarchyByClassName($thisClass,"","",$index=1,
                $outAliasesLong,$outAliasesShort);

            //replace the shorts with longs
            $where = $finder->getWhere();
            foreach($outAliasesShort as $key => $value) {
                //if the $where has $value, replace with the long version
                if(strpos($finder->getWhere(),$value) !== false) {
                    $where = preg_replace("/".$value."([\W\.])/",$outAliasesLong[$key]."$1",$where);
                }
            }

            $joins = "";
            foreach($hierarchies as $hierarchy) {
                $joins .= "

-- create the ".$hierarchy->leftClassName." to ".$hierarchy->rightClassName." join on the property ".$hierarchy->leftToRightProperty."
LEFT JOIN `".$hierarchy->rightClassName."` ".$hierarchy->propertyPathLong." ON
	CR.leftInstanceId = ".$hierarchy->parentPropertyPathLong.".id AND
	CR.leftClassName = ('".$hierarchy->leftClassName."') AND
	CR.rightClassName = ('".$hierarchy->rightClassName."') AND
	CR.leftToRightProperty = ('".$hierarchy->leftToRightProperty."') AND
	".$hierarchy->propertyPathLong.".id = CR.rightInstanceId
            ";
            }

            $ssql .= $joins;

            $ssql .= " WHERE ".$where;

            if(strlen($finder->getOrderBy()) && strlen($finder->getOrderDir())) {
                $ssql .= " ORDER BY ".$finder->getOrderBy()." ".$finder->getOrderDir()." ";
            }

            $ssql .= " LIMIT 1";

            $row = $db->getRowAssociative($ssql);
            if($row) {
                $obj = new $thisClass();
                $obj->id = $row["id"];
                if($obj->pullThis($finder->getIgnoreAccessChecks(),$finder->getDepth(),0)) {
                    return $obj;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            throw new Core_MVC_Exception("Cannot use findOne without setting the where of the finder");
        }
    }

    /**
     * Find one instance of this model by id, using the finder
     *
     * @param $id
     * @param Core_ORM_Finder $finder
     *
     * @return $this
     * @throws Core_Db_Exception
     * @throws Core_MVC_Exception
     */
    final public static function findById($id,Core_ORM_Finder $finder) {

        if(gettype($finder) == Core_PHP_Types::_OBJECT) {
            if(get_class($finder) != "Core_ORM_Finder") {
                $finder = new Core_ORM_Finder();
            }
        } else {
            $finder = new Core_ORM_Finder();
        }

        if($id > 0) {
            $db = Core_Extorio::get()->getDbInstanceDefault();
            $thisClass = get_called_class();
            $ssql = "SELECT DISTINCT base.id FROM `".$thisClass."` base ";
            $ssql .= "WHERE base.id = ".intval($id)." LIMIT 1";
            $row = $db->getRowAssociative($ssql);
            if($row) {
                $obj = new $thisClass();
                $obj->id = $row["id"];
                if($obj->pullThis($finder->getIgnoreAccessChecks(),$finder->getDepth(),0)) {
                    return $obj;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    final protected function disableAccessChecks() {
        $this->internal_accessChecksEnabled = false;
    }

    final protected function enableAccessChecks() {
        $this->internal_accessChecksEnabled = true;
    }

    /**
     * Push this object by it's id. Any objects with the same id are overwritten, otherwise a new instance is stored.
     *
     * @throws Core_Db_Exception
     */
    final public function pushThis($ignoreAccessChecks=false,$disableWebhooks=false) {
        //if the object is not persistent, set the id to null
        if(!$this->internal_isPersistent) {
            $this->id = null;
        }

        $class = get_class($this);
        $db = Core_Extorio::get()->getDbInstanceDefault();
        if($this->id > 0) {
            //check whether we are creating/updating
            $ssql = "SELECT id FROM `".$class."` WHERE id = ".$this->id." LIMIT 1";
            if($db->getRowIndices($ssql)) {
                $this->updateThis($ignoreAccessChecks,$disableWebhooks);
            } else {
                $this->createThis($ignoreAccessChecks,$disableWebhooks);
            }
        } else {
            $this->createThis($ignoreAccessChecks,$disableWebhooks);
        }
    }

    /**
     * Pull this object by it's id.
     *
     * @param bool $ignoreAccessChecks
     *
     * @return bool Whether the object was successfully pulled or not.
     */
    final public function pullThis($ignoreAccessChecks=false,$maxDepth=10,$currentDepth=0) {
        $this->internal_accessChecksEnabled = !$ignoreAccessChecks;
        if($this->internal_accessChecksEnabled) {
            $access = $this->canRetrieve();
            if(is_null($access)) {
                $access = Core_AccessEvent::runEvent("model","retrieve",get_class($this));
            }
            if(!$access) {
                return false;
            }
        }

        if($currentDepth >= $maxDepth) {
            return false;
        }

        //children will use this depth + 1
        $currentDepth++;

        $this->beforeRetrieve();

        $db = Core_Extorio::get()->getDbInstanceDefault();
        $thisClass = get_class($this);

        if($this->id > 0) {

            $ssql = "SELECT * FROM `".$thisClass."` WHERE id = ".$this->id." LIMIT 1";
            $data = $db->getRowAssociative($ssql);
            if($data) {

                $this->getSimpleAndCompexProperties();
                //set all the simple properties
                foreach($this->internal_SimpleProperties as $property) {
                    if(isset($data[$property])) {
                        switch($this->internal_propertyTypes[$property]) {
                            case Core_PHP_Types::_STRING :
                                $this->$property = strval($data[$property]);
                                break;
                            case Core_PHP_Types::_BOOLEAN :
                                $this->$property = (bool)$data[$property];
                                break;
                            case Core_PHP_Types::_INTEGER :
                                $this->$property = intval($data[$property]);
                                break;
                            case Core_PHP_Types::_FLOAT :
                                $this->$property = floatval($data[$property]);
                                break;
                        }
                    }
                }

                //get the child objects for the complex properties
                foreach($this->internal_ComplexProperties as $property) {
                    if($this->internal_propertyTypes[$property] == Core_PHP_Types::_OBJECT) {
                        //find if there are any relationships for this property
                        $relationship = Core_Relationship::findChildRelationshipByPropertyName($thisClass,$this->id,
                            $property);
                        if($relationship && $currentDepth < $maxDepth) {
                            $childClassType = $relationship->rightClassName;
                            $childObject = new $childClassType();
                            $childObject->id = $relationship->rightInstanceId;
                            $childObject->pullThis($ignoreAccessChecks,$maxDepth,$currentDepth);

                            $this->$property = $childObject;
                        } else {
                            $this->$property = null;
                        }
                    } elseif($this->internal_propertyTypes[$property] == Core_PHP_Types::_ARRAY) {
                        //find if there are any relationships for this property
                        $relationships = Core_Relationship::findChildRelationshipsByPropertyName($thisClass,
                            $this->id,$property);
                        $childObjects = array();

                        if($currentDepth < $maxDepth) {
                            foreach($relationships as $relationship) {
                                $childClassType = $relationship->rightClassName;
                                $childObject = new $childClassType;
                                $childObject->id = $relationship->rightInstanceId;
                                $childObject->pullThis($ignoreAccessChecks,$maxDepth,$currentDepth);

                                $childObjects[] = $childObject;
                            }
                        }

                        $this->$property = $childObjects;
                    }
                }

                $this->afterRetrieve();

                return true;
            }
        }

        $this->afterRetrieve();

        return false;
    }

    /**
     * Remove this object by it's id. If any of the object's direct children are non-persistent, they are removed also.
     */
    final public function removeThis($ignoreAccessChecks=false,$disableWebhooks=false) {
        $this->internal_webhooksEnabled = !$disableWebhooks;
        $this->internal_accessChecksEnabled = !$ignoreAccessChecks;
        if($this->internal_accessChecksEnabled) {
            $access = $this->canDelete();
            if(is_null($access)) {
                $access = Core_AccessEvent::runEvent("model","delete",get_class($this));
            }
            if(!$access) {
                return;
            }
        }

        $this->beforeDelete();

        $db = Core_Extorio::get()->getDbInstanceDefault();
        $thisClass = get_class($this);

        $ssql = "DELETE FROM `".$thisClass."` WHERE id = ".$this->id." LIMIT 1";
        $db->delete($ssql);

        //remove all relationships to parent objects
        $parentRelationships = Core_Relationship::findAllParentRelationships($this->id,$thisClass);
        foreach($parentRelationships as $relationship) {
            $relationship->removeThis();
        }

        $this->getSimpleAndCompexProperties();
        //go through the complex properties and remove any non-persistent children
        foreach($this->internal_ComplexProperties as $property) {
            //get the child relationships
            $childRelationships = Core_Relationship::findChildRelationshipsByPropertyName($thisClass,$this->id,
                $property);
            foreach($childRelationships as $relationship) {
                $childClassType = $relationship->rightClassName;
                /** @var Core_ORM_ComplexModel $childObject */
                $childObject = new $childClassType();
                if(!$childObject->internal_isPersistent) {
                    $childObject->id = $relationship->rightInstanceId;
                    $childObject->removeThis($ignoreAccessChecks,$disableWebhooks);
                }
            }
        }

        //get all remaining child relationships and remove them
        $remainingRelationships = Core_Relationship::findChildRelationshipsByLeftObject($this->id,$thisClass);
        foreach($remainingRelationships as $relationship) {
            $relationship->removeThis();
        }

        $this->afterDelete();

        if($this->internal_webhooksEnabled) {
            //run the webhook task
            //Core_Task::startBackgroundTask("/tasks/webhook/?model=".get_class($this)."&method=delete&rid=".$this->id);
        }
    }

    /**
     * Return this object as an array
     *
     * @return mixed
     */
    final public function toArray() {
        return json_decode(json_encode($this),true);
    }

    /**
     * Return this object as a standard object
     *
     * @return mixed
     */
    final public function toStdObject() {
        return json_decode(json_encode($this));
    }

    /**
     * Return this object as a json string
     *
     * @return string
     */
    final public function toJson() {
        return json_encode($this);
    }

    /**
     * Intelligently get the value of this object to string. This returns the value of the highest priority property,
     * as long as it is a basic type
     *
     * @return string
     */
    public function toString() {
        foreach($this->internal_propertyTypes as $name => $type) {
            if($type != Core_PHP_Types::_ARRAY && $type != Core_PHP_Types::_OBJECT) {
                return $this->$name;
                break;
            }
        }
        return "";
    }

    /**
     * Display this object in a readable fashion.
     *
     */
    final public function print_r() {
        echo "<pre>";
        print_r($this->toStdObject());
        echo "</pre>";
    }

    /**
     * Create an internal cache of this object's simple and complex properties
     */
    final protected function getSimpleAndCompexProperties() {
        if(count($this->internal_SimpleProperties) == 0 || count($this->internal_ComplexProperties) == 0) {
            $this->internal_SimpleProperties = array();
            $this->internal_ComplexProperties = array();
            foreach ($this as $key => $value) {
                if(
                    isset($this->internal_propertyTypes[$key]) &&
                    in_array($this->internal_propertyTypes[$key],$this->internal_acceptedTypes)
                ) {
                    if($this->internal_propertyTypes[$key] == Core_PHP_Types::_OBJECT ||
                        $this->internal_propertyTypes[$key] == Core_PHP_Types::_ARRAY) {
                        $this->internal_ComplexProperties[] = $key;
                    } else {
                        $this->internal_SimpleProperties[] = $key;
                    }
                }
            }
        }
    }

    /**
     * Event method. This event is triggered before an object is created in the database.
     */
    protected function beforeCreate() {

    }

    /**
     * Event method. This event is triggered after an object is created in the database.
     */
    protected function afterCreate() {

    }

    /**
     * Event method. This event is triggered before an object is updated in the database.
     */
    protected function beforeUpdate() {

    }

    /**
     * Event method. This event is triggered after an object is created in the database.
     */
    protected function afterUpdate() {

    }

    /**
     * Event method. This event is triggered before an object is deleted in the database.
     */
    protected function beforeDelete() {

    }

    /**
     * Event method. This event is triggered after an object is deleted in the database.
     */
    protected function afterDelete() {

    }

    /**
     * Event method. This event is triggered before the object is pulled from the database
     */
    protected function beforeRetrieve() {

    }

    /**
     * Event method. This event is triggered after the object is pulled from the database
     */
    protected function afterRetrieve() {

    }

    /**
     * Override method to define the conditions for when this object can/cannot be created.
     *
     * Return true if you want to allow access, return false if you do not want to allow access.
     *
     * @return bool
     */
    public function canCreate() {
        return true;
    }

    /**
     * Override method to define the conditions for when this object can/cannot be updated.
     *
     * Return true if you want to allow access, return false if you do not want to allow access.
     *
     * @return bool
     */
    public function canUpdate() {
        return true;
    }

    /**
     * Override method to define the conditions for when this object can/cannot be deleted.
     *
     * Return true if you want to allow access, return false if you do not want to allow access.
     *
     * @return bool
     */
    public function canDelete() {
        return true;
    }

    /**
     * Override method to define the conditions for when this object can/cannot be retrieved.
     *
     * Return true if you want to allow access, return false if you do not want to allow access.
     *
     * @return bool
     */
    public function canRetrieve() {
        return true;
    }

    public function canView_Edit() {
        return false;
    }

    public function canView_List() {
        return false;
    }

    public function canView_Detail() {
        return false;
    }

    public function canView_Delete() {
        return false;
    }

    /**
     * Event method. This event is triggered when the value of a property is being displayed.
     *
     * @param string $propertyName the name of the property being displayed
     */
    public function getPropertyValueForDisplay($propertyName) {

    }

    /**
     * Used internally to create this object and it's children
     *
     * @throws Core_Db_Exception
     */
    final private function createThis($ignoreAccessChecks=false,$disableWebhooks=false) {
        $this->internal_webhooksEnabled = !$disableWebhooks;
        $this->internal_accessChecksEnabled = !$ignoreAccessChecks;
        if($this->internal_accessChecksEnabled) {
            $access = $this->canCreate();
            if(is_null($access)) {
                $access = Core_AccessEvent::runEvent("model","create",get_class($this));
            }
            if(!$access) {
                return;
            }
        }

        $this->beforeCreate();

        //don't overwrite the date if it's already set
        if(is_null($this->dateCreated)) {
            $this->dateCreated = date('Y-m-d H:i:s',time());
        }

        $db = Core_Extorio::get()->getDbInstanceDefault();
        $thisClass = get_class($this);

        //build the sql for the simple properties
        $ssql = "INSERT INTO `".$thisClass."` SET ";
        $ssql .= $this->createSetForSql();
        //insert the simple properties
        $this->id = $db->insert($ssql);

        $this->afterCreate();

        //push child objects
        $this->pushChildObjects($ignoreAccessChecks,$disableWebhooks);

        if($this->internal_webhooksEnabled) {
            //run the webhook task
            //Core_Task::startBackgroundTask("/tasks/webhook/?model=".get_class($this)."&method=create&rid=".$this->id);
        }
    }

    /**
     * Used internally to update this object and it's children
     */
    final private function updateThis($ignoreAccessChecks=false,$disableWebhooks=false) {
        $this->internal_webhooksEnabled = !$disableWebhooks;
        $this->internal_accessChecksEnabled = !$ignoreAccessChecks;
        $old = self::findById($this->id,Core_ORM_Finder::newInstance());
        $updating = false;
        if($this != $old) {
            $this->internal_old = $old;
            if($this->internal_accessChecksEnabled) {
                $access = $this->canUpdate();
                if(is_null($access)) {
                    $access = Core_AccessEvent::runEvent("model","update",get_class($this));
                }
                if(!$access) {
                    return;
                }
            }
            $updating = true;
        }

        //we only need to update when the properties are changing
        if($updating) {
            $this->beforeUpdate();

            $this->dateUpdated = date('Y-m-d H:i:s',time());

            $db = Core_Extorio::get()->getDbInstanceDefault();
            $thisClass = get_class($this);

            //build the sql for the simple properties
            $ssql = "UPDATE `".$thisClass."` SET ";
            $ssql .= $this->createSetForSql();
            $ssql .= " WHERE id = ".$this->id." LIMIT 1";
            //insert the simple properties
            $db->update($ssql);

            $this->afterUpdate();
        }

        //push child objects
        $this->pushChildObjects($ignoreAccessChecks,$disableWebhooks);

        if($updating && $this->internal_webhooksEnabled) {
            //run the webhook task
            //Core_Task::startBackgroundTask("/tasks/webhook/?model=".get_class($this)."&method=update&rid=".$this->id);
        }
    }

    /**
     * Used internally to push the child objects of this object.
     */
    private function pushChildObjects($ignoreAccessChecks=false,$disableWebhooks=false) {
        $thisClass = get_class($this);
        //go through the complex properties
        $this->getSimpleAndCompexProperties();
        foreach ($this->internal_ComplexProperties as $property) {

            $childClassType = $this->internal_objectTypes[$property];

            //if the parent and child objects are the same class, make sure they are not the same instances
            if($thisClass == $childClassType) {
                if($this->internal_propertyTypes[$property] == Core_PHP_Types::_OBJECT) {
                    if(gettype($this->$property) == Core_PHP_Types::_OBJECT) {
                        if($this->id == $this->$property->id) {
                            //clear the link
                            $this->$property = null;
                        }
                    }
                } elseif($this->internal_propertyTypes[$property] == Core_PHP_Types::_ARRAY) {
                    $children = $this->$property;
                    if(gettype($children) == Core_PHP_Types::_ARRAY) {
                        for($i = 0; $i < count($children); $i++) {
                            if(gettype($children[$i]) == Core_PHP_Types::_OBJECT) {
                                if($this->id == $children[$i]->id) {
                                    unset($children[$i]);
                                }
                            }
                        }
                    }
                }
            }

            //get all existing relationships for this property
            $existingRelationships = Core_Relationship::findChildRelationshipsByPropertyName($thisClass, $this->id, $property);
            //we want to keep track of the child relationships that exist in the object that is being pushed
            //we will remove stranded relationships
            /** @var Core_Relationship[] $foundChildRelationships */
            $foundChildRelationships = array();

            $childClassType = $this->internal_objectTypes[$property];

            if ($this->internal_propertyTypes[$property] == Core_PHP_Types::_OBJECT) {
                //make sure the object is the right type
                if (gettype($this->$property) == Core_PHP_Types::_OBJECT) {
                    if (get_class($this->$property) != $childClassType) {
                        //try and convert it to the correct object
                        $childObject = new $childClassType();
                        foreach ($this->$property as $key => $value) {
                            $childObject->$key = $value;
                        }
                        $this->$property = $childObject;
                    }

                    //push the child object
                    $this->$property->pushThis($ignoreAccessChecks,$disableWebhooks);

                    //if there is not a relationship, create one
                    $relationship = Core_Relationship::findChildRelationshipByAll($this->id, $thisClass, $this->$property->id, $childClassType, $property);
                    if (!$relationship) {
                        $relationship = new Core_Relationship();
                        $relationship->leftClassName = $thisClass;
                        $relationship->leftInstanceId = $this->id;
                        $relationship->rightClassName = $childClassType;
                        $relationship->rightInstanceId = $this->$property->id;
                        $relationship->leftToRightProperty = $property;
                        $relationship->pushThis();
                    }

                    //store the relationship
                    $foundChildRelationships[] = $relationship;
                }
            } elseif ($this->internal_propertyTypes[$property] == Core_PHP_Types::_ARRAY) {
                if (gettype($this->$property) == Core_PHP_Types::_ARRAY) {
                    //need to create a reference because [] doesn't work on magic variables
                    $children = $this->$property;
                    for ($i = 0; $i < count($children); $i++) {
                        if (gettype($children[$i]) == Core_PHP_Types::_OBJECT) {
                            //make sure the object is the correct type
                            if (get_class($children[$i]) != $childClassType) {
                                //try and convert it to the correct object
                                $childObject = new $childClassType();
                                foreach ($children[$i] as $key => $value) {
                                    $childObject->$key = $value;
                                }
                                $children[$i] = $childObject;
                            }

                            $children[$i]->pushThis($ignoreAccessChecks,$disableWebhooks);

                            //if there is not a relationship, create one
                            $relationship = Core_Relationship::findChildRelationshipByAll($this->id, $thisClass, $children[$i]->id, $childClassType, $property);
                            if (!$relationship) {
                                $relationship = new Core_Relationship();
                                $relationship->leftClassName = $thisClass;
                                $relationship->leftInstanceId = $this->id;
                                $relationship->rightClassName = $childClassType;
                                $relationship->rightInstanceId = $children[$i]->id;
                                $relationship->leftToRightProperty = $property;
                                $relationship->pushThis();
                            }

                            //store the relationship
                            $foundChildRelationships[] = $relationship;
                        }
                    }
                    $this->$property = $children;
                }
            }

            //if any of the existing relationships were not found in the pushed object, we need to remove the
            //relationship. If the child object is non-persistent, the object should be removed also
            foreach ($existingRelationships as $existingRelationship) {
                $found = false;
                foreach ($foundChildRelationships as $foundRelationship) {
                    if ($foundRelationship->id == $existingRelationship->id) {
                        $found = true;
                    }
                }

                if (!$found) {
                    //the existing relationship doesn't exist anymore
                    $existingRelationship->removeThis();

                    //if the child object is non-persistent, remove it
                    $childClassType = $existingRelationship->rightClassName;
                    /** @var Core_ORM_ComplexModel $childObject */
                    $childObject = new $childClassType();
                    if (!$childObject->internal_isPersistent) {
                        $childObject->id = $existingRelationship->rightInstanceId;
                        $childObject->removeThis($ignoreAccessChecks,$disableWebhooks);
                    }
                }
            }
        }
    }

    /**
     * Create the "set" part of the sql for create/update
     *
     * @throws Core_MVC_Exception
     * @return string
     */
    final private function createSetForSql() {
        $set = "";
        $this->getSimpleAndCompexProperties();
        foreach ($this->internal_SimpleProperties as $property) {
            switch($this->internal_propertyTypes[$property]) {
                case Core_PHP_Types::_STRING :
                    $set .= "`".$property."` = ('".Core_Extorio::get()->getDbInstanceDefault()->clean(strval($this->$property))."'), ";
                    break;
                case Core_PHP_Types::_BOOLEAN :
                    $bit = $this->$property?1:0;
                    $set .= "`".$property."` = ".$bit.", ";
                    break;
                case Core_PHP_Types::_INTEGER :
                    $set .= "`".$property."` = ".intval($this->$property).", ";
                    break;
                case Core_PHP_Types::_FLOAT :
                    $set .= "`".$property."` = ".floatval($this->$property).", ";
                    break;
            }
        }
        $set = substr($set,0,strval($set)-2);

        return $set;
    }
}