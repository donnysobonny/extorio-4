<?php

/**
 * The Event class represents an event. Actions are bound to an event, and when an event is run,
 * all of the actions are run depending on their priority and override options.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Classes
 *
 * Class Core_Event
 */
final class Core_Event {
    /**
     * The name of the event
     *
     * @var string
     */
    public $name;

    /**
     * @param $name
     */
    function __construct($name) {
        $this->name = $name;
    }

    /**
     * Run this event. For each accent that is run, the response of the action is added to the return array. If the
     * response is null however (the action didn't run or return anything) then nothing is added to the array for
     * that action.
     *
     * @return array the results of each action that was run
     */
    public function run() {
        $results = array();
        $params = func_get_args();
        //get all actions to run
        $actions = Core_Extorio::get()->getEventActions($this->name);
        foreach($actions as $action) {
            $result = $action->run($params);
            if(!is_null($result)) {
                $results[] = $result;
            }
        }

        return $results;
    }
}