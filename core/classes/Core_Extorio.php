<?php
final class Core_Extorio {
    private $initiated = false;

    /**
     * All loaded
     *
     * @var Core_Extension[]
     */
    private $extensions = array();

    /**
     * @var Core_Event[]
     */
    private $events = array();
    /**
     * @var Core_Action[][]
     */
    private $actions = array();

    private $actionIndex = 0;

    /**
     * @var Core_Db[]
     */
    private $dbInstances = array();
    /**
     * @var array
     */
    private $accessChecks = array();

    /**
     * @var array
     */
    private $storedConfig = array();

    private $settings_keys = array(
        "settings_edit_mode",
        "settings_use_template",
        "settings_template_name",
        "settings_template_extension_name",
        "settings_template_display_mode"
    );
    private $settings_edit_mode = false;
    private $settings_use_template = true;
    private $settings_template_name = "";
    private $settings_template_extension_name = "";
    private $settings_template_display_mode = "full";

    /**
     * @param boolean $settings_edit_mode
     */
    public function setSettingsEditMode($settings_edit_mode) {
        $this->settings_edit_mode = $settings_edit_mode;
    }

    /**
     * @param boolean $settings_use_template
     */
    public function setSettingsUseTemplate($settings_use_template) {
        $this->settings_use_template = $settings_use_template;
    }

    /**
     * @param string $settings_template_name
     */
    public function setSettingsTemplateName($settings_template_name) {
        $this->settings_template_name = $settings_template_name;
    }

    /**
     * @param string $settings_template_extension_name
     */
    public function setSettingsTemplateExtensionName($settings_template_extension_name) {
        $this->settings_template_extension_name = $settings_template_extension_name;
    }

    /**
     * @param string $settings_template_display_mode
     */
    public function setSettingsTemplateDisplayMode($settings_template_display_mode) {
        $this->settings_template_display_mode = $settings_template_display_mode;
    }

    public function getExtension($extensionName) {
        if(isset($this->extensions[$extensionName])) {
            return $this->extensions[$extensionName];
        } else {
            return false;
        }
    }

    public function getExtensions() {
        return $this->extensions;
    }

    public function createEvent($eventName) {
        return $this->events[$eventName] = new Core_Event($eventName);
    }

    public function getEvent($eventName) {
        if(isset($this->events[$eventName])) {
            return $this->events[$eventName];
        } else {
            return false;
        }
    }

    public function createAction($eventName,$targetObject,$targetMethod,$priority=99,$overrideOtherActions=false) {
        $action = new Core_Action($eventName,$overrideOtherActions,$priority,$targetMethod,$targetObject);
        $this->actions[$action->priority.".".$this->actionIndex] = $action;
        $this->actionIndex++;
        //sort by priority
        krsort($this->actions);

        return $action;
    }

    public function getEventActions($eventName) {
        $actions = array();
        foreach($this->actions as $action) {
            if($action->eventName == $eventName) {
                if($action->overrideOtherActions) {
                    $actions = array();
                    $actions[] = $action;
                    break;
                } else {
                    $actions[] = $action;
                }
            }
        }
        return $actions;
    }

    public function createDbInstance($instanceName,$host,$username,$password,$database) {
        return $this->dbInstances[$instanceName] = new Core_Db($database,$host,$password,$username);
    }

    public function getDbInstance($instanceName) {
        if(isset($this->dbInstances[$instanceName])) {
            return $this->dbInstances[$instanceName];
        } else {
            return false;
        }
    }

    public function getDbInstanceDefault() {
        if(!isset($this->dbInstances["default"])) {
            $this->dbInstances["default"] = new Core_Db();
        }
        return $this->dbInstances["default"];
    }

    public function addAccessCheck($type,$action,$target,$result) {
        $this->accessChecks[$type."_".$action."_".$target] = $result;
    }

    public function getAccessCheck($type,$action,$target) {
        if(isset($this->accessChecks[$type."_".$action."_".$target])) {
            return $this->accessChecks[$type."_".$action."_".$target];
        } else {
            return null;
        }
    }

    public function updateStoredConfig($config) {
        if(is_array($config)) {
            foreach($config as $key => $value) {
                $this->storedConfig[$key] = $value;
            }
        }
    }

    public function getStoredConfig() {
        return $this->storedConfig;
    }

    public static function get() {
        if(!isset($_ENV["EXTORIO"])) {
            return self::init();
        } else {
            $extorio = $_ENV["EXTORIO"];
            if(!$extorio->initiated) {
                return self::init();
            } else {
                return $extorio;
            }
        }
    }

    public function start() {
        //start the core extension
        $this->startExtension("core");

        //TODO: add in installation check here
        //start installed extensions
        $intalledExtensions = Core_InstalledExtension::findAllEnabledByPriority();
        foreach($intalledExtensions as $extension) {
            $this->startExtension($extension->name);
        }

        //start the application
        $this->startExtension("application");
    }

    public function startBackgroundRequest($requestUri) {

        //clean the uri
        $requestUri = Core_Utils_String::startsWith($requestUri,"/");
        $requestUri = Core_Utils_String::removeQuotes($requestUri);

        $config = $this->getStoredConfig();
        $taskLog = "task_log_".date("d_m__H_i_s",time()).".log";
        $taskErrorLog = "task_error_log_".date("d_m__H_i_s",time()).".log";

        if(PHP_OS == "WINNT") {
            //windows command
            $command = "cd ".getcwd()."";
            $command .= " & php -f cliaccess.php \"".$requestUri."\" ";
            //if we are to create the task logs
            if($config["tasks"]["create_task_logs"] == true) {
                $command .= ">logs/".$taskLog." 2>logs/".$taskErrorLog."";
            } else {
                $command .= "> nul 2> nul";
            }
            pclose(popen('start /B cmd /C "'.$command.'"', 'w'));

        } elseif(PHP_OS == "Linux") {

            //linux command
            $command = "cd ".getcwd()."";
            $command .= "; php -f cliaccess.php '".$requestUri."' ";
            if($config["tasks"]["create_task_logs"] == true) {
                $command .= ">logs/".$taskLog." 2>logs/".$taskErrorLog." ";
            } else {
                $command .= ">/dev/null 2>/dev/null ";
            }
            $command .= "&";

            exec($command);

        } else {
            throw new Core_Task_Exception("The task system is only supported by Windows and Linux currently.");
        }
    }

    public function killBackgroundRequest($processId) {
        //kill the process
        if(PHP_OS == "WINNT") {
            $command = "cd ".getcwd()."";
            $command .= " & taskkill /F /PID ".$processId." ";
            $output = exec($command);
        } elseif (PHP_OS == "Linux") {
            $command = "cd ".getcwd()."";
            $command .= "; kill -KILL ".$processId." ";
            $output = exec($command);
        } else {
            throw new Core_Task_Exception("The task system is only supported by Windows and Linux currently.");
        }
    }

    public function startTask($extensionName,$taskName,$method = false, $methodParams = array(), $urlQueryParams = array()) {

        //make the request uri
        $requestUri = "/extorio/elements/tasks";

        if(strlen($extensionName)) {
            $requestUri .= "/".$extensionName;
        }

        if(strlen($taskName)) {
            $requestUri .= "/".$taskName;
        }

        if($method) {
            $requestUri .= "/".$method;
        }

        foreach($methodParams as $param) {
            $requestUri .= "/".$param;
        }

        if(!empty($urlQueryParams)) {
            $requestUri .= "?".http_build_query($urlQueryParams);
        }

        //start the task
        $this->startBackgroundRequest($requestUri);
    }

    public function stopTaskById($taskId) {
        $liveTask = Core_LiveTask::findById($taskId);
        if($liveTask) {
            $this->stopTask($liveTask);
        }
    }

    public function stopTaskByPID($taskPID) {
        $liveTask = Core_LiveTask::findByPid($taskPID);
        if($liveTask) {
            $this->stopTask($liveTask);
        }
    }

    /**
     * @param Core_LiveTask $task
     *
     * @throws Core_Task_Exception
     */
    private function stopTask($task) {
        //kill the process first!
        $this->killBackgroundRequest($task->pid);

        //update the task
        $task->status = Core_Task_Status::killed;
        $task->pushThis();
    }

    /**
     * Construct an extension object. Returns false if no extension file was found
     *
     * @param $extensionName
     *
     * @return Core_Extension
     */
    public function constructExtension($extensionName) {

        //check if it already started
        $extension = $this->getExtension($extensionName);
        if($extension) {
            return $extension;
        }
        if($extensionName == "core" || $extensionName == "application") {
            $files = Core_Utils_File::findFilesInFolderRecursively($extensionName."/extension");
            foreach($files as $file) {
                //there should only be one php file in this dir, which should be the extension file
                if($file->extension == "php") {
                    $type = $file->fileName;
                    if(!class_exists($type)) {
                        include_once($file->path);
                    }
                    $extension = new $type($extensionName,$extensionName);
                }
            }
        } else {
            $root = "extensions/".$extensionName;
            $files = Core_Utils_File::findFilesInFolderRecursively($root."/extension");
            foreach($files as $file) {
                //there should only be one php file in this dir, which should be the extension file
                if($file->extension == "php") {
                    if(!class_exists($file->fileName)) {
                        include_once($file->path);
                    }
                    $type = $file->fileName;
                    $extension = new $type($extensionName,$root);
                }
            }
        }

        return $extension;
    }

    public function disableExtension($extensionName) {
        $extension = $this->getExtension($extensionName);
        if(!$extension) {
            $extension = $this->constructExtension($extensionName);
        }
        if($extension) {
            $extension->disable();
        }
        return $extension;
    }

    public function enableExtension($extensionName) {
        $extension = $this->getExtension($extensionName);
        if(!$extension) {
            $extension = $this->constructExtension($extensionName);
        }
        if($extension) {
            $extension->enable();
        }
        return $extension;
    }

    /**
     * Constructs the extension then starts it if the extension was found.
     *
     * @param $extensionName
     *
     * @return Core_Extension
     * @throws Core_Extorio_Exception
     */
    private function startExtension($extensionName) {
        $extension = $this->getExtension($extensionName);
        if(!$extension) {
            $extension = $this->constructExtension($extensionName);
        }
        if($extension) {
            //store the config
            $this->updateStoredConfig($extension->getConfigAll());

            //store the extension
            $this->extensions[$extension->name] = $extension;

            $extension->onStart();

            return $extension;
        }

        return false;
    }

    public function fetchPageContentById($pageId, $pageParams = array()) {
        return $this->fetchPageContentByPage(Core_Page::findById($pageId),$pageParams);
    }

    public function fetchPageContentByName($pageName, $pageParams = array()) {
        return $this->fetchPageContentByPage(Core_Page::findByName($pageName),$pageParams);
    }

    public function fetchPageContentByAddress($pageRequestAddress) {

        $page = Core_Page::findByAddress($pageRequestAddress);
        $pageParams = array();

        //additional fields in the page request address from the page that was found are page params
        $additional = str_replace($page->requestAddress,"",$pageRequestAddress);
        $additional = Core_Utils_String::notStartsAndNotEndsWith($additional,"/");
        if(strlen($additional)) {
            $pageParams = explode("/",$additional);
        }

        return $this->fetchPageContentByPage($page,$pageParams);
    }

    /**
     * Fetch the content of a page. Note that this may override the current settings.
     *
     * @param Core_Page $page
     * @param array $pageParams
     *
     * @return string
     * @internal param string $method
     * @internal param array $params
     *
     */
    public function fetchPageContentByPage($page, $pageParams = array()) {

        $method = false;
        $params = array();

        $content = "";
        if($page) {
            //create a buffer before running the elements
            ob_start();

            //extract the settings from the page
            $this->settings_use_template = $page->useTemplate;
            $this->settings_template_display_mode = $page->templateDisplayMode;
            $this->settings_template_name = $page->templateName;
            $this->settings_template_extension_name = $page->templateExtensionName;

            //extract the settings from the uri query string to override the page settings
            $this->extractSettingsFromUri();

            //run the controller
            $controllerExtension = $this->getExtension($page->targetControllerExtensionName);
            if($controllerExtension) {
                $controller = $controllerExtension->constructController($page->targetControllerName);
                if($controller) {
                    //verify page access
                    $pageAccess = $controller->canView();
                    if(is_null($pageAccess)) {
                        $pageAccess = Core_AccessEvent::runEvent("page","view",$page->id);
                    }
                    if($pageAccess) {
                        //work out which of the page params are the method/params
                        if(is_callable(array($controller,$pageParams[0]))) {
                            $method = $pageParams[0];
                            for($i = 1; $i < count($pageParams); $i++) {
                                $params[] = $pageParams[$i];
                            }
                        } else {
                            $method = "onDefault";
                            $params = $pageParams;
                        }

                        //start the controller
                        $controller->onStart();
                        $controllerExtension->onStartController($controller);

                        //load
                        $controller->onLoad();

                        //run the target method
                        call_user_func_array(array($controller,$method),$params);

                        //complete
                        $controller->onComplete();

                        //run the view
                        $viewExtension = $this->getExtension($page->targetViewExtensionName);
                        if($viewExtension) {
                            $view = $viewExtension->constructView($page->targetViewName);
                            if($view) {
                                //make the view inherit the controller's public properties
                                foreach($controller as $key => $value) {
                                    if(!in_array($key,array(
                                        "name",
                                        "extensionName",
                                        "extensionRoot"
                                    ))) {
                                        $view->$key = $value;
                                    }
                                }

                                //start the view
                                $view->onStart();
                                $viewExtension->onStartView($view);

                                //load
                                $view->onLoad();

                                //run the target method
                                call_user_func_array(array($view,$method),$params);

                                //complete
                                $view->onComplete();
                            }
                        }
                    }
                }
            }

            $content = ob_get_contents();
            ob_end_clean();
        }
        return $content;
    }

    public function fetchLiveBlockContentById($liveBlockId, $method = false, $params = array()) {
        return $this->fetchLiveBlockContentByLiveBlock(Core_LiveBlock::findById($liveBlockId),$method,$params);
    }

    public function fetchLiveBlockContentByName($liveBlockName, $method = false, $params = array()) {
        return $this->fetchLiveBlockContentByLiveBlock(Core_LiveBlock::findByName($liveBlockName),$method,$params);
    }

    /**
     * Fetch the content of a block by it's live block
     *
     * @param Core_LiveBlock $liveBlock
     * @param bool $method
     * @param array $params
     *
     * @return string
     */
    public function fetchLiveBlockContentByLiveBlock($liveBlock, $method = false, $params = array()) {
        $content = "";
        if($liveBlock) {
            //buffer the content
            ob_start();

            //convert the live block to a block
            $block = $liveBlock->toBlock();
            if($block) {
                //verify access
                if($method == "options") {
                    $access = $block->canEdit();
                    if(is_null($access)) {
                        $access = Core_AccessEvent::runEvent("block","edit",$liveBlock->id);
                    }
                } else {
                    $access = $block->canView();
                    if(is_null($access)) {
                        $access = Core_AccessEvent::runEvent("block","view",$liveBlock->id);
                    }
                }
                if($access) {
                    //start the block
                    $block->onStart();
                    $this->getExtension($block->extensionName)->onStartBlock($block);

                    //wrap in a panel
                    $panelStart = "";
                    $panelEnd = "";

                    switch($block->panelMode) {
                        case Core_Block_PanelTypes::panel_with_header :
                            $panelStart = '<div class="panel panel-primary">
                                      <div class="panel-heading">
                                        <h3 class="panel-title">'.$block->title.'</h3>
                                      </div>
                                      <div class="panel-body">';
                            $panelEnd = '</div>
                                </div>';
                            break;
                        case Core_Block_PanelTypes::panel_no_header :
                            $panelStart = '<div class="panel panel-primary">
                                      <div class="panel-body">';
                            $panelEnd = '</div>
                                </div>';
                            break;
                        case Core_Block_PanelTypes::no_panel :

                            break;
                    }

                    echo $panelStart;

                    //load
                    $block->onLoad();

                    //run the target method
                    $methodCalled = false;
                    if($method) {
                        if(is_callable(array($block,$method))) {
                            call_user_func_array(array($block,$method),$params);
                            $methodCalled = true;
                        }
                    }
                    if(!$methodCalled) {
                        call_user_func_array(array($block,"onDefault"),$params);
                    }

                    //complete
                    $block->onComplete();

                    echo $panelEnd;
                }
            }

            $content = ob_get_contents();
            ob_end_clean();
        }
        return $content;
    }

    public function fetchLiveBlocksContentByPageIdAndArea($pageId, $area) {
        $content = "";

        //use custom ssql to join the pages
        $ssql = "SELECT B.id FROM Core_LiveBlock B
                LEFT JOIN Core_LiveBlockPage P ON P.liveBlockId = B.id
                WHERE B.isEnabled = true
                AND B.area = ".$area."
                AND P.pageId = ".$pageId."
                ORDER BY B.order ASC";
        $rows = $this->getDbInstanceDefault()->getRowsIndices($ssql);
        foreach($rows as $row) {
            $blockContent = $this->fetchLiveBlockContentById($row[0]);
            if(strlen($blockContent)) {
                $content .= $blockContent;
            }
        }

        return $content;
    }

    public function serve() {
        //find the target page
        $pageRequestAddress = Core_Utils_Server::getRequestURL();
        $page = Core_Page::findByAddress($pageRequestAddress);

        //work out the additional fields of the request url (fields that extend the page's request url)
        $additional = str_replace($page->requestAddress,"",$pageRequestAddress);
        $additional = Core_Utils_String::notStartsAndNotEndsWith($additional,"/");
        $pageParams = array();
        if(strlen($additional)) {
            $pageParams = explode("/",$additional);
        }

        //fetch the page content
        $pageContent = $this->fetchPageContentByPage($page,$pageParams);

        //fetch the block content for this page
        $blockContent = array();

        //if we are using a template
        $templateUsed = false;
        if($this->settings_use_template) {

            //if the template is not set, use the system default
            if(
                !strlen($this->settings_template_name) ||
                !strlen($this->settings_template_extension_name)
            ) {
                $this->settings_template_name = $this->storedConfig["templates"]["default_name"];
                $this->settings_template_extension_name = $this->storedConfig["templates"]["default_extension_name"];
            }

            $templateExtension = $this->getExtension($this->settings_template_extension_name);
            if($templateExtension) {
                $template = $templateExtension->constructTemplate($this->settings_template_name);
                if($template) {
                    $templateUsed = true;

                    //start the template
                    $template->onStart();
                    $templateExtension->onStartTemplate($template);

                    //only fetch the block content when we need to!
                    if($this->settings_template_display_mode == Core_Template_DisplayMode::full) {
                        for($area = 1; $area <= 28; $area++) {
                            $blockContent[$area] = $this->fetchLiveBlocksContentByPageIdAndArea($page->id,$area);
                        }
                    }

                    //assign the content based on the display mode
                    switch($this->settings_template_display_mode) {
                        case Core_Template_DisplayMode::full :
                            //all content gets assigned
                            //page content
                            $template->pageContent = $pageContent;
                            //areas
                            foreach($blockContent as $area => $content) {
                                $area = "area_".$area;
                                $template->$area = $content;
                            }
                            //messages
                            $template->errorMessages = Core_Messager::fetchErrorMessages();
                            $template->warningMessages = Core_Messager::fetchWarningMessages();
                            $template->infoMessages = Core_Messager::fetchInfoMessages();
                            $template->successMessages = Core_Messager::fetchSuccessMessages();
                            break;
                        case Core_Template_DisplayMode::basic_with_messages :
                            //only the page content and messages get assigned
                            //page content
                            $template->pageContent = $pageContent;
                            //messages
                            $template->errorMessages = Core_Messager::fetchErrorMessages();
                            $template->warningMessages = Core_Messager::fetchWarningMessages();
                            $template->infoMessages = Core_Messager::fetchInfoMessages();
                            $template->successMessages = Core_Messager::fetchSuccessMessages();
                            break;
                        case Core_Template_DisplayMode::basic_no_messages :
                            //only the page content gets assigned
                            //page content
                            $template->pageContent = $pageContent;
                            break;
                    }

                    //serve the template
                    $template->beforeServe();
                    $template->onServe();
                    $template->afterServe();
                }
            }
        }

        //if a template wasn't used, simply display the page content
        if(!$templateUsed) {
            echo $pageContent;
        }
    }

    private function extractSettingsFromUri() {
        //get any settings from the url query string
        foreach($this->settings_keys as $key) {
            if(isset($_GET[$key])) {
                $this->$key = $_GET[$key];
            }
        }
    }

    private static function init() {
        //include required files
        require_once("core/classes/Core_Logger.php");
        require_once("core/utilities/Core_Utils_File.php");
        require_once("core/utilities/Core_Utils_String.php");

        $extorio = new Core_Extorio();

        $extorio->extractSettingsFromUri();

        $extorio->initiated = true;
        $_ENV["EXTORIO"] = &$extorio;
        return $extorio;
    }

    //can only be constructed by itself!
    private function __construct() {}
}