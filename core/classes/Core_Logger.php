<?php

/**
 * The logger class. Used to create log files in the /logs directory.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Classes
 *
 * Class Core_Logger
 */
final class Core_Logger {

    /**
     *  The debug log file name
     */
    const debugFile = "debug.log";
    /**
     *  The debug log file name
     */
    const warningFile = "warning.log";
    /**
     *  The debug log file name
     */
    const errorFile = "error.log";
    /**
     *  The system error log file name
     */
    const systemErrorFile = "systemError.log";

    /**
     * @var Core_Utils_File
     */
    private $file = false;

    /**
     * Write a message to the debug log file
     *
     * @param $message
     */
    public static function debugLog($message) {
        $logger = new Core_Logger(Core_Logger::debugFile);
        $logger->writeToLog($message);
    }

    /**
     * Write a message to the warning log file
     *
     * @param $message
     */
    public static function warningLog($message) {
        $logger = new Core_Logger(Core_Logger::warningFile);
        $logger->writeToLog($message);
    }

    /**
     * Write a message to the error log file
     *
     * @param $message
     */
    public static function errorLog($message) {
        $logger = new Core_Logger(Core_Logger::errorFile);
        $logger->writeToLog($message);
    }

    /**
     * Write a system error log, used by the system error function.
     *
     * @param $number
     * @param $message
     * @param $file
     * @param $line
     */
    public static function systemErrorLog($number, $message, $file, $line) {
        if(!strlen($message)) {
            return;
        }

        $logger = new Core_Logger(Core_Logger::systemErrorFile);
        $logger->writeToLog("(".$number.") ".$message." in ".$file.":".$line);
    }

    /**
     * Log an exception
     *
     * @param Exception $exception
     */
    public static function exceptionLog($exception) {
        $fileName = get_class($exception).".log";
        $logger = new Core_Logger($fileName);
        $logger->writeToLog("(".$exception->getCode().") ".$exception->getMessage()." in ".$exception->getFile().":".$exception->getLine());
    }

    /**
     * Create a custom log (if it does not already exist) and write to the file
     *
     * @param $logFileName
     * @param $message
     */
    public static function customLog($logFileName,$message) {
        $logger = new Core_Logger($logFileName);
        $logger->writeToLog($message);
    }

    /**
     * @param $logFileName
     */
    private function __construct($logFileName) {
        //make sure file name ends in .log
        if(substr($logFileName,strlen($logFileName)-4,4) != ".log") {
            $logFileName .= ".log";
        }

        //try and open the log file
        if(file_exists("logs/".$logFileName)) {
            $this->file = Core_Utils_File::getFile("logs/".$logFileName);
        } else {
            $this->file = Core_Utils_File::createFile("logs/".$logFileName);
        }
    }

    /**
     *
     */
    private function __destruct() {
        if($this->file) {
            $this->file->close();
        }
    }


    /**
     * Write to the log
     *
     * @param $message
     */
    private function writeToLog($message) {
        if($this->file) {
            $this->file->open("a+");
            $message = "[".date('d-m-Y H:i:s',time())."] - ".$message."\n";
            $this->file->write($message);
        }
    }
}