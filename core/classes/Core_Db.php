<?php

/**
 * This class is ultimately an interface to the mysqli class. It should be used to construct connections with mysql
 * databases. You should store your connections in Extorio for best performance.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Classes
 *
 * Class Core_Db
 */
class Core_Db {
    /**
     * The mysqli link resource  if constructed
     *
     * @var mysqli
     */
    private $db_connection = null;

    function __construct($db_database=null, $db_host=null, $db_pass=null, $db_user=null) {
        //if any of the properties are null, use the default db connection details
        $config = Core_Extorio::get()->getStoredConfig();
        if(is_null($db_database)) {
            $db_database = $config["db_defaults"]["database"];
        }

        if(is_null($db_host)) {
            $db_host = $config["db_defaults"]["host"];
        }

        if(is_null($db_pass)) {
            $db_pass = $config["db_defaults"]["password"];
        }

        if(is_null($db_user)) {
            $db_user = $config["db_defaults"]["username"];
        }

        $this->db_connection = mysqli_connect(
            $db_host,
            $db_user,
            $db_pass,
            $db_database
        );

        if(!$this->db_connection) {
            throw new Core_Db_Exception("Unable to connect to database: ".mysqli_connect_error());
        }

        //try and select db
        $db = mysqli_select_db($this->db_connection,$db_database);
        if(!$db) {
            throw new Core_Db_Exception("Unable to select database: ".mysqli_error($this->db_connection));
        }
    }


    /**
     * Destructor. This method closes your database connection once your instance is no longer alive.
     *
     * @return void
     */
    public function __destruct() {
        $this->disconnect();
    }

    /**
     * Disconnect from the database
     *
     * @return void
     */
    final public function disconnect() {
        mysqli_close($this->db_connection);
    }

    /**
     * This method returns an associative array of arrays based on the sql input. It will return FALSE on fail.
     *
     * @param string $ssql the sql query
     *
     * @throws Core_Db_Exception
     * @return array The array or array() if failed.
     */
    final public function getRowsAssociative($ssql) {

        $select = mysqli_query($this->db_connection,$ssql);

        if(!$select){
            throw new Core_Db_Exception($this->getError());
        }

        $rows = array();
        while($row = mysqli_fetch_array($select,MYSQLI_ASSOC)){
            $rows[] = $row;
        }

        return $rows;
    }

    /**
     * This method returns an index array of arrays based on the sql input. It will return FALSE on fail.
     *
     * @param string $ssql the sql query
     *
     * @throws Core_Db_Exception
     * @return array The array or array() if failed.
     */
    final public function getRowsIndices($ssql) {

        $select = mysqli_query($this->db_connection,$ssql);
        if(!$select){
            throw new Core_Db_Exception($this->getError());
        }

        $rows = array();
        while($row = mysqli_fetch_array($select,MYSQLI_NUM)){
            $rows[] = $row;
        }

        return $rows;

    }

    /**
     * This method returns a single associative array based on the sql input. It will return FALSE on fail.
     *
     * @param string $ssql the sql query
     *
     * @throws Core_Db_Exception
     * @return array|bool The associative array or FALSE if failed.
     */
    final public function getRowAssociative($ssql) {

        $select = mysqli_query($this->db_connection,$ssql);
        if(!$select){
            throw new Core_Db_Exception($this->getError());
        }

        return mysqli_fetch_array($select,MYSQLI_ASSOC);
    }

    /**
     * This method returns a single index array based on the sql input. It will return FALSE on fail.
     *
     * @param string $ssql the sql query
     *
     * @throws Core_Db_Exception
     * @return array|bool The associative array or FALSE if failed.
     */
    final public function getRowIndices($ssql) {

        $select = mysqli_query($this->db_connection,$ssql);
        if(!$select){
            throw new Core_Db_Exception($this->getError());
        }

        return mysqli_fetch_array($select,MYSQLI_NUM);
    }

    /**
     * This is a generic method to execute your queries without worrying about any return data.
     *
     * @param string $ssql the sql query
     *
     * @throws Core_Db_Exception
     * @return void
     */
    final public function execute($ssql) {
        $query = mysqli_query($this->db_connection,$ssql);
        if(!$query){
            throw new Core_Db_Exception($this->getError());
        }
    }

    /**
     * This method returns the insert_id. Useful for inserting data.
     *
     * @param string $ssql the sql query
     *
     * @throws Core_Db_Exception
     * @return int
     */
    final public function insert($ssql) {
        $insert = mysqli_query($this->db_connection,$ssql);
        if(!$insert){
            throw new Core_Db_Exception($this->getError());
        }

        return $this->getInsertId();
    }

    /**
     * This method returns the number of modified rows. Useful for updating
     *
     * @param string $ssql the sql query
     * @return int
     */
    final public function update($ssql) {
        return $this->modify($ssql);
    }

    /**
     * This method returns the number of modified rows. Useful for deleting
     *
     * @param string $ssql the sql query
     * @return int
     */
    final public function delete($ssql) {
        return $this->modify($ssql);
    }

    /**
     * This method returns the number of modified rows. Useful for replacing
     *
     * @param string $ssql the sql query
     * @return int
     */
    final public function replace($ssql) {
        return $this->modify($ssql);
    }

    /**
     * This method cleans a string, readying it for insertion into sql queries.
     *
     * @param string $string the input string
     * @return string
     *
     * @todo Add additional sanitization
     */
    final public function clean($string){
        if(gettype($string) == "string") {
            $string = mysqli_real_escape_string($this->db_connection, $string);
        }

        return $string;
    }

    /**
     * Get the most recent insert id for this db connection.
     *
     * @return int insert_id
     */
    final public function getInsertId() {
        return mysqli_insert_id($this->db_connection);
    }

    /**
     * Get the effected rows in the last modification.
     *
     * @return int number of rows
     */
    final public function getAffectedRows() {
        return mysqli_affected_rows($this->db_connection);
    }

    /**
     * Get the most recent error. Returns false if none.
     *
     * @return string|boolean Returns the error or FALSE if no error
     */
    final public function getError() {
        if(strlen(mysqli_error($this->db_connection))>0){
            return mysqli_error($this->db_connection);
        } else {
            return false;
        }
    }

    /**
     * Do a modification and return the number of affected rows.
     *
     * @param $ssql
     *
     * @return int
     * @throws Core_Db_Exception
     */
    private function modify($ssql) {
        $query = mysqli_query($this->db_connection,$ssql);
        if(!$query){
            throw new Core_Db_Exception($this->getError());
        }

        //return the number of affected rows
        return $this->getAffectedRows();
    }
}
