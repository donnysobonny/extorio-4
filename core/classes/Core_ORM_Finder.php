<?php
final class Core_ORM_Finder {
    private $where = "";
    private $limitStart = false;
    private $limitResults = false;
    private $orderBy = "base.id";
    private $orderDir = "desc";
    private $ignoreAccessChecks = false;
    private $depth = 10;

    public static function newInstance() {
        return new Core_ORM_Finder();
    }

    /**
     * @return string
     */
    public function getWhere() {
        return $this->where;
    }

    /**
     * @return boolean
     */
    public function getLimitStart() {
        return $this->limitStart;
    }

    /**
     * @return boolean
     */
    public function getLimitResults() {
        return $this->limitResults;
    }

    /**
     * @return string
     */
    public function getOrderBy() {
        return $this->orderBy;
    }

    /**
     * @return string
     */
    public function getOrderDir() {
        return $this->orderDir;
    }

    /**
     * @return boolean
     */
    public function getIgnoreAccessChecks() {
        return $this->ignoreAccessChecks;
    }

    /**
     * @return int
     */
    public function getDepth() {
        return $this->depth;
    }

    public function where($where="") {
        $this->where .= $where;
        return $this;
    }

    public function limitStart($limitStart=false) {
        $this->limitStart = $limitStart;
        return $this;
    }

    public function limitResults($limitResults=false) {
        $this->limitResults = $limitResults;
        return $this;
    }

    public function orderBy($orderBy="base.id") {
        $this->orderBy = $orderBy;
        return $this;
    }

    public function orderDir($orderDir="desc") {
        $this->orderDir = $orderDir;
        return $this;
    }

    public function ignoreAccessChecks($ignoreAccessChecks=false) {
        $this->ignoreAccessChecks = $ignoreAccessChecks;
        return $this;
    }

    public function depth($depth=10) {
        $this->depth = $depth;
        return $this;
    }
}