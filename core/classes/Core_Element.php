<?php

/**
 * Controllers, views, blocks, tempates and modules are all elements. They extend this class which gives them all some
 * useful base functionality.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Classes
 *
 * Class Core_Element
 */
class Core_Element {
    /**
     * The element's name
     *
     * @var string
     */
    public $name;
    /**
     * The element's extension name (folder name)
     *
     * @var string
     */
    public $extensionName;
    /**
     * The element's extension root directory
     *
     * @var string
     */
    public $extensionRoot;

    protected $started = false;

    /**
     * can only construct internally
     *
     * @param $name
     * @param $extensionName
     * @param $extensionRoot
     *
     * @throws Exception
     */
    final public function __construct($name,$extensionName,$extensionRoot) {
        //make sure this was constructed only by Core_Extension
        $trace = debug_backtrace();
        if(
            count($trace) < 2 ||
            !isset($trace[1]["class"]) ||
            $trace[1]["class"] !== "Core_Extension"
        ) {
            throw new Exception("An element object can only be constructed by Core_Extension");
        }
        $this->name = $name;
        $this->extensionName = $extensionName;
        $this->extensionRoot = $extensionRoot;
    }

    public function onStart() {

    }

    public function onLoad() {

    }

    public function onDefault() {

    }

    public function onComplete() {

    }

    /**
     * Get the extorio instance
     *
     * @return Core_Extorio
     * @throws Core_Extorio_Exception
     */
    final protected function Extorio() {
        return Core_Extorio::get();
    }

    /**
     * Get this element's extension
     *
     * @return Core_Extension
     */
    final protected function Extension() {
        return $this->Extorio()->getExtension($this->extensionName);
    }

    /**
     * Bind a method of this object to an Extorio event
     *
     * @param string $eventName
     * @param string $method
     * @param int $priority
     * @param bool $overrideOtherActions
     */
    final protected function bindMethodToEvent($eventName,$method,$priority=99,$overrideOtherActions=false) {
        $this->Extorio()->createAction($eventName,$this,$method,$priority,$overrideOtherActions);
    }

    /**
     * Start a background task. The $request is the request uri (the url) that should point to a page. The page should then point to a task controller
     *
     * @param $request
     *
     * @throws Core_Task_Exception
     */
    final protected function startBackgroundTask($request) {
        Core_Task::startBackgroundTask($request);
    }

    final protected function runControllerInBackground($controllerName,$method=false,$urlParams=array(),$queryParams=array()) {
        $url = $this->getUrlToController($controllerName,$method,$urlParams,$queryParams);
        if(strlen($url)) {
            $this->startBackgroundTask($url);
        }
    }

    final protected function runPageInBackgroundById($pageId,$addToUrl="",$urlParams=array(),$queryParams=array()) {
        $url = $this->getUrlToPageById($pageId,$addToUrl,$urlParams,$queryParams);
        if(strlen($url)) {
            $this->startBackgroundTask($url);
        }
    }

    final protected function runPageInBackgroundByName($pageName,$addToUrl="",$urlParams=array(),$queryParams=array()) {
        $url = $this->getUrlToPageByName($pageName,$addToUrl,$urlParams,$queryParams);
        if(strlen($url)) {
            $this->startBackgroundTask($url);
        }
    }

    final protected function redirectToController($controllerName,$method=false,$urlParams=array(),$queryParams=array()) {
        $url = $this->getUrlToController($controllerName,$method,$urlParams,$queryParams);
        if(strlen($url)) {
            header("Location: ".$url);
            exit;
        }
    }

    final protected function getUrlToController($controllerName,$method=false,$urlParams=array(),$queryParams=array()) {
        $page = Core_Page::findByController($controllerName);
        if($page) {
            $url = $page->requestAddress;
            $url = Core_Utils_String::endsWith($url,"/");
            if($method) {
                $method = Core_Utils_String::notStartsWith($method,"/");
                $url .= $method;
            }

            return $this->getUrl($url,$urlParams,$queryParams);
        } else {
            return "";
        }
    }

    final protected function redirectToPageByName($pageName,$addToUrl="",$urlParams=array(),$queryParams=array()) {
        $url = $this->getUrlToPageByName($pageName,$addToUrl,$urlParams,$queryParams);
        if(strlen($url)) {
            header("Location: ".$url);
            exit;
        }
    }

    final protected function getUrlToPageByName($pageName,$addToUrl="",$urlParams=array(),$queryParams=array()) {
        $page = Core_Page::findByName($pageName);
        if($page) {
            $url = $page->requestAddress;
            $url = Core_Utils_String::notEndsWith($url,"/");
            $addToUrl = Core_Utils_String::startsWith($addToUrl,"/");
            $url .= $addToUrl;

            return $this->getUrl($url,$urlParams,$queryParams);
        } else {
            return "";
        }
    }

    final protected function redirectToPageById($pageId,$addToUrl="",$urlParams=array(),$queryParams=array()) {
        $url = $this->getUrlToPageById($pageId,$addToUrl,$urlParams,$queryParams);
        if(strlen($url)) {
            header("Location: ".$url);
            exit;
        }
    }

    final protected function getUrlToPageById($pageId,$addToUrl="",$urlParams=array(),$queryParams=array()) {
        $page = Core_Page::findById($pageId);
        if($page) {
            $url = $page->requestAddress;
            $url = Core_Utils_String::notEndsWith($url,"/");
            $addToUrl = Core_Utils_String::startsWith($addToUrl,"/");
            $url .= $addToUrl;

            return $this->getUrl($url,$urlParams,$queryParams);
        } else {
            return "";
        }
    }

    final protected function redirectToUrl($url,$urlParams=array(),$queryParams=array()) {
        header("Location: ".$this->getUrl($url,$urlParams,$queryParams));
        exit;
    }

    final protected function getUrl($url,$urlParams=array(),$queryParams=array()) {
        $url = Core_Utils_String::notEndsWith($url,"/");
        foreach($urlParams as $param) {
            $url .= "/".$param;
        }
        if(!empty($queryParams)) {
            $url .= "?".http_build_query($queryParams);
        }
        return $url;
    }
}