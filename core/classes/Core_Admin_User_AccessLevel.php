<?php
final class Core_Admin_User_AccessLevel {
	const Super_Administrator = "0";
	const Administrator_level_1 = "1";
	const Administrator_level_2 = "2";
	const Administrator_level_3 = "3";
	const Administrator_level_4 = "4";
	const Administrator_level_5 = "5";

}