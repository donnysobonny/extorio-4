<?php
final class Core_Utils_Page {
    public static function getTargetPageUrlFieldsAll() {
        $params = array();
        $extorio = Core_Extorio::get();
        $page = $extorio->getPage();
        if($page) {
            //get the fields after the target page's address
            $requestUrl = Core_Utils_Server::getRequestURL();
            $requestUrl = Core_Utils_String::endsWith($requestUrl,"/");
            $additionalFields = str_replace($page->requestAddress,"",$requestUrl);
            //remove preceeding/receeding slashes
            $additionalFields = Core_Utils_String::notStartsWith($additionalFields,"/");
            $additionalFields = Core_Utils_String::notEndsWith($additionalFields,"/");

            $params = explode("/",$additionalFields);
        }

        return $params;
    }

    /**
     * Find the target page based on the url
     *
     * @return Core_Page
     */
    public static function findTargetPage() {

        //try and find the page with the exact url first
        $exact = Core_Utils_Server::getRequestURL();
        $exact = Core_Utils_String::endsWith($exact,"/");
        $page = Core_Page::findByAddress($exact);
        if($page) {
            return $page;
        }

        //url doesnt point exactly to a page, get the fields instead
        $fields = Core_Utils_Server::getRequestURLFields();

        //keep removing the last field until we find the target page
        while(count($fields)) {
            $url = "/";
            foreach($fields as $field) {
                $url .= $field."/";
            }

            $page = Core_Page::findByAddress($url);
            if($page) {
                return $page;
            }

            //remove last field and try again
            array_pop($fields);
        }

        //page could not be found
        $config = Core_Extorio::get()->getStoredConfig();
        return Core_Page::findById($config["pages"]["page-not-found"]);
    }
}