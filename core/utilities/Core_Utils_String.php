<?php

final class Core_Utils_String {
    public static function convertToEncoding($string,$encType) {
        // detect the character encoding of the incoming file
        $encoding = mb_detect_encoding($string, "auto");

        // escape all of the question marks so we can remove artifacts from
        // the unicode conversion process
        $target = str_replace( "?", "[question_mark]", $string );

        // convert the string to the target encoding
        $target = mb_convert_encoding( $target, $encType, $encoding);

        // remove any question marks that have been introduced because of illegal characters
        $target = str_replace( "?", "", $target );

        // replace the token string "[question_mark]" with the symbol "?"
        $target = str_replace( "[question_mark]", "?", $target );

        return $target;
    }

    public static function removeQuotes($string) {
        return str_replace(array(
            "'",
            '"'
        ),"",$string);
    }

    public static function endsWith($string, $character) {
        if (substr($string, strlen($string) - 1, 1) != $character) {
            $string .= $character;
        }

        return $string;
    }

    public static function startsWith($string, $character) {
        if (substr($string, 0, 1) != $character) {
            $string = $character . $string;
        }

        return $string;
    }

    public static function startsAndEndsWith($string, $character) {
        $string = self::startsWith($string,$character);
        $string = self::endsWith($string,$character);
        return $string;
    }

    public static function notEndsWith($string, $character) {
        if (substr($string, strlen($string) - 1, 1) == $character) {
            $string = substr($string, 0, strlen($string) - 1);
        }

        return $string;
    }

    public static function notStartsWith($string, $character) {
        if (substr($string, 0, 1) == $character) {
            $string = substr($string, 1, strlen($string) - 1);
        }

        return $string;
    }

    public static function notStartsAndNotEndsWith($string, $character) {
        $string = self::notStartsWith($string,$character);
        $string = self::notEndsWith($string,$character);
        return $string;
    }

    public static function replaceSpecialCharacters($string, $replace = "") {
        return preg_replace("/[\W]/", $replace, $string);
    }

    public static function replaceNumbers($string, $replace = "") {
        return preg_replace("/[\\d]/", $replace, $string);
    }

    public static function wordsToCamelCase($string, $firstWordCapitalised = true) {
        //replace and special characters with spaces
        $string = Core_Utils_String::replaceSpecialCharacters($string, " ");
        $string = ucwords($string);
        $string = str_replace(" ","",$string);
        if (!$firstWordCapitalised) {
            $string = lcfirst($string);
        }

        return $string;
    }
}