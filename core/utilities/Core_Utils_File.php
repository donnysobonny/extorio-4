<?php

/**
 * File utilities. A useful set of methods for managing files.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Utilities
 *
 * Class Core_Utils_File
 */
final class Core_Utils_File {
    /**
     * The file size
     *
     * @var int
     */
    public $size = 0;
    /**
     * The file type
     *
     * @var string
     */
    public $type = "";
    /**
     * The directory this file exists in
     *
     * @var string
     */
    public $dirName = "";
    /**
     * The base name of this file
     *
     * @var string
     */
    public $baseName = "";
    /**
     * The extension
     *
     * @var string
     */
    public $extension = "";
    /**
     * The file name
     *
     * @var string
     */
    public $fileName = "";
    /**
     * The fill path of this file
     *
     * @var string
     */
    public $path = "";
    /**
     * The internal handle used for reading/writing
     *
     * @var Resource
     */
    private $handle = null;

    /**
     * Get all files in a folder
     *
     * @param $folderPath
     *
     * @return Core_Utils_File[]
     */
    public static function findFilesInFolder($folderPath) {
        $files = array();

        $folderPath = Core_Utils_String::endsWith($folderPath, "/");

        if (file_exists($folderPath)) {
            $all = scandir($folderPath);

            unset($all[0]);
            unset($all[1]);
            $all = array_values($all);

            foreach ($all as $potential) {
                if (is_file($folderPath . $potential)) {
                    $file = Core_Utils_File::constructFromPath($folderPath . $potential);
                    $files[] = $file;
                }
            }
        }

        return $files;
    }

    /**
     * Get all files in a folder recursively
     *
     * @param $folderPath
     *
     * @return Core_Utils_File[]
     */
    public static function findFilesInFolderRecursively($folderPath) {
        $files = array();

        $folderPath = Core_Utils_String::endsWith($folderPath, "/");

        if (file_exists($folderPath)) {
            $all = scandir($folderPath);

            unset($all[0]);
            unset($all[1]);
            $all = array_values($all);

            foreach ($all as $potential) {
                if (is_file($folderPath . $potential)) {
                    $file = Core_Utils_File::constructFromPath($folderPath . $potential);
                    $files[] = $file;
                } else {
                    $addFiles = Core_Utils_File::findFilesInFolderRecursively($folderPath.$potential);
                    foreach($addFiles as $file) {
                        $files[] = $file;
                    }
                }
            }
        }

        return $files;
    }

    /**
     * Create a new file. Returns FALSE upon failure
     *
     * @param $filePath
     *
     * @return bool|Core_Utils_File
     */
    public static function createFile($filePath) {
        $file = false;
        $fh = fopen($filePath,"w");
        if($fh) {
            $file = Core_Utils_File::getFile($filePath);
        }

        return $file;
    }

    /**
     * Delete a file.
     *
     * @param string $filePath
     *
     * @return bool
     */
    public static function deleteFile($filePath) {
        return Core_Utils_File::getFile($filePath)->delete();
    }

    /**
     * Rename a file.
     *
     * @param string $filePathFrom
     * @param string $filePathTo
     *
     * @return bool
     */
    public static function renameFile($filePathFrom,$filePathTo) {
        return Core_Utils_File::getFile($filePathFrom)->rename($filePathTo);
    }

    /**
     * Move a file
     *
     * @param string $filePathFrom
     * @param string $filePathTo
     *
     * @return bool
     */
    public static function moveFile($filePathFrom,$filePathTo) {
        return Core_Utils_File::getFile($filePathFrom)->move($filePathTo);
    }

    /**
     * Read all of the content from a file.
     *
     * @param string $filePath
     *
     * @return bool|string
     */
    public static function readFromFile($filePath) {
        $file = Core_Utils_File::getFile($filePath);
        if($file->open("r")) {
            return $file->read();
        } else {
            return false;
        }
    }

    /**
     * Overwrite all of the content of a file.
     *
     * @param string $filePath
     * @param string $content
     *
     * @return bool
     */
    public static function writeToFile($filePath,$content) {
        $file = Core_Utils_File::getFile($filePath);
        if($file->open("w+")) {
            $file->write($content);
            return true;
        } else {
            return false;
        }
    }
    /**
     * Get a File instance of a file by a path. Retruns FALSE if no file found.
     *
     * @param string $path
     *
     * @return bool|Core_Utils_File
     */
    public static function getFile($path) {
        return Core_Utils_File::constructFromPath($path);
    }

    /**
     * Construct the Core_Utils_File object from a file. Returns FALSE if file was not found.
     *
     * @param string $path
     *
     * @return bool|Core_Utils_File
     */
    private static function constructFromPath($path) {
        if(file_exists($path)) {
            $file = new Core_Utils_File();
            $file->size = filesize($path);
            $file->type = filetype($path);

            $pathinfo = pathinfo($path);
            $file->baseName = $pathinfo["basename"];
            $file->dirName = $pathinfo["dirname"];
            $file->extension = $pathinfo["extension"];
            $file->fileName = $pathinfo["filename"];

            $file->path = $path;

            return $file;
        } else {
            return false;
        }
    }

    /**
     * Remove the parent folders while scanning a directory.
     *
     * @param string $filesInDir
     *
     * @return array
     */
    private static function removeParentFolders($filesInDir) {
        if(is_array($filesInDir)) {
            unset($filesInDir[0]);
            unset($filesInDir[1]);
            return array_values($filesInDir);
        }
    }

    /**
     * Open this file for reading/writing. The mode is based on fopen() modes and will dictate whether you can
     * later read or write to a file. Returns true if the file was opened, false if it wasn't.
     *
     * @param string $mode
     *
     * @return bool
     */
    public function open($mode="a+") {
        $this->handle = fopen($this->path,$mode);
        if($this->handle) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Close a file that was opened.
     */
    public function close() {
        if($this->handle) {
            fclose($this->handle);
            $this->handle = false;
        }
    }

    /**
     * Read an open file. This returns the full content of the file in a string.
     *
     * @return bool|string
     */
    public function read() {
        if($this->handle && $this->size) {
            return fread($this->handle,$this->size);
        } else {
            return false;
        }
    }

    /**
     * Write to an open file. Note that you must have opened your file with a mode that allows you to write to the
     * file. Returns TRUE or FALSE based on whether the file was written to or not.
     *
     * @param string $content
     *
     * @return bool
     */
    public function write($content) {
        if($this->handle) {
            fwrite($this->handle,$content);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete this file.
     *
     * @return bool
     */
    public function delete() {
        return unlink($this->path);
    }

    /**
     * Move this file. Note that your destination may need write permissions based on the config of your server.
     *
     * @param string $newDir
     *
     * @return bool
     */
    public function move($newDir) {
        $newDir = Core_Utils_String::endsWith($newDir,"/");
        $moved = rename($this->path,$newDir.$this->baseName);
        if($moved) {
            $file = Core_Utils_File::getFile($newDir.$this->baseName);
            foreach($file as $key => $value) {
                $this->$key = $value;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Rename the current file.
     *
     * @param string $newBaseName
     *
     * @return bool
     */
    public function rename($newBaseName) {
        $newPath = str_replace($this->baseName,$newBaseName,$this->path);
        $renamed = rename($this->path,$newPath);
        if($renamed) {
            $file = Core_Utils_File::getFile($newPath.$this->baseName);
            foreach($file as $key => $value) {
                $this->$key = $value;
            }
            return true;
        } else {
            return false;
        }
    }
}