<?php

final class Core_Utils_Extorio {
    public static function findTargetAddress() {
        $address = str_replace("?".Core_Utils_Server::getQueryString(),"",Core_Utils_Server::getRequestURI());
        //make sure ends with /
        return Core_Utils_String::endsWith($address,"/");
    }

    /**
     * Find the target page based on the target address
     *
     * @return Core_page
     * @throws Core_Page_Exception
     */
    public static function findTargetPage() {
        $address = self::findTargetAddress();
        //try and find the page by fields
        $page = Core_Page::findByAddress($address);
        if(!$page) {
            //try and find the page not found page
            $config = Core_Extorio::get()->getConfig();
            $page = Core_Page::findById($config["pages"]["page-not-found"]);
        }

        if(!$page) {
            throw new Core_Page_Exception("Unable to locate the target page!");
        }

        return $page;
    }
}