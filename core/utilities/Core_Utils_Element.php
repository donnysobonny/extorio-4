<?php
final class Core_Utils_Element {
    public static function getClassName($extensionName,$elementName,$elementType) {
        //strip and camel case
        $extensionName = Core_Utils_String::wordsToCamelCase($extensionName);
        $elementName = Core_Utils_String::wordsToCamelCase($elementName);
        $elementType = Core_Utils_String::wordsToCamelCase($elementType);

        return $extensionName.$elementName.$elementType;
    }

    public static function createNewBlockType($extensionName,$elementName,$path) {
        $className = Core_Utils_Element::getClassName($elementName,$elementName,"block");
        //clean up the path slashes
        $path = Core_Utils_String::notStartsWith($path,"/");
        $path = Core_Utils_String::notEndsWith($path,"/");

        //check to see if the path already exists
        if(!file_exists($path)) {
            mkdir($path,0775,true);
        }

        //check to see if the file already exists
        if(!file_exists($path."/".$className.".php")) {
            //create the file
            $file = Core_Utils_File::createFile($path."/".$className.".php");
            $file->open("w");

            //use the block template
            $contents = Core_Utils_File::readFromFile("core/assets/file-templates/Core_Block_Template.txt");

            //replacers
            $contents = str_replace("*|CLASS_NAME|*",$className,$contents);

            //write the file
            $file->write($contents);

            $file->close();
        }
    }
}