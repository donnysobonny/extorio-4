<?php

/**
 * Model utility methods
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Utilities
 *
 * Class Core_Utils_Model
 */
final class Core_Utils_Model {
    /**
     * Create a new model class. This creates the tables and files for the model.
     *
     * @param $modelName
     *
     * @throws Core_MVC_Exception
     */
    final static public function createNewModel($modelName,$modelDir="application/models",$baseModel="basic") {
        $className = Core_Utils_String::wordsToCamelCase($modelName);
        //check if the model already exists
        if(!Core_Class::findByName($className)) {
            //create the new class
            $class = new Core_Class();
            $class->name = $className;
            $class->modelDir = $modelDir;
            $class->pushThis();

            return $class;
        } else {
            throw new Core_MVC_Exception("Trying to create the model '$modelName' when it already exists!");
        }
    }

    /**
     * Remove a model class. This removes the tables, data and files.
     *
     * @param $modelName
     */
    final static public function removeModel($modelName){
        $className = Core_Utils_String::wordsToCamelCase($modelName);

        $class = Core_Class::findByName($className);
        if($class) {
            $class->removeThis();
        }
    }

    /**
     * Sync the local class files with the model and it's properties
     *
     * @param $modelName
     */
    final static public function syncModelWithClassFiles($modelName) {
        $className = Core_Utils_String::wordsToCamelCase($modelName);
        $class = Core_Class::findByName($className);
        if($class) {
            //get the template
            $content = Core_Utils_File::readFromFile("core/assets/file-templates/Core_BaseModel_Template.txt");
            $content = str_replace("*|CLASSNAME|*",$className,$content);
            $content = str_replace("*|IS_PERSISTENT|*",$class->isPersistent?"true":"false",$content);

            //create the properties, types and object types
            $props = Core_Property::findAllByOwnerClassId($class->id);
            $propertyString = "";
            $propertyTypeString = "";
            $objectTypeString = "";
            foreach($props as $prop) {
                $type = "";
                if($prop->phpType != Core_PHP_Types::_OBJECT && $prop->phpType != Core_PHP_Types::_ARRAY) {
                    switch($prop->phpType) {
                        case Core_PHP_Types::_STRING :
                            $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_STRING,\n";
                            break;
                        case Core_PHP_Types::_INTEGER :
                            $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_INTEGER,\n";
                            break;
                        case Core_PHP_Types::_BOOLEAN :
                            $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_BOOLEAN,\n";
                            break;
                        case Core_PHP_Types::_FLOAT :
                            $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_FLOAT,\n";
                            break;
                        case Core_PHP_Types::_DOUBLE :
                            $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_DOUBLE,\n";
                            break;
                    }

                    $type = $prop->phpType;
                } else {
                    $childClass = Core_Class::findById($prop->childClassId);
                    $type = $childClass->name;

                    switch($prop->phpType) {
                        case Core_PHP_Types::_OBJECT :
                            $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_OBJECT,\n";
                            break;
                        case Core_PHP_Types::_ARRAY :
                            $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_ARRAY,\n";
                            break;
                    }

                    $objectTypeString .= "\t\t'".$prop->name."' => '".$type."',\n";

                    if($prop->phpType == Core_PHP_Types::_ARRAY) {
                        $type .= "[]";
                    }
                }

                $propertyString .= "
    /**
     * @var ".$type."
     */
    public $".$prop->name.";
                ";
            }

            $propertyTypeString = substr($propertyTypeString,0,strlen($propertyTypeString)-2);
            $objectTypeString = substr($objectTypeString,0,strlen($objectTypeString)-2);

            $content = str_replace("*|PROPERTIES|*",$propertyString,$content);
            $content = str_replace("*|PROPERTY_TYPES|*",$propertyTypeString,$content);
            $content = str_replace("*|OBJECT_TYPES|*",$objectTypeString,$content);

            $file = Core_Utils_File::createFile("core/models/Core_BaseModel_".$className.".php");
            $file->open("w+");
            $file->write($content);
            $file->close();
        }
    }

    /**
     * Get the basic class>properties hierarchy for all models.
     *
     * @return array
     */
    final static public function getNavigationHierarchy() {
        $hierarchy = array();
        //get all classes
        $classes = Core_Class::findAll(Core_ORM_Finder::newInstance()
            ->orderBy("base.name")
            ->orderDir("asc")
        );
        foreach($classes as $class) {
            $hierarchy[$class->name] = array();

            //get the properties
            $props = Core_Property::findAll(Core_ORM_Finder::newInstance()
                ->where("base.ownerClassId = ".$class->id." AND base.name != ('id')")
                ->orderBy("base.name")
                ->orderDir("asc")
            );
            foreach($props as $prop) {
                $hierarchy[$class->name][] = $prop->name;
            }
        }

        return $hierarchy;
    }

    /**
     * Get a complex property hierarchy for a class and all of it's child classes
     *
     * @param $modelClassName
     * @param string $currentPropertyPathLong
     * @param string $currentPropertyPathShort
     * @param int $index
     * @param array $outAliasesLong
     * @param array $outAliasesShort
     *
     * @return Core_PropertyHierarchy[]
     */
    final static public function getPropertyHierarchyByClassName($modelClassName,
                                                                 $currentPropertyPathLong="",
                                                                 $currentPropertyPathShort="",&$index=1,
                                                                 &$outAliasesLong=array(),&$outAliasesShort=array(),
                                                                $lastLeftClassName="",$lastLeftToRightPropertyName="") {
        $hierarchies = array();

        $thisClass = Core_Class::findByName($modelClassName);
        if($thisClass) {
            //get the properties of this class that are an object or array
            $properties = Core_Property::findAll(Core_ORM_Finder::newInstance()
                ->where("
                (base.phpType = ('".Core_PHP_Types::_ARRAY."') OR base.phpType = ('".Core_PHP_Types::_OBJECT."')) AND
                base.ownerClassId = ".$thisClass->id."
                ")
            );

            foreach($properties as $property) {
                $propertyClass = Core_Class::findById($property->childClassId);
                //stop recursion
                if($thisClass->name != $lastLeftClassName && $property->name != $lastLeftToRightPropertyName) {
                    $hierarchy = new Core_PropertyHierarchy();
                    $hierarchy->leftClassName = $modelClassName;
                    $hierarchy->rightClassName = $propertyClass->name;
                    $hierarchy->leftToRightProperty = $property->name;
                    $hierarchy->parentPropertyPathLong = strlen($currentPropertyPathLong)
                        ?$currentPropertyPathLong:"base";
                    $hierarchy->propertyPathShort = strlen($currentPropertyPathShort)?$currentPropertyPathShort."."
                        .$property->name:"base.".$property->name;
                    $hierarchy->propertyPathLong = strlen($currentPropertyPathLong)?$currentPropertyPathLong."_"
                        .$property->name:"base_".$property->name;
                    $hierarchy->index = $index;

                    $outAliasesShort["".strlen($hierarchy->propertyPathShort).".".$index] = $hierarchy->propertyPathShort;
                    $outAliasesLong["".strlen($hierarchy->propertyPathLong).".".$index] = $hierarchy->propertyPathLong;

                    $index++;

                    $hierarchies[] = $hierarchy;

                    //recursion
                    $additional = Core_Utils_Model::getPropertyHierarchyByClassName($hierarchy->rightClassName,
                        $hierarchy->propertyPathLong,$hierarchy->propertyPathShort,$index,$outAliasesLong,
                        $outAliasesShort,$thisClass->name,$property->name);
                    foreach($additional as $add) {
                        $hierarchies[] = $add;
                    }
                }
            }
        }

        //sort the alias arrays so that the largest aliases are first, This will mean that replacing the aliases will
        //be done in the most successful order
        krsort($outAliasesShort);
        krsort($outAliasesLong);

        return $hierarchies;
    }
}