<?php
final class Core_Utils_Array {
    final public static function valuesEmpty($array) {
        foreach($array as $values) {
            switch(gettype($values)) {
                case Core_PHP_Types::_ARRAY :
                    if(!self::valuesEmpty($values)) {
                        return false;
                    }
                    break;
                case Core_PHP_Types::_STRING :
                    if(strlen($values)) {
                        return false;
                    }
                    break;
                case Core_PHP_Types::_INTEGER :
                    return false;
                    break;
                case Core_PHP_Types::_BOOLEAN :
                    return false;
                    break;
                case Core_PHP_Types::_DOUBLE :
                    return false;
                    break;
                case Core_PHP_Types::_FLOAT :
                    return false;
                    break;
                default :
                    break;
            }
        }
        return true;
    }
}