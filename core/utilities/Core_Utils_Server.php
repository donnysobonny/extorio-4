<?php

/**
 * Set of utility methods for the server
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Utilities
 *
 * Class Core_Utils_Server
 */
final class Core_Utils_Server {

    public static function getContentType() {
        if(isset($_SERVER["CONTENT_TYPE"])) {
            return $_SERVER["CONTENT_TYPE"];
        } else {
            return false;
        }
    }

    public static function getDomain() {
        $domain = "http://";
        if(isset($_SERVER["HTTPS"])) {
            if($_SERVER["HTTPS"] == "on") {
                $domain = "https://";
            }
        }

        $domain .= $_SERVER["SERVER_NAME"];

        return $domain;
    }

    /**
     * Get the input stream as a raw string
     *
     * @return string
     */
    public static function getInputStreamRaw() {
        return file_get_contents("php://input");
    }

    /**
     * Get the request method
     *
     * @return string
     */
    public static function getMethod() {
        $method = $_SERVER["REQUEST_METHOD"];

        if($method == "POST" && array_key_exists("HTTP_X_HTTP_METHOD", $_SERVER)) {
            if ($_SERVER["HTTP_X_HTTP_METHOD"] == "DELETE") {
                $method = "DELETE";
            } else if ($_SERVER["HTTP_X_HTTP_METHOD"] == "PUT") {
                $method = "PUT";
            } else {
                return false;
            }
        }

        return strtoupper($method);
    }

    /**
     * Get the requested uri (includes the query string)
     *
     * @return string
     */
    public static function getRequestURI() {
        return $_SERVER["REQUEST_URI"];
    }

    public static function getRequestURL() {
        return $_SERVER["REDIRECT_URL"];
    }

    /**
     * Get the url fields as an array
     *
     * @return array
     */
    public static function getRequestURLFields() {
        $url = self::getRequestURL();

        //remove preceeding and trailing slashes
        $url = Core_Utils_String::notEndsWith($url,"/");
        $url = Core_Utils_String::notStartsWith($url,"/");

        return explode("/",$url);
    }

    /**
     * Get just the query string
     *
     * @return string
     */
    public static function getQueryString() {
        return $_SERVER["QUERY_STRING"];
    }

    /**
     * Get the current requests ip address.
     *
     * Retunrs null if no address could be found.
     *
     * @return string|null The ip address or NULL if couldn't be found
     */
    public static function getIpAddress() {

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        if(strlen($ip)) {
            return $ip;
        } else {
            return null;
        }
    }

    /**
     * Get the request time.
     *
     * @return string the request time
     */
    public static function getRequestTime() {

        return $_SERVER["REQUEST_TIME"];

    }

    /**
     * Get the remote port, used in api logs
     *
     * @return string the remote port. Used in api logs
     */
    public static function getRemotePort() {

        return $_SERVER["REMOTE_PORT"];

    }

    /**
     * Get the remote user for verifying that this app is registered
     *
     * @return string the remote user
     */
    public static function getRemoteUser() {

        return $_SERVER["REMOTE_USER"];

    }
}