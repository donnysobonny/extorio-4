<?php
final class Core_Utils_Folder {
    public static function findFoldersInFolder($folderPath) {
        $folders = array();

        $folderPath = Core_Utils_String::endsWith($folderPath,"/");

        if(file_exists($folderPath)) {
            $all = scandir($folderPath);

            unset($all[0]);
            unset($all[1]);
            $all = array_values($all);

            foreach($all as $potential) {
                if(is_dir($folderPath.$potential)) {
                    $folders[] = $folderPath.$potential;
                }
            }
        }

        return $folders;
    }

    public static function findFoldersInFolderRecursively($folderPath) {
        $folders = array();

        $folderPath = Core_Utils_String::endsWith($folderPath,"/");

        if(file_exists($folderPath)) {
            $all = scandir($folderPath);

            unset($all[0]);
            unset($all[1]);
            $all = array_values($all);

            foreach($all as $potential) {
                if(is_dir($folderPath.$potential)) {
                    $folders[] = $folderPath.$potential;
                    $addFolders = Core_Utils_Folder::findFoldersInFolderRecursively($folderPath.$potential);
                    foreach($addFolders as $folder) {
                        $folders[] = $folder;
                    }
                }
            }
        }

        return $folders;
    }
}