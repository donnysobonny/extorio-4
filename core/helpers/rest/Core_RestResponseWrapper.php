<?php
final class Core_RestResponseWrapper {
    public $error = false;
    public $error_message = "";
    public $status = "OK";
    public $status_code = 200;
    public $data = array();

    private $statusMessages = array(
        200 => "OK",
        400 => "Bad Request",
        401 => "Unauthorized",
        404 => "Not Found",
        405 => "Method Not Allowed",
        500 => "Internal Server Error"
    );

    public function displayAs($type="json") {
        switch($type) {
            case "json" :
                $this->displayAsJson();
                break;
            case "xml" :
                $this->displayAsXml();
                break;
            default:
                $this->displayAsJson();
                break;
        }
    }

    public function displayAsJson() {
        //set the status header
        header('Content-Type: application/json');
        header("HTTP/1.1 " . $this->status_code . " " . $this->status);
        print(json_encode($this));
        exit;
    }

    public function displayAsXml() {
        header('Content-Type: application/xml');
        header("HTTP/1.1 " . $this->status_code . " " . $this->status);

        //TODO: display xml

        exit;
    }

    public function setStatus_OK() {
        $this->setStatusCode(200);
    }

    public function setStatus_BAD_REQUEST() {
        $this->setStatusCode(400);
    }

    public function setStatus_UNAUTHORIZED() {
        $this->setStatusCode(401);
    }

    public function setStatus_NOT_FOUND() {
        $this->setStatusCode(404);
    }

    public function setStatus_METHOD_NOT_FOUND() {
        $this->setStatusCode(405);
    }

    public function setStatus_INTERNAL_SERVER_ERROR() {
        $this->setStatusCode(500);
    }

    private function setStatusCode($code) {
        $this->status_code = $code;
        $this->status = $this->statusMessages[$code];
    }
}