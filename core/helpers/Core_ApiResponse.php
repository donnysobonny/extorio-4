<?php
class Core_ApiResponse {
    public $error = false;
    public $error_message = "";
    public $error_code = 0;
    public $status_code = 200;
    public $status_message = "OK";
    public $data = null;

    public function setError($message,$code=0) {
        $this->error = true;
        $this->error_code = $code;
        $this->error_message = $message;
    }

    public function setStatusCode($statusCode=200) {
        $this->status_code = $statusCode;
        $const = "_".$statusCode;
        $this->status_message = constant("Core_HTTPStatusCodes::$const");
    }

    public function display($encodeType="json") {
        header("HTTP/1.1 " . $this->status_code . " " . $this->status_message);
        switch($encodeType) {
            case "json" :
                $this->displayAsJson();
                break;
            case "xml" :
                $this->displayAsXml();
                break;
            default :
                $this->displayAsJson();
                break;
        }
        exit;
    }

    private function displayAsJson() {
        print(json_encode($this));
    }

    private function displayAsXml() {
        echo "currently not supported";
    }
}