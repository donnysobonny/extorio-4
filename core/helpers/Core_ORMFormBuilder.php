<?php
final class Core_ORMFormBuilder {
    private $instanceId;

    /**
     * The class the builder is working on
     *
     * @var Core_Class
     */
    private $classToBuild = false;

    /**
     * The properties of the class we are building
     *
     * @var Core_Property[]
     */
    private $classToBuildProperties = array();

    /**
     * The model that we are editing (if applicable)
     *
     * @var Core_ORM_ComplexModel
     */
    private $objectToEdit = false;

    private $editing = false;

    private $tableId;

    private $part_of_array;

    private $removable;

    public $outputHtml = "";

    function __construct($baseClass,$instanceId=false,$propertyPathArray=array(),$part_of_array=false,$removable=false) {

        $this->classToBuild = Core_Class::findByName($baseClass);
        if(!$this->classToBuild) {
            throw new Core_ORM_Exception("Class ".$baseClass." not found");
        }

        $this->part_of_array = $part_of_array;

        $this->instanceId = $instanceId;
        $this->currentPropertyPathArray = $propertyPathArray;

        $this->removable = $removable;

        if($this->part_of_array) {
            $this->currentPropertyPathArray[] = uniqid();
        }

        if($this->instanceId) {
            $class = $this->classToBuild->name;
            $this->objectToEdit = new $class();
            $this->objectToEdit->id = $this->instanceId;
            $this->objectToEdit->pullThis();
            $this->editing = true;
        } else {
            $class = $this->classToBuild->name;
            $this->objectToEdit = new $class();
        }

        if($this->objectToEdit) {
            $this->classToBuildProperties = Core_Property::findAll(Core_ORM_Finder::newInstance()
                ->where("
                base.ownerClassId = ".$this->classToBuild->id."
                ")
                ->orderBy("base.position")
                ->orderDir("asc")
            );
        } else {
            $this->classToBuildProperties = Core_Property::findAll(Core_ORM_Finder::newInstance()
                ->where("
                base.ownerClassId = ".$this->classToBuild->id."
                ")
                ->orderBy("base.position")
                ->orderDir("asc")
            );
        }
    }

    public function build_form() {
        if($this->objectToEdit->canView_Edit() && $this->objectToEdit->canRetrieve()) {
            $this->startTable();
            $this->build_inputs();
            $this->endTable();

            return $this->outputHtml;
        }

        return "";
    }

    private function build_inputs() {

        //add the hidden fields if editing
        //$this->appendHiddenIdFields();

        foreach($this->classToBuildProperties as $property) {
            switch($property->inputType) {
                case Core_Input_Types::_checkbox :
                    $this->appendCheckboxField($property);
                    break;
                case Core_Input_Types::_html :
                    $this->appendHtmlField($property);
                    break;
                case Core_Input_Types::_textarea :
                    $this->appendTextAreaField($property);
                    break;
                case Core_Input_Types::_textfield :
                    $this->appendTextFieldField($property);
                    break;
                case Core_Input_Types::_numberfield :
                    $this->appendNumberField($property);
                    break;
                case Core_Input_Types::_decimalfield :
                    $this->appendDecimalField($property);
                    break;
                case Core_Input_Types::_date :
                    $this->appendDateField($property);
                    break;
                case Core_Input_Types::_datetime :
                    $this->appendDateTimeField($property);
                    break;
                case Core_Input_Types::_time :
                    $this->appendTimeField($property);
                    break;
                case Core_Input_Types::_dropdown :
                    $this->appendDropdownField($property);
                    break;
                case Core_Input_Types::_model :
                    $this->appendModelField($property);
                    break;
                case Core_Input_Types::_model_array :
                    $this->appendModelArrayField($property);
                    break;
            }
        }
    }

    private function startTable() {
        $this->tableId = uniqid();
        $this->outputHtml .= '<table id="'.$this->tableId.'" class="table model_edit_table"><tbody>';
    }

    private function endTable() {
        $this->outputHtml .= '
    </tbody>
</table>';

        if($this->removable) {
            $this->outputHtml .= '<div class="edit_form_buttons">
    <button class="btn btn-danger btn-xs"
    onclick="$(\'#'.$this->tableId.'\').html(\'<input type=&quot;hidden&quot; value=&quot;&quot; name=&quot;'.$this->getInputName($this->currentPropertyPathArray,false).'&quot; />\').hide();$(this).remove();">
        <span class="glyphicon glyphicon-remove"></span> clear
    </button>
</div>';
        }
    }

    private function appendHiddenIdFields() {
        if($this->editing) {
            foreach($this->classToBuildProperties as $property) {
                if($property->inputType != Core_Input_Types::_model && $property->inputType !=
                    Core_Input_Types::_model_array) {
                    $propertyName = $property->name;
                    if(!$property->visibleInEdit) {
                        $this->outputHtml .= '<input type="hidden" name="'.$this->getInputName($this->currentPropertyPathArray,$propertyName
                            ).'" value="'.$this->objectToEdit->$propertyName.'" />';
                    }
                }
            }
        }
    }

    /**
     * Append a checkbox field
     *
     * @param Core_Property $property
     */
    private function appendCheckboxField($property) {

        $rowStyle = "";

        if($this->editing) {
            if(!$property->visibleInEdit) {
                $rowStyle = "display: none;";
            }
        } else {
            if(!$property->visibleInCreate) {
                $rowStyle = "display: none;";
            }
        }

        $uid = uniqid();
        $propertyName = $property->name;
        $value = $this->editing?$this->objectToEdit->$propertyName:$property->checkBoxDefault;
        $checked = $value==1?'checked="checked"':'';
        $this->outputHtml .= '
        <tr style="'.$rowStyle.'">
            <td style="font-weight: bold; text-align: right;width:0px;padding:2px;">
                '.$property->name.'
            </td>
            <td>
                <input type="checkbox" id="'.$uid.'"
                 class="input_checkbox" '.$checked.' />
                <input type="hidden" name="'.$this->getInputName($this->currentPropertyPathArray,$propertyName).'"
                id="'.$uid.'_hidden" value="'.$value.'"/>
            </td>
        </tr>
        ';
    }

    private function appendHtmlField($property) {

        $rowStyle = "";

        if($this->editing) {
            if(!$property->isEditable) {
                $rowStyle = "display: none;";
            }
        } else {
            if(!$property->isCreatable) {
                $rowStyle = "display: none;";
            }
        }

        $propertyName = $property->name;
        $value = $this->editing?$this->objectToEdit->$propertyName:"";
        $this->outputHtml .= '
        <tr style="'.$rowStyle.'">
            <td style="font-weight: bold; text-align: right;width:0px;padding:2px;">
                '.$property->name.'
            </td>
            <td>
                <textarea name="'.$this->getInputName($this->currentPropertyPathArray,
                $propertyName).'">'.$value.'</textarea>
            </td>
        </tr>
        ';
    }

    private function appendTextAreaField($property) {

        $rowStyle = "";

        if($this->editing) {
            if(!$property->isEditable) {
                $rowStyle = "display: none;";
            }
        } else {
            if(!$property->isCreatable) {
                $rowStyle = "display: none;";
            }
        }

        $propertyName = $property->name;
        $value = $this->editing?$this->objectToEdit->$propertyName:"";
        $maxLength = $property->maxLength>0?$property->maxLength:"";
        $this->outputHtml .= '
        <tr style="'.$rowStyle.'">
            <td style="font-weight: bold; text-align: right;width:0px;padding:2px;">
                '.$property->name.'
            </td>
            <td>
                <textarea maxlength="'.$maxLength.'" name="'.$this->getInputName($this->currentPropertyPathArray,
                $propertyName).'">'.$value.'</textarea>
            </td>
        </tr>
        ';
    }

    /**
     * Append a textfield field
     *
     * @param Core_Property $property
     */
    private function appendTextFieldField($property) {

        $rowStyle = "";

        if($this->editing) {
            if(!$property->visibleInEdit) {
                $rowStyle = "display: none;";
            }
        } else {
            if(!$property->visibleInCreate) {
                $rowStyle = "display: none;";
            }
        }

        $maxLength = $property->maxLength>0?$property->maxLength:"";
        $propertyName = $property->name;
        $value = $this->editing?$this->objectToEdit->$propertyName:"";
        $this->outputHtml .= '
        <tr style="'.$rowStyle.'">
            <td style="font-weight: bold; text-align: right; width:0px; padding:2px;">
                '.$property->name.'
            </td>
            <td>
                <input size="'.$maxLength.'" maxlength="'.$maxLength.'" type="text" name="'.$this->getInputName
                ($this->currentPropertyPathArray,
                    $propertyName).'"
                value="'.$value.'" />
            </td>
        </tr>
        ';
    }

    private function appendNumberField($property) {

        $rowStyle = "";

        if($this->editing) {
            if(!$property->isEditable) {
                $rowStyle = "display: none;";
            }
        } else {
            if(!$property->isCreatable) {
                $rowStyle = "display: none;";
            }
        }

        $maxLength = $property->maxLength>0?$property->maxLength:"";
        $propertyName = $property->name;
        $value = $this->editing?$this->objectToEdit->$propertyName:"";
        $this->outputHtml .= '
        <tr style="'.$rowStyle.'">
            <td style="font-weight: bold; text-align: right;width:0px;padding:2px;">
                '.$property->name.'
            </td>
            <td>
                <input class="input_number" size="'.$maxLength.'"
                 maxlength="'.$maxLength.'" type="number" name="'.$this->getInputName
                ($this->currentPropertyPathArray,
                    $propertyName).'"
                value="'.$value.'" />
            </td>
        </tr>
        ';
    }

    private function appendDecimalField($property) {

        $rowStyle = "";

        if($this->editing) {
            if(!$property->isEditable) {
                $rowStyle = "display: none;";
            }
        } else {
            if(!$property->isCreatable) {
                $rowStyle = "display: none;";
            }
        }

        $propertyName = $property->name;
        $value = $this->editing?$this->objectToEdit->$propertyName:"";
        $this->outputHtml .= '
        <tr style="'.$rowStyle.'">
            <td style="font-weight: bold; text-align: right;width:0px;padding:2px;">
                '.$property->name.'
            </td>
            <td>
                <input step="any" class="input_decimal" size="4"
                 type="number" name="'.$this->getInputName
                ($this->currentPropertyPathArray,
                    $propertyName).'"
                value="'.$value.'" />
            </td>
        </tr>
        ';
    }

    private function appendDateField($property) {

        $rowStyle = "";

        if($this->editing) {
            if(!$property->isEditable) {
                $rowStyle = "display: none;";
            }
        } else {
            if(!$property->isCreatable) {
                $rowStyle = "display: none;";
            }
        }

        $propertyName = $property->name;
        $value = $this->editing?$this->objectToEdit->$propertyName:"";
        $this->outputHtml .= '
        <tr style="'.$rowStyle.'">
            <td style="font-weight: bold; text-align: right;width:0px;padding:2px;">
                '.$property->name.'
            </td>
            <td>
                <input class="input_date" type="text" name="'.$this->getInputName
                ($this->currentPropertyPathArray,
                    $propertyName).'"
                value="'.$value.'" />
            </td>
        </tr>
        ';
    }

    private function appendDateTimeField($property) {

        $rowStyle = "";

        if($this->editing) {
            if(!$property->isEditable) {
                $rowStyle = "display: none;";
            }
        } else {
            if(!$property->isCreatable) {
                $rowStyle = "display: none;";
            }
        }

        $propertyName = $property->name;
        $value = $this->editing?$this->objectToEdit->$propertyName:"";
        $this->outputHtml .= '
        <tr style="'.$rowStyle.'">
            <td style="font-weight: bold; text-align: right;width:0px;padding:2px;">
                '.$property->name.'
            </td>
            <td>
                <input class="input_datetime" type="text" name="'.$this->getInputName
                ($this->currentPropertyPathArray,
                    $propertyName).'"
                value="'.$value.'" />
            </td>
        </tr>
        ';
    }

    private function appendTimeField($property) {

        $rowStyle = "";

        if($this->editing) {
            if(!$property->isEditable) {
                $rowStyle = "display: none;";
            }
        } else {
            if(!$property->isCreatable) {
                $rowStyle = "display: none;";
            }
        }
        $propertyName = $property->name;
        $value = $this->editing?$this->objectToEdit->$propertyName:"";
        $this->outputHtml .= '
        <tr style="'.$rowStyle.'">
            <td style="font-weight: bold; text-align: right;width:0px;padding:2px;">
                '.$property->name.'
            </td>
            <td>
                <input class="input_time" type="text" name="'.$this->getInputName
                ($this->currentPropertyPathArray,
                    $propertyName).'"
                value="'.$value.'" />
            </td>
        </tr>
        ';
    }

    /**
     * Append a dropdown field
     *
     * @param Core_Property $property
     */
    private function appendDropdownField($property) {

        $rowStyle = "";

        if($this->editing) {
            if(!$property->visibleInEdit) {
                $rowStyle = "display: none;";
            }
        } else {
            if(!$property->visibleInCreate) {
                $rowStyle = "display: none;";
            }
        }

        $elements = Core_DropdownElement::findByOwnerId($property->dropDownId);
        $propertyName = $property->name;
        $value = $this->editing?$this->objectToEdit->$propertyName:"";
        $this->outputHtml .= '
        <tr style="'.$rowStyle.'">
            <td style="font-weight: bold; text-align: right;width:0px;padding:2px;">
                '.$property->name.'
            </td>
            <td>
                <select name="'.$this->getInputName($this->currentPropertyPathArray,$propertyName).'">
            ';

        foreach($elements as $element) {
            $selected ="";
            $selected = $value==$element->value?'selected="selected"':'';
            $this->outputHtml .= '<option '.$selected.' value="'.$element->value.'">'.$element->name.'</option>';
        }

        $this->outputHtml .= '
                </select>
            </td>
        </tr>
        ';
    }


    /**
     * SHORT_DESC
     *
     * LONG_DESC
     *
     * @param Core_Property $property
     */
    private function appendModelField($property) {

        $rowStyle = "";

        if($this->editing) {
            if(!$property->visibleInEdit) {
                $rowStyle = "display: none;";
            }
        } else {
            if(!$property->visibleInCreate) {
                $rowStyle = "display: none;";
            }
        }

        $propertyName = $property->name;

        $childClass = Core_Class::findById($property->childClassId);

        $this->outputHtml .= '
        <tr style="'.$rowStyle.'">
            <td style="font-weight: bold; text-align: right;width:0px;padding:2px;">
                '.$property->name.'
            </td>
            <td>';

        $uid = uniqid();

        $childPropertyPath = $this->currentPropertyPathArray;
        $childPropertyPath[] = $propertyName;

        $arrayStr = "";
        foreach($childPropertyPath as $element) {
            $arrayStr .= "'".$element."',";
        }
        $arrayStr = substr($arrayStr,0,strlen($arrayStr)-1);

        $this->outputHtml .= '<div class="edit_form_buttons">';

        //if the child object is persistent, you can only select it from here
        if($childClass->isPersistent) {
            $propertyPathQuery = "";
            foreach($childPropertyPath as $property) {
                $propertyPathQuery .= "&propertypath[]=".$property;
            }
            $this->outputHtml .= '<button onclick="
            showFrame(\''.$childClass->name.'\',\'/model-viewer-28-only/?model='.$childClass->name.'&action=list&selectable=true&targetcontainer='.$uid.$propertyPathQuery.'&append=false\',\'Cancel\');
            " type="button" class="btn
            btn-primary
            btn-xs"><span
             class="glyphicon glyphicon-search">
            </span> select...</button>';
        } else {
            //otherwise, you can only create
            $this->outputHtml .= '<button type="button" class="btn btn-primary btn-xs"
            onclick="getForm(\''.$childClass->name.'\',\'#'.$uid.'\',[';

            $this->outputHtml .= $arrayStr;

            $this->outputHtml .= '],false,false,true)"
            ><span class="glyphicon glyphicon-plus"></span> create...</button>';
        }

        $this->outputHtml .= '</div>';

        $this->outputHtml .= '<div id="'.$uid.'">';

        //if the object has a child object in this property, build an edited form,
        //otherwise just leave the select/create buttons
        if(!is_null($this->objectToEdit->$propertyName)) {
            $childBuilder = new Core_ORMFormBuilder($childClass->name,$this->objectToEdit->$propertyName->id,
                $childPropertyPath,false,true);
            $this->outputHtml .= $childBuilder->build_form();
        }

        $this->outputHtml .= "</div>";

        $this->outputHtml .= '</td>
        </tr>
        ';

    }

    /**
     * SHORT_DESC
     *
     * LONG_DESC
     *
     * @param $property
     */
    private function appendModelArrayField($property) {

        $rowStyle = "";

        if($this->editing) {
            if(!$property->isEditable) {
                $rowStyle = "display: none;";
            }
        } else {
            if(!$property->isCreatable) {
                $rowStyle = "display: none;";
            }
        }

        $propertyName = $property->name;

        $childClass = Core_Class::findById($property->childClassId);

        $this->outputHtml .= '
        <tr style="'.$rowStyle.'">
            <td style="font-weight: bold; text-align: right; width:0px; padding:2px;">
                '.$property->name.'
            </td>
            <td>';

        $uid = uniqid();

        $childPropertyPath = $this->currentPropertyPathArray;
        $childPropertyPath[] = $propertyName;

        $arrayStr = "";
        foreach($childPropertyPath as $element) {
            $arrayStr .= "'".$element."',";
        }
        $arrayStr = substr($arrayStr,0,strlen($arrayStr)-1);

        $this->outputHtml .= '<div class="edit_form_buttons">';

        if($childClass->isPersistent) {
            //if the child class is persistent, you can only select from here
            $propertyPathQuery = "";
            foreach($childPropertyPath as $property) {
                $propertyPathQuery .= "&propertypath[]=".$property;
            }
            $this->outputHtml .= '<button onclick="
            showFrame(\''.$childClass->name.'\',\'/model-viewer-28-only/?model='.$childClass->name.'&action=list&selectable=true&targetcontainer='.$uid.$propertyPathQuery.'&append=true\',\'Cancel\');
            " type="button" class="btn
            btn-primary
            btn-xs"><span
             class="glyphicon glyphicon-search">
            </span> select...</button>';
        } else {
            //if the child class is not persistent, you can only create from here
            $this->outputHtml .= '<button type="button" class="btn btn-primary btn-xs"
            onclick="getForm(\''.$childClass->name.'\',\'#'.$uid.'\',[';

            $this->outputHtml .= $arrayStr;

            $this->outputHtml .= '],false,true,true)"
            ><span class="glyphicon glyphicon-plus"></span> create...</button>';
        }

        $this->outputHtml .= '</div>';

        $this->outputHtml .= '<div id="'.$uid.'">';

        //if there are child objects in the array, generate editable forms for them, otherwise just leave the
        //select/create buttons
        $childObjects = $this->objectToEdit->$propertyName;
        if(count($childObjects)) {
            foreach($childObjects as $child) {
                $childBuilder = new Core_ORMFormBuilder($childClass->name,$child->id,
                    $childPropertyPath,true,true);
                $this->outputHtml .= $childBuilder->build_form();
            }
        }

        $this->outputHtml .= "</div>";

        $this->outputHtml .= '</td>
        </tr>
        ';
    }

    private function getInputName($propertyPathArray,$propertyName=false) {
        $name = "";
        $name .= "base";
        foreach($propertyPathArray as $prop) {
            $name .= "[".$prop."]";
        }
        if($propertyName) {
            $name .= "[".$propertyName."]";
        }

        return $name;
    }
}