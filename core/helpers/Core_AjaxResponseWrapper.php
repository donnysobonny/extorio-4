<?php
class Core_AjaxResponseWrapper {
    public $error = false;
    public $message = "";
    public $data = null;

    public function displayAsJson() {
        print(json_encode($this));
    }
}