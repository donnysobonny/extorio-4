<?php
final class Core_UrlBuilder {

    private $base;
    private $queryString;
    private $queryArray;

    public function __construct() {
        $page = Core_Extorio::get()->getTargetPage();
        if($page) {
            $this->base = $page->requestAddress;
        }
        $this->queryString = Core_Utils_Server::getQueryString();
        parse_str($this->queryString,$this->queryArray);
    }

    public static function currentBase($params=array()) {
        $builder = new Core_UrlBuilder();
        foreach($params as $key => $value) {
            $builder->queryArray[$key] = $value;
        }
        return $builder;
    }

    public static function newBase($base,$params=array()) {
        $builder = new Core_UrlBuilder();
        $builder->base = $base;
        $builder->base = Core_Utils_String::startsAndEndsWith($builder->base,"/");
        foreach($params as $key => $value) {
            $builder->queryArray[$key] = $value;
        }
        return $builder;
    }

    public static function addToBase($add,$params=array()) {
        $builder = new Core_UrlBuilder();
        $builder->base = Core_Utils_String::startsAndEndsWith($builder->base,"/");
        $add = Core_Utils_String::notStartsAndNotEndsWith($add,"/");
        $builder->base .= $add;
        foreach($params as $key => $value) {
            $builder->queryArray[$key] = $value;
        }
        return $builder;
    }

    private function display() {
        $this->queryString = http_build_query($this->queryArray);
        $url = $this->base;
        if(strlen($this->queryString)) {
            $url .= "?".$this->queryString;
        }
        echo $url;
    }

    function __toString() {
        $this->queryString = http_build_query($this->queryArray);
        $url = $this->base;
        if(strlen($this->queryString)) {
            $url .= "?".$this->queryString;
        }
        return $url;
    }


}