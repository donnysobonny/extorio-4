<?php
final class Core_Swiftmailer {

    /**
     * Create a swiftmailer message instance and return it.
     *
     * This creates a message instance and uses the defaults set in the config. You can further configure this object with
     * its setter methods.
     *
     * @param array $toArray
     * @param array $ccArray
     * @param array $bccArray
     * @param string $subject
     * @param string $body
     * @param bool $bodyIsHtml
     *
     * @return Swift_Message
     */
    public static function createMessageInstance(
        $toArray = array(),
        $ccArray = array(),
        $bccArray = array(),
        $subject = "",
        $body = "",
        $bodyIsHtml = false) {
        //load swiftmail
        Core_LibLoader::loadSwiftMailer();

        //create a message instance
        $message = Swift_Message::newInstance();
        //add in the optionals
        if(!empty($toArray)) {
            $message->setTo($toArray);
        }
        if(!empty($ccArray)) {
            $message->setCc($ccArray);
        }
        if(!empty($bccArray)) {
            $message->setBcc($bccArray);
        }
        if(strlen($subject)) {
            $message->setSubject($subject);
        }
        if(strlen($body)) {
            if($bodyIsHtml) {
                $message->setBody($body,"text/html");
            }
        }

        $config = Core_Extorio::get()->getStoredConfig();

        if(strlen($config["emails"]["defaults"]["from_email"])) {
            if(strlen($config["emails"]["defaults"]["from_name"])) {
                $message->setFrom(array($config["emails"]["defaults"]["from_email"] => $config["emails"]["defaults"]["from_name"]));
            } else {
                $message->setFrom($config["emails"]["defaults"]["from_email"]);
            }
        }

        return $message;
    }

    /**
     * Create a swiftmailer mailer instance. Use this in conjunction with a message instance to send an email.
     *
     * This create a mailer instance, using the defaults set in configs.
     *
     * @param null $host
     * @param null $port
     * @param null $encryption
     * @param null $username
     * @param null $password
     *
     * @return Swift_Mailer
     */
    public static function createMailerInstance($host = null, $port = null, $encryption = null, $username = null, $password = null) {
        $config = Core_Extorio::get()->getStoredConfig();
        $transport = false;
        switch($config["emails"]["defaults"]["transport"]) {
            case "smtp" :
                $transport = Swift_SmtpTransport::newInstance();
                if(!is_null($host)) {
                    $transport->setHost($host);
                } else {
                    $transport->setHost($config["emails"]["smtp"]["host"]);
                }
                if(!is_null($port)) {
                    $transport->setPort($port);
                } else {
                    $transport->setPort($config["emails"]["smtp"]["port"]);
                }
                if(!is_null($encryption)) {
                    $transport->setEncryption($encryption);
                } else {
                    $transport->setEncryption($config["emails"]["smtp"]["encryption"]);
                }
                if(!is_null($username)) {
                    $transport->setUsername($username);
                } else {
                    $transport->setUsername($config["emails"]["smtp"]["username"]);
                }
                if(!is_null($password)) {
                    $transport->setPassword($password);
                } else {
                    $transport->setPassword($config["emails"]["smtp"]["password"]);
                }
                break;
        }

        if($transport) {
            return Swift_Mailer::newInstance($transport);
        } else {
            return false;
        }
    }
}