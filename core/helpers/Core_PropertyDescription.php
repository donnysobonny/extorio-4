<?php
final class Core_PropertyDescription {
    public $name;
    public $inputType;
    public $PHPType;
    public $mysqlType;
    public $maxLength;
    public $classType;
    public $dropdownType;
    public $dropdownValues;
    public $isPrimary;
    public $isIndex;
    public $isUnique;

    /**
     * Construct the description from a property object
     *
     * @param Core_Property $property
     *
     * @return Core_PropertyDescription
     */
    public static function constructFromProperty($property) {
        $description = new Core_PropertyDescription();
        $description->name = $property->name;
        $description->inputType = $property->inputType;
        $description->PHPType = $property->phpType;
        $description->mysqlType = $property->mysqlType;
        $description->maxLength = $property->maxLength>0?$property->maxLength:"";

        $description->isPrimary = $property->isPrimary?true:false;
        $description->isIndex = $property->isIndex?true:false;
        $description->isUnique = $property->isUnique?true:false;

        //if model/model_array, set the class type
        if(
            $description->inputType == Core_Input_Types::_model ||
            $description->inputType == Core_Input_Types::_model_array
        ) {
            $childClass = Core_Class::findById($property->childClassId);
            $description->classType = $childClass->name;
        }

        //if the input type is dropdown, add the dropdown type and values
        if($description->inputType == Core_Input_Types::_dropdown) {
            $dropdown = Core_Dropdown::findById($property->dropDownId);
            $description->dropdownType = $dropdown->name;
            $elements = Core_DropdownElement::findByOwnerId($dropdown->id);
            foreach($elements as $element) {
                $description->dropdownValues[$element->value] = $element->name;
            }
        }

        return $description;
    }
}