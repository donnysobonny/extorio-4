<?php
$config = array(
    "version" => 4.0,
    "installed" => false,
    "data_imported" => false,
    "templates" => array(
        "default_name" => "CoreExtorioAdminTemplate",
        "default_extension_name" => "core"
    ),
    "db_defaults" => array(
        "connection_name" => "default",
        "database" => "",
        "username" => "",
        "password" => "",
        "host" => "localhost",
        "engine" => "MyISAM",
        "table_charset" => "latin1",
        "table_collate" => "latin1_general_ci"
    ),
    "pages" => array(
        "page-not-found" => 1,
        "access-denied" => 2
    ),
    "site" => array(
        "address" => "",
        "name" => ""
    ),
    "tasks" => array(
        "max_running_tasks" => 1,
        "max_run_time" => 0,
        "create_task_logs" => false
    ),
    "webhooks" => array(
        "default_verification_key" => "extorio_webhook"
    ),
    "emails" => array(
        "smtp" => array(
            "host" => "",
            "port" => 25,
            "encryption" => "tls",
            "username" => "",
            "password" => ""
        ),
        "defaults" => array(
            "transport" => "smtp",
            "from_email" => "",
            "from_name" => ""
        )
    )
);