<?php
final class CoreExtorioInstallView extends CoreExtorioInstallController {
    public function onLoad() {
        //die if already installed
        $config = $this->Extorio()->getConfig();
        if($config["installed"]) {
            throw new Core_Extorio_Exception("Trying to access the extorio installation process when it is already installed");
        }
    }

    public function onDefault() {
        if($this->action) {
            $action = $this->action;
            $this->$action();
        } else {
            ?>
            <p>
                Welcome to the Extorio installation. We must first run you through some installation steps to get you
                started. When you are ready, click the "Next" button below.
            </p>
            <p>
                <a style="float: right;" class="btn btn-primary" href="/?action=database"><span class="glyphicon
                glyphicon-share-alt"></span>
                    Next</a>
            </p>
            <?php
        }
    }

    public function onComplete() {

    }

    public function database() {
        ?>
<h4>Database setup</h4>
        <div class="alert alert-info" role="alert">Extorio requires a connection to a database in order to
            function properly. Please use the form below to specify the connection details for your database.
            Extorio will do the rest.</div>

        <p>
            Connection status: <?php
            if($this->dbFailed) {
                ?>
                <span style="color: red;">
                    <strong>
                        <?=$this->dbFailed?>
                    </strong>
                </span>
                <?php
            } else {
                ?>
                <span style="color: green">
                    <strong>
                        Connection successful
                    </strong>
                </span>
                <?php
            }
            ?>
        </p>

        <form name="database_setup" class="form-horizontal" role="form" method="post" action="">
            <div class="form-group">
                <label for="host" class="col-sm-2 control-label">Host</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="host" name="host" placeholder="Host"
                        value="<?=$this->dbHost?>">
                </div>
            </div>
            <div class="form-group">
                <label for="username" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username"
                           value="<?=$this->dbUser?>">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="password" name="password" placeholder="Password"
                           value="<?=$this->dbPass?>">
                </div>
            </div>
            <div class="form-group">
                <label for="database" class="col-sm-2 control-label">Database</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="database" name="database" placeholder="Database"
                           value="<?=$this->dbDatabase?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="database_setup_submitted" class="btn btn-default">Save and test
                        connection</button>
                </div>
            </div>
        </form>
        <p>
            <a style="float: right;" class="btn btn-primary <?php
            if($this->dbFailed) echo 'disabled'
            ?>" href="/?action=dataimport"
                <?php
                if($this->dbFailed) echo 'disabled="disabled"'
                ?>><span class="glyphicon
                glyphicon-share-alt"></span>
                Next</a>
        </p>
        <?php
    }

    public function dataimport() {
        ?>
        <h4>Database import</h4>
        <div class="alert alert-info" role="alert">
            Extorio comes with it's own det of default data. This data must be imported before we can continue. To import the data, click the button below.
        </div>
        <a class="btn btn-success" href="/?action=dataimport&import=true"><span class="glyphicon glyphicon-download"></span> Import data</a>

        <p>
            <a style="float: right;" class="btn btn-primary <?php
            if(!$this->dataImported) echo 'disabled'
            ?>" href="/?action=sitesettings"
                <?php
                if(!$this->dataImported) echo 'disabled="disabled"'
                ?>><span class="glyphicon
                glyphicon-share-alt"></span>
                Next</a>
        </p>
        <?php
    }

    public function sitesettings() {
        ?>
        <h4>Site settings</h4>
        <div class="alert alert-info" role="alert">
            Configure your site settings below
        </div>

        <form name="site_setup" class="form-horizontal" role="form" method="post" action="">
            <div class="form-group">
                <label for="address" class="col-sm-2 control-label">Domain</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Site domain"
                           value="<?=$this->siteAddress?>">
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Site name"
                           value="<?=$this->siteName?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="site_setup_submitted" class="btn btn-default">Save</button>
                </div>
            </div>
        </form>

        <p>
            <a style="float: right;" class="btn btn-primary " href="/?action=adminsetup">
                <span class="glyphicon glyphicon-share-alt"></span> Next
            </a>
        </p>
        <?php
    }

    public function adminsetup() {
        ?>
        <h4>Admin setup</h4>
        <div class="alert alert-info" role="alert">
            Before you can use Extorio's admin interfaces, you will need to create an admin account.
        </div>

        <form name="admin_setup" class="form-horizontal" role="form" method="post" action="">
            <div class="form-group">
                <label for="username" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username"
                           value="">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                           value="">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password"
                           value="<?=$this->siteName?>">
                </div>
            </div>
            <div class="form-group">
                <label for="cpassword" class="col-sm-2 control-label">Confirm password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm password"
                           value="<?=$this->siteName?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="admin_setup_submitted" class="btn btn-primary btn-lg">Save and complete installation!</button>
                </div>
            </div>
        </form>
        <?php
    }
}