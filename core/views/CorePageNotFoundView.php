<?php
final class CorePageNotFoundView extends CorePageNotFoundController {
    public function onLoad() {
        ?>
        <div class="jumbotron">
            <h1>Oops...this is embarrassing</h1>
            <p>The resource that you are looking for no longer exists, or the web address that you entered is
                invalid.</p>
            <p><a class="btn btn-primary btn-lg" role="button" href="/">Go to homepage</a></p>
        </div>
        <?php
    }

    public function onDefault() {

    }

    public function onComplete() {

    }
}