<?php
class CoreExtorioAdminLoginView extends CoreExtorioAdminLoginController {
    /**
     * This method is called immediately after the view is loaded by the framework.
     */
    public function onLoad() {
        ?>
        <form name="admin_login" method="post" action="" class="form-horizontal" role="form">
            <div class="form-group">
                <label for="username" class="col-sm-2 control-label">Username/Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username or email address" value="<?=$this->username?>">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="<?=$this->password?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="admin_login_submitted" class="btn btn-primary">Log in</button>
                </div>
            </div>
        </form>
        <?php
    }

    /**
     * This method is called if the view is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        ?>

        <?php
    }

    /**
     * This method is the very last method that is called on the view.
     */
    public function onComplete() {
        ?>

        <?php
    }
}