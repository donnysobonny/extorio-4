<?php

class CoreExtorioAdminModelsView extends CoreExtorioAdminModelsController {
    /**
     * This method is called immediately after the view is loaded by the framework.
     */
    public function onLoad() {
        ?>

    <?php
    }

    /**
     * This method is called if the view is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        ?>
        <script src="/core/assets/js/admin/Core_Admin_Models.js"></script>

        <ol class="breadcrumb">
            <li class="active">Models</li>
        </ol>

        <form class="form-inline" name="create_model" method="post" action="" role="form">
            <div class="form-group">
                <div class="input-group">
                    <label class="sr-only" for="name">Model name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="New model name">
                </div>
            </div>
            <div class="form-group">
                <label class="sr-only" for="extension">Extension</label>
                <select name="extension" id="extension" class="form-control">
                    <?php
                    foreach($this->extensions as $extension) {
                        ?>
                    <option value="<?=$extension?>"><?=$extension?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label class="sr-only" for="type">Type</label>
                <select name="type" id="type" class="form-control">
                    <option value="basic">basic</option>
                    <option value="user">user</option>
                    <option value="address">address</option>
                </select>
            </div>
            <button type="submit" name="create_model_submitted" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Create new model</button>
        </form>

        <table class="table table-striped" id="class_table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Persistent</th>
                <th>Fields</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($this->allClasses as $class) {
                ?>
                <tr id="class_row_<?= $class->id ?>">
                    <td><?= $class->name ?></td>
                    <td><?= $class->type ?></td>
                    <td>
                        <?php
                        if($class->isHidden) {
                            ?>
                            <input type="checkbox" <?php
                            if ($class->isPersistent)
                                echo 'checked="checked"';
                            ?> disabled="disabled" />
                        <?php
                        } else {
                            ?>
                            <input class="persistent" type="checkbox" classid="<?= $class->id ?>"
                                   id="persistent_<?= $class->id ?>" <?php
                            if ($class->isPersistent)
                                echo 'checked="checked"';
                            ?> />
                        <?php
                        }
                        ?>

                    </td>
                    <td>
                        <?php
                        if(!$class->isHidden) {
                            ?>
                            <a href="/extorio/admin/models/fields/<?=$class->id?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon glyphicon-pencil"></span> manage fields...</a>
                            <?php
                        }
                        ?>
                        <button class="btn btn-primary btn-xs describe" classname="<?= $class->name ?>"><span class="glyphicon glyphicon glyphicon-info-sign"></span> describe</button>
                        <button class="btn btn-primary btn-xs structure" classname="<?= $class->name ?>"><span class="glyphicon glyphicon glyphicon-info-sign"></span> structure</button>
                    </td>
                    <td>
                        <?php
                        if(!$class->isHidden) {
                            ?>
                            <button class="btn btn-primary btn-xs"
                                    onclick="Core_Extorio.Frame.openFrame('large','Managing model access for: <?=$class->name?>','/extorio/admin/access/model/<?=$class->name?>',function(){
                                        Core_Extorio.Spinner.showFullPageSpinner_black();
                                        location.reload();
                                        })"><span class="glyphicon glyphicon-check"></span> access
                            </button>
                            <button classid="<?= $class->id ?>" class="btn btn-danger btn-xs delete_model"><span class="glyphicon glyphicon-trash"></span> delete</button>
                        <?php
                        } else {
                            ?>
                            <a class="btn btn-primary btn-xs disabled" href=""><span class="glyphicon glyphicon-pencil"></span>
                                access</a>
                            <button disabled="disabled" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span> delete</button>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
        <script>
            $(function () {

                $('#class_table').DataTable({
                    "paging": false,
                    "ordering": false
                });

                $('.delete_model').bind("click",function() {

                    var classId = $(this).attr("classid");

                    Core_Extorio.Modal.openModal("small","Delete model?","By deleting this model, you will also be removing all objects and data associated to it. Are you sure you want to delete?","Cancel","Yes, delete",function() {
                        Core_Extorio.Spinner.showFullPageSpinner_black();

                        Core_ORM_BasicModel.remove("Core_Class",classId,function(data) {

                            //delete the row
                            $('#class_row_'+classId).remove();

                            Core_Extorio.Spinner.hideFullPageSpinner();
                        })
                    })
                });

                $('.persistent').bind("change", function () {
                    //when the persistent check is changed, update the core class
                    var check = $(this).prop("checked");
                    check = check ? 1 : 0;
                    var classId = $(this).attr("classid");

                    Core_Extorio.Spinner.showFullPageSpinner_black();

                    //update the basic model
                    Core_ORM_BasicModel.update("Core_Class", {
                        "id": classId,
                        "isPersistent": check
                    }, function(data) {
                        Core_Extorio.Spinner.hideFullPageSpinner();
                    });
                });

                $('.describe').bind("click",function() {

                    var className = $(this).attr("classname");

                    //get the description
                    Core_Extorio.Spinner.showFullPageSpinner_black();

                    Core_ORM_ComplexModel.describe_html(className,function(html) {
                        Core_Extorio.Spinner.hideFullPageSpinner();
                        Core_Extorio.Dialog.openDialog("large","Viewing description of: "+className,html,true,"Close");
                    });
                });

                $('.structure').bind("click",function() {
                    //get the structure
                    Core_Extorio.Spinner.showFullPageSpinner_black();

                    Core_ORM_ComplexModel.structure_html($(this).attr("classname"),function(html) {
                        Core_Extorio.Spinner.hideFullPageSpinner();
                        Core_Extorio.Dialog.openDialog("large","Viewing structure of: "+$(this).attr("classname"),html,true,"Close");
                    });
                })
            })
        </script>
    <?php
    }

    public function fields($modelId = false) {
        ?>
        <script src="/core/assets/js/admin/Core_Admin_Models.js"></script>

        <ol class="breadcrumb">
            <li><a href="/extorio/admin/models">Models</a></li>
            <li class="active"><?=$this->class->name?></li>
            <li><a href="/extorio/admin/models/createfield/<?=$modelId?>"><span class="glyphicon glyphicon-plus"></span> Create new field</a></li>
        </ol>

        <table id="field_table" class="table table-striped">
            <thead>
                <tr>
                    <th colspan="2"></th>
                    <th colspan="3">Indexing</th>
                    <th colspan="4">Visible in...</th>
                    <th colspan="2"></th>
                </tr>
                <tr>
                    <th>Name</th>
                    <th>Input type</th>
                    <th>Primary</th>
                    <th>Indexed</th>
                    <th>Unique</th>
                    <th>List view</th>
                    <th>Detail view</th>
                    <th>Edit view</th>
                    <th>Create view</th>
                    <th>Position</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($this->properties as $property) {
                    ?>
                <tr class="property_row" id="property_row_<?=$property->id?>" propertyid="<?=$property->id?>" propertyposition="<?=$property->position?>">
                    <td><?=$property->name?></td>
                    <td><?=$property->inputType?></td>
                    <td>
                        <input type="checkbox" disabled="disabled" <?php
                        if($property->isPrimary) {
                            echo 'checked="checked"';
                        }
                        ?>  />
                    </td>
                    <td>
                        <input type="checkbox" propertyid="<?=$property->id?>" class="property_is_index" <?php
                        if($property->isIndex) {
                            echo 'checked="checked"';
                        }
                        ?>  />
                    </td>
                    <td>
                        <input type="checkbox" propertyid="<?=$property->id?>" class="property_is_unique" <?php
                        if($property->isUnique) {
                            echo 'checked="checked"';
                        }
                        ?>  />
                    </td>
                    <td>
                        <input type="checkbox" propertyid="<?=$property->id?>" class="property_visible_list" <?php
                        if($property->visibleInList) {
                            echo 'checked="checked"';
                        }
                        ?>  />
                    </td>
                    <td>
                        <input type="checkbox" propertyid="<?=$property->id?>" class="property_visible_detail" <?php
                        if($property->visibleInDetail) {
                            echo 'checked="checked"';
                        }
                        ?>  />
                    </td>
                    <td>
                        <input type="checkbox" propertyid="<?=$property->id?>" class="property_visible_edit" <?php
                        if($property->visibleInEdit) {
                            echo 'checked="checked"';
                        }
                        ?>  />
                    </td>
                    <td>
                        <input type="checkbox" propertyid="<?=$property->id?>" class="property_visible_create" <?php
                        if($property->visibleInCreate) {
                            echo 'checked="checked"';
                        }
                        ?>  />
                    </td>
                    <td>
                        <button propertyid="<?=$property->id?>" class="btn btn-xs btn-default property_move_up"><span class="glyphicon glyphicon-arrow-up"></span></button>
                        <button propertyid="<?=$property->id?>" class="btn btn-xs btn-default property_move_down"><span class="glyphicon glyphicon-arrow-down"></span></button>
                    </td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="/extorio/admin/models/editfield/<?=$modelId?>/<?=$property->id?>"><span class="glyphicon glyphicon-pencil"></span> edit</a>
                        <button class="btn btn-xs btn-danger delete_property" propertyid="<?=$property->id?>"><span class="glyphicon glyphicon-trash"></span> delete</button>
                    </td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            function updatePositioning() {
                //re-get the rows
                var rows = [];
                var html = '';

                //get the current rows un-sorted
                $('#field_table .property_row').each(function() {
                    var propertyId = $(this).attr("propertyid");
                    var propertyposition = $(this).attr("propertyposition");

                    rows[propertyposition] = '<tr class="property_row" id="property_row_' + propertyId + '" propertyposition="' + propertyposition + '" propertyid="' + propertyId + '">' +
                    '           ' + $(this).html() +
                    '</tr>';
                });

                //add in the rows in their position order
                for(var i = 0; i < rows.length; i++) {
                    html += rows[i];
                }

                //add in the rows html
                $('#field_table tbody').html(html);

                //show/hide the positioning buttons
                var n = 0;
                $('#field_table .property_row').each(function() {

                    if(n != 0) {
                        //can move up
                        $(this).find('.property_move_up').show();
                    } else {
                        $(this).find('.property_move_up').hide();
                    }

                    if(n < (rows.length - 1)) {
                        //can move down
                        $(this).find('.property_move_down').show();
                    } else {
                        $(this).find('.property_move_down').hide();
                    }

                    n += 1;
                });

                bindings();
            }

            function bindings() {
                $('.delete_property').bind("click", function() {
                    var propertyid = $(this).attr("propertyid");

                    Core_Extorio.Spinner.showFullPageSpinner_black();
                    //delete the property
                    Core_ORM_BasicModel.remove("Core_Property",propertyid,function(data) {
                        location.reload();
                    })
                });

                $('.property_move_up').bind("click", function() {
                    var thisRow = $('#property_row_' + $(this).attr("propertyid"));
                    var propertyId = thisRow.attr("propertyid");
                    var oldPosition = parseInt(thisRow.attr("propertyposition"));
                    var newPosition = oldPosition - 1;

                    //update the basic model
                    Core_Extorio.Spinner.showFullPageSpinner_black();
                    Core_ORM_BasicModel.update("Core_Property",{
                        "id":propertyId,
                        "position":newPosition
                    },function() {
                        //update the position
                        thisRow.attr("propertyposition",newPosition);

                        //find the row that needs to move up
                        $('.property_row').each(function() {
                            if($(this).attr("propertyposition") == newPosition && $(this).attr("propertyid") != propertyId) {
                                thisRow = $('#property_row_' + $(this).attr("propertyid"));
                                propertyId = thisRow.attr("propertyid");
                                oldPosition = parseInt(thisRow.attr("propertyposition"));
                                newPosition = oldPosition + 1;

                                //update the basic model
                                Core_ORM_BasicModel.update("Core_Property",{
                                    "id":propertyId,
                                    "position":newPosition
                                },function() {
                                    thisRow.attr("propertyposition",newPosition);
                                    //update positioning
                                    updatePositioning();

                                    Core_Extorio.Spinner.hideFullPageSpinner();
                                })
                            }
                        })
                    })
                });

                $('.property_move_down').bind("click", function() {
                    var thisRow = $('#property_row_' + $(this).attr("propertyid"));
                    var propertyId = thisRow.attr("propertyid");
                    var oldPosition = parseInt(thisRow.attr("propertyposition"));
                    var newPosition = oldPosition + 1;

                    //update the basic model
                    Core_Extorio.Spinner.showFullPageSpinner_black();
                    Core_ORM_BasicModel.update("Core_Property",{
                        "id":propertyId,
                        "position":newPosition
                    },function() {
                        //update the position
                        thisRow.attr("propertyposition",newPosition);

                        //find the row that needs to move up
                        $('.property_row').each(function() {
                            if($(this).attr("propertyposition") == newPosition && $(this).attr("propertyid") != propertyId) {
                                thisRow = $('#property_row_' + $(this).attr("propertyid"));
                                propertyId = thisRow.attr("propertyid");
                                oldPosition = parseInt(thisRow.attr("propertyposition"));
                                newPosition = oldPosition - 1;

                                //update the basic model
                                Core_ORM_BasicModel.update("Core_Property",{
                                    "id":propertyId,
                                    "position":newPosition
                                },function() {
                                    thisRow.attr("propertyposition",newPosition);
                                    //update positioning
                                    updatePositioning();

                                    Core_Extorio.Spinner.hideFullPageSpinner();
                                })
                            }
                        })
                    })
                });

                $('.property_is_index').bind("change", function() {
                    var propertyId = $(this).attr("propertyid");
                    var check = $(this).prop("checked")?1:0;

                    Core_Extorio.Spinner.showFullPageSpinner_black();

                    //update the property
                    Core_ORM_BasicModel.update("Core_Property",{
                        "id":propertyId,
                        "isIndex":check
                    },function(data) {
                        Core_Extorio.Spinner.hideFullPageSpinner();
                    })
                });

                $('.property_is_unique').bind("change", function() {
                    var propertyId = $(this).attr("propertyid");
                    var check = $(this).prop("checked")?1:0;

                    Core_Extorio.Spinner.showFullPageSpinner_black();

                    //update the property
                    Core_ORM_BasicModel.update("Core_Property",{
                        "id":propertyId,
                        "isUnique":check
                    },function(data) {
                        Core_Extorio.Spinner.hideFullPageSpinner();
                    })
                });

                $('.property_visible_list').bind("change", function() {
                    var propertyId = $(this).attr("propertyid");
                    var check = $(this).prop("checked")?1:0;

                    Core_Extorio.Spinner.showFullPageSpinner_black();

                    //update the property
                    Core_ORM_BasicModel.update("Core_Property",{
                        "id":propertyId,
                        "visibleInList":check
                    },function(data) {
                        Core_Extorio.Spinner.hideFullPageSpinner();
                    })
                });

                $('.property_visible_detail').bind("change", function() {
                    var propertyId = $(this).attr("propertyid");
                    var check = $(this).prop("checked")?1:0;

                    Core_Extorio.Spinner.showFullPageSpinner_black();

                    //update the property
                    Core_ORM_BasicModel.update("Core_Property",{
                        "id":propertyId,
                        "visibleInDetail":check
                    },function(data) {
                        Core_Extorio.Spinner.hideFullPageSpinner();
                    })
                });

                $('.property_visible_create').bind("change", function() {
                    var propertyId = $(this).attr("propertyid");
                    var check = $(this).prop("checked")?1:0;

                    Core_Extorio.Spinner.showFullPageSpinner_black();

                    //update the property
                    Core_ORM_BasicModel.update("Core_Property",{
                        "id":propertyId,
                        "visibleInCreate":check
                    },function(data) {
                        Core_Extorio.Spinner.hideFullPageSpinner();
                    })
                });

                $('.property_visible_edit').bind("change", function() {
                    var propertyId = $(this).attr("propertyid");
                    var check = $(this).prop("checked")?1:0;

                    Core_Extorio.Spinner.showFullPageSpinner_black();

                    //update the property
                    Core_ORM_BasicModel.update("Core_Property",{
                        "id":propertyId,
                        "visibleInEdit":check
                    },function(data) {
                        Core_Extorio.Spinner.hideFullPageSpinner();
                    })
                });
            }

            $(function() {

                //update the positioning
                updatePositioning();
            });
        </script>
        <?php
    }

    public function createfield($modelId = false) {

        $dropdowns = $this->allDropdowns;
        $classes = $this->allClasses;

        ?>
        <script src="/core/assets/js/admin/Core_Admin_Models.js"></script>

        <ol class="breadcrumb">
            <li><a href="/extorio/admin/models">Models</a></li>
            <li><a href="/extorio/admin/models/fields/<?=$modelId?>"><?=$this->class->name?></a></li>
            <li class="active"><span class="glyphicon glyphicon-plus"></span> Create new field</li>
        </ol>

        <form name="create_property" action="" method="post" class="form-horizontal" role="form">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Field Name</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Field name">
                </div>
            </div>
            <div class="form-group">
                <label for="input_type" class="col-sm-2 control-label">Input Type</label>

                <div class="col-sm-10">
                    <select class="form-control" id="input_type" name="input_type">
                        <option
                            value="<?= Core_Input_Types::_textfield ?>"><?= Core_Input_Types::_textfield ?></option>
                        <option value="<?= Core_Input_Types::_textarea ?>"><?= Core_Input_Types::_textarea ?></option>
                        <option
                            value="<?= Core_Input_Types::_numberfield ?>"><?= Core_Input_Types::_numberfield ?></option>
                        <option
                            value="<?= Core_Input_Types::_decimalfield ?>"><?= Core_Input_Types::_decimalfield ?></option>
                        <option value="<?= Core_Input_Types::_textarea ?>"><?= Core_Input_Types::_textarea ?></option>
                        <option value="<?= Core_Input_Types::_html ?>"><?= Core_Input_Types::_html ?></option>
                        <option value="<?= Core_Input_Types::_checkbox ?>"><?= Core_Input_Types::_checkbox ?></option>
                        <option value="<?= Core_Input_Types::_date ?>"><?= Core_Input_Types::_date ?></option>
                        <option value="<?= Core_Input_Types::_datetime ?>"><?= Core_Input_Types::_datetime ?></option>
                        <option value="<?= Core_Input_Types::_time ?>"><?= Core_Input_Types::_time ?></option>
                        <option value="<?= Core_Input_Types::_dropdown ?>"><?= Core_Input_Types::_dropdown ?></option>
                        <option value="<?= Core_Input_Types::_model ?>"><?= Core_Input_Types::_model ?></option>
                        <option
                            value="<?= Core_Input_Types::_model_array ?>"><?= Core_Input_Types::_model_array ?></option>
                    </select>
                </div>
            </div>
            <div id="checkbox_default_box" style="display: none;" class="form-group">
                <label for="checkbox_default" class="col-sm-2 control-label">Default State</label>

                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input id="checkbox_default" name="checkbox_default" type="checkbox"> &nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <div id="script_tags_allowed_box" style="display: none;" class="form-group">
                <label for="script_tags_allowed" class="col-sm-2 control-label">Script tags allowed</label>

                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input id="script_tags_allowed" name="script_tags_allowed" type="checkbox"> &nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <div id="dropdown_id_box" style="display: none;" class="form-group">
                <label for="dropdown_id" class="col-sm-2 control-label">Dropdown Type</label>

                <div class="col-sm-10">
                    <select class="form-control" id="dropdown_id" name="dropdown_id">
                        <?php
                        foreach ($dropdowns as $dropdown) {
                            ?>
                            <option value="<?= $dropdown->id ?>"><?= $dropdown->name ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div id="child_class_id_box" style="display: none;" class="form-group">
                <label for="child_class_id" class="col-sm-2 control-label">Model Type</label>

                <div class="col-sm-10">
                    <select class="form-control" id="child_class_id" name="child_class_id">
                        <?php
                        foreach ($classes as $class) {
                            ?>
                            <option value="<?= $class->id ?>"><?= $class->name ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div id="max_length_box" style="display: none;" class="form-group">
                <label for="max_length" class="col-sm-2 control-label">Max length</label>

                <div class="col-sm-10">
                    <input type="number" class="form-control" id="max_length" name="max_length"
                           placeholder="Max length">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" name="create_property_submitted" value="Create" class="btn btn-default"/>
                </div>
            </div>
        </form>
        <script>
            $(function () {
                selectTextfield();
            });
        </script>
        <?php
    }

    public function editfield($modelId = false, $fieldId = false) {

        $property = $this->property;
        $dropdowns = $this->allDropdowns;
        $classes = $this->allClasses;

        ?>
        <script src="/core/assets/js/admin/Core_Admin_Models.js"></script>

        <ol class="breadcrumb">
            <li><a href="/extorio/admin/models">Models</a></li>
            <li><a href="/extorio/admin/models/fields/<?=$modelId?>"><?=$this->class->name?></a></li>
            <li class="active"><?=$this->property->name?></li>
        </ol>

        <form name="update_property" action="" method="post" class="form-horizontal" role="form">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Field Name</label>

                <div class="col-sm-10">
                    <input value="<?= $property->name ?>" type="text" class="form-control" id="name" name="name"
                           placeholder="Field name">
                </div>
            </div>
            <div class="form-group">
                <label for="input_type" class="col-sm-2 control-label">Input Type</label>

                <div class="col-sm-10">
                    <select class="form-control" id="input_type" name="input_type">
                        <option <?php
                        if ($property->inputType == Core_Input_Types::_textfield)
                            echo 'selected="selected"';
                        ?> value="<?= Core_Input_Types::_textfield ?>"><?= Core_Input_Types::_textfield
                            ?></option>
                        <option <?php
                        if ($property->inputType == Core_Input_Types::_textarea)
                            echo 'selected="selected"';
                        ?> value="<?= Core_Input_Types::_textarea ?>"><?= Core_Input_Types::_textarea ?></option>
                        <option <?php
                        if ($property->inputType == Core_Input_Types::_numberfield)
                            echo 'selected="selected"';
                        ?> value="<?= Core_Input_Types::_numberfield ?>"><?= Core_Input_Types::_numberfield
                            ?></option>
                        <option <?php
                        if ($property->inputType == Core_Input_Types::_decimalfield)
                            echo 'selected="selected"';
                        ?> value="<?= Core_Input_Types::_decimalfield ?>"><?= Core_Input_Types::_decimalfield
                            ?></option>
                        <option <?php
                        if ($property->inputType == Core_Input_Types::_html)
                            echo 'selected="selected"';
                        ?> value="<?= Core_Input_Types::_html ?>"><?= Core_Input_Types::_html ?></option>
                        <option <?php
                        if ($property->inputType == Core_Input_Types::_checkbox)
                            echo 'selected="selected"';
                        ?> value="<?= Core_Input_Types::_checkbox ?>"><?= Core_Input_Types::_checkbox ?></option>
                        <option <?php
                        if ($property->inputType == Core_Input_Types::_date)
                            echo 'selected="selected"';
                        ?> value="<?= Core_Input_Types::_date ?>"><?= Core_Input_Types::_date ?></option>
                        <option <?php
                        if ($property->inputType == Core_Input_Types::_datetime)
                            echo 'selected="selected"';
                        ?> value="<?= Core_Input_Types::_datetime ?>"><?= Core_Input_Types::_datetime ?></option>
                        <option <?php
                        if ($property->inputType == Core_Input_Types::_time)
                            echo 'selected="selected"';
                        ?> value="<?= Core_Input_Types::_time ?>"><?= Core_Input_Types::_time ?></option>
                        <option <?php
                        if ($property->inputType == Core_Input_Types::_dropdown)
                            echo 'selected="selected"';
                        ?> value="<?= Core_Input_Types::_dropdown ?>"><?= Core_Input_Types::_dropdown ?></option>
                        <option <?php
                        if ($property->inputType == Core_Input_Types::_model)
                            echo 'selected="selected"';
                        ?> value="<?= Core_Input_Types::_model ?>"><?= Core_Input_Types::_model ?></option>
                        <option <?php
                        if ($property->inputType == Core_Input_Types::_model_array)
                            echo 'selected="selected"';
                        ?> value="<?= Core_Input_Types::_model_array ?>"><?= Core_Input_Types::_model_array
                            ?></option>
                    </select>
                </div>
            </div>
            <div id="checkbox_default_box" style="display: none;" class="form-group">
                <label for="checkbox_default" class="col-sm-2 control-label">Default State</label>

                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if ($property->checkBoxDefault) {
                                echo 'checked="checked"';
                            }
                            ?> id="checkbox_default" name="checkbox_default" type="checkbox"> &nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <div id="script_tags_allowed_box" style="display: none;" class="form-group">
                <label for="script_tags_allowed" class="col-sm-2 control-label">Script tags allowed</label>

                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if ($property->htmlScriptsAllowed) {
                                echo 'checked="checked"';
                            }
                            ?> id="script_tags_allowed" name="script_tags_allowed" type="checkbox"> &nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <div id="dropdown_id_box" style="display: none;" class="form-group">
                <label for="dropdown_id" class="col-sm-2 control-label">Dropdown Type</label>

                <div class="col-sm-10">
                    <select class="form-control" id="dropdown_id" name="dropdown_id">
                        <?php
                        foreach ($dropdowns as $dropdown) {
                            ?>
                            <option <?php
                            if ($property->dropDownId == $dropdown->id) {
                                echo 'selected="selected"';
                            }
                            ?> value="<?= $dropdown->id ?>"><?= $dropdown->name ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div id="child_class_id_box" style="display: none;" class="form-group">
                <label for="child_class_id" class="col-sm-2 control-label">Model Type</label>

                <div class="col-sm-10">
                    <select class="form-control" id="child_class_id" name="child_class_id">
                        <?php
                        foreach ($classes as $class) {
                            ?>
                            <option <?php
                            if ($property->childClassId == $class->id) {
                                echo 'selected="selected"';
                            }
                            ?> value="<?= $class->id ?>"><?= $class->name ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div id="max_length_box" style="display: none;" class="form-group">
                <label for="max_length" class="col-sm-2 control-label">Max length</label>

                <div class="col-sm-10">
                    <input type="number" class="form-control" id="max_length" name="max_length" placeholder="Max length"
                           value="<?= $property->maxLength ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" name="update_property_submitted" value="Update" class="btn btn-default"/>
                </div>
            </div>
        </form>
        <script>
            $(function () {
                var currentInputType = '<?=$property->inputType?>';
                switch (currentInputType) {
                    case "checkbox" :
                        selectCheckbox();
                        break;
                    case "html" :
                        selectHtml();
                        break;
                    case "textarea" :
                        selectTextarea();
                        break;
                    case "textfield" :
                        selectTextfield();
                        break;
                    case "numberfield" :
                        selectNumberfield();
                        break;
                    case "decimalfield" :
                        selectDecimalfield();
                        break;
                    case "date" :
                        selectDate();
                        break;
                    case "datetime" :
                        selectDatetime();
                        break;
                    case "time" :
                        selectTime();
                        break;
                    case "dropdown" :
                        selectDropdown();
                        break;
                    case "model" :
                        selectModel();
                        break;
                    case "model_array" :
                        selectModel();
                        break;
                }
            });
        </script>
        <?php
    }

    /**
     * This method is the very last method that is called on the view.
     */
    public function onComplete() {
        ?><?php
    }
}