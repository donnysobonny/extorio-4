<?php
class CoreExtorioAdminModelViewerView extends CoreExtorioAdminModelViewerController {
    /**
     * This method is called immediately after the view is loaded by the framework.
     */
    public function onLoad() {
        ?>

        <?php
    }

    /**
     * This method is called if the view is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        ?>

        <?php
        //run the target action
        $action = $this->action."View";
        $this->$action();
    }

    private function editView() {
        $uid = uniqid();
        ?>
        <div id="model_detail_modal" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Detail view</h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <ol class="breadcrumb">
            <li>Model viewer</li>
            <li><a href="/extorio/admin/model-viewer/<?=$this->model?>/list"><?=$this->model?></a></li>
            <li><?=$this->rid?></li>
            <li>edit</li>
        </ol>
        <form name="edit_model_form" method="post" action="">
            <div class="model_view_menu_buttons">
                <button class="btn btn-primary btn-sm" name="edit_model_form_submitted"><span class="glyphicon glyphicon-save"></span> save</button>
                <button onclick="
                $('#model_detail_modal .modal-body').loadDetail('<?=$this->model?>',{rid:<?=$this->rid?>});
                $('#model_detail_modal').modal();
                " type="button" class="btn btn-sm"><span class="glyphicon glyphicon-open"></span> detail</button>
                <button onclick="
                Core_Extorio.Modal.openModal('small','Delete object?','Deleting this object may result in objects being deleted that are linked to this object. Are you sure that you want to delete this object?','Cancel','Delete',function() {
                    Core_ORM_ComplexModel.remove('<?=$this->model?>',<?=$this->rid?>,function() {
                        location.href = '/extorio/admin/model-viewer/<?=$this->model?>/list';
                    });
                });
                " type="button" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span> delete</button>
            </div>
            <div id="<?=$uid?>">

            </div>
            <div class="model_view_menu_buttons">
                <button class="btn btn-primary btn-sm" name="edit_model_form_submitted"><span class="glyphicon glyphicon-save"></span> save</button>
                <button onclick="
                    $('#model_detail_modal .modal-body').loadDetail('<?=$this->model?>',{rid:<?=$this->rid?>});
                    $('#model_detail_modal').modal();
                    " type="button" class="btn btn-sm"><span class="glyphicon glyphicon-open"></span> detail</button>
                <button type="button" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span> delete</button>
            </div>
        </form>
        <script>
            $(function() {
                $('#<?=$uid?>').loadEditForm('<?=$this->model?>',{
                    rid:<?=$this->rid?>,
                    removable:false
                });
            })
        </script>
        <?php
    }

    private function createView() {
        $uid = uniqid();
        ?>
        <ol class="breadcrumb">
            <li>Model viewer</li>
            <li><a href="/extorio/admin/model-viewer/<?=$this->model?>/list"><?=$this->model?></a></li>
            <li class="active"><span class="glyphicon glyphicon-plus"></span> Create new...</li>
        </ol>
        <form name="create_model_form" method="post" action="">
            <div class="model_view_menu_buttons">
                <button class="btn btn-primary btn-sm" name="create_model_form_submitted"><span class="glyphicon glyphicon-save"></span> save</button>
            </div>
            <div id="<?=$uid?>">

            </div>
            <div class="model_view_menu_buttons">
                <button class="btn btn-primary btn-sm" name="create_model_form_submitted"><span class="glyphicon glyphicon-save"></span> save</button>
            </div>
        </form>
        <script>
            $(function() {
                $('#<?=$uid?>').loadEditForm('<?=$this->model?>',{
                    removable:false
                });
            })
        </script>
        <?php
    }

    private function detailView() {
        ?>
        <ol class="breadcrumb">
            <li>Model viewer</li>
            <li><a href="/extorio/admin/model-viewer/<?=$this->model?>/list"><?=$this->model?></a></li>
            <li><?=$this->rid?></li>
            <li>detail</li>
        </ol>
        <div class="model_view_menu_buttons">
            <a class="btn btn-sm btn-primary" href="/extorio/admin/model-viewer/<?=$this->model?>/edit/<?=$this->rid?>"><span class="glyphicon glyphicon-edit"></span> edit</a>
            <button class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-trash"></span> delete</button>
        </div>
        <div id="model_detail">

        </div>
        <div class="model_view_menu_buttons">
            <a class="btn btn-sm btn-primary" href="/extorio/admin/model-viewer/<?=$this->model?>/edit/<?=$this->rid?>"><span class="glyphicon glyphicon-edit"></span> edit</a>
            <button class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-trash"></span> delete</button>
        </div>
        <script>
            $(function() {
                $('#model_detail').loadDetail('<?=$this->model?>',{
                    rid:<?=$this->rid?>
                });
            })
        </script>
        <?php
    }

    private function listView() {
        ?>
        <ol class="breadcrumb">
            <li>Model viewer</li>
            <li class="active"><?=$this->model?></li>
            <li class="active"><a href="/extorio/admin/model-viewer/<?=$this->model?>/create"><span class="glyphicon glyphicon-plus"></span> Create new...</a></li>
        </ol>
        <div id="model_list">

        </div>
        <script>
            $(function() {
                $('#model_list').loadList('<?=$this->model?>');
            });
        </script>
        <?php
    }

    /**
     * This method is the very last method that is called on the view.
     */
    public function onComplete() {
        ?>

        <?php
    }
}