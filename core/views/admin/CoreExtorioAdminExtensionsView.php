<?php
class CoreExtorioAdminExtensionsView extends CoreExtorioAdminExtensionsController {
    /**
     * This method is called immediately after the view is loaded by the framework.
     */
    public function onLoad() {
        ?>

        <?php
    }

    /**
     * This method is called if the view is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        ?>
        <ol class="breadcrumb">
            <li class="active">Extensions</li>
        </ol>
        <table class="table table-striped" id="extensions_table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Enabled</th>
                    <th>Load priority</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($this->installedExtensions as $extension) {
                    ?>
                <tr>
                    <td><?=$extension->name?></td>
                    <td>
                        <?php
                        if($extension->isEnabled) {
                            ?>
                        <span class="glyphicon glyphicon-ok"></span>
                            <?php
                        } else {
                            ?>
                        <span class="glyphicon glyphicon-remove"></span>
                            <?php
                        }
                        ?>
                    </td>
                    <td>
                        <form name="edit_priority_<?=$extension->id?>" method="post" action="">
                            <table>
                                <tbody>
                                    <tr>
                                        <td style="padding: 0 2px;">
                                            <input size="4" type="text" id="priority" name="priority" placeholder="Enter number" value="<?=$extension->loadPriority?>">
                                        </td>
                                        <td style="padding: 0 2px;">
                                            <input type="hidden" name="extension_id" value="<?=$extension->id?>" />
                                            <button name="edit_priority_submitted" type="submit" class="btn btn-default btn-xs">Update</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </td>
                    <td>
                        <?php
                        if($extension->isEnabled) {
                            ?>
                        <form style="display: inline-block;" name="disable_extension" method="post" action="">
                            <input type="hidden" name="extension_id" value="<?=$extension->id?>" />
                            <button name="disable_submitted" type="submit" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-remove"></span> disable</button>
                        </form>
                            <?php
                        } else {
                            ?>
                            <form style="display: inline-block;" name="enable_extension" method="post" action="">
                                <input type="hidden" name="extension_id" value="<?=$extension->id?>" />
                                <button name="enable_submitted" type="submit" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span> enable</button>
                            </form>
                            <form style="display: inline-block;" name="uninstall_extension" method="post" action="">
                                <input type="hidden" name="extension_id" value="<?=$extension->id?>" />
                                <button name="uninstall_submitted" type="submit" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span> uninstall</button>
                            </form>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('#extensions_table').DataTable({
                    "paging":   false,
                    "ordering": false
                });
            });
        </script>
        <?php
    }

    /**
     * This method is the very last method that is called on the view.
     */
    public function onComplete() {
        ?>

        <?php
    }
}