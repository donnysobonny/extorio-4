<?php
class CoreExtorioAdminLogoutView extends CoreExtorioAdminLogoutController {
    /**
     * This method is called immediately after the view is loaded by the framework.
     */
    public function onLoad() {
        ?>

        <?php
    }

    /**
     * This method is called if the view is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        ?>

        <?php
    }

    /**
     * This method is the very last method that is called on the view.
     */
    public function onComplete() {
        ?>

        <?php
    }
}