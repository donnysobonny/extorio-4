<?php
final class CoreExtorioAdminPagesView extends CoreExtorioAdminPagesController {
    public function onStart() {

    }

    public function onLoad() {

    }

    public function onDefault() {
        ?>
        <ol class="breadcrumb">
            <li class="active">Pages</li>
            <li><a href="/extorio/admin/pages/create"><span class="glyphicon glyphicon-plus"></span> Create new page...</a></li>
        </ol>
<table class="table table-striped" id="pages_table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th>Enabled</th>
            <th>Template</th>
            <th>Display mode</th>
            <th>Address</th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach($this->allPages as $page) {
        ?>
        <tr>
            <td><?=$page->name?></td>
            <td><?=$page->title?></td>
            <td>
                <?php
                if($page->isEnabled) {
                    ?>
                    <span class="glyphicon glyphicon-ok"></span>
                    <?php
                } else {
                    ?>
                    <span class="glyphicon glyphicon-remove"></span>
                    <?php
                }
                ?>
            </td>
            <td>
                <?php
                if($page->useTemplate) {
                    if(strlen($page->templateName)) {
                        echo $page->templateName." (".$page->templateExtensionName.")";
                    } else {
                        echo "SYSTEM DEFAULT";
                    }
                } else {
                    echo "no template";
                }
                ?>
            </td>
            <td>
                <?php
                if($page->useTemplate) {
                    echo $page->templateDisplayMode;
                } else {
                    echo "none";
                }
                ?>
            </td>
            <td>
                <?php
                $confid = $this->Extorio()->getStoredConfig();
                echo $confid["site"]["address"].$page->requestAddress;
                ?>
            </td>
            <td>
                <a class="btn btn-primary btn-xs" href="<?=$page->requestAddress?>"><span class="glyphicon glyphicon-eye-open"></span> view</a>
                <a class="btn btn-primary btn-xs" href="/extorio/admin/pages/edit/<?=$page->id?>"><span class="glyphicon glyphicon-pencil"></span> edit</a>
                <button class="btn btn-primary btn-xs"
                        onclick="Core_Extorio.Frame.openFrame('large','Managing page access for: <?=$page->name?>','/extorio/admin/access/page/<?=$page->id?>',function(){
                            Core_Extorio.Spinner.showFullPageSpinner_black();
                            location.reload();
                            })"><span class="glyphicon glyphicon-check"></span> access
                </button>
                <a class="btn btn-danger btn-xs" href="/extorio/admin/pages/delete/<?=$page->id?>"><span class="glyphicon glyphicon-trash"></span> delete</a> 
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
        <script>
            $('#pages_table').DataTable();
        </script>
        <?php
    }

    public function onComplete() {

    }

    public function create() {
        ?>
        <ol class="breadcrumb">
            <li><a href="/extorio/admin/pages">Pages</a></li>
            <li class="active"><span class="glyphicon glyphicon-plus"></span> Create new page...</li>
        </ol>
        <form name="edit_page" method="post" action="" class="form-horizontal" role="form">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Page name">
                </div>
            </div>
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Page title">
                </div>
            </div>
            <div class="form-group">
                <label for="enabled" class="col-sm-2 control-label">Enabled</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input checked="checked" id="enabled" name="enabled" type="checkbox">&nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="use_template" class="col-sm-2 control-label">Use template</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input checked="checked" id="use_template" name="use_template" type="checkbox">&nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <div id="template_select" class="form-group">
                <label for="template" class="col-sm-2 control-label">Template</label>
                <div class="col-sm-10">
                    <select class="form-control" name="template" id="template">
                        <option value="">--SYSTEM DEFAULT--</option>
                        <?php
                        foreach($this->allTemplates as $extension => $templates) {
                            ?>
                            <optgroup label="<?=$extension?>">
                                <?php
                                foreach($templates as $template) {
                                    ?>
                                    <option value="<?=$extension?>:<?=$template?>"><?=$template?></option>
                                    <?php
                                }
                                ?>
                            </optgroup>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div id="display_mode_select" class="form-group">
                <label for="display_mode" class="col-sm-2 control-label">Template display mode</label>
                <div class="col-sm-10">
                    <select name="display_mode" id="display_mode" class="form-control">
                        <option value="full">full</option>
                        <option value="basic_with_messages">basic with messages</option>
                        <option value="basic_no_messages">basic without messages</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="address" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><?php
                            $config = $this->Extorio()->getStoredConfig();
                            echo $config["site"]["address"];
                            ?></span>
                        <input type="text" class="form-control" id="address" name="address" placeholder="Page address">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="display_mode" class="col-sm-2 control-label">Extension</label>
                <div class="col-sm-2">
                    <select name="extension" id="extension" class="form-control">
                        <option value="application">application</option>
                        <?php
                        foreach($this->extensions as $extension) {
                            if($extension != "application") {
                                ?>
                                <option value="<?=$extension?>"><?=$extension?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button name="edit_page_submitted" type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>
        <script>
            $(function() {
                $('#use_template').bind("change",function() {
                    if($(this).prop("checked")) {
                        $('#template_select').fadeIn();
                        $('#display_mode_select').fadeIn();
                        $('#display_mode').val('full');
                    } else {
                        $('#template_select').fadeOut();
                        $('#display_mode_select').fadeOut();
                        $('#display_mode').val('none');
                        $('#template').val('');
                    }
                });


            })
        </script>
        <?php
    }

    public function edit($pageId=false) {
        ?>
        <ol class="breadcrumb">
            <li><a href="/extorio/admin/pages">Pages</a></li>
            <li class="active"><?=$this->page->name?></li>
        </ol>
        <form name="edit_page" method="post" action="" class="form-horizontal" role="form">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Page name" value="<?=$this->page->name?>">
                </div>
            </div>
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Page title" value="<?=$this->page->title?>">
                </div>
            </div>
            <div class="form-group">
                <label for="enabled" class="col-sm-2 control-label">Enabled</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input id="enabled" name="enabled" type="checkbox" <?php
                            if($this->page->isEnabled) echo 'checked="checked"';
                            ?>>&nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="use_template" class="col-sm-2 control-label">Use template</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input id="use_template" name="use_template" type="checkbox" <?php
                            if($this->page->useTemplate) echo 'checked="checked"';
                            ?>>&nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <div style="<?php
            if(!$this->page->useTemplate) echo "display: none;";
            ?>" id="template_select" class="form-group">
                <label for="template" class="col-sm-2 control-label">Template</label>
                <div class="col-sm-10">
                    <select class="form-control" name="template" id="template">
                        <option value="">--SYSTEM DEFAULT--</option>
                        <?php
                        foreach($this->allTemplates as $extension => $templates) {
                            ?>
                            <optgroup label="<?=$extension?>">
                                <?php
                                foreach($templates as $template) {
                                    ?>
                                    <option <?php
                                    if($this->page->templateExtensionName == $extension && $this->page->templateName == $template) echo 'selected="selected"';
                                    ?> value="<?=$extension?>:<?=$template?>"><?=$template?></option>
                                <?php
                                }
                                ?>
                            </optgroup>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div style="<?php
            if(!$this->page->useTemplate) echo "display: none;";
            ?>" id="display_mode_select" class="form-group">
                <label for="display_mode" class="col-sm-2 control-label">Template display mode</label>
                <div class="col-sm-10">
                    <select name="display_mode" id="display_mode" class="form-control">
                        <option <?php
                        if($this->page->templateDisplayMode == "full") echo 'selected="selected"';
                        ?> value="full">full</option>
                        <option <?php
                        if($this->page->templateDisplayMode == "basic_with_messages") echo 'selected="selected"';
                        ?> value="basic_with_messages">basic with messages</option>
                        <option <?php
                        if($this->page->templateDisplayMode == "basic_no_messages") echo 'selected="selected"';
                        ?> value="basic_no_messages">basic without messages</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="address" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><?php
                            $config = $this->Extorio()->getStoredConfig();
                            echo $config["site"]["address"];
                            ?></span>
                        <input type="text" class="form-control" id="address" name="address" placeholder="Page address" value="<?=$this->page->requestAddress?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button name="edit_page_submitted" type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
        <script>
            $(function() {
                $('#use_template').bind("change",function() {
                    if($(this).prop("checked")) {
                        $('#template_select').fadeIn();
                        $('#display_mode_select').fadeIn();
                        $('#display_mode').val('full');
                    } else {
                        $('#template_select').fadeOut();
                        $('#display_mode_select').fadeOut();
                        $('#display_mode').val('none');
                        $('#template').val('');
                    }
                });


            })
        </script>
    <?php
    }

    public function delete() {

    }
}