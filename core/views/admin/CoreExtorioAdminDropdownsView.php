<?php
class CoreExtorioAdminDropdownsView extends CoreExtorioAdminDropdownsController {
    /**
     * This method is called immediately after the view is loaded by the framework.
     */
    public function onLoad() {
        ?>

        <?php
    }

    /**
     * This method is called if the view is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        ?>
        <ol class="breadcrumb">
            <li class="active">Dropdowns</li>
        </ol>

        <form name="new_dropdown" method="post" action="" class="form-inline" role="form">
            <div class="form-group">
                <div class="input-group">
                    <label class="sr-only" for="name">Dropdown name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Dropdown name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <label class="sr-only" for="path">Class path</label>
                    <select id="path" name="path" class="form-control">
                        <?php
                        foreach($this->allExtensions as $extension) {
                            ?>
                            <option value="<?=$extension?>"><?=$extension?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <button name="new_dropdown_submitted" type="submit" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Create new dropdown</button>
        </form>

<table id="dropdowns_table" class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Class path</th>
            <th>
                <span class="glyphicon glyphicon-cog"></span>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($this->allDropdowns as $dropdown) {
            ?>
        <tr>
            <td><?=$dropdown->name?></td>
            <td><?=$dropdown->classPath?></td>
            <td>
                <a class="btn btn-primary btn-xs" href="/extorio/admin/dropdowns/elements/<?=$dropdown->id?>"><span class="glyphicon glyphicon-pencil"></span> manage elements...</a>
                <button dropdownid="<?=$dropdown->id?>" class="btn btn-danger btn-xs delete_dropdown"><span class="glyphicon glyphicon-trash"></span> delete</button>
            </td>
        </tr>
            <?php
        }
        ?>
    </tbody>
</table>
        <script>
            $(function() {
                $('#dropdowns_table').DataTable({
                    "paging": false,
                    "ordering": false
                });

                $('.delete_dropdown').bind("click",function(){
                    var dropdownId = $(this).attr("dropdownid");

                    Core_Extorio.Modal.openModal("small","Delete dropdown?","Deleting this dropdown will remove all of its elements and files. Are you sure that you want to delete it?","Cancel","Delete",function() {
                        Core_Extorio.Spinner.showFullPageSpinner_black();

                        //delete the dropdown
                        Core_ORM_BasicModel.remove("Core_Dropdown",dropdownId,function(data) {
                            //refresh
                            location.reload();
                        });
                    });
                });
            })
        </script>
        <?php
    }

    public function elements($dropdownId = false) {
        ?>
        <ol class="breadcrumb">
            <li><a href="/extorio/admin/dropdowns">Dropdowns</a></li>
            <li class="active"><?=$this->dropdown->name?></li>
        </ol>

        <form name="new_element" method="post" action="" class="form-inline" role="form">
            <div class="form-group">
                <div class="input-group">
                    <label class="sr-only" for="name">Element name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Element name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <label class="sr-only" for="value">Element value</label>
                    <input type="text" class="form-control" id="value" name="value" placeholder="Element value">
                </div>
            </div>
            <button name="new_element_submitted" type="submit" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Create new element</button>
        </form>

        <table id="elements_table" class="table table-striped">
            <thead>
                <tr>
                    <th>name</th>
                    <th>value</th>
                    <th>
                        <span class="glyphicon glyphicon-cog"></span>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($this->dropdownElements as $element) {
                    ?>
                <tr>
                    <td><?=$element->name?></td>
                    <td>
                        <input type="text" elementid="<?=$element->id?>" class="element_value" id="element_value_<?=$element->id?>" value="<?=$element->value?>" />
                    </td>
                    <td>
                        <button elementid="<?=$element->id?>" class="btn btn-xs btn-primary save_element"><span class="glyphicon glyphicon-pencil"></span> save</button>
                        <button elementid="<?=$element->id?>" class="btn btn-xs btn-danger delete_element"><span class="glyphicon glyphicon-trash"></span> delete</button>
                    </td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            function updateElement(id) {
                var element_value = $('#element_value_'+id).val();

                Core_Extorio.Spinner.showFullPageSpinner_black();

                Core_ORM_BasicModel.update("Core_DropdownElement",{
                    "id":id,
                    "value":element_value
                },function() {
                    Core_Extorio.Spinner.hideFullPageSpinner();
                })
            }

            $(function() {
                $('#elements_table').DataTable({
                    "paging": false
                });

                $('.save_element').bind("click",function() {
                    updateElement($(this).attr("elementid"));
                });

                $('.delete_element').bind("click", function() {
                    var elementid = $(this).attr("elementid");

                    Core_Extorio.Modal.openModal("small","Delete element?","Are you sure that you want to delete this element?","Cancel","Delete",function() {
                        Core_Extorio.Spinner.showFullPageSpinner_black();

                        //delete the basic model
                        Core_ORM_BasicModel.remove("Core_DropdownElement",elementid,function(data) {
                            location.reload();
                        });
                    });
                })
            })
        </script>
        <?php
    }

    /**
     * This method is the very last method that is called on the view.
     */
    public function onComplete() {
        ?>

        <?php
    }
}