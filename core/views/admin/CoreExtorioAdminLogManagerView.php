<?php
class CoreExtorioAdminLogManagerView extends CoreExtorioAdminLogManagerController {
    /**
     * This method is called immediately after the view is loaded by the framework.
     */
    public function onStart() {

    }

    public function onLoad() {
        ?>
        <ol class="breadcrumb">
            <li class="active">Log manager</li>
        </ol>
        <a class="btn btn-primary" href="<?=$this->getUrlBase()?>"><span class="glyphicon glyphicon-refresh"></span> Refresh</a>
        <a class="btn btn-danger" href="<?=$this->getUrlToMethod("remove_all")?>"><span class="glyphicon glyphicon-trash"></span> Remove all logs</a>
        <p></p>
        <table class="table table-striped" id="log_files_table">
            <thead>
                <tr>
                    <th>File name</th>
                    <th>File size</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($this->logFiles as $file) {
                    if($file->baseName != "index.html") {
                        ?>
                        <tr>
                            <td><?=$file->baseName?></td>
                            <td><?=$file->size?></td>
                            <td>
                                <a class="btn btn-primary btn-xs" href="<?=$this->getUrlToMethod("view",array($file->baseName))?>"><span class="glyphicon glyphicon-eye-open"></span> view</a>
                                <a class="btn btn-danger btn-xs" href="<?=$this->getUrlToMethod("delete",array($file->baseName))?>"><span class="glyphicon glyphicon-trash"></span> delete</a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
        <script>
            $('#log_files_table').DataTable();
        </script>
        <?php
    }

    /**
     * This method is called if the view is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        ?>

        <?php
    }

    public function remove_all() {

    }

    /**
     * This method is the very last method that is called on the view.
     */
    public function onComplete() {
        ?>

        <?php
    }
}