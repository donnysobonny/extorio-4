<?php
class CoreExtorioAdminAccountView extends CoreExtorioAdminAccountController {
    /**
     * This method is called immediately after the view is loaded by the framework.
     */
    public function onLoad() {
        ?>

        <?php
    }

    /**
     * This method is called if the view is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Change email</h3>
            </div>
            <div class="panel-body">
                <div class="alert alert-info" role="alert">
                    <strong>Current email:</strong> <?php
                    if($this->currentAdmin) echo $this->currentAdmin->email; else echo "none";
                    ?>
                </div>
                <form name="email_change" method="post" action="">
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter new email">
                    </div>
                    <button type="submit" name="email_change_submitted" class="btn btn-default">Update</button>
                </form>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Change username</h3>
            </div>
            <div class="panel-body">
                <div class="alert alert-info" role="alert">
                    <strong>Current username:</strong> <?php
                    if($this->currentAdmin) echo $this->currentAdmin->username; else echo "none";
                    ?>
                </div>
                <form name="change_username" method="post" action="">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Enter new username">
                    </div>
                    <button type="submit" name="change_username_submitted" class="btn btn-default">Update</button>
                </form>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Change password</h3>
            </div>
            <div class="panel-body">
                <form name="change_password" method="post" action="">
                    <div class="form-group">
                        <label for="cur_password">Current password</label>
                        <input type="password" class="form-control" id="cur_password" name="cur_password" placeholder="Enter current password">
                    </div>
                    <div class="form-group">
                        <label for="new_password">New password</label>
                        <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Enter new password">
                    </div>
                    <div class="form-group">
                        <label for="con_password">Confirm new password</label>
                        <input type="password" class="form-control" id="con_password" name="con_password" placeholder="Confirm new password">
                    </div>
                    <button type="submit" name="change_password_submitted" class="btn btn-default">Update</button>
                </form>
            </div>
        </div>
        <?php
    }

    /**
     * This method is the very last method that is called on the view.
     */
    public function onComplete() {
        ?>

        <?php
    }
}