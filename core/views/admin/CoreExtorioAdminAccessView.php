<?php

class CoreExtorioAdminAccessView extends CoreExtorioAdminAccessController {
    /**
     * This method is called immediately after the view is loaded by the framework.
     */
    public function onLoad() {
        ?>

    <?php
    }

    /**
     * This method is called if the view is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault($type = false, $target = false) {
        ?>
        <ol class="breadcrumb">
            <li class="active">Access events</li>
        </ol>
        <?php
        foreach ($this->events as $event) {
            ?>
            <h3>Access actions for the <strong><?= $event->action ?></strong> event</h3>
            <hr/>
            <a class="btn btn-primary btn-xs" href="/extorio/admin/access/create/<?= $event->id ?>/<?= $event->action ?>"><span class="glyphicon glyphicon-plus"></span> create new action</a>
            <p></p>
            <table class="action_table table table-striped">
                <thead>
                <tr>
                    <th>Ordering</th>
                    <th>User type</th>
                    <th>Access level</th>
                    <th>Redirect on failure</th>
                    <th>Redirect on success</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($this->actions[$event->id] as $action) {
                    ?>
                    <tr>
                        <td><?= $action->order ?></td>
                        <td><?= $action->userType ?></td>
                        <td><?= $action->accessLevel ?></td>
                        <td><?= $action->redirectOnFailure ?></td>
                        <td><?= $action->redirectOnSuccess ?></td>
                        <th>
                            <a class="btn btn-primary btn-xs" href="/extorio/admin/access/edit/<?= $action->id; ?>"><span class="glyphicon glyphicon-pencil"></span> edit</a>
                            <button actionid="<?=$action->id;?>" class="btn btn-danger btn-xs delete_action"><span class="glyphicon glyphicon-trash"></span> delete
                            </button>
                        </th>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
        <?php
        }
        ?>
        <script>
            $(function () {
                $('.action_table').DataTable({
                    "paging": false,
                    "info": false,
                    "searching": false
                });

                $('.delete_action').bind("click",function() {
                    var actionid = $(this).attr("actionid");

                    Core_Extorio.Spinner.showFullPageSpinner_black();

                    //delete the basic model
                    Core_ORM_BasicModel.remove("Core_AccessAction",actionid,function(data) {
                        location.reload();
                    });
                });
            })
        </script>
    <?php
    }

    public function create() {
        ?>
        <ol class="breadcrumb">
            <li><a href="/extorio/admin/access/<?=$this->event->type?>/<?=$this->event->target?>">Access events</a></li>
            <li class="active">Creating action for the <strong><?=$this->event->action?></strong> event</li>
        </ol>
        <form name="action_edit" action="" method="post" class="form-horizontal" role="form">
            <div class="form-group">
                <label for="usertype" class="col-sm-2 control-label">Target user type</label>

                <div class="col-sm-10">
                    <select name="usertype" id="usertype" class="form-control">
                        <option value="">--SELECT--</option>
                        <?php
                        foreach ($this->userTypes as $userType) {
                            ?>
                            <option value="<?= $userType ?>"><?= $userType ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div style="display: none;" id="accesslevel_select" class="form-group">
                <label for="accesslevel" class="col-sm-2 control-label">User access level</label>

                <div class="col-sm-10">
                    <select name="accesslevel" id="accesslevel" class="form-control">

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="redir_failure" class="col-sm-2 control-label">Redirect on failure</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" id="redir_failure" name="redir_failure"
                           placeholder="Redirect url on failure (optional)">
                </div>
            </div>
            <div class="form-group">
                <label for="redir_success" class="col-sm-2 control-label">Redirect on success</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" id="redir_success" name="redir_success"
                           placeholder="Redirect url on success (optional)">
                </div>
            </div>
            <div class="form-group">
                <label for="order" class="col-sm-2 control-label">Ordering</label>

                <div class="col-sm-2">
                    <input type="number" class="form-control" id="order" name="order"
                           placeholder="Ordering (lower numbers are processed first)">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="action_edit_submitted" class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>
        <script>
            $(function () {
                var accessLevels = <?=json_encode($this->accessLevels)?>;
                $('#usertype').bind("change", function () {
                    $('#accesslevel').html('');
                    var type = $(this).val();
                    if (type != "") {
                        $('#accesslevel_select').fadeIn();
                        $('#accesslevel').append('<option value="">--any access level--</option>');
                        $('#accesslevel').val('');
                        $.each(accessLevels[type], function (i, v) {
                            $('#accesslevel').append('<option value="' + i + '">' + v + '</option>');
                        });
                    } else {
                        $('#accesslevel_select').fadeOut();
                        $('#accesslevel').val('');
                    }
                })
            });
        </script>
    <?php
    }

    public function edit() {
        ?>
        <ol class="breadcrumb">
            <li><a href="/extorio/admin/access/<?=$this->event->type?>/<?=$this->event->target?>">Access events</a></li>
            <li class="active">Editing action for the <strong><?=$this->event->action?></strong> event</li>
        </ol>
        <form name="action_edit" action="" method="post" class="form-horizontal" role="form">
            <div class="form-group">
                <label for="usertype" class="col-sm-2 control-label">Target user type</label>

                <div class="col-sm-10">
                    <select name="usertype" id="usertype" class="form-control">
                        <option value="">--SELECT--</option>
                        <?php
                        foreach ($this->userTypes as $userType) {
                            ?>
                            <option <?php
                            if ($this->editAction->userType == $userType)
                                echo 'selected="selected"';
                            ?> value="<?= $userType ?>"><?= $userType ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div style="display: none;" id="accesslevel_select" class="form-group">
                <label for="accesslevel" class="col-sm-2 control-label">User access level</label>

                <div class="col-sm-10">
                    <select name="accesslevel" id="accesslevel" class="form-control">

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="redir_failure" class="col-sm-2 control-label">Redirect on failure</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" id="redir_failure" name="redir_failure"
                           value="<?= $this->editAction->redirectOnFailure ?>"
                           placeholder="Redirect url on failure (optional)">
                </div>
            </div>
            <div class="form-group">
                <label for="redir_success" class="col-sm-2 control-label">Redirect on success</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" id="redir_success" name="redir_success"
                           value="<?= $this->editAction->redirectOnSuccess ?>"
                           placeholder="Redirect url on success (optional)">
                </div>
            </div>
            <div class="form-group">
                <label for="order" class="col-sm-2 control-label">Ordering</label>

                <div class="col-sm-2">
                    <input type="number" class="form-control" id="order" name="order"
                           value="<?= $this->editAction->order ?>"
                           placeholder="Ordering (lower numbers are processed first)">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="action_edit_submitted" class="btn btn-primary">Update</button>
                </div>
            </div>
        </form>
        <script>
            $(function () {
                var accessLevels = <?=json_encode($this->accessLevels)?>;
                var selectedUserType = '<?=$this->editAction->userType?>';
                var selectedAccessLevel = '<?=$this->editAction->accessLevel?>';

                //load the access levels for the selected user type
                $('#accesslevel_select').show();
                $('#accesslevel').html('');
                $('#accesslevel').append('<option value="">--any access level--</option>');
                $.each(accessLevels[selectedUserType], function (i, v) {
                    if (parseInt(i) === parseInt(selectedAccessLevel)) {
                        $('#accesslevel').append('<option selected="selected" value="' + i + '">' + v + '</option>');
                    } else {
                        $('#accesslevel').append('<option value="' + i + '">' + v + '</option>');
                    }
                });

                $('#usertype').bind("change", function () {
                    $('#accesslevel').html('');
                    var type = $(this).val();
                    if (type != "") {
                        $('#accesslevel_select').fadeIn();
                        $('#accesslevel').append('<option value="">--any access level--</option>');
                        $('#accesslevel').val('');
                        $.each(accessLevels[type], function (i, v) {
                            $('#accesslevel').append('<option value="' + i + '">' + v + '</option>');
                        });
                    } else {
                        $('#accesslevel_select').fadeOut();
                        $('#accesslevel').val('');
                    }
                })
            });
        </script>
    <?php
    }

    /**
     * This method is the very last method that is called on the view.
     */
    public function onComplete() {
        ?>

    <?php
    }
}