<?php
class CoreExtorioAdminBlocksView extends CoreExtorioAdminBlocksController {
    /**
     * This method is called immediately after the view is loaded by the framework.
     */
    public function onLoad() {

    }

    /**
     * This method is called if the view is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        ?>
        <ol class="breadcrumb">
            <li class="active">Blocks</li>
            <li><a href="/extorio/admin/blocks/create"><span class="glyphicon glyphicon-plus"></span> Create new block...</a></li>
        </ol>
        <table class="table table-striped" id="live_blocks_table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Title</th>
                    <th>Enabled</th>
                    <th>Type</th>
                    <th>Panel mode</th>
                    <th>Area</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($this->allLiveBlocks as $liveBlock) {
                    ?>
                <tr>
                    <td><?=$liveBlock->name?></td>
                    <td><?=$liveBlock->title?></td>
                    <td><?php
                        if($liveBlock->isEnabled) {
                            ?>
                            <span class="glyphicon glyphicon-ok"></span>
                            <?php
                        } else {
                            ?>
                            <span class="glyphicon glyphicon-remove"></span>
                            <?php
                        }
                        ?></td>
                    <td><?php
                        echo $liveBlock->blockType." (".$liveBlock->blockExtensionName.")";
                        ?></td>
                    <td><?=$liveBlock->panelMode?></td>
                    <td><?="area".$liveBlock->area?></td>
                    <td>
                        <a class="btn btn-primary btn-xs" href="/extorio/admin/blocks/edit/<?=$liveBlock->id?>"><span class="glyphicon glyphicon-pencil"></span> edit</a>
                        <button class="btn btn-primary btn-xs"
                                onclick="Core_Extorio.Frame.openFrame('large','Managing block access for: <?=$liveBlock->name?>','/extorio/admin/access/block/<?=$liveBlock->id?>',function(){
                                    Core_Extorio.Spinner.showFullPageSpinner_black();
                                    location.reload();
                                    })"><span class="glyphicon glyphicon-check"></span> access
                        </button>
                        <a class="btn btn-danger btn-xs" href="/extorio/admin/blocks/delete/<?=$liveBlock->id?>"><span class="glyphicon glyphicon-trash"></span> delete</a>
                    </td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('#live_blocks_table').DataTable();
            });
        </script>
        <?php
    }

    public function create() {
        ?>
        <ol class="breadcrumb">
            <li><a href="/extorio/admin/blocks">Blocks</a></li>
            <li class="active"><span class="glyphicon glyphicon-plus"></span> Create new block...</li>
        </ol>
        <form name="block_edit" method="post" action="" class="form-horizontal" role="form">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                </div>
            </div>
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                </div>
            </div>
            <div class="form-group">
                <label for="block_type" class="col-sm-2 control-label">Type</label>
                <div class="col-sm-3">
                    <select id="block_type" name="block_type" class="form-control">
                        <?php
                        foreach($this->installedBlocks as $extension => $blocks) {
                            ?>
                            <optgroup label="<?=$extension?>">
                                <?php
                                foreach($blocks as $block) {
                                    ?>
                                <option value="<?=$extension?>:<?=$block?>"><?=$block?></option>
                                    <?php
                                }
                                ?>
                            </optgroup>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="panel_mode" class="col-sm-2 control-label">Panel mode</label>
                <div class="col-sm-3">
                    <select id="panel_mode" name="panel_mode" class="form-control">
                        <option value="panel_with_header">panel with header</option>
                        <option value="panel_no_header">panel with no header</option>
                        <option value="no_panel">no panel</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="enabled" class="col-sm-2 control-label">Enabled</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input checked="checked" name="enabled" id="enabled" type="checkbox">&nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="all_pages" class="col-sm-2 control-label">All pages</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input name="all_pages" id="all_pages" type="checkbox">&nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <div id="pages_select" class="form-group">
                <label for="pages" class="col-sm-2 control-label">Pages</label>
                <div class="col-sm-6">
                    <select id="pages" name="pages[]" class="form-control" multiple="multiple">
                        <?php
                        foreach($this->allPages as $page) {
                            ?>
                        <option value="<?=$page->id?>"><?=$page->name?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="area" class="col-sm-2 control-label">Area</label>
                <div class="col-sm-2">
                    <select id="area" name="area" class="form-control">
                        <?php
                        for($i = 1; $i <= 28; $i++) {
                            ?>
                        <option value="<?=$i?>">area<?=$i?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="order" class="col-sm-2 control-label">Ordering</label>
                <div class="col-sm-2">
                    <input type="number" class="form-control" id="order" name="order" placeholder="Ordering (lower numbers appear first)">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="block_edit_submitted" class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>
        <script>
            $(function() {
                $('#all_pages').bind("change", function() {
                    var checked = $(this).prop("checked");
                    if(checked) {
                        //select all pages
                        $('#pages option').each(function() {
                            $(this).prop("selected",true);
                        });
                    } else {
                        //deselect all pages
                        $('#pages option').each(function() {
                            $(this).prop("selected",false);
                        });
                    }
                });
            });
        </script>
        <?php
    }

    public function edit() {
        ?>
        <ol class="breadcrumb">
            <li><a href="/extorio/admin/blocks">Blocks</a></li>
            <li class="active"><?=$this->editBlock->name?></li>
        </ol>
        <form name="block_edit" method="post" action="" class="form-horizontal" role="form">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?=$this->editBlock->name?>">
                </div>
            </div>
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="<?=$this->editBlock->title?>">
                </div>
            </div>
            <div class="form-group">
                <label for="block_type" class="col-sm-2 control-label">Type</label>
                <div class="col-sm-3">
                    <select id="block_type" name="block_type" class="form-control">
                        <?php
                        foreach($this->installedBlocks as $extension => $blocks) {
                            ?>
                            <optgroup label="<?=$extension?>">
                                <?php
                                foreach($blocks as $block) {
                                    ?>
                                    <option <?php
                                    if($this->editBlock->blockExtensionName == $extension && $this->editBlock->blockType == $block) {
                                        echo 'selected="selected"';
                                    }
                                    ?> value="<?=$extension?>:<?=$block?>"><?=$block?></option>
                                <?php
                                }
                                ?>
                            </optgroup>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="panel_mode" class="col-sm-2 control-label">Panel mode</label>
                <div class="col-sm-3">
                    <select id="panel_mode" name="panel_mode" class="form-control">
                        <option <?php
                        if($this->editBlock->panelMode == "panel_with_header") echo 'selected="selected"';
                        ?> value="panel_with_header">panel with header</option>
                        <option <?php
                        if($this->editBlock->panelMode == "panel_no_header") echo 'selected="selected"';
                        ?> value="panel_no_header">panel with no header</option>
                        <option <?php
                        if($this->editBlock->panelMode == "no_panel") echo 'selected="selected"';
                        ?> value="no_panel">no panel</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="enabled" class="col-sm-2 control-label">Enabled</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->editBlock->isEnabled) echo 'checked="checked"';
                            ?> name="enabled" id="enabled" type="checkbox">&nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="all_pages" class="col-sm-2 control-label">All pages</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->editBlock->allPages) echo 'checked="checked"';
                            ?> name="all_pages" id="all_pages" type="checkbox">&nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <div id="pages_select" class="form-group">
                <label for="pages" class="col-sm-2 control-label">Pages</label>
                <div class="col-sm-6">
                    <select id="pages" name="pages[]" class="form-control" multiple="multiple">
                        <?php
                        foreach($this->allPages as $page) {
                            ?>
                            <option <?php
                            if(in_array($page->id,$this->editBlockPagesIds)) echo 'selected="selected"';
                            ?> value="<?=$page->id?>"><?=$page->name?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="area" class="col-sm-2 control-label">Area</label>
                <div class="col-sm-2">
                    <select id="area" name="area" class="form-control">
                        <?php
                        for($i = 1; $i <= 28; $i++) {
                            ?>
                            <option <?php
                            if($this->editBlock->area == $i) echo 'selected="selected"';
                            ?> value="<?=$i?>">area<?=$i?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="order" class="col-sm-2 control-label">Ordering</label>
                <div class="col-sm-2">
                    <input type="number" class="form-control" id="order" name="order" placeholder="Ordering (lower numbers appear first)" value="<?=$this->editBlock->order?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="block_edit_submitted" class="btn btn-primary">Update</button>
                </div>
            </div>
        </form>
        <script>
            $(function() {
                $('#all_pages').bind("change", function() {
                    var checked = $(this).prop("checked");
                    if(checked) {
                        //select all pages
                        $('#pages option').each(function() {
                            $(this).prop("selected",true);
                        });
                    } else {
                        //deselect all pages
                        $('#pages option').each(function() {
                            $(this).prop("selected",false);
                        });
                    }
                });
            });
        </script>
    <?php
    }

    public function delete() {

    }

    /**
     * This method is the very last method that is called on the view.
     */
    public function onComplete() {
        ?>

        <?php
    }
}