<?php
class CoreExtorioAdminTaskManagerView extends CoreExtorioAdminTaskManagerController {
    /**
     * This method is called immediately after the view is loaded by the framework.
     */
    public function onLoad() {
        ?>
        <ol class="breadcrumb">
            <li class="active">Task manager</li>
        </ol>
        <a class="btn btn-primary" href="<?=$this->getUrlBase()?>"><span class="glyphicon glyphicon-refresh"></span> Refresh</a>
        <a class="btn btn-danger" href="<?=$this->getUrlToMethod("clear_all")?>"><span class="glyphicon glyphicon-remove"></span> Clear all non-running tasks</a>
        <p></p>
<table class="table table-striped" id="tasks_table">
    <thead>
        <tr>
            <th>PID</th>
            <th>Task</th>
            <th>Extension</th>
            <th>Status</th>
            <th>Failed message</th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($this->allLiveTasks as $liveTask) {
            ?>
            <tr>
                <td><?=$liveTask->pid?></td>
                <td><?=$liveTask->taskName?></td>
                <td><?=$liveTask->taskExtensionName?></td>
                <td>
                    <?php
                    $color = "white";
                    switch($liveTask->status) {
                        case Core_Task_Status::completed :
                            $color = "lightgreen";
                            break;
                        case Core_Task_Status::failed :
                            $color = "yellow";
                            break;
                        case Core_Task_Status::running :
                            $color = "lime";
                            break;
                        case Core_Task_Status::killed :
                            $color = "darkred";
                            break;
                        case Core_Task_Status::pending :
                            $color = "orange";
                            break;
                    }
                    ?>
                    <span style="font-weight: bold; color: <?=$color?>;"><?=$liveTask->status?></span>
                </td>
                <td><?=$liveTask->failedMessage?></td>
                <td>
                    <?php
                    if($liveTask->status == Core_Task_Status::running || $liveTask->status == Core_Task_Status::pending) {
                        ?>
                    <a class="btn btn-danger btn-xs" href="<?=$this->getUrlToMethod("kill",array($liveTask->id))?>"><span class="glyphicon glyphicon-remove"></span> kill task</a>
                        <?php
                    }
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
        <script>
            $(function() {
                $('#tasks_table').DataTable({
                    "order": [[ 0, "desc" ]]
                });
            });
        </script>
        <?php
    }

    /**
     * This method is called if the view is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        ?>

        <?php
    }

    /**
     * This method is the very last method that is called on the view.
     */
    public function onComplete() {
        ?>

        <?php
    }

    public function kill() {

    }

    public function clear_all() {

    }
}