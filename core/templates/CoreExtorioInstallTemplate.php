<?php
final class CoreExtorioInstallTemplate extends Core_Template {
    public function onServe() {
        //display messsges
        foreach($this->errorMessages as $message) {
            ?>
            <div style="padding: 10px; padding-bottom: 0px;">
                <div class="alert alert-danger" role="alert"><?=$message?></div>
            </div>
        <?php
        }

        foreach($this->infoMessages as $message) {
            ?>
            <div style="padding: 10px; padding-bottom: 0px;">
                <div class="alert alert-info" role="alert"><?=$message?></div>
            </div>
        <?php
        }

        foreach($this->successMessages as $message) {
            ?>
            <div style="padding: 10px; padding-bottom: 0px;">
                <div class="alert alert-success" role="alert"><?=$message?></div>
            </div>
        <?php
        }

        foreach($this->warningMessages as $message) {
            ?>
            <div style="padding: 10px; padding-bottom: 0px;">
                <div class="alert alert-warning" role="alert"><?=$message?></div>
            </div>
        <?php
        }
        ?>
        <div style="margin: 15px auto; width: 1000px;" class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Extorio Installation</h3>
            </div>
            <div class="panel-body">
                <?php
                if($this->viewContent) {
                    echo $this->viewContent;
                }
                ?>
            </div>
        </div>
        <?php
    }
}