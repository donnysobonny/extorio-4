<?php
final class CoreExtorioBlockViewerTemplate extends Core_Template {
    public function onServe() {
        //display messsges
        foreach($this->errorMessages as $message) {
            ?>
            <div style="padding: 10px; padding-bottom: 0px;">
                <div class="alert alert-danger" role="alert"><?=$message?></div>
            </div>
        <?php
        }

        foreach($this->infoMessages as $message) {
            ?>
            <div style="padding: 10px; padding-bottom: 0px;">
                <div class="alert alert-info" role="alert"><?=$message?></div>
            </div>
        <?php
        }

        foreach($this->successMessages as $message) {
            ?>
            <div style="padding: 10px; padding-bottom: 0px;">
                <div class="alert alert-success" role="alert"><?=$message?></div>
            </div>
        <?php
        }

        foreach($this->warningMessages as $message) {
            ?>
            <div style="padding: 10px; padding-bottom: 0px;">
                <div class="alert alert-warning" role="alert"><?=$message?></div>
            </div>
        <?php
        }

        //only display the view content
        echo $this->viewContent;
    }
}