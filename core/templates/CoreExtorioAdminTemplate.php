<?php
final class CoreExtorioAdminTemplate extends Core_Template {
    public function onServe($content) {
        ?>
        <script type="application/javascript" src="/core/assets/js/admin/Core_ORM_BasicModel.js"></script>
        <script type="application/javascript" src="/core/assets/js/admin/Core_Admin_ModelViewer.js"></script>
        <link rel="stylesheet" href="/core/assets/css/Core_Admin.css">
        <?php

        if($this->area_1 || $this->area_2 || $this->area_3) {
            ?>
            <div id="banner_top">
                <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td><?=$this->area_1?></td>
                        <td><?=$this->area_2?></td>
                        <td><?=$this->area_3?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        <?php
        }
        //display messsges
        foreach($this->errorMessages as $message) {
            ?>
            <div style="padding: 10px; padding-bottom: 0px;">
                <div class="alert alert-danger" role="alert"><?=$message?></div>
            </div>
        <?php
        }

        foreach($this->infoMessages as $message) {
            ?>
            <div style="padding: 10px; padding-bottom: 0px;">
                <div class="alert alert-info" role="alert"><?=$message?></div>
            </div>
        <?php
        }

        foreach($this->successMessages as $message) {
            ?>
            <div style="padding: 10px; padding-bottom: 0px;">
                <div class="alert alert-success" role="alert"><?=$message?></div>
            </div>
        <?php
        }

        foreach($this->warningMessages as $message) {
            ?>
            <div style="padding: 10px; padding-bottom: 0px;">
                <div class="alert alert-warning" role="alert"><?=$message?></div>
            </div>
        <?php
        }
        ?>
        <div style="margin: 15px;" id="center_outer_container">
            <?php
            if($this->area_4) {
                ?>
                <div id="center_header_container">
                    <?=$this->area_4?>
                </div>
            <?php
            }

            if(
                $this->area_5 ||
                $this->area_6 ||
                $this->area_7 ||
                $this->area_8 ||
                $this->area_9 ||
                $this->area_10 ||
                $this->area_11 ||
                $this->area_12 ||
                $this->area_13 ||
                $this->area_14 ||
                $this->area_15 ||
                $this->area_16 ||
                $this->area_17 ||
                $this->area_18 ||
                $this->pageContent
            ) {
                ?>
                <div class="panel panel-primary" id="center_mainbody_container">
                    <div class="panel-body">
                        <?php
                        if($this->area_5 || $this->area_6 || $this->area_7) {
                            ?>
                            <div id="center_mainbody_header_container">
                                <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td><?=$this->area_5?></td>
                                            <td><?=$this->area_6?></td>
                                            <td><?=$this->area_7?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        <?php
                        }
                        ?>


                        <div id="center_mainbody_center_container">
                            <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                <tr>
                                    <?php
                                    if($this->area_8 || $this->area_9 || $this->area_10) {
                                        ?>
                                        <td style="width: 150px; vertical-align: top;">
                                            <div id="center_mainbody_center_left_container">
                                                <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <div id="center_mainbody_center_left_top_container">
                                                                <?=$this->area_8?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="center_mainbody_center_left_middle_container">
                                                                <?=$this->area_9?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="center_mainbody_center_left_bottom_container">
                                                                <?=$this->area_10?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    <?php
                                    }

                                    if($this->area_14 || $this->pageContent || $this->area_15) {
                                        ?>
                                        <td style="vertical-align: top;">
                                            <div id="center_mainbody_center_center_container">
                                                <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <div id="center_mainbody_center_center_top_container">
                                                                <?=$this->area_14?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="center_mainbody_center_center_middle_container">
                                                                <?=$this->pageContent?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="center_mainbody_center_center_bottom_container">
                                                                <?=$this->area_15?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    <?php
                                    }

                                    if($this->area_11 || $this->area_12 || $this->area_13) {
                                        ?>
                                        <td style="width: 150px; vertical-align: top;">
                                            <div id="center_mainbody_center_right_container">
                                                <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <div id="center_mainbody_center_right_top_container">
                                                                <?=$this->area_11?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="center_mainbody_center_right_middle_container">
                                                                <?=$this->area_12?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="center_mainbody_center_right_bottom_container">
                                                                <?=$this->area_13?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    <?php
                                    }
                                    ?>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <?php
                        if($this->area_16 || $this->area_17 || $this->area_18) {
                            ?>
                            <div id="center_mainbody_footer_container">
                                <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td><?=$this->area_16?></td>
                                        <td><?=$this->area_17?></td>
                                        <td><?=$this->area_18?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        <?php
                        }
                        ?>

                    </div>
                </div>
            <?php
            }
            ?>

            <?php
            if($this->area_19) {
                ?>
                <div id="center_footer_container">
                    <?=$this->area_19?>
                </div>
            <?php
            }
            if($this->area_20 || $this->area_21 || $this->area_22) {
                ?>
                <div id="banner_bottom">
                    <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                        <tr>
                            <td><?=$this->area_20?></td>
                            <td><?=$this->area_21?></td>
                            <td><?=$this->area_22?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            <?php
            }
            ?>
        </div>
        <?php
    }
}