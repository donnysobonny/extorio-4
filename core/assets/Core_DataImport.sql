
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_AccessAction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` int(11) NOT NULL,
  `userType` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `accessLevel` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `redirectOnSuccess` text COLLATE utf8_unicode_ci NOT NULL,
  `redirectOnFailure` text COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `eventId` (`eventId`,`userType`,`accessLevel`,`order`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_AccessAction` WRITE;
/*!40000 ALTER TABLE `Core_AccessAction` DISABLE KEYS */;
REPLACE INTO `Core_AccessAction` (`id`, `eventId`, `userType`, `accessLevel`, `redirectOnSuccess`, `redirectOnFailure`, `order`) VALUES (1,21,'Core_Admin_User','','','',1),(2,4,'Core_Admin_User','','','/extorio/admin/login',1),(3,14,'Core_Admin_User','','','/extorio/admin/login',1),(4,7,'Core_Admin_User','','','/extorio/admin/login',1),(5,17,'Core_Admin_User','','','/extorio/admin/login',1),(6,9,'Core_Admin_User','','','/extorio/admin/login',1),(7,19,'Core_Admin_User','','','/extorio/admin/login',1),(8,3,'Core_Admin_User','','','/extorio/admin/login',1),(9,13,'Core_Admin_User','','','/extorio/admin/login',1),(20,36,'Core_Admin_User','','','/extorio/admin/login',1),(35,121,'Core_Admin_User','','','/extorio/admin/login',1),(15,30,'Core_Admin_User','','','',1),(19,35,'Core_Admin_User','','','/extorio/admin/login',1),(30,60,'Core_Admin_User','','','/extorio/admin/login',1),(29,59,'Core_Admin_User','','','/extorio/admin/login',1),(34,29,'Core_Admin_User','','','',1),(36,122,'Core_Admin_User','','','/extorio/admin/login',1),(37,218,'Core_Admin_User','','','/extorio/admin/login/',1),(38,217,'Core_Admin_User','','','/extorio/admin/login/',1),(39,34,'Core_Admin_User','','','/extorio/admin/login/',1),(40,33,'Core_Admin_User','','','/extorio/admin/login/',1),(41,32,'Core_Admin_User','','','/extorio/admin/login/',1),(42,31,'Core_Admin_User','','','/extorio/admin/login/',1),(43,28,'Core_Admin_User','','','/extorio/admin/login/',1),(44,27,'Core_Admin_User','','','/extorio/admin/login/',1);
/*!40000 ALTER TABLE `Core_AccessAction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_AccessEvent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `target` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`,`action`,`target`)
) ENGINE=MyISAM AUTO_INCREMENT=225 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_AccessEvent` WRITE;
/*!40000 ALTER TABLE `Core_AccessEvent` DISABLE KEYS */;
REPLACE INTO `Core_AccessEvent` (`id`, `type`, `action`, `target`) VALUES (1,'page','view','1'),(2,'page','view','2'),(3,'page','view','3'),(4,'page','view','6'),(5,'page','view','18'),(6,'page','view','19'),(7,'page','view','20'),(60,'page','edit','40'),(9,'page','view','25'),(58,'block','edit','7'),(11,'page','edit','1'),(12,'page','edit','2'),(13,'page','edit','3'),(14,'page','edit','6'),(15,'page','edit','18'),(16,'page','edit','19'),(17,'page','edit','20'),(59,'page','view','40'),(19,'page','edit','25'),(57,'block','view','7'),(21,'block','view','5'),(22,'block','edit','5'),(35,'page','view','33'),(27,'page','view','30'),(28,'page','edit','30'),(29,'block','view','6'),(30,'block','edit','6'),(31,'page','view','31'),(32,'page','edit','31'),(33,'page','view','32'),(34,'page','edit','32'),(36,'page','edit','33'),(43,'page','view','37'),(44,'page','edit','37'),(45,'page','view','38'),(46,'page','edit','38'),(69,'page','view','41'),(70,'page','edit','41'),(73,'model','create','105'),(74,'model','retrieve','105'),(75,'model','update','105'),(76,'model','delete','105'),(77,'model','view_edit','105'),(78,'model','view_detail','105'),(79,'model','view_list','105'),(80,'model','view_delete','105'),(81,'model','create','106'),(82,'model','retrieve','106'),(83,'model','update','106'),(84,'model','delete','106'),(85,'model','view_edit','106'),(86,'model','view_detail','106'),(87,'model','view_list','106'),(88,'model','view_delete','106'),(108,'block','edit','8'),(109,'block','view','9'),(110,'block','edit','9'),(121,'page','view','45'),(122,'page','edit','45'),(107,'block','view','8'),(157,'block','view','10'),(158,'block','edit','10'),(217,'page','view','48'),(218,'page','edit','48');
/*!40000 ALTER TABLE `Core_AccessEvent` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_Address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `line1` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `line2` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `city` varchar(40) COLLATE latin1_general_ci NOT NULL,
  `town` varchar(40) COLLATE latin1_general_ci NOT NULL,
  `region` varchar(40) COLLATE latin1_general_ci NOT NULL,
  `country` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `postcode` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `phone` varchar(16) COLLATE latin1_general_ci NOT NULL,
  `fax` varchar(16) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_Address` WRITE;
/*!40000 ALTER TABLE `Core_Address` DISABLE KEYS */;
/*!40000 ALTER TABLE `Core_Address` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_Admin_User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `username` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(256) COLLATE latin1_general_ci NOT NULL,
  `emailVerified` tinyint(1) NOT NULL,
  `canLogin` tinyint(1) NOT NULL,
  `numLogin` int(8) NOT NULL,
  `dateLogin` datetime NOT NULL,
  `accessLevel` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `createdByAdmin` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `updatedByAdmin` varchar(60) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username_index` (`username`),
  KEY `email_index` (`email`),
  KEY `emailVerified_index` (`emailVerified`),
  KEY `canLogin_index` (`canLogin`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_ApiUserSession` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userType` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(11) NOT NULL,
  `session` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `remoteIp` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userType` (`userType`,`userId`,`session`),
  KEY `remoteIp` (`remoteIp`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_Class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `type` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `isPersistent` tinyint(1) NOT NULL DEFAULT '1',
  `modelDir` text COLLATE latin1_general_ci NOT NULL,
  `isHidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `isHidden` (`isHidden`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=121 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_Class` WRITE;
/*!40000 ALTER TABLE `Core_Class` DISABLE KEYS */;
REPLACE INTO `Core_Class` (`id`, `name`, `type`, `isPersistent`, `modelDir`, `isHidden`) VALUES (77,'Core_Address','address',0,'core/models',1),(72,'Core_Admin_User','user',1,'core/models',1);
/*!40000 ALTER TABLE `Core_Class` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_CustomList` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `ownerClassId` int(11) NOT NULL,
  `query` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerClassId` (`ownerClassId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_CustomList` WRITE;
/*!40000 ALTER TABLE `Core_CustomList` DISABLE KEYS */;
/*!40000 ALTER TABLE `Core_CustomList` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_Dropdown` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `classPath` text COLLATE latin1_general_ci NOT NULL,
  `isHidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `isHidden` (`isHidden`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_Dropdown` WRITE;
/*!40000 ALTER TABLE `Core_Dropdown` DISABLE KEYS */;
REPLACE INTO `Core_Dropdown` (`id`, `name`, `classPath`, `isHidden`) VALUES (17,'Core_User_AccessLevel','core/classes',1),(15,'Core_PHP_Types','core/classes',1),(16,'Core_Model_Types','core/classes',1),(18,'Core_Admin_User_AccessLevel','core/classes',1),(19,'Core_Address_Country','core/classes',0),(32,'Core_AccessEvent_Type','core/classes',0),(33,'Core_Block_PanelTypes','core/classes',0),(34,'Core_HTTPStatusCodes','core/classes',0),(35,'Core_Input_Types','core/classes',0),(36,'Core_MySQL_Types','core/classes',0),(37,'Core_Task_Status','core/classes',0),(38,'Core_Template_DisplayMode','core/classes',0);
/*!40000 ALTER TABLE `Core_Dropdown` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_DropdownElement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ownerDropdownId` int(11) NOT NULL,
  `value` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `name` varchar(60) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerDropdownId` (`ownerDropdownId`,`value`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=461 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_DropdownElement` WRITE;
/*!40000 ALTER TABLE `Core_DropdownElement` DISABLE KEYS */;
REPLACE INTO `Core_DropdownElement` (`id`, `ownerDropdownId`, `value`, `name`) VALUES (61,18,'0','Super Administrator'),(60,17,'1','user'),(59,17,'0','super user'),(62,18,'1','Administrator level 1'),(63,18,'2','Administrator level 2'),(46,15,'integer','_INTEGER'),(47,15,'boolean','_BOOLEAN'),(48,15,'double','_DOUBLE'),(49,15,'float','_FLOAT'),(50,15,'string','_STRING'),(51,15,'array','_ARRAY'),(52,15,'object','_OBJECT'),(53,15,'resource','_RESOURCE'),(54,15,'NULL','_NULL'),(55,15,'unknown type','_UNKNOWN'),(56,16,'basic','_BASIC'),(57,16,'user','_USER'),(58,16,'address','_ADDRESS'),(64,18,'3','Administrator level 3'),(65,18,'4','Administrator level 4'),(66,18,'5','Administrator level 5'),(67,19,'GB','United Kingdom'),(89,19,'US','United States'),(90,19,'EN','England'),(91,19,'SS','Scotland'),(92,19,'ND','Northern Ireland'),(93,19,'AF','Afghanistan'),(94,19,'AX','Aland Islands'),(95,19,'AL','Albania'),(96,19,'DZ','Algeria'),(97,19,'AS','American Samoa'),(98,19,'AD','Andorra'),(99,19,'AO','Angola'),(100,19,'AI','Anguilla'),(101,19,'AQ','Antarctica'),(102,19,'AG','Antigua And Barbuda'),(103,19,'AR','Argentina'),(104,19,'AM','Armenia'),(105,19,'AW','Aruba'),(106,19,'AU','Australia'),(107,19,'AT','Austria'),(108,19,'AZ','Azerbaijan'),(109,19,'BS','Bahamas'),(110,19,'BH','Bahrain'),(111,19,'BD','Bangladesh'),(112,19,'BB','Barbados'),(113,19,'BY','Belarus'),(114,19,'BE','Belgium'),(115,19,'BZ','Belize'),(116,19,'BJ','Benin'),(117,19,'BM','Bermuda'),(118,19,'BT','Bhutan'),(119,19,'BO','Bolivia'),(120,19,'BA','Bosnia And Herzegovina'),(121,19,'BW','Botswana'),(122,19,'BV','Bouvet Island'),(123,19,'BR','Brazil'),(124,19,'IO','British Indian Ocean Territory'),(125,19,'BN','Brunei Darussalam'),(126,19,'BG','Bulgaria'),(127,19,'BF','Burkina Faso'),(128,19,'BI','Burundi'),(129,19,'KH','Cambodia'),(130,19,'CM','Cameroon'),(131,19,'CA','Canada'),(132,19,'CV','Cape Verde'),(133,19,'KY','Cayman Islands'),(134,19,'CF','Central African Republic'),(135,19,'TD','Chad'),(136,19,'CL','Chile'),(137,19,'CN','China'),(138,19,'CX','Christmas Island'),(139,19,'CC','Cocos (Keeling) Islands'),(140,19,'CO','Colombia'),(141,19,'KM','Comoros'),(142,19,'CG','Congo'),(143,19,'CD','Congo, The Democratic Republic Of The'),(144,19,'CK','Cook Islands'),(145,19,'CR','Costa Rica'),(146,19,'CI','Cote D\'Ivoire'),(147,19,'HR','Croatia'),(148,19,'CU','Cuba'),(149,19,'CY','Cyprus'),(150,19,'CZ','Czech Republic'),(151,19,'DK','Denmark'),(152,19,'DJ','Djibouti'),(153,19,'DM','Dominica'),(154,19,'DO','Dominican Republic'),(155,19,'EC','Ecuador'),(156,19,'EG','Egypt'),(157,19,'SV','El Salvador'),(158,19,'GQ','Equatorial Guinea'),(159,19,'ER','Eritrea'),(160,19,'EE','Estonia'),(161,19,'ET','Ethiopia'),(162,19,'FK','Falkland Islands (Malvinas)'),(163,19,'FO','Faroe Islands'),(164,19,'FJ','Fiji'),(165,19,'FI','Finland'),(166,19,'FR','France'),(167,19,'GF','French Guiana'),(168,19,'PF','French Polynesia'),(169,19,'TF','French Southern Territories'),(170,19,'GA','Gabon'),(171,19,'GM','Gambia'),(172,19,'GE','Georgia'),(173,19,'DE','Germany'),(174,19,'GH','Ghana'),(175,19,'GI','Gibraltar'),(176,19,'GR','Greece'),(177,19,'GL','Greenland'),(178,19,'GD','Grenada'),(179,19,'GP','Guadeloupe'),(180,19,'GU','Guam'),(181,19,'GT','Guatemala'),(182,19,'GG','Guernsey'),(183,19,'GN','Guinea'),(184,19,'GW','Guinea-Bissau'),(185,19,'GY','Guyana'),(186,19,'HT','Haiti'),(187,19,'HM','Heard Island And Mcdonald Islands'),(188,19,'VA','Holy See (Vatican City State)'),(189,19,'HN','Honduras'),(190,19,'HK','Hong Kong'),(191,19,'HU','Hungary'),(192,19,'IS','Iceland'),(193,19,'IN','India'),(194,19,'ID','Indonesia'),(195,19,'IR','Iran, Islamic Republic Of'),(196,19,'IQ','Iraq'),(197,19,'IE','Ireland'),(198,19,'IM','Isle Of Man'),(199,19,'IL','Israel'),(200,19,'IT','Italy'),(201,19,'JM','Jamaica'),(202,19,'JP','Japan'),(203,19,'JE','Jersey'),(204,19,'JO','Jordan'),(205,19,'KZ','Kazakhstan'),(206,19,'KE','Kenya'),(207,19,'KI','Kiribati'),(208,19,'KP','Korea, Democratic People\'s Republic Of'),(209,19,'KR','Korea, Republic Of'),(210,19,'KW','Kuwait'),(211,19,'KG','Kyrgyzstan'),(212,19,'LA','Lao People\'s Democratic Republic'),(213,19,'LV','Latvia'),(214,19,'LB','Lebanon'),(215,19,'LS','Lesotho'),(216,19,'LR','Liberia'),(217,19,'LY','Libyan Arab Jamahiriya'),(218,19,'LI','Liechtenstein'),(219,19,'LT','Lithuania'),(220,19,'LU','Luxembourg'),(221,19,'MO','Macao'),(222,19,'MK','Macedonia, The Former Yugoslav Republic Of'),(223,19,'MG','Madagascar'),(224,19,'MW','Malawi'),(225,19,'MY','Malaysia'),(226,19,'MV','Maldives'),(227,19,'ML','Mali'),(228,19,'MT','Malta'),(229,19,'MH','Marshall Islands'),(230,19,'MQ','Martinique'),(231,19,'MR','Mauritania'),(232,19,'MU','Mauritius'),(233,19,'YT','Mayotte'),(234,19,'MX','Mexico'),(235,19,'FM','Micronesia, Federated States Of'),(236,19,'MD','Moldova, Republic Of'),(237,19,'MC','Monaco'),(238,19,'MN','Mongolia'),(239,19,'ME','Montenegro'),(240,19,'MS','Montserrat'),(241,19,'MA','Morocco'),(242,19,'MZ','Mozambique'),(243,19,'MM','Myanmar'),(244,19,'NA','Namibia'),(245,19,'NR','Nauru'),(246,19,'NP','Nepal'),(247,19,'NL','Netherlands'),(248,19,'AN','Netherlands Antilles'),(249,19,'NC','New Caledonia'),(250,19,'NZ','New Zealand'),(251,19,'NI','Nicaragua'),(252,19,'NE','Niger'),(253,19,'NG','Nigeria'),(254,19,'NU','Niue'),(255,19,'NF','Norfolk Island'),(256,19,'MP','Northern Mariana Islands'),(257,19,'NO','Norway'),(258,19,'OM','Oman'),(259,19,'PK','Pakistan'),(260,19,'PW','Palau'),(261,19,'PS','Palestinian Territory, Occupied'),(262,19,'PA','Panama'),(263,19,'PG','Papua New Guinea'),(264,19,'PY','Paraguay'),(265,19,'PE','Peru'),(266,19,'PH','Philippines'),(267,19,'PN','Pitcairn'),(268,19,'PL','Poland'),(269,19,'PT','Portugal'),(270,19,'PR','Puerto Rico'),(271,19,'QA','Qatar'),(272,19,'RE','Reunion'),(273,19,'RO','Romania'),(274,19,'RU','Russian Federation'),(275,19,'RW','Rwanda'),(276,19,'BL','Saint Barthelemy'),(277,19,'SH','Saint Helena'),(278,19,'KN','Saint Kitts And Nevis'),(279,19,'LC','Saint Lucia'),(280,19,'MF','Saint Martin'),(281,19,'PM','Saint Pierre And Miquelon'),(282,19,'VC','Saint Vincent And The Grenadines'),(283,19,'WS','Samoa'),(284,19,'SM','San Marino'),(285,19,'ST','Sao Tome And Principe'),(286,19,'SA','Saudi Arabia'),(287,19,'SN','Senegal'),(288,19,'RS','Serbia'),(289,19,'SC','Seychelles'),(290,19,'SL','Sierra Leone'),(291,19,'SG','Singapore'),(292,19,'SK','Slovakia'),(293,19,'SI','Slovenia'),(294,19,'SB','Solomon Islands'),(295,19,'SO','Somalia'),(296,19,'ZA','South Africa'),(297,19,'GS','South Georgia And The South Sandwich Islands'),(298,19,'ES','Spain'),(299,19,'LK','Sri Lanka'),(300,19,'SD','Sudan'),(301,19,'SR','Suriname'),(302,19,'SJ','Svalbard And Jan Mayen'),(303,19,'SZ','Swaziland'),(304,19,'SE','Sweden'),(305,19,'CH','Switzerland'),(306,19,'SY','Syrian Arab Republic'),(307,19,'TW','Taiwan, Province Of China'),(308,19,'TJ','Tajikistan'),(309,19,'TZ','Tanzania, United Republic Of'),(310,19,'TH','Thailand'),(311,19,'TL','Timor-Leste'),(312,19,'TG','Togo'),(313,19,'TK','Tokelau'),(314,19,'TO','Tonga'),(315,19,'TT','Trinidad And Tobago'),(316,19,'TN','Tunisia'),(317,19,'TR','Turkey'),(318,19,'TM','Turkmenistan'),(319,19,'TC','Turks And Caicos Islands'),(320,19,'TV','Tuvalu'),(321,19,'UG','Uganda'),(322,19,'UA','Ukraine'),(323,19,'AE','United Arab Emirates'),(324,19,'UM','United States Minor Outlying Islands'),(325,19,'UY','Uruguay'),(326,19,'UZ','Uzbekistan'),(327,19,'VU','Vanuatu'),(328,19,'VE','Venezuela'),(329,19,'VN','Viet Nam'),(330,19,'VG','Virgin Islands, British'),(331,19,'VI','Virgin Islands, U.S.'),(332,19,'WF','Wallis And Futuna'),(333,19,'EH','Western Sahara'),(334,19,'YE','Yemen'),(335,19,'ZM','Zambia'),(336,19,'ZW','Zimbabwe'),(337,19,'WA','Wales'),(348,32,'block','block'),(347,32,'page','page'),(349,32,'model','model'),(350,33,'panel_with_header','panel_with_header'),(351,33,'panel_no_header','panel_no_header'),(352,33,'no_panel','no_panel'),(357,34,'Continue','_100'),(358,34,'Switching Protocols','_101'),(359,34,'Processing','_102'),(360,34,'OK','_200'),(361,34,'Created','_201'),(362,34,'Accepted','_202'),(363,34,'Non-Authoritative Information','_203'),(364,34,'No Content','_204'),(365,34,'Reset Content','_205'),(366,34,'Partial Content','_206'),(367,34,'Multi-Status','_207'),(368,34,'Already Reported','_208'),(369,34,'IM Used','_226'),(370,34,'Multiple Choices','_300'),(371,34,'Moved Permanently','_301'),(372,34,'Found','_302'),(373,34,'See Other','_303'),(374,34,'Not Modified','_304'),(375,34,'Use Proxy','_305'),(376,34,'(Unused)','_306'),(377,34,'Temporary Redirect','_307'),(378,34,'Permanent Redirect','_308'),(379,34,'Bad Request','_400'),(380,34,'Unauthorized','_401'),(381,34,'Payment Required','_402'),(382,34,'Forbidden','_403'),(383,34,'Not Found','_404'),(384,34,'Method Not Allowed','_405'),(385,34,'Not Acceptable','_406'),(386,34,'Proxy Authentication Required','_407'),(387,34,'Request Timeout','_408'),(388,34,'Conflict','_409'),(389,34,'Gone','_410'),(390,34,'Length Required','_411'),(391,34,'Precondition Failed','_412'),(392,34,'Payload Too Large','_413'),(393,34,'URI Too Long','_414'),(394,34,'Unsupported Media Type','_415'),(395,34,'Range Not Satisfiable','_416'),(396,34,'Expectation Failed','_417'),(397,34,'Unprocessable Entity','_422'),(398,34,'Locked','_423'),(399,34,'Failed Dependency','_424'),(400,34,'Upgrade Required','_426'),(401,34,'Precondition Required','_428'),(402,34,'Too Many Requests','_429'),(403,34,'Request Header Fields Too Large','_431'),(404,34,'Internal Server Error','_500'),(405,34,'Not Implemented','_501'),(406,34,'Bad Gateway','_502'),(407,34,'Service Unavailable','_503'),(408,34,'Gateway Timeout','_504'),(409,34,'HTTP Version Not Supported','_505'),(410,34,'Variant Also Negotiates','_506'),(411,34,'Insufficient Storage','_507'),(412,34,'Loop Detected','_508'),(413,34,'Not Extended','_510'),(414,34,'Network Authentication Required','_511'),(415,35,'checkbox','_checkbox'),(416,35,'html','_html'),(417,35,'textarea','_textarea'),(418,35,'textfield','_textfield'),(419,35,'numberfield','_numberfield'),(420,35,'decimalfield','_decimalfield'),(421,35,'date','_date'),(422,35,'datetime','_datetime'),(423,35,'time','_time'),(424,35,'dropdown','_dropdown'),(425,35,'model','_model'),(426,35,'model_array','_model_array'),(427,36,'INT','_INT'),(428,36,'TINYINT','_TINYINT'),(429,36,'SMALLINT','_SMALLINT'),(430,36,'MEDIUMINT','_MEDIUMINT'),(431,36,'BIGINT','_BIGINT'),(432,36,'FLOAT','_FLOAT'),(433,36,'DOUBLE','_DOUBLE'),(434,36,'DECIMAL','_DECIMAL'),(435,36,'DATE','_DATE'),(436,36,'DATETIME','_DATETIME'),(437,36,'TIMESTAMP','_TIMESTAMP'),(438,36,'TIME','_TIME'),(439,36,'YEAR','_YEAR'),(440,36,'CHAR','_CHAR'),(441,36,'VARCHAR','_VARCHAR'),(442,36,'BLOB','_BLOB'),(443,36,'TEXT','_TEXT'),(444,36,'TINYBLOB','_TINYBLOB'),(445,36,'TINYTEXT','_TINYTEXT'),(446,36,'MEDIUMBLOB','_MEDIUMBLOB'),(447,36,'MEDIUMTEXT','_MEDIUMTEXT'),(448,36,'LONGBLOB','_LONGBLOB'),(449,36,'LONGTEXT','_LONGTEXT'),(450,36,'ENUM','_ENUM'),(451,37,'pending','pending'),(452,37,'running','running'),(453,37,'completed','completed'),(454,37,'failed','failed'),(455,37,'killed','killed'),(456,38,'full','full'),(458,38,'basic without messages','basic_no_messages'),(459,38,'basic with messages','basic_with_messages');
/*!40000 ALTER TABLE `Core_DropdownElement` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_InstalledExtension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `isEnabled` tinyint(1) NOT NULL,
  `loadPriority` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`,`isEnabled`,`loadPriority`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_InstalledExtension` WRITE;
/*!40000 ALTER TABLE `Core_InstalledExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `Core_InstalledExtension` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_InstalledWebhook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `method` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `endpoint` text COLLATE utf8_unicode_ci NOT NULL,
  `verificationKey` text COLLATE utf8_unicode_ci NOT NULL,
  `isEnabled` tinyint(1) NOT NULL,
  `userType` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model` (`model`,`method`,`isEnabled`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_InstalledWebhook` WRITE;
/*!40000 ALTER TABLE `Core_InstalledWebhook` DISABLE KEYS */;
/*!40000 ALTER TABLE `Core_InstalledWebhook` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_LiveBlock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `isEnabled` tinyint(1) NOT NULL,
  `blockType` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `blockExtensionName` text COLLATE utf8_unicode_ci NOT NULL,
  `panelMode` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `area` int(11) NOT NULL,
  `optionsJson` longtext COLLATE utf8_unicode_ci NOT NULL,
  `allPages` tinyint(1) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`,`isEnabled`,`blockType`,`area`,`allPages`),
  KEY `order` (`order`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_LiveBlock` WRITE;
/*!40000 ALTER TABLE `Core_LiveBlock` DISABLE KEYS */;
REPLACE INTO `Core_LiveBlock` (`id`, `title`, `name`, `isEnabled`, `blockType`, `blockExtensionName`, `panelMode`, `area`, `optionsJson`, `allPages`, `order`) VALUES (6,'Extorio admin :: admin account menu top','Extorio admin :: admin account menu top',1,'CoreExtorioAdminAccountMenuBlock','core','no_panel',4,'',1,1),(5,'Extorio admin :: admin menu left','Extorio admin :: admin menu left',1,'CoreExtorioAdminMenuBlock','core','no_panel',1,'',1,1);
/*!40000 ALTER TABLE `Core_LiveBlock` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_LiveBlockPage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `liveBlockId` int(11) NOT NULL,
  `pageId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `liveBlockId` (`liveBlockId`),
  KEY `pageId` (`pageId`)
) ENGINE=MyISAM AUTO_INCREMENT=446 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_LiveBlockPage` WRITE;
/*!40000 ALTER TABLE `Core_LiveBlockPage` DISABLE KEYS */;
REPLACE INTO `Core_LiveBlockPage` (`id`, `liveBlockId`, `pageId`) VALUES (236,5,32),(237,5,20),(242,5,19),(239,5,40),(420,6,2),(421,6,1),(422,6,6),(423,6,32),(233,5,2),(234,5,1),(238,5,31),(247,5,37),(430,6,45),(246,5,38),(235,5,6),(424,6,20),(425,6,31),(426,6,40),(240,5,25),(427,6,25),(241,5,18),(428,6,18),(429,6,19),(243,5,33),(244,5,3),(245,5,30),(248,5,41),(431,6,33),(432,6,3),(433,6,30),(434,6,38),(435,6,37),(436,6,41),(343,5,45),(438,6,48),(439,5,48);
/*!40000 ALTER TABLE `Core_LiveBlockPage` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_LiveTask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `taskExtensionName` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `taskName` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `failedMessage` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`,`taskExtensionName`,`taskName`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_LiveTask` WRITE;
/*!40000 ALTER TABLE `Core_LiveTask` DISABLE KEYS */;
/*!40000 ALTER TABLE `Core_LiveTask` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_Page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `isEnabled` tinyint(1) NOT NULL,
  `templateDisplayMode` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `templateName` text COLLATE utf8_unicode_ci NOT NULL,
  `templateExtensionName` text COLLATE utf8_unicode_ci NOT NULL,
  `useTemplate` tinyint(1) NOT NULL,
  `requestAddress` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `targetControllerName` text COLLATE utf8_unicode_ci NOT NULL,
  `targetControllerExtensionName` text COLLATE utf8_unicode_ci NOT NULL,
  `targetViewName` text COLLATE utf8_unicode_ci NOT NULL,
  `targetViewExtensionName` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`,`isEnabled`),
  KEY `address` (`requestAddress`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_Page` WRITE;
/*!40000 ALTER TABLE `Core_Page` DISABLE KEYS */;
REPLACE INTO `Core_Page` (`id`, `name`, `title`, `isEnabled`, `templateDisplayMode`, `templateName`, `templateExtensionName`, `useTemplate`, `requestAddress`, `targetControllerName`, `targetControllerExtensionName`, `targetViewName`, `targetViewExtensionName`) VALUES (1,'404 page not found','404 page not found',1,'full','','',1,'/page-not-found/','CorePageNotFoundController','core','CorePageNotFoundView','core'),(2,'403 access denied','403 Access Denied',1,'full','','',1,'/access-denied/','CoreAccessDeniedController','core','CoreAccessDeniedView','core'),(3,'extorio admin :: page manager','Extorio Admin :: Page Manager',1,'full','CoreExtorioAdminTemplate','core',1,'/extorio/admin/pages/','CoreExtorioAdminPagesController','core','CoreExtorioAdminPagesView','core'),(6,'extorio admin :: access management','Extorio Admin :: Access Management',1,'full','CoreExtorioAdminTemplate','core',1,'/extorio/admin/access/','CoreExtorioAdminAccessController','core','CoreExtorioAdminAccessView','core'),(20,'extorio admin :: block manager','Extorio Admin :: Block Manager',1,'full','CoreExtorioAdminTemplate','core',1,'/extorio/admin/blocks/','CoreExtorioAdminBlocksController','core','CoreExtorioAdminBlocksView','core'),(18,'extorio admin :: login','Extorio Admin :: Login',1,'full','CoreExtorioAdminTemplate','core',1,'/extorio/admin/login/','CoreExtorioAdminLoginController','core','CoreExtorioAdminLoginView','core'),(19,'extorio admin :: logout','Extorio Admin :: Logout',1,'','','',0,'/extorio/admin/logout/','CoreExtorioAdminLogoutController','core','CoreExtorioAdminLogoutView','core'),(25,'extorio admin :: log manager','Extorio Admin :: Log Manager',1,'full','CoreExtorioAdminTemplate','core',1,'/extorio/admin/log-manager/','CoreExtorioAdminLogManagerController','core','CoreExtorioAdminLogManagerView','core'),(33,'Extorio admin :: models','Extorio Admin :: Models',1,'full','CoreExtorioAdminTemplate','core',1,'/extorio/admin/models/','CoreExtorioAdminModelsController','core','CoreExtorioAdminModelsView','core'),(30,'Extorio admin :: task manager','Extorio admin :: Task Manager',1,'full','CoreExtorioAdminTemplate','core',1,'/extorio/admin/task-manager/','CoreExtorioAdminTaskManagerController','core','CoreExtorioAdminTaskManagerView','core'),(31,'extorio admin :: dashboard','Extorio Admin :: Dashboard',1,'full','CoreExtorioAdminTemplate','core',1,'/extorio/admin/','CoreExtorioAdminController','core','CoreExtorioAdminView','core'),(32,'extorio admin :: account','Extorio Admin :: Account',1,'full','CoreExtorioAdminTemplate','core',1,'/extorio/admin/account/','CoreExtorioAdminAccountController','core','CoreExtorioAdminAccountView','core'),(37,'Extorio elements :: blocks','Extorio Elements :: Blocks',1,'','','',0,'/extorio/elements/blocks/','CoreExtorioElementsBlocksController','core','CoreExtorioElementsBlocksView','core'),(38,'Extorio elements :: apis','Extorio Elements :: apis',1,'','','',0,'/extorio/elements/apis/','CoreExtorioElementsApisController','core','CoreExtorioElementsApisView','core'),(40,'Extorio admin :: dropdowns','Extorio Admin :: Dropdowns',1,'full','CoreExtorioAdminTemplate','core',1,'/extorio/admin/dropdowns/','CoreExtorioAdminDropdownsController','core','CoreExtorioAdminDropdownsView','core'),(41,'Extorio elements :: tasks','Extorio elements :: tasks',1,'','','',0,'/extorio/elements/tasks/','CoreExtorioElementsTasksController','core','CoreExtorioElementsTasksView','core'),(45,'extorio admin :: model viewer','Extorio Admin :: Model viewer',1,'full','CoreExtorioAdminTemplate','core',1,'/extorio/admin/model-viewer/','CoreExtorioAdminModelViewerController','core','CoreExtorioAdminModelViewerView','core'),(48,'extorio admin :: extensions','Extorio Admin :: Extensions',1,'full','CoreExtorioAdminTemplate','core',1,'/extorio/admin/extensions/','CoreExtorioAdminExtensionsController','core','CoreExtorioAdminExtensionsView','core');
/*!40000 ALTER TABLE `Core_Page` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_Property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `ownerClassId` int(11) NOT NULL,
  `inputType` text COLLATE latin1_general_ci NOT NULL,
  `checkBoxDefault` tinyint(1) NOT NULL,
  `htmlScriptsAllowed` tinyint(1) NOT NULL,
  `dropDownId` int(11) NOT NULL,
  `maxLength` int(11) NOT NULL,
  `phpType` text COLLATE latin1_general_ci NOT NULL,
  `mysqlType` text COLLATE latin1_general_ci NOT NULL,
  `childClassId` int(11) NOT NULL,
  `isPrimary` tinyint(1) NOT NULL,
  `isIndex` tinyint(1) NOT NULL,
  `isUnique` tinyint(1) NOT NULL,
  `visibleInList` tinyint(1) NOT NULL,
  `visibleInDetail` tinyint(1) NOT NULL,
  `visibleInCreate` tinyint(1) NOT NULL,
  `visibleInEdit` tinyint(1) NOT NULL,
  `isCustom` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`,`ownerClassId`,`childClassId`),
  KEY `isCustom` (`isCustom`)
) ENGINE=MyISAM AUTO_INCREMENT=961 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_Property` WRITE;
/*!40000 ALTER TABLE `Core_Property` DISABLE KEYS */;
REPLACE INTO `Core_Property` (`id`, `name`, `ownerClassId`, `inputType`, `checkBoxDefault`, `htmlScriptsAllowed`, `dropDownId`, `maxLength`, `phpType`, `mysqlType`, `childClassId`, `isPrimary`, `isIndex`, `isUnique`, `visibleInList`, `visibleInDetail`, `visibleInCreate`, `visibleInEdit`, `isCustom`, `position`) VALUES (661,'id',77,'numberfield',0,0,0,11,'integer','INT',0,1,1,1,0,0,1,0,0,1),(662,'dateCreated',77,'datetime',0,0,0,0,'string','DATETIME',0,0,0,0,0,1,0,0,0,11),(663,'dateUpdated',77,'datetime',0,0,0,0,'string','DATETIME',0,0,0,0,0,1,0,0,0,10),(664,'line1',77,'textfield',0,0,0,60,'string','VARCHAR',0,0,0,0,1,1,1,1,0,1),(665,'line2',77,'textfield',0,0,0,60,'string','VARCHAR',0,0,0,0,1,1,1,1,0,2),(666,'city',77,'textfield',0,0,0,40,'string','VARCHAR',0,0,0,0,1,1,1,1,0,3),(667,'town',77,'textfield',0,0,0,40,'string','VARCHAR',0,0,0,0,1,1,1,1,0,4),(668,'region',77,'textfield',0,0,0,40,'string','VARCHAR',0,0,0,0,1,1,1,1,0,5),(669,'country',77,'dropdown',0,0,19,60,'string','VARCHAR',0,0,0,0,1,1,1,1,0,6),(670,'postcode',77,'textfield',0,0,0,10,'string','VARCHAR',0,0,0,0,1,1,1,1,0,7),(671,'phone',77,'textfield',0,0,0,16,'string','VARCHAR',0,0,0,0,1,1,1,1,0,8),(672,'fax',77,'textfield',0,0,0,16,'string','VARCHAR',0,0,0,0,1,1,1,1,0,9),(636,'id',72,'numberfield',0,0,0,11,'integer','INT',0,1,1,1,0,0,1,0,0,0),(637,'dateCreated',72,'datetime',0,0,0,0,'string','DATETIME',0,0,0,0,0,1,0,0,0,1),(638,'dateUpdated',72,'datetime',0,0,0,0,'string','DATETIME',0,0,0,0,0,1,0,0,0,2),(639,'username',72,'textfield',0,0,0,60,'string','VARCHAR',0,0,1,0,1,1,1,1,0,3),(640,'password',72,'textfield',0,0,0,60,'string','VARCHAR',0,0,0,0,1,1,1,1,0,4),(641,'email',72,'textfield',0,0,0,256,'string','VARCHAR',0,0,1,0,1,1,1,1,0,5),(642,'emailVerified',72,'checkbox',0,0,0,1,'boolean','TINYINT',0,0,1,0,0,1,1,1,0,6),(643,'canLogin',72,'checkbox',0,0,0,1,'boolean','TINYINT',0,0,1,0,0,1,1,1,0,7),(644,'numLogin',72,'numberfield',0,0,0,8,'integer','INT',0,0,0,0,1,1,0,0,0,8),(645,'dateLogin',72,'datetime',0,0,0,0,'string','DATETIME',0,0,0,0,1,1,0,0,0,9),(646,'accessLevel',72,'dropdown',0,0,18,60,'string','VARCHAR',0,0,0,0,1,1,1,1,0,10),(772,'createdByAdmin',72,'textfield',0,0,0,60,'string','VARCHAR',0,0,0,0,1,1,1,1,1,12),(773,'updatedByAdmin',72,'textfield',0,0,0,60,'string','VARCHAR',0,0,0,0,1,1,1,1,1,12);
/*!40000 ALTER TABLE `Core_Property` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_Relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leftInstanceId` int(11) NOT NULL,
  `leftClassName` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `rightInstanceId` int(11) NOT NULL,
  `rightClassName` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `leftToRightProperty` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `leftInstanceId` (`leftInstanceId`,`leftClassName`,`rightInstanceId`,`rightClassName`,`leftToRightProperty`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_Relationship` WRITE;
/*!40000 ALTER TABLE `Core_Relationship` DISABLE KEYS */;
/*!40000 ALTER TABLE `Core_Relationship` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `Core_WebhookRequest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intalledWebhookId` int(11) NOT NULL,
  `generalError` text COLLATE utf8_unicode_ci NOT NULL,
  `outDataJson` longtext COLLATE utf8_unicode_ci NOT NULL,
  `curlError` text COLLATE utf8_unicode_ci NOT NULL,
  `responseStatus` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `$intalledWebhookId` (`intalledWebhookId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `Core_WebhookRequest` WRITE;
/*!40000 ALTER TABLE `Core_WebhookRequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `Core_WebhookRequest` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

