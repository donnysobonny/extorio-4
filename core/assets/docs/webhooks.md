Overview
--------

All complex models can have webhooks assigned to them on either the create, update or delete modification event. Webhooks
are designed to notify external systems of changes to a certain complex model.


How it works
------------

The process is quite simple:
    1 - The create/update/delete event is triggered on a complex model object
    2 - A background task is run on the CoreWebhookTaskController, passing the model, event type and record id.
    3 - The installed webhooks on the target model and event type are found. These webhooks contain the endpoint url
    that the webhook is to send the notification to.
    4 - The notification is sent to the endpoint url, containing the model, method (event type) and object data. Also
    included in the data sent to the endpoint url is a token and a unix time stamp (used to verify the webhook)
    5 - The receiving end is to response with a http status code (eg: 200)
    6 - Information on the webhook request is stored locally in Extorio for monitoring of webhook requests.


Creating a webhook
------------------

To create a webhook, log in as a super administrator and click on "webhooks" from the admin dropdown menu. Here you can
view, create and edit webhooks.

When creating/editing a webhook, you will enter the following information:

name - an informative name to describe the purpose of the webhook
model - the complex model that this webhook is targeting
method - the modification event of the complex model that this webhook is targeting (create/update/delete)
endpoint - the url that this webhook will send the notification to
verification key - a key that is used to generate the "token" sent in the notification to verify that the webhook has come from us
enabled - whether the webhook is enabled or not
user - the user that is used to action the webhook. This effects how data is sent in the notification based on the user access privelleges of the selected user.


Recieving a webhook
-------------------

The data that is sent in a webhook is in JSON format, and can be retrieved like do:

$data = json_decode(file_get_contents("php://input"),true); //the second param "true" will set $data to an array

The data will always contain the following information:
model - the complex model that is being modified
method - the modification event (create/update/delete)
token - the token used to verify that the notification is valid
time - the time, used to verify the token
data - the data of the newly modified object. If the method is "delete", then this will only contain the id of the object being removed

Verifying the token
-------------------

Before verifying the token, you must know the verification key of the webhook set in Extorio. Once you know this, you can verify the token
by doing the following:

$verification_key = "extorio_webhook"; //the verification key of this webhook set in Extorio
if($data["token"] == md5($verification_key.":".$data["time"])) {
    //verified!
}

It is important to verify the token, to make sure that the notification was genuinely sent from Extorio.

Example
-------

Below is a full example of recieving a webhook notification from Extorio

//get the data from the input stream
$data = json_decode(file_get_contents("php://input"),true); //the second param "true" will set $data to an array

//data will be null if it could not be converted
if($data == null) {
    die('could not get data from input stream!');
}

//validate the token
$verification_key = "extorio_webhook"; //the verification key of this webhook set in Extorio
if($data["token"] == md5($verification_key.":".$data["time"])) {

    //the modified object is located in the "data" field of $data:
    $modifiedObject = $data["data"];

    //do what you want with the modified object data here, based on the $data["method"] value (event type)

}

//optionally return a http status code other than 200 here if you want to notify extorio of a problem. Otherwise your
//server will return 200 (200 represents an "OK" status) to extorio.


Reviewing webhook notifications in Extorio
------------------------------------------

You can view past sent webhook notifications in "Webhook requests" in the admin dropdown menu while logged in as a
super admin.

Here you can see which webhook was used, and information such as any errors, the out data that was sent and the user
that was used to action the webhook.

Webhooks are run as background tasks. Therefore it might be useful to also check the "Task manager" section (also found
in the admin dropdown menu for super admins) in cases where webhooks aren't working. If a webhook doesn't work, it is
most likely because there was a problem running the background task. Therefore the task manager should shed some light.
