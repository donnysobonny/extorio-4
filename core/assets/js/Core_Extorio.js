function Spinner() {
    this.showFullPageSpinner_white = function () {
        //remove any existing full page spinner
        var html = ' ' +
            '<div style="display: none;" id="extorio_full_page_spinner_outer">' +
            '   <div id="extorio_full_page_spinner_inner">' +
            '       <img src="/core/assets/images/spinner_white.png" alt="loading..." />' +
            '   </div>' +
            '</div>';
        $('#extorio_full_page_spinner_outer').remove();
        $('body').append(html);
        $('#extorio_full_page_spinner_outer').fadeIn(150);
    };

    this.showFullPageSpinner_black = function () {
        //remove any existing full page spinner
        var html = ' ' +
            '<div style="display: none;" id="extorio_full_page_spinner_outer">' +
            '   <div id="extorio_full_page_spinner_inner">' +
            '       <img src="/core/assets/images/spinner_black.png" alt="loading..." />' +
            '   </div>' +
            '</div>';
        $('#extorio_full_page_spinner_outer').remove();
        $('body').append(html);
        $('#extorio_full_page_spinner_outer').fadeIn(150);
    };

    this.hideFullPageSpinner = function () {
        $('#extorio_full_page_spinner_outer').fadeOut(150, function () {
            $('#extorio_full_page_spinner_outer').remove();
        });
    }

    this.removeFullPageSpinner = function () {
        $('#extorio_full_page_spinner_outer').remove();
    };

    this.appendSpinner_white = function (selector) {
        var html = ' ' +
            '<div style="display: none;" class="extorio_appended_spinner_outer">' +
            '   <div class="extorio_appended_spinner_inner">' +
            '       <img src="/core/assets/images/spinner_white.png" alt="loading...">' +
            '   </div>' +
            '</div>';
        $(selector + " .extorio_appended_spinner_outer").remove();
        $(selector).append(html);
        $(selector + " .extorio_appended_spinner_outer").fadeIn(150);
    };

    this.appendSpinner_black = function (selector) {
        var html = ' ' +
            '<div style="display: none;" class="extorio_appended_spinner_outer">' +
            '   <div class="extorio_appended_spinner_inner">' +
            '       <img src="/core/assets/images/spinner_black.png" alt="loading...">' +
            '   </div>' +
            '</div>';
        $(selector + " .extorio_appended_spinner_outer").remove();
        $(selector).append(html);
        $(selector + " .extorio_appended_spinner_outer").fadeIn(150);
    };

    this.hideSpinner = function (selector, onHide) {
        $(selector + " .extorio_appended_spinner_outer").fadeOut(150, function () {
            onHide();
            $(selector + " .extorio_appended_spinner_outer").remove();
        });
    };

    this.removeSpinner = function (selector) {
        $(selector + " .extorio_appended_spinner_outer").remove();
    };
}

function Modal() {
    this.openModal = function (modalSize, titleText, messageText, closeButtonText, continueButtonText, onContinue, onClose) {
        var size = "";
        if (modalSize === "large") {
            size = "modal-lg";
        } else if (modalSize === "small") {
            size = "modal-sm";
        }
        var html = ' ' +
            '<div id="extorio_modal" class="modal fade">' +
            '   <div class="modal-dialog ' + size + '">' +
            '       <div class="modal-content">' +
            '           <div class="modal-header">' +
            '               <button type="button" class="close" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <span class="sr-only">Close</span></button>' +
            '               <h4 class="modal-title"> ' + titleText + ' </h4>' +
            '           </div>' +
            '           <div class="modal-body">' +
            '               ' + messageText +
            '           </div>' +
            '           <div class="modal-footer">' +
            '               <button type="button" class="btn btn-default modal-close" data-dismiss="modal">' + closeButtonText + '</button>' +
            '               <button type="button" class="btn btn-primary modal-continue">' + continueButtonText + '</button>' +
            '           </div>' +
            '       </div><!-- /.modal-content -->' +
            '   </div><!-- /.modal-dialog -->' +
            '</div><!-- /.modal -->';
        $('#extorio_modal').remove();
        $('body').append(html);
        $('#extorio_modal').modal();
        $('#extorio_modal .modal-continue').bind("click", onContinue);
        $('#extorio_modal .modal-continue').bind("click", function () {
            $('#extorio_modal').modal('hide');
        });
        $('#extorio_modal .modal-close').bind("click", onClose);
        $('#extorio_modal .modal-close').bind("click", function () {
            $('#extorio_modal').modal('hide');
        });
    };
}

function Dialog() {
    this.openDialog = function (dialogSize, titleText, messageText, footerEnabled, closeButtonText, onClose) {
        var size = "";
        if (dialogSize === "large") {
            size = "modal-lg";
        } else if (dialogSize === "small") {
            size = "modal-sm";
        }
        var html = ' ' +
            '<div id="extorio_dialog" class="modal fade">' +
            '   <div class="modal-dialog ' + size + '">' +
            '       <div class="modal-content">' +
            '           <div class="modal-header">' +
            '               <button type="button" class="close" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <span class="sr-only">Close</span></button>' +
            '               <h4 class="modal-title"> ' + titleText + ' </h4>' +
            '           </div>' +
            '           <div class="modal-body">' +
            '               ' + messageText +
            '           </div>';
        if (footerEnabled) {
            html += '           <div class="modal-footer">' +
                '               <button type="button" class="btn btn-default modal-close" data-dismiss="modal">' + closeButtonText + '</button>' +
                '           </div>';
        }
        html += '       </div><!-- /.modal-content -->' +
            '   </div><!-- /.modal-dialog -->' +
            '</div><!-- /.modal -->';
        $('#extorio_dialog').remove();
        $('body').append(html);
        $('#extorio_dialog').modal();
        $('#extorio_dialog .modal-close').bind("click", onClose);
        $('#extorio_dialog .modal-close').bind("click", function () {
            $('#extorio_dialog').modal('hide');
        });
    };
}

function Frame() {
    this.openFrame = function (frameSize, titleText, frameUrl, onClose) {
        var size = "";
        if (frameSize === "large") {
            size = "modal-lg";
        } else if (frameSize === "small") {
            size = "modal-sm";
        }
        var html = ' ' +
            '<div id="extorio_frame" class="modal fade">' +
            '   <div class="modal-dialog ' + size + '">' +
            '       <div class="modal-content">' +
            '           <div class="modal-header">' +
            '               <button type="button" class="close" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <span class="sr-only">Close</span></button>' +
            '               <h4 class="modal-title"> ' + titleText + ' </h4>' +
            '           </div>' +
            '           <div class="modal-body">' +
            '               <div id="extorio_frame_preloader">' +
            '                   ' +
            '               </div>' +
            '               <iframe scrolling="no" style="width: 100%; display: none;" id="bootstrap_frame_frame" frameborder="0" seamless="seamless" src="' + frameUrl + '"></iframe>' +
            '           </div>' +
            '       </div><!-- /.modal-content -->' +
            '   </div><!-- /.modal-dialog -->' +
            '</div><!-- /.modal -->';
        $('#extorio_frame').remove();
        $('body').append(html);
        Core_Extorio.Spinner.appendSpinner_black('#extorio_frame_preloader');
        $('#extorio_frame').modal();
        if (onClose !== undefined) {
            $('#extorio_frame').on("hidden.bs.modal", function () {
                onClose();
            });
        }

        $('#extorio_frame iframe').load(function () {
            var mydiv = $(this).contents().find("html");
            //whenever the body changes, set the height of the frame
            mydiv.bind("DOMSubtreeModified", function () {
                $('#extorio_frame iframe').height($(this).height());
            });
            setTimeout(function () {
                $('#extorio_frame_preloader').remove();
                $('#extorio_frame iframe').fadeIn();
                $('#extorio_frame iframe').height($('#extorio_frame iframe').contents().find("html").height());
                setInterval(function () {
                    $('#extorio_frame iframe').height($('#extorio_frame iframe').contents().find("html").height());
                }, 1000);
            }, 1000);
        });
    };
}

function Core_Extorio() {
    this.Spinner = new Spinner();
    this.Modal = new Modal();
    this.Dialog = new Dialog();
    this.Frame = new Frame();
}

Core_Extorio = new Core_Extorio();

//on page load
$(function() {
    //fix for multiple bootstrap modals
    $('.modal').on('show.bs.modal', function(event) {
        var idx = $('.modal:visible').length;
        $(this).css('z-index', 1040 + (10 * idx));
    });
    $('.modal').on('shown.bs.modal', function(event) {
        $('.modal-backdrop').not('.stacked').addClass('stacked');
    });

    //enable all tooltips
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
});
