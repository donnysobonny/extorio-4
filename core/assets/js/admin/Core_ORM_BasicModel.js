function Core_ORM_BasicModel() {

    this.runAjax = function (endpoint, type, postData, onSuccess) {
        $.ajax({
            url: "/extorio/elements/apis/core/basic-models" + endpoint,
            dataType: "json",
            type: type,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            data: postData,
            error: function(a,b,c) {
                alert(c);
            },
            success: function(data) {
                if(typeof onSuccess === "function") {
                    onSuccess(data.data);
                }
            }
        });
    };

    this.findAll = function(model,options,onSuccess) {
        this.runAjax("/"+model,"GET",options,onSuccess);
    };

    this.findCount = function(model, options, onSuccess) {
        this.runAjax("/"+model+"/count","GET",options,onSuccess);
    };

    this.findById = function (model, id, onSuccess) {
        this.runAjax("/" + model + "/" + id, "GET", {}, onSuccess);
    };

    this.findOne = function (model, query, onSuccess) {
        this.findAll(model,{
            "query":query
        },onSuccess);
    };

    this.create = function (model, object_data, onSuccess) {
        this.runAjax("/"+model,"POST",{
            "object_data": object_data
        },onSuccess);
    };

    this.update = function (model, object_data, onSuccess) {
        this.runAjax("/"+model,"PUT",{
            "object_data": object_data
        },onSuccess);
    };

    this.remove = function (model, id, onSuccess) {
        this.runAjax("/" + model + "/" + id, "DELETE", {}, onSuccess);
    };

}

Core_ORM_BasicModel = new Core_ORM_BasicModel();