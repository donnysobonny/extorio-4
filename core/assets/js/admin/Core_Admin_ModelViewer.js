function initEditFormInputs() {
    //set html inputs to use froala
    $('.model_view_input_html').editable({inlineMode: false});

    //set dates to uset the datetimepicker
    $('.model_view_input_date').datetimepicker({
        format: "Y-m-d",
        timepicker: false,
        yearStart: 1900,
        yearEnd: 2050
    });
    $('.model_view_input_datetime').datetimepicker({
        format: "Y-m-d H:i:00",
        yearStart: 1900,
        yearEnd: 2050,
        step: 15
    });
    $('.model_view_input_time').datetimepicker({
        format: "H:i:00",
        datepicker: false,
        step: 1
    });

    //only allow numbers in number inputs
    $('.model_view_input_numberfield').on('keypress', function (ev) {
        var keyCode = window.event ? ev.keyCode : ev.which;
        //codes for 0-9
        if (keyCode < 48 || keyCode > 57) {
            //codes for backspace, delete, enter
            if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
                ev.preventDefault();
            }
        }
    });

    //allow numbers and dots for decimals
    $('.model_view_input_decimalfield').on('keypress', function (ev) {
        var keyCode = window.event ? ev.keyCode : ev.which;
        //codes for 0-9
        if (keyCode < 48 || keyCode > 57) {
            //codes for backspace, delete, enter and period
            if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey && keyCode != 46) {
                ev.preventDefault();
            }
        }
    });
}

//create the jquery function for loading the edit form
$.fn.loadEditForm = function (modelName, options) {
    var selected = this;
    $.ajax({
        url: "/extorio/elements/apis/core/model-viewer/" + modelName + "/edit",
        dataType: "json",
        type: "GET",
        data: options,
        error: function (a, b, c) {
            alert(c);
        },
        success: function (data) {
            //make sure no errors or bad status
            if (data.error) {
                alert(data.error_message);
            } else if (data.status_code != 200) {
                alert(data.status_message);
            } else {
                selected.html(data.data);
                initEditFormInputs();
            }
        }
    });
};

//create the jquery function for appending an edit form
$.fn.appendEditForm = function (modelName, options) {
    var selected = this;
    $.ajax({
        url: "/extorio/elements/apis/core/model-viewer/" + modelName + "/edit",
        dataType: "json",
        type: "GET",
        data: options,
        error: function (a, b, c) {
            alert(c);
        },
        success: function (data) {
            //make sure no errors or bad status
            if (data.error) {
                alert(data.error_message);
            } else if (data.status_code != 200) {
                alert(data.status_message);
            } else {
                selected.append(data.data);
                initEditFormInputs();
            }
        }
    });
};

//create the jquery function for loading a model in detail
$.fn.loadDetail = function (modelName, options) {
    var selected = this;
    $.ajax({
        url: "/extorio/elements/apis/core/model-viewer/" + modelName + "/detail",
        dataType: "json",
        type: "GET",
        data: options,
        error: function (a, b, c) {
            alert(c);
        },
        success: function (data) {
            //make sure no errors or bad status
            if (data.error) {
                alert(data.error_message);
            } else if (data.status_code != 200) {
                alert(data.status_message);
            } else {
                selected.html(data.data);
            }
        }
    });
};

//create the jquery function for appending a model in detail
$.fn.appendDetail = function (modelName, options) {
    var selected = this;
    $.ajax({
        url: "/extorio/elements/apis/core/model-viewer/" + modelName + "/detail",
        dataType: "json",
        type: "GET",
        data: options,
        error: function (a, b, c) {
            alert(c);
        },
        success: function (data) {
            //make sure no errors or bad status
            if (data.error) {
                alert(data.error_message);
            } else if (data.status_code != 200) {
                alert(data.status_message);
            } else {
                selected.append(data.data);
            }
        }
    });
};

//create the jquery function for loading a list
$.fn.loadList = function (modelName, options) {
    var selected = this;
    $.ajax({
        url: "/extorio/elements/apis/core/model-viewer/" + modelName + "/list",
        dataType: "json",
        type: "GET",
        data: options,
        error: function (a, b, c) {
            alert(c);
        },
        success: function (data) {
            //make sure no errors or bad status
            if (data.error) {
                alert(data.error_message);
            } else if (data.status_code != 200) {
                alert(data.status_message);
            } else {
                selected.html(data.data);
            }
        }
    });
};

