function updateProperty(PROPERTYID) {
    showFullPageLoader();

    $.ajax({
        url: '/admin/ORM/settings-ajax/',
        error: function(a,b,c) {
            showDialog("Ajax Error!",c,"Close");
        },
        type: "POST",
        data: {
            "action" : "update_property",
            "property_id" : PROPERTYID,
            "indexed" : $('#indexed_'+PROPERTYID).prop("checked"),
            "unique" : $('#unique_'+PROPERTYID).prop("checked"),
            "list" : $('#list_'+PROPERTYID).prop("checked"),
            "detail" : $('#detail_'+PROPERTYID).prop("checked"),
            "create" : $('#create_'+PROPERTYID).prop("checked"),
            "edit" : $('#edit_'+PROPERTYID).prop("checked")
        }
    }).done(function(){
        hideFullPageLoader();
    });
}

function deleteProperty(PROPERTYID) {
    showModal("Delete field?","Deleting a field will also remove all data related to id. Are you sure you want to delete this field?","Cancel","Continue",function() {
        deletePropertyCommit(PROPERTYID);
    });
}

function deletePropertyCommit(PROPERTYID) {
    showFullPageLoader();

    $.ajax({
        url: '/admin/ORM/settings-ajax/',
        error: function(a,b,c) {
            showDialog("Ajax Error!",c,"Close");
        },
        type: "POST",
        data: {
            "action" : "delete_property",
            "property_id" : PROPERTYID
        }
    }).done(function(){
        hideFullPageLoader();
        //remove the row
        var tr = $('#field_row_'+PROPERTYID);
        //re init the table with the row removed
        $('#field_table').DataTable().row(tr).remove().draw();
    });
}

function selectCheckbox() {
    $('#checkbox_default_box').fadeIn();

    $('#script_tags_allowed_box').fadeOut();
    $('#dropdown_id_box').fadeOut();
    $('#child_class_id_box').fadeOut();
    $('#max_length_box').fadeOut();
}

function selectTextfield() {
    $('#max_length_box').fadeIn();

    $('#script_tags_allowed_box').fadeOut();
    $('#dropdown_id_box').fadeOut();
    $('#child_class_id_box').fadeOut();
    $('#checkbox_default_box').fadeOut();
}

function selectTextarea() {
    $('#max_length_box').fadeIn();

    $('#script_tags_allowed_box').fadeOut();
    $('#dropdown_id_box').fadeOut();
    $('#child_class_id_box').fadeOut();
    $('#checkbox_default_box').fadeOut();
}

function selectNumberfield() {
    $('#max_length_box').fadeIn();

    $('#script_tags_allowed_box').fadeOut();
    $('#dropdown_id_box').fadeOut();
    $('#child_class_id_box').fadeOut();
    $('#checkbox_default_box').fadeOut();
}

function selectDecimalfield() {
    $('#script_tags_allowed_box').fadeOut();
    $('#dropdown_id_box').fadeOut();
    $('#child_class_id_box').fadeOut();
    $('#checkbox_default_box').fadeOut();
    $('#max_length_box').fadeOut();
}

function selectDate() {
    $('#script_tags_allowed_box').fadeOut();
    $('#dropdown_id_box').fadeOut();
    $('#child_class_id_box').fadeOut();
    $('#checkbox_default_box').fadeOut();
    $('#max_length_box').fadeOut();
}

function selectDatetime() {
    $('#script_tags_allowed_box').fadeOut();
    $('#dropdown_id_box').fadeOut();
    $('#child_class_id_box').fadeOut();
    $('#checkbox_default_box').fadeOut();
    $('#max_length_box').fadeOut();
}

function selectTime() {
    $('#script_tags_allowed_box').fadeOut();
    $('#dropdown_id_box').fadeOut();
    $('#child_class_id_box').fadeOut();
    $('#checkbox_default_box').fadeOut();
    $('#max_length_box').fadeOut();
}

function selectHtml() {
    $('#script_tags_allowed_box').fadeIn();

    $('#dropdown_id_box').fadeOut();
    $('#child_class_id_box').fadeOut();
    $('#checkbox_default_box').fadeOut();
    $('#max_length_box').fadeOut();
}

function selectDropdown() {
    $('#dropdown_id_box').fadeIn();

    $('#child_class_id_box').fadeOut();
    $('#checkbox_default_box').fadeOut();
    $('#max_length_box').fadeOut();
    $('#script_tags_allowed_box').fadeOut();
}

function selectModel() {
    $('#child_class_id_box').fadeIn();

    $('#checkbox_default_box').fadeOut();
    $('#max_length_box').fadeOut();
    $('#script_tags_allowed_box').fadeOut();
    $('#dropdown_id_box').fadeOut();
}

$(function(){
    $('#input_type').bind("change",function(){
        switch($(this).val()) {
            case "checkbox" :
                selectCheckbox();
                break;
            case "html" :
                selectHtml();
                break;
            case "textarea" :
                selectTextarea()
                break;
            case "textfield" :
                selectTextfield();
                break;
            case "numberfield" :
                selectNumberfield();
                break;
            case "decimalfield" :
                selectDecimalfield();
                break;
            case "date" :
                selectDate();
                break;
            case "datetime" :
                selectDatetime();
                break;
            case "time" :
                selectTime();
                break;
            case "dropdown" :
                selectDropdown();
                break;
            case "model" :
                selectModel();
                break;
            case "model_array" :
                selectModel();
                break;
        }
    });
});
