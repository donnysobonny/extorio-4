<?php
final class CoreExtorioAdminMenuBlock extends Core_Block {
    public function canEdit() {
        //no options
        return false;
    }

    public function canView() {
        //only admins
        return Core_Admin_User::authenticate();
    }

    public function onDefault() {
        //get the menu items
        $results = $this->Extorio()->getEvent("extorio_admin_menu_get_menu_items")->run();

        $itemSets = array();
        //merge all of the base keys together
        foreach($results as $result) {
            $itemSets[] = $result;
        }
        
        ?>
        <style>
            body {
                padding-left: 230px;
                padding-top: 42px;
            }

            #extorio_admin_menu_top_banner {
                display: block;
                height: 42px;
                left: 230px;
                position: absolute;
                right: 0;
                top: 0;
            }

            #extorio_admin_menu_container {
                bottom: 0;
                left: 0;
                padding: 5px;
                position: fixed;
                top: 0;
                width: 230px;
            }

            #extorio_admin_menu_container .panel {
                margin-bottom: 5px;
            }

            #extorio_admin_menu_container .panel-heading {
                padding: 3px 9px;
                cursor: pointer;
            }

            #extorio_admin_menu_container .panel-body {
                padding: 4px;
            }

            #extorio_admin_menu_container .list-group {
                margin-bottom: 0px;
            }

            #extorio_admin_menu_container .list-group-item {
                padding: 10px 9px;
            }

            #extorio_admin_menu_container .list-group-item-heading {
                font-size: 13px;
                font-weight: 500;
                margin: 0;
            }

            #extorio_admin_menu_container .extorio_admin_menu_item_description {
                color: #666;
                font-size: 11px;
                font-style: italic;
                margin-bottom: 4px;
                padding: 2px;
            }
        </style>
        <div id="extorio_admin_menu_top_banner" class="bg-primary"></div>
        <div id="extorio_admin_menu_container" class="bg-primary">
            <?php
            $openSets = isset($_COOKIE["extorio_admin_menu_open_sets"])?json_decode($_COOKIE["extorio_admin_menu_open_sets"],true):array();
            $set = 0;
            foreach($itemSets as $itemSet) {
                $in = in_array($set,$openSets) ? "in" : "";
                $expanded = $in == "in" ? "true" : "false";
                ?>
            <div class="panel panel-success">
                <?php
                if(isset($itemSet["display_header"]) && $itemSet["display_header"] == true) {
                    ?>
                    <div data-set="<?=$set?>" data-toggle="collapse" data-target="#<?=$set?>_collapse" aria-expanded="<?=$expanded?>" aria-controls="<?=$set?>_collapse" class="panel-heading collapse_toggle">
                        <h3 class="panel-title"><?php
                            if(isset($itemSet["header_icon_class"]) && strlen($itemSet["header_icon_class"])) {
                                ?>
                            <span class="<?=$itemSet["header_icon_class"]?>"></span>
                                <?php
                            } else {
                                ?>
                                <span class="glyphicon glyphicon-unchecked"></span>
                            <?php
                            }
                            ?> <?=$itemSet["header_label"]?></h3>
                    </div>
                    <?php
                }
                ?>

                <div id="<?=$set?>_collapse" class="panel-body collapse <?=$in?>">
                    <?php
                    if(isset($itemSet["description"]) && strlen($itemSet["description"])) {
                        ?>
                    <div class="extorio_admin_menu_item_description">
                        <?=$itemSet["description"]?>
                    </div>
                        <?php
                    }
                    ?>
                    <div class="list-group">
                    <?php
                    foreach($itemSet["items"] as $item) {
                        $iconClass = strlen($item["icon_class"])?$item["icon_class"]:"glyphicon glyphicon-unchecked";
                        if(isset($item["description"]) && strlen($item["description"])) {
                            ?>
                            <a data-toggle="tooltip" data-placement="right" title="<?=$item["description"]?>" href="<?=$item["href"]?>" class="list-group-item <?php
                            if(strpos(Core_Utils_Server::getRequestURI(),$item["href_active"]) !== false) {
                                echo "list-group-item-success";
                            }
                            ?>">
                                <h4 class="list-group-item-heading"><span class="<?=$iconClass?>"></span>&nbsp;&nbsp;<?=$item["label"]?></h4>
                            </a>
                        <?php
                        } else {
                            ?>
                            <a href="<?=$item["href"]?>" class="list-group-item <?php
                            if(strpos(Core_Utils_Server::getRequestURI(),$item["href_active"]) !== false) {
                                echo "list-group-item-success";
                            }
                            ?>">
                                <h4 class="list-group-item-heading"><span class="<?=$iconClass?>"></span>&nbsp;&nbsp;<?=$item["label"]?></h4>
                            </a>
                        <?php
                        }
                    }
                    ?>
                    </div>
                </div>
            </div>
                <?php
                $set++;
            }
            ?>
        </div>
        <script>
            $(function() {

                $.cookie.json = true;

                function setOpenSetsCookie() {
                    var openMenuItems = [];
                    $('.collapse_toggle').each(function() {
                        if($(this).attr("aria-expanded") == "true") {
                            openMenuItems.push($(this).attr("data-set"));
                        }
                    });
                    $.cookie("extorio_admin_menu_open_sets",openMenuItems,{path: '/'});
                }

                $('.collapse').on('shown.bs.collapse', function () {
                    setOpenSetsCookie();
                }).on('hidden.bs.collapse', function () {
                    setOpenSetsCookie();
                });

            })
        </script>
        <?php
    }




}