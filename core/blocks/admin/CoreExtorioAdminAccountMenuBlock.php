<?php
final class CoreExtorioAdminAccountMenuBlock extends Core_Block {
    public function onStart() {

    }

    public function onLoad() {

    }

    public function onDefault() {
        //get the logged in admin
        $loggedInAdmin = Core_Admin_User::getLoggedInUser();
        ?>
        <style>
            #extorio_admin_account_menu {
                color: #808080;
                text-align: right;
            }
        </style>
<div id="extorio_admin_account_menu">
    <?php
    if(!$loggedInAdmin) {
        ?>
        Not logged in as admin. <a href="<?=$this->getUrlToPageByName("extorio admin :: login","",array(),array(
            "r" => Core_Utils_Server::getRequestURI()
        ))?>"><span class="glyphicon glyphicon-chevron-right"></span> Log in</a>
        <?php
    } else {
        ?>
        Logged in as <?=$loggedInAdmin->username?> (admin)
        <a href="<?=$this->getUrlToPageByName("extorio admin :: dashboard")?>"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a>
        <a href="<?=$this->getUrlToPageByName("extorio admin :: account")?>"><span class="glyphicon glyphicon-user"></span> Account</a>
        <a href="<?=$this->getUrlToPageByName("extorio admin :: logout")?>"><span class="glyphicon glyphicon-off"></span> Logout</a>
        <?php
    }
    ?>
</div>
        <p></p>
        <?php
    }

    public function options() {

    }

    public function onComplete() {

    }

    public function canView() {

    }

    public function canEdit() {
        return false;
    }
}