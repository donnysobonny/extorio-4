<?php
class CoreExtorioInstallController extends Core_ControlView {

    public $action = false;

    public $dbHost;
    public $dbUser;
    public $dbPass;
    public $dbDatabase;
    public $dbFailed = false;

    public $dataImported = false;

    public $siteAddress = "";
    public $siteName = "";

    public function onLoad() {
        if(isset($_GET["action"])) {
            $this->action = $_GET["action"];
        }
    }

    public function onDefault() {
        if($this->action) {
            $action = $this->action;
            $this->$action();
        }
    }

    public function onComplete() {

    }

    public function database() {
        $coreModule = $this->Extorio()->getLoadedCoreModule();

        if(isset($_REQUEST["database_setup_submitted"])) {
            $currentConfig = $coreModule->getConfig();
            $currentConfigOverride = $coreModule->getConfigOverride();
            $currentConfigOverride["db_defaults"] = array(
                "connection_name" => "default",
                "database" => $_REQUEST["database"],
                "username" => $_REQUEST["username"],
                "password" => $_REQUEST["password"],
                "host" => $_REQUEST["host"],
                "engine" => $currentConfig["db_defaults"]["engine"],
                "table_charset" => $currentConfig["db_defaults"]["table_charset"],
                "table_collate" => $currentConfig["db_defaults"]["table_collate"]
            );

            $coreModule->setConfigOverride($currentConfigOverride);
        }

        $config = $coreModule->getConfig();
        $config = $config["db_defaults"];
        $this->dbHost = $config["host"];
        $this->dbUser = $config["username"];
        $this->dbPass = $config["password"];
        $this->dbDatabase = $config["database"];

        //try and connect
        try{
            $db = new Core_Db($this->dbDatabase,$this->dbHost,$this->dbPass,$this->dbUser);
        } catch(Core_Db_Exception $ex) {
            $this->dbFailed = $ex->getMessage();
        }
    }

    public function dataimport() {

        $coreModule = $this->Extorio()->getLoadedCoreModule();

        if($_GET["import"] == "true") {
            $importSql = Core_Utils_File::readFromFile("core/assets/Core_DataImport.sql");



            $commands = explode(";",$importSql);
            //remove last element
            array_pop($commands);

            $db = $this->Extorio()->getDbConnectionDefault();
            $error = false;
            foreach($commands as $command) {
                try{
                    $db->execute($command);
                } catch(Exception $ex) {
                    $error = $ex->getMessage();
                }
            }
            if($error) {
                $this->addErrorMessage($error);
            } else {
                $this->addInfoMessage("Data imported successfully");

                //update the override
                $currentOverride = $coreModule->getConfigOverride();
                $currentOverride["data_imported"] = true;
                $coreModule->setConfigOverride($currentOverride);
            }
        }
        $coreConfigAll = $coreModule->getConfig();

        $this->dataImported = $coreConfigAll["data_imported"];
    }

    public function sitesettings() {
        $coreModule = $this->Extorio()->getLoadedCoreModule();

        if(isset($_REQUEST["site_setup_submitted"])) {
            $coreConfigOverride = $coreModule->getConfigOverride();

            $coreConfigOverride["site"]["address"] = $_REQUEST["address"];
            $coreConfigOverride["site"]["name"] = $_REQUEST["name"];

            $coreModule->setConfigOverride($coreConfigOverride);

            $this->addInfoMessage("Site settings updated");
        }

        $coreConfigAll = $coreModule->getConfig();
        $coreConfigOverride = $coreModule->getConfigOverride();

        //set the site name automatically for convenience
        if($coreConfigAll["site"]["address"] == "") {
            $coreConfigOverride["site"]["address"] = Core_Utils_Server::getDomain();
            $coreModule->setConfigOverride($coreConfigOverride);

            //re get the config
            $coreConfigAll = $coreModule->getConfig();
        }

        $this->siteAddress = $coreConfigAll["site"]["address"];
        $this->siteName = $coreConfigAll["site"]["name"];
    }

    public function adminsetup() {
        $coreModule = $this->Extorio()->getLoadedCoreModule();

        if(isset($_REQUEST["admin_setup_submitted"])) {

            $username = $_REQUEST["username"];
            $email = $_REQUEST["email"];
            $password = $_REQUEST["password"];
            $cpassword = $_REQUEST["cpassword"];

            $error = false;

            //make sure the passwords match
            if($password != $cpassword) {
                $error = "Passwords do not match";
            }

            $admin = new Core_Admin_User();

            if(!$error) {
                $admin->username = $username;
                $admin->password = $password;
                $admin->email = $email;
                $admin->emailVerified = true;
                $admin->canLogin = true;
                $admin->accessLevel = Core_Admin_User_AccessLevel::Super_Administrator;

                try{
                    $admin->pushThis(true,true);
                } catch(Exception $ex) {
                    $error = $ex->getMessage();
                }
            }

            if($error) {
                $this->addErrorMessage($error);
            } else {

                $configOverride = $coreModule->getConfigOverride();
                $configOverride["installed"] = true;
                $coreModule->setConfigOverride($configOverride);

                Core_Admin_User::tryLogin($admin->username,$admin->password);

                header("Location: /admin");
                exit;
            }

        }
    }
}