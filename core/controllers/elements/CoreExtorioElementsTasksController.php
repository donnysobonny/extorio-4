<?php
class CoreExtorioElementsTasksController extends Core_ControlView {
    /**
     * Any additional public properties that you add to your controller will be inherited (passed to) the linked view.
     */

    /**
     * @var Core_LiveTask
     */
    private $liveTask = false;
    private $run_in_background = true;

    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {

        //check whether the task is to run in the background or not
        if(isset($_GET["run_in_background"])) {
            $this->run_in_background = $_GET["run_in_background"] == "false" ? false : true;
        }
    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {

        //if the task is not running in the background, run the task in the current request
        if(!$this->run_in_background) {
            set_time_limit(0);

            //bind exceptions
            $this->bindMethodToEvent("on_exception","on_exception_action",999999,true);

            //create the live task instance
            $this->liveTask = new Core_LiveTask();
            $this->liveTask->status = Core_Task_Status::pending;
            $this->liveTask->pid = getmypid();
            $this->liveTask->pushThis();

            //get the args
            $args = func_get_args();
            if(count($args) < 2) {
                throw new Core_Task_Exception("Trying to run a task without specifying the extension and task in the request");
            }

            $extensionName = $args[0];
            $taskName = $args[1];

            //make sure the extension exists
            $extension = $this->Extorio()->getExtension($extensionName);
            if(!$extension) {
                throw new Core_Task_Exception("Trying to run a task of the ".$extensionName." extension but the extension was not loaded");
            }

            //make sure the task can be found
            $task = $extension->constructTask($taskName);
            if(!$task) {
                //construct using naming policy
                $task = $extension->constructTask(Core_Utils_Element::getClassName($extension->name,$taskName,"task"));
            }
            if(!$task) {
                throw new Core_Task_Exception("Trying to run a task by the name ".$taskName." but the task could not be found");
            }

            //set the using task/extension
            $this->liveTask->taskName = $task->name;
            $this->liveTask->taskExtensionName = $extension->name;
            $this->liveTask->pushThis();

            //make sure we can access the task
            if(!$task->canAccess()) {
                throw new Core_Task_Exception("Access denied");
            }

            $method = "onDefault";
            $params = array();
            //work out which of the remaining args are the method/params
            if(count($args) > 2) {
                if(is_callable(array($task,$args[2]))) {
                    $method = $args[2];
                    for($i = 3; $i < count($args); $i++) {
                        $params[] = $args[$i];
                    }
                } else {
                    for($i = 2; $i < count($args); $i++) {
                        $params[] = $args[$i];
                    }
                }
            }

            //wait until there is space to run the task
            $config = $this->Extorio()->getStoredConfig();
            while(count(Core_LiveTask::findAllByStatus(Core_Task_Status::running)) >= $config["tasks"]["max_running_tasks"]) {
                //sleep for 2 seconds
                sleep(2);
            }

            //task is running!
            $this->liveTask->status = Core_Task_Status::running;
            $this->liveTask->pushThis();

            $task->onStart();
            $extension->onStartTask($task);

            $task->onLoad();

            call_user_func_array(array($task,$method),$params);

            $task->onComplete();
        } else {
            //task needs to run in the background
            $queryParams = array();
            parse_str(Core_Utils_Server::getQueryString(),$queryParams);
            $queryParams["run_in_background"] = "false";
            $this->Extorio()->startBackgroundRequest(Core_Utils_Server::getRequestURL()."?".http_build_query($queryParams));
        }
    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {
        //if the task is not running in the background, update the live task
        if(!$this->run_in_background) {
            //task is complete!
            $this->liveTask->status = Core_Task_Status::completed;
            $this->liveTask->pushThis();
        }
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     * 
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {
        //can always view
        return true;
    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     * 
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {
        //can never edit
        return false;
    }

    /**
     * @param Exception $exception
     */
    public function on_exception_action($exception) {
        if($this->liveTask) {
            Core_Logger::customLog("taskErrorLog.log",print_r($exception->getTrace(),true));
            $this->liveTask->status = Core_Task_Status::failed;
            $this->liveTask->failedMessage = $exception->getMessage();
            $this->liveTask->pushThis();
        } else {
            Core_Logger::customLog("taskErrorLog.log",print_r($exception->getTrace(),true));
        }
    }
}