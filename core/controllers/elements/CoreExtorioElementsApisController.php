<?php
class CoreExtorioElementsApisController extends Core_ControlView {
    /**
     * Any additional public properties that you add to your controller will be inherited (passed to) the linked view.
     */

    /**
     * @var Core_Api
     */
    private $apiElement;

    private $contentTypes_DecodeType = array(
        "multipart/form-data" => "query",
        "application/x-www-form-urlencoded" => "query",
        "application/json" => "json",
        "text/json" => "json",
        "application/xhtml+xml" => "xml",
        "text/xml" => "xml",
        "application/xml" => "xml"
    );

    private $inputStream = "";
    private $getParams = array();

    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {
        //setting up CORS
        //allow any origin.
        header("Access-Control-Allow-Origin: *");
        //allow the basic http methods, plus any requested methods
        $addMethods = "";
        if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"])) {
            $addMethods = ", ".$_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"];
        }
        header("Access-Control-Allow-Methods: PUT, POST, GET, DELETE, OPTIONS".$addMethods);
        //allow any custom headers, otherwise no need (most likely an ajax request)
        if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"])) {
            header("Access-Control-Allow-Headers: ".$_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]);
        }

        $this->inputStream = Core_Utils_Server::getInputStreamRaw();
        //manually parse the url query to avoid issues where the $this->getParams is limited to size
        parse_str(Core_Utils_Server::getQueryString(),$this->getParams);
    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        $args = func_get_args();

        //if we haven't even got the extension and api name in the url, api cannot start
        if(count($args) < 2) {
            $this->unableToStartApi();
        }

        //get the params from the url
        $apiExtensionName = $args[0];
        $apiName = $args[1];

        //try and find the api
        $extension = $this->Extorio()->getExtension($apiExtensionName);
        if($extension) {
            $this->apiElement = $extension->constructApi($apiName);
            if(!$this->apiElement) {
                //name might not be literal
                $apiClassName = Core_Utils_Element::getClassName($apiExtensionName,$apiName,"api");
                $this->apiElement = $extension->constructApi($apiClassName);
            }
            if(!$this->apiElement) {
                $this->unableToStartApi();
            }
        } else {
            $this->unableToStartApi();
        }

        //api is started by this point
        $this->apiElement->output = new Core_ApiResponse();
        $this->apiElement->onStart();
        $extension->onStartApi($this->apiElement);

        //bind exceptions
        $this->Extorio()->createAction("on_exception",$this->apiElement,"api_on_exception",999999,true);

        //pass the http method
        $this->apiElement->httpMethod = Core_Utils_Server::getMethod();
        if(!$this->apiElement->httpMethod) {
            $this->apiElement->throwError("Invalid http request method",0,400);
        }

        //if the http method is OPTIONS, we just need to respond with the http header
        if($this->apiElement->httpMethod == "OPTIONS") {
            $this->apiElement->output->display($this->apiElement->output_type);
            exit;
        }

        //check whether we need to validate the content type
        $contentTypeCheck = false;
        if(strlen($this->inputStream)) {
            $contentTypeCheck = true;
        }

        //work out the url method, params and fields
        if(is_callable(array($this->apiElement,$args[2]))) {
            $this->apiElement->urlMethod = $args[2];
            for($i = 3; $i < count($args); $i++) {
                $this->apiElement->urlParams[] = $args[$i];
            }
        } else {
            //method is onDefault
            $this->apiElement->urlMethod = "onDefault";
            //all fields are params
            for($i = 2; $i < count($args); $i++) {
                $this->apiElement->urlParams[] = $args[$i];
            }
        }
        //and the fields
        for($i = 2; $i < count($args); $i++) {
            $this->apiElement->urlFields[] = $args[$i];
        }

        $params = array();
        if($contentTypeCheck) {
            //make sure the content type is set, and get it
            $contentType = Core_Utils_Server::getContentType();
            if(!$contentType) {
                $this->apiElement->throwError("A content type must be specified.",0,400);
            }
            //find the decode type (how we should decode the payload/params based on the content type)
            $decodeType = false;
            foreach($this->contentTypes_DecodeType as $type => $decode) {
                if(strpos($contentType,$type) !== false) {
                    $decodeType = $decode;
                    break;
                }
            }
            if(!$decodeType) {
                $this->apiElement->throwError("Unable to decode the request, invalid content type",0,400);
            }
            //and finally get the params based on the request method and the content type
            switch($this->apiElement->httpMethod) {
                case "GET" :
                    //params only come from the url's query string
                    $params = $this->getParams;
                    break;
                case "POST" :
                    //try $_POST first
                    if(!empty($_POST)) {
                        $params = $_POST;
                    } else {
                        //some frameworks send the POST params in the stream
                        switch($decodeType) {
                            case "query" :
                                parse_str($this->inputStream,$params);
                                break;
                            case "json" :
                                $params = json_decode($this->inputStream,true);
                                if(is_null($params)) {
                                    throw new Exception("Content type specified as JSON, however the request could not be parsed as JSON");
                                }
                                break;
                            case "xml" :
                                throw new Exception("XML currently not supported");
                                break;
                        }
                    }

                    //let any GET params override
                    foreach($this->getParams as $key => $value) {
                        $params[$key] = $value;
                    }

                    break;
                default :
                    //custom requests send params in the stream
                    switch($decodeType) {
                        case "query" :
                            parse_str($this->inputStream,$params);
                            break;
                        case "json" :
                            $params = json_decode($this->inputStream,true);
                            if(is_null($params)) {
                                throw new Exception("Content type specified as JSON, however the request could not be parsed as JSON");
                            }
                            break;
                        case "xml" :
                            throw new Exception("XML currently not supported");
                            break;
                    }

                    //let any GET params override
                    foreach($this->getParams as $key => $value) {
                        $params[$key] = $value;
                    }
                    break;
            }
        } else {
            //just use the params in the url
            $params = $this->getParams;
        }

        //reflect the api element so that we can check whether the property is accessible
        $apiElementReflector = new ReflectionClass($this->apiElement);
        foreach($params as $key => $value) {
            if(!in_array($key,$this->apiElement->internal_properties)) {

                //fix bool string values
                switch($value) {
                    case "false" :
                        $value = false;
                        break;
                    case "true" :
                        $value = true;
                        break;
                }

                //place in the postParams property
                $this->apiElement->postParams[$key] = $value;

                //if the api element has this property defined, make sure it is public otherwise don't overwrite it
                $elementProperty = false;
                try{
                    $elementProperty = $apiElementReflector->getProperty($key);
                } catch(ReflectionException $ex) {
                    $elementProperty = false;
                }

                if($elementProperty) {
                    if($elementProperty->isPublic()) {
                        //overwrite
                        $this->apiElement->$key = $value;
                    }
                } else {
                    //not defined, magically assign
                    $this->apiElement->$key = $value;
                }
            }
        }

        //if the session was in the params, check that we need to log in a user
        if(isset($this->apiElement->postParams["session"])) {

            //try and find a session that matches this ip/session
            $apiUserSession = Core_ApiUserSession::findBySession($this->apiElement->postParams["session"],Core_Utils_Server::getIpAddress());
            if($apiUserSession) {
                /** @var Core_BaseModel_Type_User $type */
                $type = $apiUserSession->userType;

                //make sure the user exists and can login
                $foundUser = $type::findOne(Core_ORM_Finder::newInstance()->where("base.id = ".$apiUserSession->userId." AND base.canLogin = 1")->ignoreAccessChecks(true));
                if($foundUser) {
                    //set this user as the logged in user
                    $foundUser->setLoggedInUserId($foundUser->id);
                } else {
                    $this->apiElement->throwError("Invalid user session.",0,400);
                }

            } else {
                //if you pass a session, it must be valid, otherwise don't pass a session!
                $this->apiElement->throwError("Invalid user session.",0,400);
            }
        }

        //make sure we can access the api
        if(!$this->apiElement->canAccess()) {
            $this->apiElement->throwError("Unauthorized",0,401);
        }

        //lets run the api
        $this->apiElement->onLoad();

        //run the target method and params
        call_user_func_array(array($this->apiElement,$this->apiElement->urlMethod),$this->apiElement->urlParams);

        $this->apiElement->onComplete();
    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {
        //if the element hasn't already displayed
        $this->apiElement->output->display($this->apiElement->output_type);
    }

    private function unableToStartApi() {
        header("HTTP/1.1 404 Not Found");
        exit;
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     * 
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     * 
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {
        
    }
}