<?php
class CoreExtorioAdminPagesController extends Core_ControlView {

    /**
     * @var Core_Page[]
     */
    public $allPages = array();

    public $allTemplates = array();

    /**
     * @var Core_Page
     */
    public $page = false;

    public $extensions = array();

    public function onLoad() {

    }

    public function onDefault() {
        $this->allPages = Core_Page::findAll(Core_ORM_Finder::newInstance()
            ->orderBy("base.name")
            ->orderDir("asc")
        );
    }

    public function onComplete() {

    }

    public function create() {

        if(isset($_POST["edit_page_submitted"])) {
            $page = new Core_Page();
            $page->name = $_POST["name"];
            $page->title = $_POST["title"];
            $page->isEnabled = isset($_POST["enabled"])?true:false;
            $page->useTemplate = isset($_POST["use_template"])?true:false;
            $page->requestAddress = $_POST["address"];
            $page->templateDisplayMode = $_POST["display_mode"];
            $templateData = $_POST["template"];
            if(strlen($templateData)) {
                $templateData = explode(":",$templateData);
                $page->templateName = $templateData[1];
                $page->templateExtensionName = $templateData[0];
            } else {
                $page->templateName = "";
                $page->templateExtensionName = "";
            }

            $page->targetControllerExtensionName = $page->targetViewExtensionName = $_POST["extension"];

            $error = false;
            try {
                $page->pushThis();
            } catch(Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                Core_Messager::addErrorMessage($error);
            } else {
                Core_Messager::addSuccessMessage("Page successfully created");
                $this->redirectToMethod();
            }
        }

        foreach($this->Extorio()->getExtensions() as $extension) {

            $this->extensions[] = $extension->name;

            $templates = $extension->constructAllTemplates();
            foreach($templates as $template) {
                $this->allTemplates[$template->extensionName][] = $template->name;
            }
        }
    }

    public function edit($pageId=false) {

        if($pageId) {
            $this->page = Core_Page::findById($pageId);
        }

        if(isset($_POST["edit_page_submitted"])) {
            $this->page->name = $_POST["name"];
            $this->page->title = $_POST["title"];
            $this->page->isEnabled = isset($_POST["enabled"])?true:false;
            $this->page->useTemplate = isset($_POST["use_template"])?true:false;
            $this->page->requestAddress = $_POST["address"];
            $this->page->templateDisplayMode = $_POST["display_mode"];
            $templateData = $_POST["template"];
            if(strlen($templateData)) {
                $templateData = explode(":",$templateData);
                $this->page->templateName = $templateData[1];
                $this->page->templateExtensionName = $templateData[0];
            } else {
                $this->page->templateName = "";
                $this->page->templateExtensionName = "";
            }

            $error = false;
            try {
                $this->page->pushThis();
            } catch(Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                Core_Messager::addErrorMessage($error);
            } else {
                Core_Messager::addSuccessMessage("Page successfully updated");
                $this->redirectToMethod();
            }
        }

        foreach($this->Extorio()->getExtensions() as $extension) {
            $templates = $extension->constructAllTemplates();
            foreach($templates as $template) {
                $this->allTemplates[$template->extensionName][] = $template->name;
            }
        }
    }

    public function delete($pageId=false) {
        if($pageId) {
            $page = Core_Page::findById($pageId);
            if($page) {
                $page->removeThis();
            }
        }

        Core_Messager::addInfoMessage("Page successfully deleted");
        $this->redirectToMethod();
    }

    public function canView() {

    }

    public function canEdit() {

    }
}