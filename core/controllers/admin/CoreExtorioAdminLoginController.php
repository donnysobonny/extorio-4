<?php
class CoreExtorioAdminLoginController extends Core_ControlView {
    /**
     * Any additional public properties that you add to your controller will be inherited (passed to) the linked view.
     */
    public $username;
    public $password;

    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {
        if(isset($_POST["admin_login_submitted"])) {
            $this->username = $_POST["username"];
            $this->password = $_POST["password"];

            $error = false;
            try{
                Core_Admin_User::tryLogin($this->username,$this->password);
            } catch(Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                Core_Messager::addErrorMessage($error);
            } else {
                if(isset($_GET["r"])) {
                    header("Location: ".$_GET["r"]);
                    exit;
                } else {
                    header("Location: /extorio/admin/");
                    exit;
                }
            }
        }
    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        
    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     * 
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {
        //always view
        return true;
    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     * 
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {
        //never edit
        return false;
    }
}