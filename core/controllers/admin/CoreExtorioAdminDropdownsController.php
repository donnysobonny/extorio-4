<?php
class CoreExtorioAdminDropdownsController extends Core_ControlView {
    /**
     * Any additional public properties that you add to your controller will be inherited (passed to) the linked view.
     */

    /**
     * @var Core_Dropdown[]
     */
    public $allDropdowns = array();

    /**
     * @var Core_Dropdown
     */
    public $dropdown = false;

    /**
     * @var Core_DropdownElement[]
     */
    public $dropdownElements = array();

    public $allExtensions = array();

    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {
        
    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {

        if(isset($_POST["new_dropdown_submitted"])) {
            $dropdown = new Core_Dropdown();
            $dropdown->name = $_POST["name"];
            $dropdown->classPath = $_POST["path"];
            $dropdown->isHidden = false;

            $error = false;
            try{
                $dropdown->pushThis();
            } catch(Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                Core_Messager::addErrorMessage($error);
            } else {
                Core_Messager::addSuccessMessage("Dropdown created successfully");
                $this->redirectToMethod();
            }
        }

        foreach($this->Extorio()->getExtensions() as $extension) {
            $this->allExtensions[] = $extension->root."/classes";
        }

        $this->allDropdowns = Core_Dropdown::findAll(Core_ORM_Finder::newInstance()->orderBy("base.name")->orderDir("asc"));
    }

    public function elements($dropdownId=false) {
        if($dropdownId) {

            if(isset($_POST["new_element_submitted"])) {
                $element = new Core_DropdownElement();
                $element->ownerDropdownId = $dropdownId;
                $element->name = $_POST["name"];
                $element->value = $_POST["value"];

                $error = false;

                try{
                    $element->pushThis();
                } catch(Exception $ex) {
                    $error = $ex->getMessage();
                }

                if($error) {
                    Core_Messager::addErrorMessage($error);
                } else {
                    Core_Messager::addSuccessMessage("Element created successfully");
                    $this->redirectToMethod("elements",array($dropdownId));
                }

            }

            $this->dropdown = Core_Dropdown::findById($dropdownId);
            $this->dropdownElements = Core_DropdownElement::findByOwnerId($dropdownId);
        }
    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     * 
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     * 
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {
        
    }
}