<?php
class CoreExtorioAdminTaskManagerController extends Core_ControlView {
    /**
     * Any additional public properties that you add to your controller will be inherited (passed to) the linked view.
     */
    /**
     * @var Core_LiveTask[]
     */
    public $allLiveTasks = array();

    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {
        $this->allLiveTasks = Core_LiveTask::findAll(Core_ORM_Finder::newInstance());
    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        
    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {
        
    }

    public function kill($id) {
        $this->Extorio()->stopTaskById($id);

        Core_Messager::addSuccessMessage("Task successfully killed");
        $this->redirectToMethod();
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     * 
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     * 
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {
        
    }

    public function clear_all() {
        $tasks = Core_LiveTask::findAll(Core_ORM_Finder::newInstance()
            ->where("
            base.status != ('running') AND base.status != ('pending')
            ")
        );
        foreach($tasks as $task) {
            $task->removeThis();
        }

        Core_Messager::addSuccessMessage("All non-running tasks cleared");
        $this->redirectToMethod();
    }
}