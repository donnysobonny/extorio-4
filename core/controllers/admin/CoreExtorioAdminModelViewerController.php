<?php
class CoreExtorioAdminModelViewerController extends Core_ControlView {
    /**
     * Any additional public properties that you add to your controller will be inherited (passed to) the linked view.
     */

    public $model = false;
    public $action = false;
    public $rid = false;

    /**
     * @var Core_Class
     */
    public $modelClass = false;

    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {
        
    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault($model=false,$action=false,$rid=false) {

        $this->model = $model;
        $this->action = $action;
        $this->rid = $rid;

        //make checks that the controller is being used properly
        if(!$this->model || !$this->action) {
            throw new Core_MVC_Exception("You must specify the model and the action in the url");
        }

        if(
            ($this->action == "edit" || $this->action == "detail") &&
            !$this->rid
        ) {
            throw new Core_MVC_Exception("For the edit and detail view, you must specify the id in the url");
        }

        //make sure the model exists as a complex model
        $this->modelClass = Core_Class::findByName($this->model);
        if(!$this->modelClass) {
            throw new Core_MVC_Exception("The model ".$this->model." does not exist");
        }

        //make sure action is valid
        if(!in_array($this->action,array(
            "edit",
            "create",
            "detail",
            "list"
        ))) {
            throw new Core_MVC_Exception("The action ".$this->action." is not a recognised action");
        }

        //run the action
        $action = $this->action."View";
        $this->$action();
    }

    private function editView() {
        if(isset($_POST["edit_model_form_submitted"])) {

            /** @var Core_ORM_ComplexModel $type */
            $type = $this->model;

            $error = false;

            try{
//                echo "<pre>";
//                print_r($_POST["base"]);
//                echo "</pre>";

                //construct from array
                $object = $type::constructFromArray($_POST["base"]);

                //$object->print_r();

                //try and push
                $object->pushThis();
            } catch(Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                Core_Messager::addErrorMessage($error);
            } else {
                Core_Messager::addSuccessMessage($this->model." updated successfully!");
            }
        }
    }

    private function createView() {
        if(isset($_POST["create_model_form_submitted"])) {

            /** @var Core_ORM_ComplexModel $type */
            $type = $this->model;

            $error = false;

            try{
//                echo "<pre>";
//                print_r($_POST["base"]);
//                echo "</pre>";

                //construct from array
                $object = $type::constructFromArray($_POST["base"]);

//                $object->print_r();

                //try and push
                $object->pushThis();
            } catch(Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                Core_Messager::addErrorMessage($error);
            } else {
                Core_Messager::addSuccessMessage($this->model." created successfully!");
            }
        }
    }

    private function detailView() {

    }

    private function listView() {

    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     * 
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     * 
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {
        
    }
}