<?php
class CoreExtorioAdminExtensionsController extends Core_ControlView {
    /**
     * Any additional public properties that you add to your controller will be inherited (passed to) the linked view.
     */

    /**
     * @var Core_InstalledExtension[]
     */
    public $installedExtensions = array();


    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {

    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {

        //enable
        if(isset($_POST["enable_submitted"])) {
            $id = $_POST["extension_id"];
            $installed = Core_InstalledExtension::findById($id);
            if($installed) {
                $error = false;
                try {
                    $this->Extorio()->enableExtension($installed->name);
                } catch(Exception $ex) {
                    $error = $ex->getMessage();
                }

                if($error) {
                    Core_Messager::addErrorMessage($error);
                } else {
                    Core_Messager::addSuccessMessage($installed->name." successfully enabled");
                }
            }
        }

        //disable
        if(isset($_POST["disable_submitted"])) {
            $id = $_POST["extension_id"];
            $installed = Core_InstalledExtension::findById($id);
            if($installed) {
                $error = false;
                try {
                    $this->Extorio()->disableExtension($installed->name);
                } catch(Exception $ex) {
                    $error = $ex->getMessage();
                }

                if($error) {
                    Core_Messager::addErrorMessage($error);
                } else {
                    Core_Messager::addSuccessMessage($installed->name." successfully enabled");
                }
            }
        }

        //TODO uninstall extension

        //find the folders directly inside of the extensions directory. Make sure that every extension has an installed extension entry
        $folders = Core_Utils_Folder::findFoldersInFolder("extensions");
        foreach($folders as $folder) {
            //remove the extensions/ part of the path
            $folder = str_replace("extensions/","",$folder);

            //if the extension does not have an entry, create a disabled one
            $check = Core_InstalledExtension::findOne(Core_ORM_Finder::newInstance()->where("base.name = ('".$folder."')"));
            if(!$check) {
                $installed = new Core_InstalledExtension();
                $installed->name = $folder;
                $installed->isEnabled = false;
                $installed->loadPriority = 0;
                $installed->pushThis();
            }
        }

        //get installed extensions
        $this->installedExtensions = Core_InstalledExtension::findAll(Core_ORM_Finder::newInstance()->orderBy("base.loadPriority"));
    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     * 
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     * 
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {
        
    }
}