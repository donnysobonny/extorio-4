<?php
class CoreExtorioAdminBlocksController extends Core_ControlView {
    /**
     * Any additional public properties that you add to your controller will be inherited (passed to) the linked view.
     */
    public $installedBlocks = array();

    /**
     * @var Core_LiveBlock[]
     */
    public $allLiveBlocks = array();

    /**
     * @var Core_Page[]
     */
    public $allPages = array();

    /**
     * @var Core_LiveBlock
     */
    public $editBlock = false;
    /**
     * @var int[]
     */
    public $editBlockPagesIds = array();

    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {

    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        $this->allLiveBlocks = Core_LiveBlock::findAll(Core_ORM_Finder::newInstance()->orderBy("base.name")->orderDir("asc"));
    }

    public function create() {
        foreach($this->Extorio()->getExtensions() as $extensions) {
            $blocks = $extensions->constructAllBlocks();
            foreach($blocks as $block) {
                $this->installedBlocks[$block->extensionName][] = $block->name;
            }
        }
        $this->allPages = Core_Page::findAll(Core_ORM_Finder::newInstance()->orderBy("base.name")->orderDir("asc"));

        if(isset($_POST["block_edit_submitted"])) {
            $block = new Core_LiveBlock();
            $block->name = $_POST["name"];
            $block->title = $_POST["title"];
            $blockData = $_POST["block_type"];
            $blockData = explode(":",$blockData);
            $block->blockType = $blockData[1];
            $block->blockExtensionName = $blockData[0];
            $block->panelMode = $_POST["panel_mode"];
            $block->isEnabled = isset($_POST["enabled"])?true:false;
            $block->allPages = isset($_POST["all_pages"])?true:false;
            $block->area = $_POST["area"];
            $block->order = $_POST["order"];

            $error = false;
            try{
                $block->pushThis();
            } catch(Exception $ex) {
                $error = $ex->getMessage();
            }

            if(!$error) {
                //add the pages
                foreach($_POST["pages"] as $page) {
                    $blockPage = new Core_LiveBlockPage();
                    $blockPage->liveBlockId = $block->id;
                    $blockPage->pageId = $page;
                    $blockPage->pushThis();
                }
            }

            if($error) {
                Core_Messager::addErrorMessage($error);
            } else {
                Core_Messager::addSuccessMessage("Block created successfully");
                $this->redirectToMethod();
            }
        }
    }

    public function edit($blockId) {
        $this->editBlock = Core_LiveBlock::findById($blockId);

        foreach($this->Extorio()->getExtensions() as $extension) {
            $blocks = $extension->constructAllBlocks();
            foreach($blocks as $block) {
                $this->installedBlocks[$block->extensionName][] = $block->name;
            }
        }
        $this->allPages = Core_Page::findAll(Core_ORM_Finder::newInstance()->orderBy("base.name")->orderDir("asc"));

        $pages = Core_LiveBlockPage::findAll(Core_ORM_Finder::newInstance()->where("base.liveBlockId = ".$this->editBlock->id));
        foreach($pages as $page) {
            $this->editBlockPagesIds[] = $page->pageId;
        }

        if(isset($_POST["block_edit_submitted"])) {
            $this->editBlock->name = $_POST["name"];
            $this->editBlock->title = $_POST["title"];
            $blockData = $_POST["block_type"];
            $blockData = explode(":",$blockData);
            $this->editBlock->blockType = $blockData[1];
            $this->editBlock->blockExtensionName = $blockData[0];
            $this->editBlock->panelMode = $_POST["panel_mode"];
            $this->editBlock->isEnabled = isset($_POST["enabled"])?true:false;
            $this->editBlock->allPages = isset($_POST["all_pages"])?true:false;
            $this->editBlock->area = $_POST["area"];
            $this->editBlock->order = $_POST["order"];

            $error = false;
            try{
                $this->editBlock->pushThis();
            } catch(Exception $ex) {
                $error = $ex->getMessage();
            }

            if(!$error) {
                //delete existing pages first
                $existing = Core_LiveBlockPage::findAll(Core_ORM_Finder::newInstance()->where("base.liveBlockId = ".$this->editBlock->id));
                foreach($existing as $page) {
                    $page->removeThis();
                }

                //add the pages
                foreach($_POST["pages"] as $page) {
                    $blockPage = new Core_LiveBlockPage();
                    $blockPage->liveBlockId = $this->editBlock->id;
                    $blockPage->pageId = $page;
                    $blockPage->pushThis();
                }
            }

            if($error) {
                Core_Messager::addErrorMessage($error);
            } else {
                Core_Messager::addSuccessMessage("Block successfully updated");
                $this->redirectToMethod();
            }
        }
        
    }

    public function delete($blockId) {
        $liveBlock = Core_LiveBlock::findById($blockId);
        if($liveBlock) {
            $liveBlock->removeThis();
        }

        Core_Messager::addSuccessMessage("Block successfully deleted");
        $this->redirectToMethod();
    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     * 
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     * 
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {
        
    }
}