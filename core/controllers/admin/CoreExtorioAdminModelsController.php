<?php
class CoreExtorioAdminModelsController extends Core_ControlView {
    /**
     * Any additional public properties that you add to your controller will be inherited (passed to) the linked view.
     */
    /**
     * @var Core_Class[]
     */
    public $allClasses = array();
    public $extensions = array();

    /**
     * @var Core_Dropdown
     */
    public $allDropdowns = array();

    /**
     * @var Core_Class
     */
    public $class = false;

    /**
     * @var Core_Property
     */
    public $property = false;

    /**
     * @var Core_Property[]
     */
    public $properties = array();

    /**
     * @var Core_AjaxResponseWrapper
     */
    private $jsonResponse = false;

    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {
        
    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {

        if(isset($_POST["create_model_submitted"])) {
            $model = new Core_Class();
            $model->name = $_POST["name"];
            $model->modelDir = $_POST["extension"];
            $model->type = $_POST["type"];

            $error = false;

            try {
                $model->pushThis();
            } catch(Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                Core_Messager::addErrorMessage($error);
            } else {
                Core_Messager::addSuccessMessage("Model created successfully");
            }
        }

        $this->allClasses = Core_Class::findAll(Core_ORM_Finder::newInstance()
            ->orderBy("base.name")
            ->orderDir("asc")
        );
        foreach($this->Extorio()->getExtensions() as $extension) {
            $this->extensions[] = $extension->root."/models";
        }
    }

    public function fields($modelId=false) {
        if($modelId) {
            $this->class = Core_Class::findById($modelId);
            $this->properties = Core_Property::findAllByOwnerClassId($modelId);
        }
    }

    public function createfield($modelId=false) {
        if($modelId) {
            $this->class = Core_Class::findById($modelId);
            $this->allClasses = Core_Class::findAll(Core_ORM_Finder::newInstance()
                ->orderBy("base.name")
                ->orderDir("asc")
            );
            $this->allDropdowns = Core_Dropdown::findAll(Core_ORM_Finder::newInstance()
                ->orderBy("base.name")
                ->orderDir("asc")
            );

            if(isset($_POST["create_property_submitted"])) {
                $name = $_POST["name"];
                $input_type = $_POST["input_type"];
                $checkbox_default = isset($_POST["checkbox_default"])?true:false;
                $script_tags_allowed = isset($_POST["script_tags_allowed"])?true:false;
                $dropdown_id = $_POST["dropdown_id"];
                $child_class_id = $_POST["child_class_id"];
                $max_length = $_POST["max_length"];

                $prop = new Core_Property();
                $prop->name = Core_Utils_String::wordsToCamelCase($name,false);
                $prop->ownerClassId = $modelId;
                $prop->inputType = $input_type;
                $prop->checkBoxDefault = $checkbox_default;
                $prop->htmlScriptsAllowed = $script_tags_allowed;
                $prop->dropDownId = $dropdown_id;
                $prop->childClassId = $child_class_id;
                $prop->maxLength = $max_length;

                $error = false;

                try{
                    $prop->pushThis();
                } catch(Exception $ex) {
                    $error = $ex->getMessage();
                }

                if($error) {
                    Core_Messager::addErrorMessage($error);
                } else {
                    Core_Messager::addSuccessMessage("Field successfully created");
                    $this->redirectToMethod("fields",array($modelId));
                }
            }
        }
    }

    public function editfield($modelId=false,$fieldId=false) {
        if($modelId && $fieldId) {
            $this->class = Core_Class::findById($modelId);
            $this->allClasses = Core_Class::findAll(Core_ORM_Finder::newInstance()
                ->orderBy("base.name")
                ->orderDir("asc")
            );
            $this->allDropdowns = Core_Dropdown::findAll(Core_ORM_Finder::newInstance()
                ->orderBy("base.name")
                ->orderDir("asc")
            );
            $this->property = Core_Property::findById($fieldId);

            if (isset($_POST["update_property_submitted"])) {
                $name = $_POST["name"];
                $input_type = $_POST["input_type"];
                $checkbox_default = isset($_POST["checkbox_default"])?true:false;
                $script_tags_allowed = isset($_POST["script_tags_allowed"])?true:false;
                $dropdown_id = $_POST["dropdown_id"];
                $child_class_id = $_POST["child_class_id"];
                $max_length = $_POST["max_length"];

                $this->property->name = $name;
                $this->property->inputType = $input_type;
                $this->property->checkBoxDefault = $checkbox_default;
                $this->property->htmlScriptsAllowed = $script_tags_allowed;
                $this->property->dropDownId = $dropdown_id;
                $this->property->childClassId = $child_class_id;
                $this->property->maxLength = $max_length;
                
                $error = false;
                
                try{
                    $this->property->pushThis();
                } catch(Exception $ex) {
                    $error = $ex->getMessage();
                }
                
                if($error) {
                    Core_Messager::addErrorMessage($error);
                } else {
                    Core_Messager::addSuccessMessage("Field successfully updated");
                    $this->redirectToMethod("fields",array($modelId));
                }
            }
        }
    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     * 
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     * 
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {
        
    }
}