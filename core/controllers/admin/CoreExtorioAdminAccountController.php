<?php
class CoreExtorioAdminAccountController extends Core_ControlView {
    /**
     * Any additional public properties that you add to your controller will be inherited (passed to) the linked view.
     */

    /**
     * @var Core_Admin_User
     */
    public $currentAdmin = false;

    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {
        
    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {

        //change email
        if(isset($_POST["email_change_submitted"])) {
            $currentAdmin = Core_Admin_User::getLoggedInUser();
            if($currentAdmin) {
                $currentAdmin->email = $_POST["email"];
                $error = false;
                try{
                    $currentAdmin->pushThis();
                } catch(Exception $ex) {
                    $error = $ex->getMessage();
                }

                if($error) {
                    Core_Messager::addErrorMessage($error);
                } else {
                    Core_Messager::addSuccessMessage("Email updated successfully");
                }
            }
        }

        //change username
        if(isset($_POST["change_username_submitted"])) {
            $currentAdmin = Core_Admin_User::getLoggedInUser();
            if($currentAdmin) {
                $currentAdmin->username = $_POST["username"];
                $error = false;
                try{
                    $currentAdmin->pushThis();
                } catch(Exception $ex) {
                    $error = $ex->getMessage();
                }

                if($error) {
                    Core_Messager::addErrorMessage($error);
                } else {
                    Core_Messager::addSuccessMessage("Username updated successfully");
                }
            }
        }

        //change password
        if(isset($_POST["change_password_submitted"])) {
            $currentAdmin = Core_Admin_User::getLoggedInUser();
            if($currentAdmin) {
                $error = false;
                $cur_password = $_POST["cur_password"];
                $new_password = $_POST["new_password"];
                $con_password = $_POST["con_password"];

                if(
                    $cur_password != $currentAdmin->password ||
                    strlen($new_password) < 6 ||
                    $new_password != $con_password
                ) {
                    $error = "Could not update your password";
                }

                if(!$error) {
                    $currentAdmin->password = $new_password;
                    try {
                        $currentAdmin->pushThis();
                    } catch(Exception $ex) {
                        $error = $ex->getMessage();
                    }
                }

                if($error) {
                    Core_Messager::addErrorMessage($error);
                } else {
                    Core_Messager::addSuccessMessage("Password updated successfully");
                }
            }
        }

        $this->currentAdmin = Core_Admin_User::getLoggedInUser();
    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     * 
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     * 
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {
        
    }
}