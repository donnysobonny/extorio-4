<?php
class CoreExtorioAdminLogManagerController extends Core_ControlView {
    /**
     * Any additional public properties that you add to your controller will be inherited (passed to) the linked view.
     */
    /**
     * @var Core_Utils_File[]
     */
    public $logFiles = array();

    public function onStart() {

    }

    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {
        //get the log files
        $this->logFiles = Core_Utils_File::findFilesInFolderRecursively("logs");
    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {

    }

    public function remove_all() {
        foreach($this->logFiles as $file) {
            if($file->baseName != "index.html") {
                unlink($file->path);
            }
        }

        Core_Messager::addInfoMessage("All logs successfully removed");
        $this->redirectToMethod();
    }

    public function view($baseName) {
        foreach($this->logFiles as $file) {
            if($file->baseName == $baseName) {
                //open file for reading
                $file->open("r");
                $content = $file->read();
                $file->close();
                ?>
                <div id="log_view" class="modal fade">
                    <div style="width: 80%;" class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title">Viewing log file: <?=$file->baseName?></h4>
                            </div>
                            <div class="modal-body">
                                <pre style="font-size: 9px;"><?=$content?></pre>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <script>
                    $(function() {
                        $('#log_view').modal();
                    });
                </script>
                <?php
            }
        }
    }

    public function delete($baseName) {
        foreach($this->logFiles as $file) {
            if($file->baseName == $baseName) {
                unlink($file->path);
            }
        }

        Core_Messager::addInfoMessage("File successfully deleted");
        $this->redirectToMethod();
    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     * 
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     * 
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {
        
    }
}