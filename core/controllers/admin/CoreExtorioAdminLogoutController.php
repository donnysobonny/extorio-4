<?php
class CoreExtorioAdminLogoutController extends Core_ControlView {
    /**
     * Any additional public properties that you add to your controller will be inherited (passed to) the linked view.
     */


    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {
        Core_Admin_User::logout();

        header("Location: /extorio/admin/login");
        exit;
    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault() {
        
    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     * 
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {
        
    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     * 
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {
        
    }
}