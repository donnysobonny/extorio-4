<?php

class CoreExtorioAdminAccessController extends Core_ControlView {

    public $type = "";
    public $target = false;

    /**
     * @var Core_Page
     */
    public $page = false;
    /**
     * @var Core_LiveBlock
     */
    public $block = false;
    /**
     * @var Core_Class
     */
    public $model = false;

    public $eventActions = array();

    /**
     * @var Core_AccessEvent[]
     */
    public $events = array();
    /**
     * @var Core_AccessAction[][]
     */
    public $actions = array();

    public $eventId = false;
    /**
     * @var Core_AccessEvent
     */
    public $event = false;
    public $action = false;

    /**
     * @var Core_AccessAction
     */
    public $editAction = false;

    public $userTypes = array();
    public $accessLevels = array();

    /**
     * This method is called immediately after the controller is loaded by the framework.
     */
    public function onLoad() {
        //the access page will always be displayed in an iframe
        $this->Extorio()->setSettingsTemplateDisplayMode(Core_Template_DisplayMode::basic_with_messages);
    }

    /**
     * This method is called if the controller is accessed without a target method, otherwise
     * the target method is called.
     */
    public function onDefault($type = false, $target = false) {
        $this->type = $type;
        $this->target = $target;

        if (!$type || !$target) {
            throw new Exception("You must specify a type and target");
        }

        //get the type's object
        switch($type) {
            case "page" :
                $this->page = Core_Page::findById($target);
                break;
            case "block" :
                $this->block = Core_LiveBlock::findById($target);
                break;
            case "model" :
                $this->model = Core_Class::findByName($target);
                break;
        }

        //get the events and actions
        $this->events = Core_AccessEvent::findAll(Core_ORM_Finder::newInstance()
            ->where("base.type = ('" . $this->type . "') AND base.target = ('" . $this->target . "')")
        );
        foreach ($this->events as $event) {
            $actions = Core_AccessAction::findAll(Core_ORM_Finder::newInstance()->where("base.eventId = " . $event->id));
            $this->actions[$event->id] = $actions;
        }
    }

    /**
     * This method is the very last method that is called on the controller.
     */
    public function onComplete() {

    }

    public function create($eventId, $action) {
        $this->event = Core_AccessEvent::findById($eventId);

        //this should be opened in a frame, so set template to basic mode
        $this->Extorio()->setSettingsTemplateDisplayMode(Core_Template_DisplayMode::basic_with_messages);

        if (isset($_POST["action_edit_submitted"])) {
            $action = new Core_AccessAction();
            $action->eventId = $eventId;
            $action->userType = $_POST["usertype"];
            $action->accessLevel = $_POST["accesslevel"];
            $action->redirectOnFailure = $_POST["redir_failure"];
            $action->redirectOnSuccess = $_POST["redir_success"];
            $action->order = $_POST["order"];

            $error = false;
            try {
                $action->pushThis();
            } catch (Exception $ex) {
                $error = $ex->getMessage();
            }

            if ($error) {
                Core_Messager::addErrorMessage($error);
            } else {
                Core_Messager::addSuccessMessage("Action successfully created");
                $this->redirectToMethod(false,array(
                    $this->event->type,
                    $this->event->target
                ));
            }
        }

        $this->eventId = $eventId;
        $this->action = $action;
        $event = Core_AccessEvent::findById($eventId);
        if ($event) {
            switch ($event->type) {
                case "page" :
                    $this->eventActions = array("edit", "view");
                    break;
                case "block" :
                    $this->eventActions = array("edit", "view");
                    break;
                case "model" :
                    $this->eventActions = array("create", "update", "delete", "retrieve", "view_list", "view_edit", "view_detail", "view_delete");
                    break;
            }
        }

        //get all user types
        $userClasses = Core_Class::findAll(Core_ORM_Finder::newInstance()
            ->where("base.type = ('user')")
            ->orderBy("base.name")
            ->orderDir("asc")
        );
        //get the access level property to get the dropdown values
        foreach ($userClasses as $userClass) {
            $accessLevelProp = Core_Property::findOne(Core_ORM_Finder::newInstance()
                ->where("base.ownerClassId = " . $userClass->id . " AND base.name = ('accessLevel')")
            );
            if ($accessLevelProp) {
                $dropdown = Core_Dropdown::findById($accessLevelProp->dropDownId);
                $elements = Core_DropdownElement::findByOwnerId($dropdown->id);
                $accessLevels = array();
                foreach ($elements as $element) {
                    $this->accessLevels[$userClass->name][$element->value] = $element->name;
                }
            }
            $this->userTypes[] = $userClass->name;
        }
    }

    public function edit($actionId) {
        //this should be opened in a frame, so set template to blank mode
        $this->Extorio()->setSettingsTemplateDisplayMode(Core_Template_DisplayMode::basic_with_messages);

        //get all user types
        $userClasses = Core_Class::findAll(Core_ORM_Finder::newInstance()
            ->where("base.type = ('user')")
            ->orderBy("base.name")
            ->orderDir("asc")
        );
        //get the access level property to get the dropdown values
        foreach ($userClasses as $userClass) {
            $accessLevelProp = Core_Property::findOne(Core_ORM_Finder::newInstance()
                ->where("base.ownerClassId = " . $userClass->id . " AND base.name = ('accessLevel')")
            );
            if ($accessLevelProp) {
                $dropdown = Core_Dropdown::findById($accessLevelProp->dropDownId);
                $elements = Core_DropdownElement::findByOwnerId($dropdown->id);
                $accessLevels = array();
                foreach ($elements as $element) {
                    $this->accessLevels[$userClass->name][$element->value] = $element->name;
                }
            }
            $this->userTypes[] = $userClass->name;
        }

        $this->editAction = Core_AccessAction::findById($actionId);

        $this->event = Core_AccessEvent::findById($this->editAction->eventId);


        if (isset($_POST["action_edit_submitted"])) {
            $this->editAction->userType = $_POST["usertype"];
            $this->editAction->accessLevel = $_POST["accesslevel"];
            $this->editAction->redirectOnFailure = $_POST["redir_failure"];
            $this->editAction->redirectOnSuccess = $_POST["redir_success"];
            $this->editAction->order = $_POST["order"];

            $error = false;

            try {
                $this->editAction->pushThis();
            } catch (Exception $ex) {
                $error = $ex->getMessage();
            }

            if ($error) {
                Core_Messager::addErrorMessage($error);
            } else {
                Core_Messager::addSuccessMessage("Action successfully created");
            }
        }
    }

    public function delete($actionId) {
        $action = Core_AccessAction::findById($actionId);
        if($action) {
            $action->removeThis();
        }
    }

    /**
     * Here you can specify the conditions that need to be met in order for the page to be viewed.
     *
     * If you don't return anything (or NULL), then the viewing access can be defined by an admin user.
     * If you return true, you are allowing viewing access. If you return false, you are not.
     */
    public function canView() {

    }

    /**
     * Here you can specify the conditions that need to be met in order for this page to be edited.
     *
     * If you don't return anything (or NULL), then the editing access can be defined by an admin user.
     * If you return true, you are allowing editing access. If you return false, you are not.
     */
    public function canEdit() {

    }
}