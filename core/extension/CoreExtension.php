<?php
final class CoreExtension extends Core_Extension {
    public function onStart() {
        $this->Extorio()->createAction("on_exception",$this,"on_exception_action",1,false);


        //create the event to get the admin menu items. The actions should return an array in the following format:
        //[
        //  display_header : (bool),
        //  header_label : (string),
        //  header_icon_class : (string),
        //  description : (string),
        //  items : [
        //      {
        //          label : (string),
        //          description : (string),
        //          icon_class : (string),
        //          href : (string),
        //          href_active : (string)
        //      }
        //  ]
        //]
        $this->Extorio()->createEvent("extorio_admin_menu_get_menu_items");

        //bind the menu items with high priority to try and keep it on the top
        $this->Extorio()->createAction("extorio_admin_menu_get_menu_items",$this,"on_get_menu_items_main",999999);
        //and the data structure next
        $this->Extorio()->createAction("extorio_admin_menu_get_menu_items",$this,"on_get_menu_items_data_management",999998);
        //bind the model viewer menu items second last
        $this->Extorio()->createAction("extorio_admin_menu_get_menu_items",$this,"on_get_menu_items_model_viewer",2);
        //bind the utilities menu items last
        $this->Extorio()->createAction("extorio_admin_menu_get_menu_items",$this,"on_get_menu_items_utilities",1);
    }

    public function on_get_menu_items_main() {
        $itemSet = array(
            "display_header" => true,
            "header_label" => "Extorio",
            "items" => array(
                array(
                    "label" => "Dashboard",
                    "description" => "Welcome to Extorio!",
                    "href" => "/extorio/admin/dashboard",
                    "href_active" => "/extorio/admin/dashboard",
                    "icon_class" => "glyphicon glyphicon-dashboard"
                ),
                array(
                    "label" => "Admin users",
                    "description" => "Manage admin users!",
                    "href" => "/extorio/admin/model-viewer/Core_Admin_User/list",
                    "href_active" => "/extorio/admin/model-viewer/Core_Admin_User/",
                    "icon_class" => "glyphicon glyphicon-user"
                ),
                array(
                    "label" => "Pages",
                    "description" => "Manage the pages of your website",
                    "href" => "/extorio/admin/pages",
                    "href_active" => "/extorio/admin/pages",
                    "icon_class" => "glyphicon glyphicon-film"
                ),
                array(
                    "label" => "Blocks",
                    "description" => "Manage the blocks of your website",
                    "href" => "/extorio/admin/blocks",
                    "href_active" => "/extorio/admin/blocks",
                    "icon_class" => "glyphicon glyphicon-th"
                ),
                array(
                    "label" => "Extensions",
                    "description" => "Install and manage your installed extensions",
                    "href" => "/extorio/admin/extensions",
                    "href_active" => "/extorio/admin/extensions",
                    "icon_class" => "glyphicon glyphicon-th-large"
                )
            )
        );

        return $itemSet;
    }

    public function on_get_menu_items_data_management() {
        return array(
            "display_header" => true,
            "header_label" => "Data management",
            "items" => array(
                array(
                    "label" => "Models",
                    "description" => "Manage the models of your data structure",
                    "href" => "/extorio/admin/models",
                    "href_active" => "/extorio/admin/models",
                    "icon_class" => "glyphicon glyphicon-hdd"
                ),
                array(
                    "label" => "Dropdowns",
                    "description" => "Manage the dropdowns used by models",
                    "href" => "/extorio/admin/dropdowns",
                    "href_active" => "/extorio/admin/dropdowns",
                    "icon_class" => "glyphicon glyphicon-sort-by-attributes"
                )
            )
        );
    }

    public function on_get_menu_items_utilities() {
        return array(
            "display_header" => true,
            "header_label" => "Utilities",
            "items" => array(
                array(
                    "label" => "Log manager",
                    "description" => "View and manage the interally created log files",
                    "href" => "/extorio/admin/log-manager",
                    "href_active" => "/extorio/admin/log-manager",
                    "icon_class" => "glyphicon glyphicon-folder-open"
                ),
                array(
                    "label" => "Task manager",
                    "description" => "View and stop currently running tasks",
                    "href" => "/extorio/admin/task-manager",
                    "href_active" => "/extorio/admin/task-manager",
                    "icon_class" => "glyphicon glyphicon-tasks"
                )
            )
        );
    }

    public function on_get_menu_items_model_viewer() {
        //if there are any persistent, non hidden classes
        $classes = Core_Class::findAll(Core_ORM_Finder::newInstance()
            ->where("(base.isHidden = 0 OR base.isHidden IS NULL) AND base.isPersistent = 1")
            ->orderBy("base.name")
            ->orderDir("asc")
        );
        if(count($classes)) {
            $itemSet = array(
                "display_header" => true,
                "header_label" => "Model viewer",
                "description" => "View model data"
            );

            foreach($classes as $class) {
                $itemSet["items"][] = array(
                    "label" => $class->name,
                    "href" => "/extorio/admin/model-viewer/".$class->name."/list",
                    "href_active" => "/extorio/admin/model-viewer/".$class->name."/"
                );
            }
            return $itemSet;
        } else {
            return null;
        }
    }

    public function on_exception_action($exception) {
        echo "<pre>";
        echo $exception;
        echo "</pre>";
    }
}