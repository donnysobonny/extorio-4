<?php
final class CoreModelListDatatablesApi extends Core_Api {

    private $model = false;
    /**
     * @var Core_Class
     */
    private $modelClass = false;
    /**
     * @var Core_Property[]
     */
    private $modelProperties = array();

    public $draw = false;
    public $start = false;
    public $length = false;
    public $search = array();
    public $order = array();
    public $columns = array();

    public $selectable = false;
    public $selectable_target = false;
    public $selectable_options = array();

    /**
     * SHORT_DESC
     *
     * @var Core_ApiResponse_ModelsListDatatables
     */
    public $output = false;

    public function canAccess() {
        //admin only
        return Core_Admin_User::authenticate();
    }

    public function onStart() {
        $this->output = new Core_ApiResponse_ModelsListDatatables();
        //override exceptions to place the error in error
        $this->Extorio()->createAction("on_exception",$this->output,"on_exception_action",9999999,true);
    }

    public function onLoad() {
        $this->output->draw = intval($this->draw);
    }

    public function onDefault($model=false) {
        $this->model = $model;
        //Core_Logger::debugLog(print_r($this,true));

        //make sure the model exists
        $this->modelClass = Core_Class::findByName($model);
        if(!$this->modelClass) {
            throw new Exception($model." is not a valid model");
        }
        $this->modelProperties = Core_Property::findAllByOwnerClassId($this->modelClass->id);

        foreach($this->modelProperties as $property) {
            $this->modelProperties[$property->name] = $property;
        }

        //if we are searching, build the where part of the query
        $where = "";
        if(strlen($this->search["value"])) {
            //go through each column, find the ones that are searchable and add them to the query
            foreach($this->columns as $column) {
                if($column["searchable"] == "true") {
                    $where .= "base.".$column["name"]." LIKE ('%".$this->search["value"]."%') OR ";
                }
            }
            $where = substr($where,0,strlen($where)-3);
        }

        //make sure the ordering is correct (sometimes it wont be!)
        $orderBy = "base.id";
        $orderDir = "desc";
        //if the order column is orderable, override
        if($this->columns[$this->order[0]["column"]]["orderable"] == "true") {
            $orderBy = "base.".$this->columns[$this->order[0]["column"]]["name"];
            $orderDir = $this->order[0]["dir"];
        }

        //find the results with a depth of TWO (get base and direct children)!
        /** @var Core_ORM_ComplexModel $type */
        $type = $this->model;
        $results = $type::findAll(Core_ORM_Finder::newInstance()
            ->where($where)
            ->limitResults($this->length)
            ->limitStart($this->start)
            ->orderBy($orderBy)
            ->orderDir($orderDir)
            ->depth(2)
        );

        //construct the data array, adding in only the fields that we want to display
        $n=0;
        foreach($results as $result) {
            if($this->selectable) {
                $propertyPathString = '';
                foreach($this->selectable_options["property_path"] as $property) {
                    $propertyPathString .= "'".$property."',";
                }
                $propertyPathString = substr($propertyPathString,0,strlen($propertyPathString)-1);
                $propertyPathString = "[".$propertyPathString."]";
                if($this->selectable_options["part_of_array"] == "true") {
                    $this->output->data[$n][] = '<button type="button" onclick="
    $(\''.$this->selectable_target.'\').appendEditForm(\''.$this->model.'\',{
        rid:'.$result->id.',
        removable:true,
        part_of_array:true,
        property_path:'.$propertyPathString.'
    });$(\'.modal.fade\').modal(\'hide\');
">select</button>';
                } else {
                    $this->output->data[$n][] = '<button type="button" onclick="
    $(\''.$this->selectable_target.'\').loadEditForm(\''.$this->model.'\',{
        rid:'.$result->id.',
        removable:true,
        part_of_array:false,
        property_path:'.$propertyPathString.'
    });$(\'.modal.fade\').modal(\'hide\');
">select</button>';
                }
            }

            foreach($this->columns as $column) {
                $name = $column["name"];
                //display the value based on the input type
                switch($this->modelProperties[$name]->inputType) {
                    case Core_Input_Types::_checkbox :
                        if($result->$name) {
                            $this->output->data[$n][] = '<span class="glyphicon glyphicon-ok"></span>';
                        } else {
                            $this->output->data[$n][] = '<span class="glyphicon glyphicon-remove"></span>';
                        }
                        break;
                    case Core_Input_Types::_date :
                        $this->output->data[$n][] = date("Y/m/d",strtotime($result->$name));
                        break;
                    case Core_Input_Types::_datetime :
                        $this->output->data[$n][] = date("Y/m/d H:i:s",strtotime($result->$name));
                        break;
                    case Core_Input_Types::_decimalfield :
                        $this->output->data[$n][] = $result->$name;
                        break;
                    case Core_Input_Types::_dropdown :
                        //get the element that is being used to output the name rather than the value
                        $element = Core_DropdownElement::findOne(Core_ORM_Finder::newInstance()
                            ->where("base.ownerDropdownId = ".$this->modelProperties[$name]->dropDownId." AND base.value = ('".$result->$name."')")
                        );
                        if($element) {
                            $this->output->data[$n][] = $element->name;
                        } else {
                            $this->output->data[$n][] = $result->$name;
                        }
                        break;
                    case Core_Input_Types::_html :
                        $this->output->data[$n][] = $result->$name;
                        break;
                    case Core_Input_Types::_model :
                        if($result->$name) {
                            $child = $result->$name;
                            $this->output->data[$n][] = '<button onclick="
    $(\'#model_detail_modal .modal-body\').loadDetail(\''.get_class($child).'\',{
                rid:'.$child->id.'
            });
    $(\'#model_detail_modal\').modal();
" type="button" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-open"></span> '.$child->toString().'</button> ';
                        } else {
                            $this->output->data[$n][] = "none";
                        }
                        break;
                    case Core_Input_Types::_model_array :
                        if($result->$name) {
                            $children = $result->$name;
                            $display = "";
                            foreach($children as $child) {
                                $display .= '<button onclick="
    $(\'#model_detail_modal .modal-body\').loadDetail(\''.get_class($child).'\',{
                rid:'.$child->id.'
            });
    $(\'#model_detail_modal\').modal();
" type="button" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-open"></span> '.$child->toString().'</button> ';
                            }
                            $this->output->data[$n][] = $display;
                        } else {
                            $this->output->data[$n][] = "none";
                        }
                        break;
                    case Core_Input_Types::_numberfield :
                        $this->output->data[$n][] = $result->$name;
                        break;
                    case Core_Input_Types::_textarea :
                        $this->output->data[$n][] = $result->$name;
                        break;
                    case Core_Input_Types::_textfield :
                        $this->output->data[$n][] = $result->$name;
                        break;
                    case Core_Input_Types::_time :
                        $this->output->data[$n][] = $result->$name;
                        break;
                }
            }

            $this->output->data[$n][] = '
            <button type="button" onclick="
            $(\'#model_detail_modal .modal-body\').loadDetail(\''.get_class($result).'\',{
                rid:'.$result->id.'
            });
            $(\'#model_detail_modal\').modal();
            " class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-open"></span> detail</button>
            <a class="btn btn-xs btn-primary" href="/extorio/admin/model-viewer/'.get_class($result).'/edit/'.$result->id.'"><span class="glyphicon glyphicon-edit"></span> edit</a>
            <button type="button" onclick="
            Core_Extorio.Modal.openModal(\'small\',\'Delete object?\',\'Deleting this object may result in objects being deleted that are linked to this object. Are you sure that you want to delete this object?\',\'Cancel\',\'Delete\',function() {
                Core_ORM_ComplexModel.remove(\''.get_class($result).'\','.$result->id.',function() {
                    location.reload();
                });
            });
            " class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span> delete</button>
            ';

            $n++;
        }

        $count = $type::findCount(Core_ORM_Finder::newInstance()
            ->where($where)
        );

        $this->output->recordsTotal = $count;
        $this->output->recordsFiltered = $count;
    }

    public function onComplete() {

    }
}