<?php

final class CoreModelViewerApi extends Core_Api {

    private $model = false;
    private $action = false;
    /**
     * @var Core_Class
     */
    private $modelClass = false;

    /**
     * @var Core_Property[]
     */
    private $modelProperties = array();


    public $rid = false;

    /**
     * @var Core_ORM_ComplexModel
     */
    private $objectToEdit = false;

    public $property_path = array();

    public $part_of_array = false;

    public $removable = false;

    public $selectable = false;

    public $selectable_target = false;

    public $selectable_options = array();

    public function canAccess() {
        //admins only
        return Core_Admin_User::authenticate();
    }

    public function onStart() {

    }

    public function onLoad() {

    }

    public function onDefault($model = false, $action = false) {
        //make sure api accessed properly
        if (!$model || !$action) {
            throw new Exception("Model and/or action not found in the request url");
        }

        //make sure the model is valid
        $this->modelClass = Core_Class::findByName($model);
        if (!$this->modelClass) {
            throw new Exception("The model " . $model . " was not found.");
        }

        //validate the action
        if (!in_array($action, array("edit", "create", "detail", "list"))) {
            throw new Exception("The action " . $action . " was not recognised");
        }

        $this->model = $model;
        $this->action = $action;

        //get the model we are editing if there is one
        if ($this->rid) {
            /** @var Core_ORM_ComplexModel $type */
            $type = $this->model;
            $this->objectToEdit = $type::findById($this->rid, Core_ORM_Finder::newInstance());
        }

        //if part of array, add random element to the property path
        if ($this->part_of_array) {
            $this->property_path[] = uniqid();
        }

        //run the action
        $action = $action . "View";
        $this->$action();
    }

    private function detailView() {
        $this->modelProperties = Core_Property::findAllByOwnerClassId($this->modelClass->id);
        $tableId = uniqid();
        $this->output->data = '<table id="'.$tableId.'" class="model_view_detail"><tbody>';

        $class = "odd";
        foreach($this->modelProperties as $property) {
            $name = $property->name;
            $this->output->data .= '<tr class="'.$class.'">
    <td class="model_view_detail_left_cell">'.$property->name.'</td>
    <td class="model_view_detail_right_cell">
        ';
            $class = $class == "odd"?"even":"odd";
            switch($property->inputType) {
                case Core_Input_Types::_checkbox :
                    $this->output->data .= $this->getDetailRow_checkbox($property,$this->objectToEdit->$name);
                    break;
                case Core_Input_Types::_date :
                    $this->output->data .= $this->getDetailRow_date($property,$this->objectToEdit->$name);
                    break;
                case Core_Input_Types::_datetime :
                    $this->output->data .= $this->getDetailRow_datetime($property,$this->objectToEdit->$name);
                    break;
                case Core_Input_Types::_decimalfield :
                    $this->output->data .= $this->getDetailRow_decimalfield($property,$this->objectToEdit->$name);
                    break;
                case Core_Input_Types::_dropdown :
                    $this->output->data .= $this->getDetailRow_dropdown($property,$this->objectToEdit->$name);
                    break;
                case Core_Input_Types::_html :
                    $this->output->data .= $this->getDetailRow_html($property,$this->objectToEdit->$name);
                    break;
                case Core_Input_Types::_model :
                    $this->output->data .= $this->getDetailRow_model($property,$this->objectToEdit->$name);
                    break;
                case Core_Input_Types::_model_array :
                    $this->output->data .= $this->getDetailRow_model_array($property,$this->objectToEdit->$name);
                    break;
                case Core_Input_Types::_numberfield :
                    $this->output->data .= $this->getDetailRow_numberfield($property,$this->objectToEdit->$name);
                    break;
                case Core_Input_Types::_textarea :
                    $this->output->data .= $this->getDetailRow_textarea($property,$this->objectToEdit->$name);
                    break;
                case Core_Input_Types::_textfield :
                    $this->output->data .= $this->getDetailRow_textfield($property,$this->objectToEdit->$name);
                    break;
                case Core_Input_Types::_time :
                    $this->output->data .= $this->getDetailRow_time($property,$this->objectToEdit->$name);
                    break;
            }

            $this->output->data .= '
    </td>
</tr>';
        }

        $this->output->data .= '</tbody></table>';
    }

    /**
     * @param Core_Property $property
     * @param mixed $value
     *
     * @return string
     */
    private function getDetailRow_checkbox($property, $value="") {
        if(strlen($value)) {
            if($value == "1") {
                return '<span class="glyphicon glyphicon-ok"></span>';
            } else {
                return '<span class="glyphicon glyphicon-remove"></span>';
            }
        } else {
            return '<span class="glyphicon glyphicon-remove"></span>';
        }
    }

    /**
     * @param Core_Property $property
     * @param mixed $value
     *
     * @return string
     */
    private function getDetailRow_date($property, $value="") {
        return $value;
    }

    /**
     * @param Core_Property $property
     * @param mixed $value
     *
     * @return string
     */
    private function getDetailRow_datetime($property, $value="") {
        return $value;
    }

    /**
     * @param Core_Property $property
     * @param mixed $value
     *
     * @return string
     */
    private function getDetailRow_decimalfield($property, $value="") {
        return $value;
    }

    /**
     * @param Core_Property $property
     * @param mixed $value
     *
     * @return string
     */
    private function getDetailRow_dropdown($property, $value="") {
        //get the element's name that this property is using
        $element = Core_DropdownElement::findOne(Core_ORM_Finder::newInstance()
            ->where("base.ownerDropdownId = ".$property->dropDownId." AND value = ('".$value."')")
        );
        if($element) {
            return $element->name;
        } else {
            return "";
        }
    }

    /**
     * @param Core_Property $property
     * @param mixed $value
     *
     * @return string
     */
    private function getDetailRow_html($property, $value="") {
        return $value;
    }

    /**
     * @param Core_Property $property
     * @param mixed $value
     *
     * @return string
     */
    private function getDetailRow_model($property, $value=null) {
        $html = "";
        if($value) {
            $childClass = Core_Class::findById($property->childClassId);
            $divId = uniqid();
            $html = '<div id="'.$divId.'" class="model_view_detail_child_model">
    ';
            $html .= '
</div>';
            $html .= '
            <script>
                $(function() {
                    $(\'#'.$divId.'\').loadDetail(\''.$childClass->name.'\',{
                        rid:'.$value->id.'
                    })
                });
            </script>
            ';
        }

        return $html;
    }

    /**
     * @param Core_Property $property
     * @param mixed $value
     *
     * @return string
     */
    private function getDetailRow_model_array($property, $value=array()) {
        $html = "";

        if($value) {
            $childClass = Core_Class::findById($property->childClassId);
            $divId = uniqid();
            $html = '<div id="'.$divId.'" class="model_view_detail_child_model">
    ';
            $html .= '
</div>';
            $html .= '
            <script>
            $(function() {
                ';
            foreach($value as $child) {
                $html .= '
                $(\'#'.$divId.'\').appendDetail(\''.$childClass->name.'\',{
                    rid:'.$child->id.'
                });
                ';
            }
            $html .= '
            });
</script>
            ';
        }

        return $html;
    }

    /**
     * @param Core_Property $property
     * @param mixed $value
     *
     * @return string
     */
    private function getDetailRow_numberfield($property, $value="") {
        return $value;
    }

    /**
     * @param Core_Property $property
     * @param mixed $value
     *
     * @return string
     */
    private function getDetailRow_textarea($property, $value="") {
        return $value;
    }

    /**
     * @param Core_Property $property
     * @param mixed $value
     *
     * @return string
     */
    private function getDetailRow_textfield($property, $value="") {
        return $value;
    }

    /**
     * @param Core_Property $property
     * @param mixed $value
     *
     * @return string
     */
    private function getDetailRow_time($property, $value="") {

    }

    private function listView() {
        //get this model's properties that should exist in list view
        $this->modelProperties = Core_Property::findAll(Core_ORM_Finder::newInstance()->where("base.visibleInList = 1 AND base.ownerClassId = " . $this->modelClass->id)->orderBy("base.position")->orderDir("asc"));
        $tableId = uniqid();

        $this->output->data = '
        <div id="model_detail_modal" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Detail view</h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->';

        $this->output->data .= '<table id="' . $tableId . '" class="table table-striped">
    <thead>
        <tr>';

        if ($this->selectable) {
            $this->output->data .= "<th></th>";
        }

        foreach ($this->modelProperties as $property) {
            $this->output->data .= '<th>' . $property->name . '</th>';
        }

        $this->output->data .= '<th><span class="glyphicon glyphicon-cog"></span></th>';

        $this->output->data .= '
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<script>
    $(function() {
        $("#' . $tableId . '").DataTable({
            serverSide: true,
            columns:[
                ';
        $selectable = $this->selectable ? "true" : "false";
        if ($this->selectable) {
            $this->output->data .= '{searchable:false,orderable:false},';
        }

        foreach ($this->modelProperties as $property) {
            if (($property->isPrimary || $property->isIndex || $property->isUnique) && $property->inputType != Core_Input_Types::_model && $property->inputType != Core_Input_Types::_model_array
            ) {
                $this->output->data .= '{
                            name: "' . $property->name . '",
                            searchable: true,
                            orderable: true
                        },';
            } else {
                $this->output->data .= '{
                            name: "' . $property->name . '",
                            searchable: false,
                            orderable: false
                        },';
            }
        }

        $this->output->data .= '{searchable:false,orderable:false}';

        $this->output->data .= '
            ],
            ajax: {
                url: "/extorio/elements/apis/core/CoreModelListDatatablesApi/' . $this->model . '",
                data: {
                    selectable:' . $selectable . ',
                    selectable_target:\'' . $this->selectable_target . '\',
                    selectable_options:' . json_encode($this->selectable_options) . '
                }
            }
        });
    });
</script>
';
    }

    private function editView() {
        //get this model's properties
        $this->modelProperties = Core_Property::findAllByOwnerClassId($this->modelClass->id);
        $tableUid = uniqid();
        $this->output->data = "<table id='" . $tableUid . "' class='model_view_table'>
    <tbody>";

        foreach ($this->modelProperties as $property) {
            if ($this->objectToEdit) {
                $name = $property->name;
                $row = $this->getEditFormInput($property, $this->property_path, $this->objectToEdit->$name);
                if (!is_null($row)) {
                    $this->output->data .= $row;
                }
            } else {
                $row = $this->getEditFormInput($property, $this->property_path);
                if (!is_null($row)) {
                    $this->output->data .= $row;
                }
            }
        }

        $this->output->data .= "
    <tbody>
</table>";
        if ($this->removable) {
            $this->output->data .= '<a href="javascript:void();" type="button" onclick="$(\'#' . $tableUid . '\').remove();$(this).remove();"><span class="glyphicon glyphicon-chevron-up"></span> clear</a>';
        }
    }

    /**
     * @param Core_Property $property
     * @param array $propertyPath
     * @param mixed $value
     */
    private function getEditFormInput($property, $propertyPath, $value = "") {

        $html = "";

        //add this property to the path
        $propertyPath[] = $property->name;

        $html .= '<tr>
    <td class="model_view_table_left_cell">' . $property->name . '</td>
    <td class="model_view_table_right_cell">';

        switch ($property->inputType) {
            case Core_Input_Types::_checkbox :
                $html .= $this->getEditFormInput_checkbox($property, $propertyPath, $value);
                break;
            case Core_Input_Types::_date :
                $html .= $this->getEditFormInput_date($property, $propertyPath, $value);
                break;
            case Core_Input_Types::_datetime :
                $html .= $this->getEditFormInput_datetime($property, $propertyPath, $value);
                break;
            case Core_Input_Types::_decimalfield :
                $html .= $this->getEditFormInput_decimalfield($property, $propertyPath, $value);
                break;
            case Core_Input_Types::_dropdown :
                $html .= $this->getEditFormInput_dropdown($property, $propertyPath, $value);
                break;
            case Core_Input_Types::_html :
                $html .= $this->getEditFormInput_html($property, $propertyPath, $value);
                break;
            case Core_Input_Types::_model :
                $html .= $this->getEditFormInput_model($property, $propertyPath, $value);
                break;
            case Core_Input_Types::_model_array :
                $html .= $this->getEditFormInput_model_array($property, $propertyPath, $value);
                break;
            case Core_Input_Types::_numberfield :
                $html .= $this->getEditFormInput_numberfield($property, $propertyPath, $value);
                break;
            case Core_Input_Types::_textarea :
                $html .= $this->getEditFormInput_textarea($property, $propertyPath, $value);
                break;
            case Core_Input_Types::_textfield :
                $html .= $this->getEditFormInput_textfield($property, $propertyPath, $value);
                break;
            case Core_Input_Types::_time :
                $html .= $this->getEditFormInput_time($property, $propertyPath, $value);
                break;
        }

        $html .= '</td>
</tr>';

        return $html;
    }

    /**
     * @param Core_Property $property
     * @param array $propertyPath
     * @param Core_ORM_ComplexModel $value
     *
     * @return string
     */
    private function getEditFormInput_model($property, $propertyPath, $value) {

        //get the child class
        $childModelClass = Core_Class::findById($property->childClassId);

        $divId = uniqid();
        $html = '';

        //generate the property path string
        $propertyPathString = $this->getPropertyPathString($propertyPath);

        //create the empty hidden input for when this object is not assigned/cleared
        $html .= '<input type="hidden" name="'.$this->getEditFormInput_NameValue($propertyPath).'" value="" />';

        $html .= '<div class="model_view_child_model_buttons">';

        //add the select/create button based on whether the model is persistent or not
        if ($childModelClass->isPersistent) {
            $selUid = uniqid();
            $modUid = uniqid();
            $html .= '
<div id="' . $modUid . '" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Select a ' . $childModelClass->name . '</h4>
      </div>
      <div class="modal-body">
        <div id="' . $selUid . '"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>';
            $html .= '<button class="btn btn-xs btn-primary" type="button" onclick="$(\'#' . $selUid . '\').loadList(\'' . $childModelClass->name . '\',{
    selectable:true,
    selectable_target:\'#' . $divId . '\',
    selectable_options: {
        part_of_array: false,
        removable:true,
        property_path:' . $propertyPathString . '
    }
});$(\'#' . $modUid . '\').modal();"><span class="glyphicon glyphicon-search"></span> select</button>';
            //select from list
            if ($value) {
                //load an edit form with the child object
                if ($value->id) {
                    $html .= '
                    <script>
    $(function() {
        $(\'#' . $divId . '\').loadEditForm(\'' . $childModelClass->name . '\',{
            rid: ' . $value->id . ',
            removable: true,
            property_path: ' . $propertyPathString . '
        })
    })
</script>
                    ';
                }
            }
        } else {
            $html .= '<button class="btn btn-xs btn-primary" type="button" onclick="$(\'#' . $divId . '\').loadEditForm(\'' . $childModelClass->name . '\',{
                    removable:true,
                    property_path:' . $propertyPathString . '
                })"><span class="glyphicon glyphicon-plus"></span> create</button>';
            //we are either creating an edit or a create form based on whether a child object exists here or not
            if ($value) {
                //load an edit form with the child object
                if ($value->id) {
                    $html .= '
                    <script>
    $(function() {
        $(\'#' . $divId . '\').loadEditForm(\'' . $childModelClass->name . '\',{
            rid: ' . $value->id . ',
            removable: true,
            property_path: ' . $propertyPathString . '
        })
    })
</script>
                    ';
                }
            }
        }

        $html .= '</div>';

        $html .= '<div id="' . $divId . '" class="model_view_child_model">';

        $html .= '</div>';

        return $html;
    }

    /**
     * @param Core_Property $property
     * @param array $propertyPath
     * @param Core_ORM_ComplexModel $value
     *
     * @return string
     */
    private function getEditFormInput_model_array($property, $propertyPath, $value) {
        //get the child class
        $childModelClass = Core_Class::findById($property->childClassId);

        $divId = uniqid();
        $html = '';

        //generate the property path string
        $propertyPathString = $this->getPropertyPathString($propertyPath);

        //create the empty hidden input for when this object is not assigned/cleared
        $html .= '<input type="hidden" name="'.$this->getEditFormInput_NameValue($propertyPath).'" value="" />';

        $html .= '<div class="model_view_child_model_buttons">';

        //add the select/create button based on whether the model is persistent or not
        if ($childModelClass->isPersistent) {
            $selUid = uniqid();
            $modUid = uniqid();
            $html .= '
<div id="' . $modUid . '" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Select a ' . $childModelClass->name . '</h4>
      </div>
      <div class="modal-body">
        <div id="' . $selUid . '"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>';
            $html .= '<button class="btn btn-xs btn-primary" type="button" onclick="$(\'#' . $selUid . '\').loadList(\'' . $childModelClass->name . '\',{
    selectable:true,
    selectable_target:\'#' . $divId . '\',
    selectable_options: {
        part_of_array: true,
        removable:true,
        property_path:' . $propertyPathString . '
    }
});$(\'#' . $modUid . '\').modal();"><span class="glyphicon glyphicon-search"></span> select</button>';
            //select from list
            if ($value) {
                $html .= '<script>
                    $(function() {                        ';
                foreach ($value as $child) {
                    $html .= '$(\'#' . $divId . '\').appendEditForm(\'' . $childModelClass->name . '\',{
                            rid: ' . $child->id . ',
                            removable: true,
                            part_of_array: true,
                            property_path: ' . $propertyPathString . '
                        });
                        ';
                }
                $html .= '
                    });';
                $html .= '</script>';
            }
        } else {
            $html .= '<button class="btn btn-xs btn-primary" type="button" onclick="$(\'#' . $divId . '\').appendEditForm(\'' . $childModelClass->name . '\',{
                    removable:true,
                    property_path:' . $propertyPathString . ',
                    part_of_array:true
                })"><span class="glyphicon glyphicon-plus"></span> create</button>';
            //we are either creating an edit or a create form based on whether a child object exists here or not
            if ($value) {
                $html .= '<script>
                    $(function() {                        ';
                foreach ($value as $child) {
                    $html .= '$(\'#' . $divId . '\').appendEditForm(\'' . $childModelClass->name . '\',{
                            rid: ' . $child->id . ',
                            removable: true,
                            part_of_array: true,
                            property_path: ' . $propertyPathString . '
                        });
                        ';
                }
                $html .= '
                    });';
                $html .= '</script>';
            }
        }
        $html .= '</div>';

        $html .= '<div id="' . $divId . '" class="model_view_child_model">';

        $html .= '</div>';

        return $html;
    }

    /**
     * @param Core_Property $property
     * @param array $propertyPath
     * @param mixed $value
     *
     * @return string
     */
    private function getEditFormInput_checkbox($property, $propertyPath, $value) {
        $cbUid = uniqid();
        $checked = $property->checkBoxDefault;
        if (strlen($value)) {
            $checked = $value;
        }
        $checkedAttr = $checked ? 'checked="checked"' : '';
        $html = '<input onchange="var checked = $(this).prop(\'checked\');if(checked) {$(\'#' . $cbUid . '\').val(1);} else {$(\'#' . $cbUid . '\').val(0);}" ' . $checkedAttr . ' type="checkbox" class="model_view_input_' . $property->inputType . '" />';
        $html .= '<input id="' . $cbUid . '" type="hidden" name="' . $this->getEditFormInput_NameValue($propertyPath) . '" value="' . $checked . '" />';
        return $html;
    }

    private function getEditFormInput_date($property, $propertyPath, $value) {
        return '<input class="model_view_input_' . $property->inputType . '" size="9" type="text" name="' . $this->getEditFormInput_NameValue($propertyPath) . '" value="' . $value . '" />';
    }

    private function getEditFormInput_datetime($property, $propertyPath, $value) {
        return '<input class="model_view_input_' . $property->inputType . '" size="16" type="text" name="' . $this->getEditFormInput_NameValue($propertyPath) . '" value="' . $value . '" />';
    }

    /**
     * @param Core_Property $property
     * @param array $propertyPath
     * @param mixed $value
     *
     * @return string
     */
    private function getEditFormInput_decimalfield($property, $propertyPath, $value) {
        $size = $property->maxLength;
        if ($size > 100) {
            $size = 100;
        }
        if ($size == 0) {
            $size = 10;
        }
        $maxLength = $property->maxLength;
        if ($maxLength == 0) {
            $maxLength = -1;
        }
        return '<input name="' . $this->getEditFormInput_NameValue($propertyPath) . '" class="model_view_input_' . $property->inputType . '" type="text" size="' . $size . '" maxlength="' . $maxLength . '" value="' . $value . '" />';
    }

    /**
     * @param Core_Property $property
     * @param array $propertyPath
     * @param mixed $value
     *
     * @return string
     */
    private function getEditFormInput_dropdown($property, $propertyPath, $value) {
        //get the elements for this dropdown
        $elements = Core_DropdownElement::findByOwnerId($property->dropDownId);
        $html = '<select name="' . $this->getEditFormInput_NameValue($propertyPath) . '" class="model_view_input_' . $property->inputType . '">';
        foreach ($elements as $element) {
            if ($value == $element->value) {
                $html .= '<option selected="selected" value="' . $element->value . '">' . $element->name . '</option>';
            } else {
                $html .= '<option value="' . $element->value . '">' . $element->name . '</option>';
            }
        }
        $html .= "</select>";
        return $html;
    }

    /**
     * @param Core_Property $property
     * @param array $propertyPath
     * @param mixed $value
     *
     * @return string
     */
    private function getEditFormInput_html($property, $propertyPath, $value) {
        $maxLength = $property->maxLength;
        if ($maxLength == 0) {
            $maxLength = -1;
        }
        return '<textarea class="froala_editable_non_inline model_view_input_' . $property->inputType . '" maxlength="' . $maxLength . '" cols="40" rows="4" name="' . $this->getEditFormInput_NameValue($propertyPath) . '">' . $value . '</textarea>';
    }

    /**
     * @param Core_Property $property
     * @param array $propertyPath
     * @param mixed $value
     *
     * @return string
     */
    private function getEditFormInput_numberfield($property, $propertyPath, $value) {
        $size = $property->maxLength;
        if ($size > 100) {
            $size = 100;
        }
        if ($size == 0) {
            $size = 20;
        }
        $maxLength = $property->maxLength;
        if ($maxLength == 0) {
            $maxLength = -1;
        }
        return '<input name="' . $this->getEditFormInput_NameValue($propertyPath) . '" class="model_view_input_' . $property->inputType . '" type="text" size="' . $size . '" maxlength="' . $maxLength . '" value="' . $value . '" />';
    }

    /**
     * @param Core_Property $property
     * @param array $propertyPath
     * @param mixed $value
     *
     * @return string
     */
    private function getEditFormInput_textarea($property, $propertyPath, $value) {
        $maxLength = $property->maxLength;
        if ($maxLength == 0) {
            $maxLength = -1;
        }
        return '<textarea class="model_view_input_' . $property->inputType . '" maxlength="' . $maxLength . '" cols="40" rows="4" name="' . $this->getEditFormInput_NameValue($propertyPath) . '">' . $value . '</textarea>';
    }

    /**
     * @param Core_Property $property
     * @param array $propertyPath
     * @param mixed $value
     *
     * @return string
     */
    private function getEditFormInput_textfield($property, $propertyPath, $value) {
        $size = $property->maxLength;
        if ($size > 100) {
            $size = 100;
        }
        if ($size == 0) {
            $size = 20;
        }
        $maxLength = $property->maxLength;
        if ($maxLength == 0) {
            $maxLength = -1;
        }
        return '<input class="model_view_input_' . $property->inputType . '" size="' . $size . '" maxlength="' . $maxLength . '" type="text" name="' . $this->getEditFormInput_NameValue($propertyPath) . '" value="' . $value . '" />';
    }

    private function getEditFormInput_time($property, $propertyPath, $value) {
        return '<input class="model_view_input_' . $property->inputType . '" size="6" type="text" name="' . $this->getEditFormInput_NameValue($propertyPath) . '" value="' . $value . '" />';
    }

    private function getEditFormInput_NameValue($propertyPath) {
        $value = "base";
        foreach ($propertyPath as $name) {
            $value .= "[" . $name . "]";
        }
        return $value;
    }

    private function getPropertyPathString($propertyPath) {
        $string = "";
        foreach ($propertyPath as $property) {
            $string .= "'" . $property . "',";
        }
        $string = substr($string, 0, strlen($string) - 1);

        return "[" . $string . "]";
    }
}