<?php
final class CoreComplexModelsApi extends Core_Api {

    private $model = false;
    /**
     * @var Core_Class
     */
    private $modelClass = false;
    private $action = false;
    public $id = false;
    public $query = "";
    public $limit_results = false;
    public $limit_start = false;
    public $order_by = "base.id";
    public $order_dir = "desc";
    public $depth = 10;

    public $object_data = array();

    public $username = false;
    public $password = false;

    public $session = false;

    private $modelMethods = array(
        "login",
        "logout",
        "count",
        "describe",
        "describe_html",
        "structure_html"
    );

    public function canAccess() {
        //can always access
        return true;
    }

    public function onStart() {

    }

    public function onLoad() {

    }

    public function onDefault($model=false,$action=false) {
        $this->model = $model;
        $this->action = $action;
        //make sure the model is specified and exists
        if($this->model) {
            $this->modelClass = Core_Class::findByName($this->model);
            if(!$this->modelClass) {
                $this->throwError("The model ".$this->model." does not exist",0,400);
            }
        } else {
            $this->throwError("You must specify a model in your url",0,400);
        }

        //if the action is numeric, it's the id
        if(is_numeric($this->action)) {
            $this->id = $this->action;
            $this->action = false;
        }

        //if there is no action, it's a simple request
        if(!$this->action) {
            switch($this->httpMethod) {
                case "GET" :
                    $this->get();
                    break;
                case "POST" :
                    $this->post();
                    break;
                case "PUT" :
                    $this->put();
                    break;
                case "DELETE" :
                    $this->delete();
                    break;
                default :
                    $this->get();
                    break;
            }
        } else {
            //make sure the model method is valid
            if(!in_array($this->action,$this->modelMethods)) {
                $this->throwError($this->action." is not a valid model method.",0,400);
            }

            //run the model method
            $action = $this->action;
            $this->$action();
        }
    }

    private function login() {
        //login must be get requests
        $this->validateHttpMethod("POST",true);

        /** @var Core_BaseModel_Type_User $type */
        $type = $this->model;

        //make sure the model is a user type
        if($this->modelClass->type != "user") {
            $this->throwError($this->model." is not a user model, therefore you cannot log in as this model.");
        }

        //make sure username and password provided
        if(!$this->username || !$this->password) {
            $this->throwError("You must provide a username and password in order to log in.",0,400);
        }

        //find the user
        $user = $type::findOne(Core_ORM_Finder::newInstance()->where("
        (base.username = ('".$this->username."') OR base.email = ('".$this->username."')) AND
        base.password = ('".$this->password."')
        ")->ignoreAccessChecks(true));
        if($user) {
            //make sure the user can log in and has validated their email
            if(!$user->canLogin || !$user->emailVerified) {
                $this->throwError("There was a problem logging in to your account.",0,400);
            }

            //clear any sessions that already exist for this user
            $sessions = Core_ApiUserSession::findAll(Core_ORM_Finder::newInstance()->where("base.userType = ('".$this->model."') AND base.userId = ".$user->id));
            foreach($sessions as $session) {
                $session->removeThis();
            }

            //create a new session for this ip and user
            $session = new Core_ApiUserSession();
            $session->userType = $this->model;
            $session->userId = $user->id;
            $session->remoteIp = Core_Utils_Server::getIpAddress();
            $session->session = md5($user->id.":".$user->username.":".time());
            $session->pushThis();

            //set the user as the logged in user
            $type::setLoggedInUserId($user->id);

            //return the session
            $this->output->data = $session->session;
        } else {
            $this->throwError("Username and/or password incorrect",0,400);
        }
    }

    private function logout() {
        //login must be get requests
        $this->validateHttpMethod("POST",true);

        /** @var Core_BaseModel_Type_User $type */
        $type = $this->model;

        //make sure the model is a user type
        if($this->modelClass->type != "user") {
            $this->throwError($this->model." is not a user model, therefore you cannot log out as this model..");
        }

        //make sure a session exists
        if(!$this->session) {
            $this->throwError("You must specify the session that you are logging out from.");
        }

        //find the session
        $apiSession = Core_ApiUserSession::findBySession($this->session,Core_Utils_Server::getIpAddress());
        if($apiSession) {
            //delete the session
            $apiSession->removeThis();

            //log any users of this type out (kill the server session)
            $type::logout();
        }
    }

    private function count() {
        //counts must be get requests
        $this->validateHttpMethod("GET",true);

        /** @var Core_ORM_ComplexModel $type */
        $type = $this->model;

        $this->output->data = $type::findCount(Core_ORM_Finder::newInstance()
            ->where($this->query)
            ->limitStart($this->limit_start)
            ->limitResults($this->limit_results)
            ->orderBy($this->order_by)
            ->orderDir($this->order_dir)
            ->depth($this->depth)
        );
    }

    private function describe() {
        //describe must be get requests
        $this->validateHttpMethod("GET",true);

        /** @var Core_ORM_ComplexModel $type */
        $type = $this->model;

        $this->output->data = $type::getDescription_Array();
    }

    private function describe_html() {
        //describe must be get requests
        $this->validateHttpMethod("GET",true);

        /** @var Core_ORM_ComplexModel $type */
        $type = $this->model;

        $this->output->data = $type::getDescription_Html();
    }

    private function structure_html() {
        //structure must be get requests
        $this->validateHttpMethod("GET",true);

        /** @var Core_ORM_ComplexModel $type */
        $type = $this->model;

        $this->output->data = $type::getStructure();
    }

    private function get() {
        /** @var Core_ORM_ComplexModel $type */
        $type = $this->model;

        //if there is an id, we are finding one by id, otherwise we are finding all
        if($this->id) {
            $this->output->data = $type::findById($this->id,Core_ORM_Finder::newInstance()
                ->where($this->query)
                ->limitResults($this->limit_results)
                ->limitStart($this->limit_start)
                ->orderBy($this->order_by)
                ->orderDir($this->order_dir)
                ->depth($this->depth)
            );
        } else {
            $this->output->data = $type::findAll(Core_ORM_Finder::newInstance()
                ->where($this->query)
                ->limitResults($this->limit_results)
                ->limitStart($this->limit_start)
                ->orderBy($this->order_by)
                ->orderDir($this->order_dir)
                ->depth($this->depth));
        }
    }

    private function post() {
        /** @var Core_ORM_ComplexModel $type */
        $type = $this->model;

        //the id might be in the object data
        if(!$this->id && isset($this->object_data["id"])) {
            $this->id = $this->object_data["id"];
        }

        //if there is an id, make sure the object doesn't already exist
        if($this->id) {
            $existing = $type::findById($this->id,Core_ORM_Finder::newInstance());
            if($existing) {
                $this->throwError("A ".$this->model." already exists with the id ".$this->id.". If you want to update this object, use a PUT request instead.");
            }
        }

        //check that the object data is not empty
        if(Core_Utils_Array::valuesEmpty($this->object_data)) {
            $this->throwError("object_data was empty. You cannot create an empty object!",0,400);
        }

        //construct from array
        if($this->id) {
            //make sure the id is part of the object data
            $this->object_data["id"] = $this->id;
        }
        $object = $type::constructFromArray($this->object_data);
        if($object) {
            $object->pushThis();
            //re-pull. Access checks may stop certain modifications being made, so re-pulling will show that
            $object->pullThis();

            $this->output->data = $object;
        } else {
            $this->throwError("There was a problem creating your object");
        }
    }

    private function put() {
        /** @var Core_ORM_ComplexModel $type */
        $type = $this->model;

        //the id might be in the object data
        if(!$this->id && isset($this->object_data["id"])) {
            $this->id = $this->object_data["id"];
        }

        //make sure there is an id, and an object exists with this id
        if($this->id) {
            $existing = $type::findById($this->id,Core_ORM_Finder::newInstance());
            if(!$existing) {
                $this->throwError("You are trying to update a ".$this->model." with the id of ".$this->id.", however an object does not exist with this id. If you want to create this object, use a POST request isntead.");
            }
        } else {
            $this->throwError("You are trying to update a ".$this->model." but you have not sent the id of the object that you want to update. If you are trying to create, use a POST request");
        }

        //check that the object data is not empty
        if(Core_Utils_Array::valuesEmpty($this->object_data)) {
            $this->throwError("object_data was empty. You must update atleast one property!",0,400);
        }

        //construct from array
        //make sure the id is part of the object data
        $this->object_data["id"] = $this->id;
        $object = $type::constructFromArray($this->object_data);
        if($object) {
            $object->pushThis();
            //re-pull. Access checks may stop certain modifications being made, so re-pulling will show that
            $object->pullThis();

            $this->output->data = $object;
        } else {
            $this->throwError("There was a problem updating your object");
        }
    }

    private function delete() {
        /** @var Core_ORM_ComplexModel $type */
        $type = $this->model;

        //the id might be in the object data
        if(!$this->id && isset($this->object_data["id"])) {
            $this->id = $this->object_data["id"];
        }

        //make sure there is an id. At the moment, you can only delete one object at a time, later we may change this.
        if(!$this->id) {
            $this->throwError("You must specify the id of the object that you want to delete.");
        }

        $object = $type::findById($this->id,Core_ORM_Finder::newInstance());
        if($object) {
            $object->removeThis();
        }
    }

    public function onComplete() {

    }
}