<?php
final class CoreBasicModelsApi extends Core_Api {

    private $model = false;
    private $action = false;
    public $id = false;
    public $query = "";
    public $limit_results = false;
    public $limit_start = false;
    public $order_by = "base.id";
    public $order_dir = "desc";

    public $object_data = array();

    private $modelMethods = array(
        "count"
    );

    public function canAccess() {
        //for admins only!
        return Core_Admin_User::authenticate();
    }

    public function onStart() {

    }

    public function onLoad() {

    }

    public function onDefault($model=false,$action=false) {
        $this->model = $model;
        $this->action = $action;
        //make sure the model is specified and exists
        if($this->model) {
            $db = $this->Extorio()->getDbInstanceDefault();
            $ssql = "DESCRIBE `".$this->model."`";
            if(!$db->getRowIndices($ssql)) {
                $this->throwError("The model ".$this->model." does not exist",0,400);
            }
        } else {
            $this->throwError("You must specify a model in your url",0,400);
        }

        //if the action is numeric, it's the id
        if(is_numeric($this->action)) {
            $this->id = $this->action;
            $this->action = false;
        }

        //if there is no action, it's a simple request
        if(!$this->action) {
            switch($this->httpMethod) {
                case "GET" :
                    $this->get();
                    break;
                case "POST" :
                    $this->post();
                    break;
                case "PUT" :
                    $this->put();
                    break;
                case "DELETE" :
                    $this->delete();
                    break;
                default :
                    $this->get();
                    break;
            }
        } else {
            //make sure the model method is valid
            if(!in_array($this->action,$this->modelMethods)) {
                $this->throwError($this->action." is not a valid model method.",0,400);
            }

            //run the model method
            $action = $this->action;
            $this->$action();
        }
    }

    private function get() {
        /** @var Core_ORM_BasicModel $type */
        $type = $this->model;

        //if there is an id, we are finding one by id, otherwise we are finding all
        if($this->id) {
            $this->output->data = $type::findById($this->id);
        } else {
            $this->output->data = $type::findAll(Core_ORM_Finder::newInstance()
                ->where($this->query)
                ->limitResults($this->limit_results)
                ->limitStart($this->limit_start)
                ->orderBy($this->order_by)
                ->orderDir($this->order_dir)
            );
        }
    }

    private function put() {
        /** @var Core_ORM_BasicModel $type */
        $type = $this->model;

        //the id might be in the object data
        if(!$this->id && isset($this->object_data["id"])) {
            $this->id = $this->object_data["id"];
        }

        //make sure there is an id, and an object exists with this id
        if($this->id) {
            $existing = $type::findById($this->id);
            if(!$existing) {
                $this->throwError("You are trying to update a ".$this->model." with the id of ".$this->id.", however an object does not exist with this id. If you want to create this object, use a POST request isntead.");
            }
        } else {
            $this->throwError("You are trying to update a ".$this->model." but you have not sent the id of the object that you want to update. If you are trying to create, use a POST request");
        }

        //check that the object data is not empty
        if(Core_Utils_Array::valuesEmpty($this->object_data)) {
            $this->throwError("object_data was empty. You must update atleast one property!",0,400);
        }

        //make sure the id is part of the object data
        $this->object_data["id"] = $this->id;
        $object = $type::constructFromArray($this->object_data);
        if($object) {
            $object->pushThis();
            //re-pull to get changes
            $object->pullThis();

            $this->output->data = $object;
        } else {
            $this->throwError("There was a problem updating your object");
        }
    }

    private function post() {
        /** @var Core_ORM_BasicModel $type */
        $type = $this->model;

        //the id might be in the object data
        if(!$this->id && isset($this->object_data["id"])) {
            $this->id = $this->object_data["id"];
        }

        //if there is an id, make sure the object doesn't already exist
        if($this->id) {
            $existing = $type::findById($this->id,true);
            if($existing) {
                $this->throwError("A ".$this->model." already exists with the id ".$this->id.". If you want to update this object, use a PUT request instead.");
            }
        }

        //check that the object data is not empty
        if(Core_Utils_Array::valuesEmpty($this->object_data)) {
            $this->throwError("object_data was empty. You cannot create an empty object!",0,400);
        }

        if($this->id) {
            //make sure the id is part of the object data
            $this->object_data["id"] = $this->id;
        }
        $object = $type::constructFromArray($this->object_data);
        if($object) {
            $object->pushThis();
            //re-pull. Access checks may stop certain modifications being made, so re-pulling will show that
            $object->pullThis();

            $this->output->data = $object;
        } else {
            $this->throwError("There was a problem creating your object");
        }
    }

    private function delete() {
        /** @var Core_ORM_BasicModel $type */
        $type = $this->model;

        //the id might be in the object data
        if(!$this->id && isset($this->object_data["id"])) {
            $this->id = $this->object_data["id"];
        }

        //make sure there is an id. At the moment, you can only delete one object at a time, later we may change this.
        if(!$this->id) {
            $this->throwError("You must specify the id of the object that you want to delete.");
        }

        $object = $type::findById($this->id,true);
        if($object) {
            $object->removeThis();
        }
    }

    private function count() {
        //counts must be a get request
        $this->validateHttpMethod("GET",true);

        /** @var Core_ORM_BasicModel $type */
        $type = $this->model;

        $this->output->data = $type::findCount(Core_ORM_Finder::newInstance()
            ->where($this->query)
            ->limitResults($this->limit_results)
            ->limitStart($this->limit_start)
            ->orderBy($this->order_by)
            ->orderDir($this->order_dir)
        );
    }

    public function onComplete() {

    }
}