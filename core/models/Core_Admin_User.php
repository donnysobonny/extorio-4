<?php
/**
 * This class extends the internal base class and should be used to add functionality to your model.
 *
 * Class Core_Admin_User
 */
class Core_Admin_User extends Core_BaseModel_Core_Admin_User {
    /**
     * Override method to define the conditions for when this object can/cannot be created.
     *
     * Return true if you want to allow access, return false if you do not want to allow access.
     *
     * @return bool
     */
    public function canCreate() {
        //only super admins can create
        if(Core_Admin_User::loggedInUserCanAccessLevel(Core_Admin_User_AccessLevel::Super_Administrator)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Override method to define the conditions for when this object can/cannot be updated.
     *
     * Return true if you want to allow access, return false if you do not want to allow access.
     *
     * @return bool
     */
    public function canUpdate() {
        $loggedInAdmin = Core_Admin_User::getLoggedInUser();
        if($loggedInAdmin) {
            //you can update your own account
            if($loggedInAdmin->id == $this->id) {
                return true;
            }

            //super admins can update anyone's account
            if($loggedInAdmin->accessLevel == Core_Admin_User_AccessLevel::Super_Administrator) {
                return true;
            }
        }

        return false;
    }

    /**
     * Override method to define the conditions for when this object can/cannot be deleted.
     *
     * Return true if you want to allow access, return false if you do not want to allow access.
     *
     * @return bool
     */
    public function canDelete() {
        //only super admins can delete
        if(Core_Admin_User::loggedInUserCanAccessLevel(Core_Admin_User_AccessLevel::Super_Administrator)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Override method to define the conditions for when this object can/cannot be retrieved.
     *
     * Return true if you want to allow access, return false if you do not want to allow access.
     *
     * @return bool
     */
    public function canRetrieve() {
        $loggedInAdmin = Core_Admin_User::getLoggedInUser();
        if($loggedInAdmin) {
            //you can retrieve your own account
            if($loggedInAdmin->id == $this->id) {
                return true;
            }

            //super admins can update anyone's account
            if($loggedInAdmin->accessLevel == Core_Admin_User_AccessLevel::Super_Administrator) {
                return true;
            }
        }

        return false;
    }

    public function canView_Edit() {
        //super admin only
        if(Core_Admin_User::authenticateAsAccessLevel(Core_Admin_User_AccessLevel::Super_Administrator)) {
            return true;
        }

        return false;
    }

    public function canView_List() {
        //super admin only
        if(Core_Admin_User::authenticateAsAccessLevel(Core_Admin_User_AccessLevel::Super_Administrator)) {
            return true;
        }

        return false;
    }

    public function canView_Detail() {
        //super admin or your own account
        if(Core_Admin_User::authenticateAsAccessLevel(Core_Admin_User_AccessLevel::Super_Administrator)) {
            return true;
        }

        $loggedInUser = Core_Admin_User::getLoggedInUser();
        if($loggedInUser->id == $this->id) {
            return true;
        }

        return false;
    }

    public function canView_Delete() {
        //super admins only
        return Core_Admin_User::authenticateAsAccessLevel(Core_Admin_User_AccessLevel::Super_Administrator);
    }

    /**
     * This is called before an object is created.
     */
    protected function beforeCreate() {
        $admin = Core_Admin_User::getLoggedInUser();
        if($admin) {
            $this->createdByAdmin = $admin->username;
        }

        //make sure email/username unique
        $this->makeSureUsernameIsUnique();
        $this->makeSureEmailIsUnique();
    }

    /**
     * This is called after an object is created.
     */
    protected function afterCreate() {

    }

    /**
     * This is called before an object is updated.
     */
    protected function beforeUpdate() {
        $admin = Core_Admin_User::getLoggedInUser();
        if($admin) {
            $this->updatedByAdmin = $admin->username;
        }

        if($this->internal_old->username != $this->username) {
            //make sure username unique
            $this->makeSureUsernameIsUnique();
        }

        if($this->internal_old->email != $this->email) {
            //make sure email is unique
            $this->makeSureEmailIsUnique();
        }

        if($this->internal_old->accessLevel != $this->accessLevel) {
            //make sure can update access level
            $this->makeSureCanUpdateAccessLevel();
        }
    }

    /**
     * This is called after an object is updated.
     */
    protected function afterUpdate() {

    }

    /**
     * This is called before an object is deleted.
     */
    protected function beforeDelete() {

    }

    /**
     * This is called after an object is deleted.
     */
    protected function afterDelete() {

    }

    /**
     * This is called before an object is retrieved.
     */
    protected function beforeRetrieve() {

    }

    /**
     * This is called after an object is retrieved.
     */
    protected function afterRetrieve() {

    }

    /**
     * This is called when a property is being displayed. This is useful for changing how a property is displayed in
     * a list.
     *
     * @param string $propertyName
     */
    public function getPropertyValueForDisplay($propertyName) {

    }

    final private function makeSureUsernameIsUnique() {
        $check = self::findByUsername($this->username,true);
        if($check) {
            throw new Core_User_Exception("This username is already taken");
        }
    }

    final private function makeSureEmailIsUnique() {
        $check = self::findByEmail($this->email,true);
        if($check) {
            throw new Core_User_Exception("This email is already taken");
        }
    }

    final private function makeSureCanUpdateAccessLevel() {
        //only super admins can update access levels
        if(!Core_Admin_User::authenticateAsAccessLevel(Core_Admin_User_AccessLevel::Super_Administrator)) {
            throw new Core_User_Exception("You are unauthorized to update the access level of this user");
        }
    }
}