<?php
class Core_BaseModel_Type_Address extends Core_BaseModel_Type_Basic {

    public $line1;
    public $line2;
    public $city;
    public $town;
    public $region;
    public $country;
    public $postcode;
    public $phone;
    public $fax;

}