<?php
/*
 * !!IMPORTANT!!
 * THIS FILE SHOULD NOT BE MODIFIED! This file is used internally by EORM3. Modifying this file could break the system
 * entirely.
 */
class Core_BaseModel_Core_Admin_User extends Core_BaseModel_Type_User {
    
    /**
     * @var integer
     */
    public $id;
                
    /**
     * @var string
     */
    public $dateCreated;
                
    /**
     * @var string
     */
    public $dateUpdated;
                
    /**
     * @var string
     */
    public $username;
                
    /**
     * @var string
     */
    public $password;
                
    /**
     * @var string
     */
    public $email;
                
    /**
     * @var boolean
     */
    public $emailVerified;
                
    /**
     * @var boolean
     */
    public $canLogin;
                
    /**
     * @var integer
     */
    public $numLogin;
                
    /**
     * @var string
     */
    public $dateLogin;
                
    /**
     * @var string
     */
    public $accessLevel;
                
    /**
     * @var string
     */
    public $createdByAdmin;
                
    /**
     * @var string
     */
    public $updatedByAdmin;
                
    protected $internal_isPersistent = true;
    protected $internal_propertyTypes = array(
		'id' => Core_PHP_Types::_INTEGER,
		'dateCreated' => Core_PHP_Types::_STRING,
		'dateUpdated' => Core_PHP_Types::_STRING,
		'username' => Core_PHP_Types::_STRING,
		'password' => Core_PHP_Types::_STRING,
		'email' => Core_PHP_Types::_STRING,
		'emailVerified' => Core_PHP_Types::_BOOLEAN,
		'canLogin' => Core_PHP_Types::_BOOLEAN,
		'numLogin' => Core_PHP_Types::_INTEGER,
		'dateLogin' => Core_PHP_Types::_STRING,
		'accessLevel' => Core_PHP_Types::_STRING,
		'createdByAdmin' => Core_PHP_Types::_STRING,
		'updatedByAdmin' => Core_PHP_Types::_STRING
    );
    protected $internal_objectTypes = array(

    );
}