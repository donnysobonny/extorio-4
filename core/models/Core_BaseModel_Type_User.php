<?php

/**
 * The User Type model template. This provides a set of useful methods that a user model might use.
 *
 * Note that inputs are not validated. You will need to add validation to your modification event methods.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Models
 *
 * Class Core_BaseModel_Type_User
 */
class Core_BaseModel_Type_User extends Core_BaseModel_Type_Basic {
    /**
     * The username
     *
     * @var string
     */
    public $username;
    /**
     * The password
     *
     * @var string
     */
    public $password;
    /**
     * The email address
     *
     * @var string
     */
    public $email;
    /**
     * Whether the email has been verified
     *
     * @var bool
     */
    public $emailVerified;
    /**
     * Whether the user can log in
     *
     * @var bool
     */
    public $canLogin;
    /**
     * The number of times the user has logged in
     *
     * @var int
     */
    public $numLogin;
    /**
     * The date the user last logged in
     *
     * @var string
     */
    public $dateLogin;
    /**
     * The access level of this user
     *
     * @var string
     */
    public $accessLevel;

    /**
     * Authenticate whether a user is logged in, and optionally redirect if the user is not logged in.
     *
     * @param string $redirectOnFail The url to redirect to if authentication fails.
     *
     * @return bool
     */
    final public static function authenticate($redirectOnFail=false) {
        if(!self::getLoggedInUser()) {
            //if redirect
            if(strlen($redirectOnFail)) {
                header("Location: ".$redirectOnFail);
                exit;
            }

            return false;
        } else {
            return true;
        }
    }

    /**
     * Authenticate whether a user is logged in and can access a certain level. Optionally redirect if the user is
     * not logged/cannot access level.
     *
     * @param int $accessLevel
     * @param string $redirectOnFail
     *
     * @return bool
     */
    final public static function authenticateAsAccessLevel($accessLevel,$redirectOnFail=false) {
        $authenticated = false;
        $user = self::getLoggedInUser();
        if($user) {
            if($user->canAccessLevel($accessLevel)) {
                $authenticated = true;
            }
        }

        if(!$authenticated) {
            if(strlen($redirectOnFail)) {
                header("Location: ".$redirectOnFail);
                exit;
            }
        }

        return $authenticated;
    }

    /**
     * Try to log a user in by username/email and password. Throws an exception upon failure,
     * or sets the user as the logged in user.
     *
     * @param $username
     * @param $password
     *
     * @throws Core_MVC_Exception
     * @throws Core_User_Exception
     */
    final public static function tryLogin($username,$password) {

        //find the user without access checks
        $user = self::findOne(Core_ORM_Finder::newInstance()->where("
        (base.username = ('".$username."') OR base.email = ('".$username."')) AND
        base.password = ('".$password."')
        ")->ignoreAccessChecks(true));

        if($user) {
            //make sure the email is validated
            if(!$user->emailVerified) {
                throw new Core_User_Exception("Email address is not validated. Please locate the validation email and
                 follow the instructions within the email.");
            }

            //make sure the user can log in
            if(!$user->canLogin) {
                throw new Core_User_Exception("There was a problem logging in to your account. Please try again later
                 or contact support.");
            }

            //set the user to logged in
            self::setLoggedInUserId($user->id);

            //update the user without access checks and webhooks
            $user->dateLogin = date("Y-m-d H:i:s",time());
            $user->numLogin++;
            $user->pushThis(true,true);
        } else {
            throw new Core_User_Exception("Username and/or password incorrect");
        }
    }

    /**
     * Log the user out. This simply unsets the logged in user.
     */
    final public static function logout() {
        self::setLoggedInUserId(false);
    }

    /**
     * Check whether the logged in user can access a certain level
     *
     * @param int $level
     *
     * @return bool
     */
    final public static function loggedInUserCanAccessLevel($level) {
        $user = self::getLoggedInUser();
        if($user) {
            return $user->canAccessLevel($level);
        }
        return false;
    }

    /**
     * Find a user by username
     *
     * @param $username
     *
     * @return $this
     * @throws Core_MVC_Exception
     */
    final public static function findByUsername($username,$ignoreAccessChecks=false) {
        return self::findOne(Core_ORM_Finder::newInstance()->where("base.username = ('".$username."')")->ignoreAccessChecks($ignoreAccessChecks));
    }

    /**
     * Find a user by email
     *
     * @param $username
     *
     * @return $this
     * @throws Core_MVC_Exception
     */
    final public static function findByEmail($email,$ignoreAccessChecks=false) {
        return self::findOne(Core_ORM_Finder::newInstance()->where("base.email = ('".$email."')")->ignoreAccessChecks($ignoreAccessChecks));
    }

    /**
     * Get the logged in user or FALSE if no user is logged in
     *
     * @return $this
     * @throws Core_MVC_Exception
     */
    final public static function getLoggedInUser() {
        $id = self::getLoggedInUserId();
        if($id) {
            $class = get_called_class();
            /** @var Core_BaseModel_Type_User $user */
            $user = new $class();
            $user->id = $id;
            $user->pullThis(true);
            return $user;
        } else {
            return false;
        }
    }

    /**
     * Get the logged in user id
     *
     * @return int
     */
    final private static function getLoggedInUserId() {
        $key = get_called_class()."_logged_in_user_id";
        if(isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }

        if(isset($_ENV[$key])) {
            return $_ENV[$key];
        }

        return false;
    }

    /**
     * set the logged in user id
     *
     * @param $id
     */
    final public static function setLoggedInUserId($id) {
        $key = get_called_class()."_logged_in_user_id";
        $_SESSION[$key] = $id;
        $_ENV[$key] = $id;
    }

    /**
     * Check whether this user can access a certain level
     *
     * @param int $level
     *
     * @return bool
     */
    final public function canAccessLevel($level) {
        if(intval($this->accessLevel) <= $level) {
            return true;
        } else {
            return false;
        }
    }
}