<?php
/*
 * !!IMPORTANT!!
 * THIS FILE SHOULD NOT BE MODIFIED! This file is used internally by EORM3. Modifying this file could break the system
 * entirely.
 */
class Core_BaseModel_Core_Address extends Core_BaseModel_Type_Address {
    
    /**
     * @var integer
     */
    public $id;
                
    /**
     * @var string
     */
    public $line1;
                
    /**
     * @var string
     */
    public $line2;
                
    /**
     * @var string
     */
    public $city;
                
    /**
     * @var string
     */
    public $town;
                
    /**
     * @var string
     */
    public $region;
                
    /**
     * @var string
     */
    public $country;
                
    /**
     * @var string
     */
    public $postcode;
                
    /**
     * @var string
     */
    public $phone;
                
    /**
     * @var string
     */
    public $fax;
                
    /**
     * @var string
     */
    public $dateUpdated;
                
    /**
     * @var string
     */
    public $dateCreated;
                
    protected $internal_isPersistent = false;
    protected $internal_propertyTypes = array(
		'id' => Core_PHP_Types::_INTEGER,
		'line1' => Core_PHP_Types::_STRING,
		'line2' => Core_PHP_Types::_STRING,
		'city' => Core_PHP_Types::_STRING,
		'town' => Core_PHP_Types::_STRING,
		'region' => Core_PHP_Types::_STRING,
		'country' => Core_PHP_Types::_STRING,
		'postcode' => Core_PHP_Types::_STRING,
		'phone' => Core_PHP_Types::_STRING,
		'fax' => Core_PHP_Types::_STRING,
		'dateUpdated' => Core_PHP_Types::_STRING,
		'dateCreated' => Core_PHP_Types::_STRING
    );
    protected $internal_objectTypes = array(

    );
}