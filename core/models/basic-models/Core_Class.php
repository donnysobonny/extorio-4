<?php
final class Core_Class extends Core_ORM_BasicModel {
    public $id;
    public $name;
    public $isPersistent = true;

    public $type;

    public $modelDir = "";

    public $isHidden = false;

    public static function findByName($className) {
        return Core_Class::findOne(Core_ORM_Finder::newInstance()
            ->where("base.name = ('".$className."')")
        );
    }

    protected function beforeCreate() {
        $this->name = Core_Utils_String::wordsToCamelCase($this->name);

        $this->nameIsUnique();
    }

    protected function afterCreate() {
        //create the table
        $this->createTable();

        //create the access events
        $this->createAccessEvents();

        //set default properties
        $this->setDefaultProperties();

        //sync with the class file
        $this->syncWithClassFile();
    }

    protected function afterUpdate() {
        $this->syncWithClassFile();
    }

    protected function beforeUpdate() {
        //check if the name has changed
        $old = Core_Class::findById($this->id);
        if($old->name != $this->name) {
            //name cannot be changed!
            $this->name = $old->name;
        }

        if($old->type != $this->type) {
            //type cannot be changed!
            $this->type = $old->type;
        }
    }

    protected function afterDelete() {
        //delete the properties
        $props = Core_Property::findAllByOwnerClassId($this->id);
        foreach($props as $prop) {
            $prop->removeThis();
        }

        //delete any instances of this object
        $className = $this->name;
        $objects = $className::findAll();
        foreach($objects as $object) {
            $object->removeThis();
        }

        //delete the table
        $this->deleteTable();

        //delete all access events for this model
        $events = Core_AccessEvent::findAll(Core_ORM_Finder::newInstance()
            ->where("base.type = ('model') AND target = ('".$this->name."')")
        );
        foreach($events as $event) {
            $event->removeThis();
        }

        //delete any properties that use this class as a child object
        $props = Core_Property::findAll(Core_ORM_Finder::newInstance()
            ->where("
            base.childClassId = ".$this->id."
            ")
        );
        foreach($props as $prop) {
            $prop->removeThis();
        }
    }

    public function syncWithClassFile() {
        $extend = "";
        switch($this->type) {
            case Core_Model_Types::_BASIC :
                $extend = "Core_BaseModel_Type_Basic";
                break;
            case Core_Model_Types::_USER :
                $extend = "Core_BaseModel_Type_User";
                break;
            case Core_Model_Types::_ADDRESS :
                $extend = "Core_BaseModel_Type_Address";
                break;
            default :
                throw new Core_MVC_Exception("The type ".$this->type." is not currently supported");
                break;
        }

        //if the extended file doesn't already exist, create it
        if(!file_exists($this->modelDir."/".$this->name.".php")) {
            $file = Core_Utils_File::createFile($this->modelDir."/".$this->name.".php");
            $file->open("w+");
            $file->write('<?php
/**
 * This class extends the internal base class and should be used to add functionality to your model.
 *
 * Class '.$this->name.'
 */
class '.$this->name.' extends Core_BaseModel_'.$this->name.' {
    /**
     * Modify this object before it is created.
     */
    protected function beforeCreate() {

    }

    /**
     * Modify this object after it is created.
     */
    protected function afterCreate() {

    }

    /**
     * Modify this object before it is updated.
     */
    protected function beforeUpdate() {

    }

    /**
     * Modify this object after it is updated.
     */
    protected function afterUpdate() {

    }

    /**
     * Modify this object before it is deleted.
     */
    protected function beforeDelete() {

    }

    /**
     * Modify this object after it is deleted.
     */
    protected function afterDelete() {

    }

    /**
     * Modify this object before it is retrieved.
     */
    protected function beforeRetrieve() {

    }

    /**
     * Modify this object after it is retrieved.
     */
    protected function afterRetrieve() {

    }

    /**
     * Override the access event/actions on this object.
     *
     * By returning true, creation is granted. By returning false, the creation will fail.
     *
     * By returning null/void, the access event/action system will take over.
     */
    public function canCreate() {

    }

    /**
     * Override the access event/actions on this object.
     *
     * By returning true, updating is granted. By returning false, the updating will fail.
     *
     * By returning null/void, the access event/action system will take over.
     */
    public function canUpdate() {

    }

    /**
     * Override the access event/actions on this object.
     *
     * By returning true, deletion is granted. By returning false, the deletion will fail.
     *
     * By returning null/void, the access event/action system will take over.
     */
    public function canDelete() {

    }

    /**
     * Override the access event/actions on this object.
     *
     * By returning true, retrieval is granted. By returning false, the retrieval will fail.
     *
     * By returning null/void, the access event/action system will take over.
     */
    public function canRetrieve() {

    }

    /**
     * Override the access event/actions on this object.
     *
     * By returning true, this object can be viewed in the orm edit view, by returning false, it cannot.
     *
     * By returning null/void, the access event/action system will take over.
     */
    public function canView_Edit() {

    }

    /**
     * Override the access event/actions on this object.
     *
     * By returning true, this object can be viewed in the orm list view, by returning false, it cannot.
     *
     * By returning null/void, the access event/action system will take over.
     */
    public function canView_List() {

    }

    /**
     * Override the access event/actions on this object.
     *
     * By returning true, this object can be viewed in the orm detail view, by returning false, it cannot.
     *
     * By returning null/void, the access event/action system will take over.
     */
    public function canView_Detail() {

    }

    /**
     * Override the access event/actions on this object.
     *
     * By returning true, this object can be viewed in the orm delete view, by returning false, it cannot.
     *
     * By returning null/void, the access event/action system will take over.
     */
    public function canView_Delete() {

    }
}');
            $file->close();
        }

        //create the basemodel class file
        $file = Core_Utils_File::createFile("core/models/Core_BaseModel_".$this->name.".php");
        $file->open("w+");

        $content = Core_Utils_File::readFromFile("core/assets/file-templates/Core_BaseModel_Template.txt");
        $content = str_replace("*|CLASSNAME|*",$this->name,$content);
        $content = str_replace("*|EXTENDS|*",$extend,$content);
        $content = str_replace("*|IS_PERSISTENT|*",$this->isPersistent?"true":"false",$content);

        //create the properties, types and object types
        $props = Core_Property::findAllByOwnerClassId($this->id);
        $propertyString = "";
        $propertyTypeString = "";
        $objectTypeString = "";
        foreach($props as $prop) {
            $type = "";
            if($prop->phpType != Core_PHP_Types::_OBJECT && $prop->phpType != Core_PHP_Types::_ARRAY) {
                switch($prop->phpType) {
                    case Core_PHP_Types::_STRING :
                        $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_STRING,\n";
                        break;
                    case Core_PHP_Types::_INTEGER :
                        $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_INTEGER,\n";
                        break;
                    case Core_PHP_Types::_BOOLEAN :
                        $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_BOOLEAN,\n";
                        break;
                    case Core_PHP_Types::_FLOAT :
                        $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_FLOAT,\n";
                        break;
                    case Core_PHP_Types::_DOUBLE :
                        $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_DOUBLE,\n";
                        break;
                }

                $type = $prop->phpType;
            } else {
                $childClass = Core_Class::findById($prop->childClassId);
                $type = $childClass->name;

                switch($prop->phpType) {
                    case Core_PHP_Types::_OBJECT :
                        $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_OBJECT,\n";
                        break;
                    case Core_PHP_Types::_ARRAY :
                        $propertyTypeString .= "\t\t'".$prop->name."' => Core_PHP_Types::_ARRAY,\n";
                        break;
                }

                $objectTypeString .= "\t\t'".$prop->name."' => '".$type."',\n";

                if($prop->phpType == Core_PHP_Types::_ARRAY) {
                    $type .= "[]";
                }
            }

            $propertyString .= "
    /**
     * @var ".$type."
     */
    public $".$prop->name.";
                ";
        }

        $propertyTypeString = substr($propertyTypeString,0,strlen($propertyTypeString)-2);
        $objectTypeString = substr($objectTypeString,0,strlen($objectTypeString)-2);

        $content = str_replace("*|PROPERTIES|*",$propertyString,$content);
        $content = str_replace("*|PROPERTY_TYPES|*",$propertyTypeString,$content);
        $content = str_replace("*|OBJECT_TYPES|*",$objectTypeString,$content);

        $file->write($content);
        $file->close();
    }

    private function createTable() {
        //create the table
        $config = Core_Extorio::get()->getStoredConfig();
        $ssql = "CREATE TABLE IF NOT EXISTS `".$this->name."` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        PRIMARY KEY (`id`)
        )ENGINE=".$config["db_defaults"]["engine"]." DEFAULT CHARSET=".$config["db_defaults"]["table_charset"]."
        COLLATE=".$config["db_defaults"]["table_collate"]." AUTO_INCREMENT=1
        ";
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $db->execute($ssql);
    }

    private function createAccessEvents() {
        $events = array(
            "create",
            "retrieve",
            "update",
            "delete",
            "view_edit",
            "view_detail",
            "view_list",
            "view_delete"
        );

        foreach($events as $event) {
            $accessEvent = new Core_AccessEvent();
            $accessEvent->type = "model";
            $accessEvent->action = $event;
            $accessEvent->target = $this->name;
            $accessEvent->pushThis();
        }
    }

    private function setDefaultProperties() {
        switch($this->type) {
            case Core_Model_Types::_BASIC :
                $this->setDefaultProperties_Basic();
                break;
            case Core_Model_Types::_USER :
                $this->setDefaultProperties_User();
                break;
            case Core_Model_Types::_ADDRESS :
                $this->setDefaultProperties_Address();
                break;
        }
    }

    private function setDefaultProperties_Basic() {
        //create the id property
        $prop = new Core_Property();
        $prop->name = "id";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_numberfield;
        $prop->maxLength = 11;
        $prop->isPrimary = true;
        $prop->isIndex = false;
        $prop->isUnique = false;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = false;
        $prop->visibleInList = false;
        $prop->visibleInDetail = false;
        $prop->pushThis();

        //create the dateCreated property
        $prop = new Core_Property();
        $prop->name = "dateCreated";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_datetime;
        $prop->maxLength = false;
        $prop->isCustom = false;
        $prop->visibleInCreate = false;
        $prop->visibleInEdit = false;
        $prop->visibleInList = false;
        $prop->visibleInDetail = true;
        $prop->pushThis();

        //create the dateUpdated property
        $prop = new Core_Property();
        $prop->name = "dateUpdated";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_datetime;
        $prop->maxLength = false;
        $prop->isCustom = false;
        $prop->visibleInCreate = false;
        $prop->visibleInEdit = false;
        $prop->visibleInList = false;
        $prop->visibleInDetail = true;
        $prop->pushThis();
    }

    private function setDefaultProperties_User() {
        //set the basic properties first
        $this->setDefaultProperties_Basic();

        //create the username
        $prop = new Core_Property();
        $prop->name = "username";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_textfield;
        $prop->maxLength = 60;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();
        $prop->isIndex = true;
        $prop->pushThis();

        //create the password
        $prop = new Core_Property();
        $prop->name = "password";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_textfield;
        $prop->maxLength = 60;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();

        //create the email property
        $prop = new Core_Property();
        $prop->name = "email";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_textfield;
        $prop->maxLength = 160;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();
        $prop->isIndex = true;
        $prop->pushThis();

        //create the emailVerified property
        $prop = new Core_Property();
        $prop->name = "emailVerified";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_checkbox;
        $prop->checkBoxDefault = false;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = false;
        $prop->visibleInDetail = true;
        $prop->pushThis();
        $prop->isIndex = true;
        $prop->pushThis();

        //create the canLogin property
        $prop = new Core_Property();
        $prop->name = "canLogin";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_checkbox;
        $prop->checkBoxDefault = false;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = false;
        $prop->visibleInDetail = true;
        $prop->pushThis();
        $prop->isIndex = true;
        $prop->pushThis();

        //create the numLogin property
        $prop = new Core_Property();
        $prop->name = "numLogin";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_numberfield;
        $prop->maxLength = 8;
        $prop->isCustom = false;
        $prop->visibleInCreate = false;
        $prop->visibleInEdit = false;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();

        //create the dateLogin property
        $prop = new Core_Property();
        $prop->name = "dateLogin";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_datetime;
        $prop->maxLength = false;
        $prop->isCustom = false;
        $prop->visibleInCreate = false;
        $prop->visibleInEdit = false;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();

        //create the access level property
        $dropdown = Core_Dropdown::findByName("Core_User_AccessLevel");
        if($dropdown) {
            $prop = new Core_Property();
            $prop->name = "accessLevel";
            $prop->ownerClassId = $this->id;
            $prop->inputType = Core_Input_Types::_dropdown;
            $prop->dropDownId = $dropdown->id;
            $prop->isCustom = false;
            $prop->visibleInList = true;
            $prop->visibleInDetail = true;
            $prop->pushThis();
        }
    }

    private function setDefaultProperties_Address() {

        $this->isPersistent = false;
        $this->pushThis();

        //set the basic properties first
        $this->setDefaultProperties_Basic();

        //create the line1 property
        $prop = new Core_Property();
        $prop->name = "line1";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_textfield;
        $prop->maxLength = 60;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();

        //create the line2 property
        $prop = new Core_Property();
        $prop->name = "line2";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_textfield;
        $prop->maxLength = 60;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();

        //create the city property
        $prop = new Core_Property();
        $prop->name = "city";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_textfield;
        $prop->maxLength = 40;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();

        //create the town property
        $prop = new Core_Property();
        $prop->name = "town";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_textfield;
        $prop->maxLength = 40;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();

        //create the region property
        $prop = new Core_Property();
        $prop->name = "region";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_textfield;
        $prop->maxLength = 40;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();

        //create the country property
        $dropdown = Core_Dropdown::findByName("Core_Address_Country");
        if($dropdown) {
            $prop = new Core_Property();
            $prop->name = "country";
            $prop->ownerClassId = $this->id;
            $prop->inputType = Core_Input_Types::_dropdown;
            $prop->dropDownId = $dropdown->id;
            $prop->isCustom = false;
            $prop->visibleInCreate = true;
            $prop->visibleInEdit = true;
            $prop->visibleInList = true;
            $prop->visibleInDetail = true;
            $prop->pushThis();
        }

        //create the postcode property
        $prop = new Core_Property();
        $prop->name = "postcode";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_textfield;
        $prop->maxLength = 10;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();

        //create the phone property
        $prop = new Core_Property();
        $prop->name = "phone";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_textfield;
        $prop->maxLength = 16;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();

        //create the fax property
        $prop = new Core_Property();
        $prop->name = "fax";
        $prop->ownerClassId = $this->id;
        $prop->inputType = Core_Input_Types::_textfield;
        $prop->maxLength = 16;
        $prop->isCustom = false;
        $prop->visibleInCreate = true;
        $prop->visibleInEdit = true;
        $prop->visibleInList = true;
        $prop->visibleInDetail = true;
        $prop->pushThis();

    }

    private function deleteTable() {
        $ssql = "DROP TABLE IF EXISTS `".$this->name."`";
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $db->execute($ssql);
    }

    private function nameIsUnique() {
        $check = Core_Class::findByName($this->name);
        if($check) {
            throw new Core_ORM_Exception("Trying to give a class the name ".$this->name." but this class already
            exists as a model");
        }
    }
}