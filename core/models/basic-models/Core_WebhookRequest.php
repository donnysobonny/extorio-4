<?php
final class Core_WebhookRequest extends Core_ORM_BasicModel {
    public $id;
    public $intalledWebhookId;
    public $generalError;
    public $outDataJson;
    public $curlError;
    public $responseStatus;
    public $dateCreated;
    public $dateUpdated;

    protected function beforeCreate() {
        $this->dateCreated = date("Y-m-d H:i:s",time());
    }

    protected function beforeUpdate() {
        $this->dateUpdated = date("Y-m-d H:i:s",time());
    }
}