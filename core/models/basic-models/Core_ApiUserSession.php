<?php
final class Core_ApiUserSession extends Core_ORM_BasicModel {
    public $id;
    public $userType;
    public $userId;
    public $session;
    public $remoteIp;
    public $dateCreated;

    public static function findBySession($session,$remoteIp) {
        return self::findOne(Core_ORM_Finder::newInstance()
            ->where("base.session = ('".$session."') AND base.remoteIp = ('".$remoteIp."')")
        );
    }

    protected function beforeCreate() {
        $this->dateCreated = date("Y-m-d H:i:s",time());
    }

    protected function beforeUpdate() {
        $this->dateCreated = date("Y-m-d H:i:s",time());
    }
}