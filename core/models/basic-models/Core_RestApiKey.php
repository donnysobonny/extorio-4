<?php
final class Core_RestApiKey extends Core_ORM_BasicModel {
    public $id;
    public $name;
    public $numOfCalls;
    public $key;

    public static function findByName($name) {
        return self::findOne(Core_ORM_Finder::newInstance()
            ->where("base.name = ('".$name."')")
        );
    }

    public static function findByKey($key) {
        return self::findOne(Core_ORM_Finder::newInstance()
            ->where("base.key = ('".$key."')")
        );
    }
}