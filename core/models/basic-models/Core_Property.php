<?php

/**
 * A property that belongs to a model class.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Models
 *
 * Class Core_Property
 */
final class Core_Property extends Core_ORM_BasicModel {
    /**
     * id
     *
     * @var int
     */
    public $id;
    /**
     * the name of the property
     *
     * @var string
     */
    public $name;
    /**
     * the id of the owner class
     *
     * @var int
     */
    public $ownerClassId;
    /**
     * the input type
     *
     * @var string
     */
    public $inputType = "textfield";
    /**
     * if the input type is a checkbox, the default state of the checkbox
     *
     * @var bool
     */
    public $checkBoxDefault = false;
    /**
     * if the input type is html, whether script tags are allowed
     *
     * @var bool
     */
    public $htmlScriptsAllowed = false;
    /**
     * if the input type is a dropdown, the id of the dropdown list
     *
     * @var
     */
    public $dropDownId;
    /**
     * the maximum length (if applicable) of the property
     *
     * @var int
     */
    public $maxLength = false;

    /**
     * the php type
     *
     * @var string
     */
    public $phpType = "string";
    /**
     * the mysql type
     *
     * @var string
     */
    public $mysqlType = "varchar";

    /**
     * if the property is an object, this is the class id of the class type
     *
     * @var int
     */
    public $childClassId;

    /**
     * whether the property is the primary key
     *
     * @var bool
     */
    public $isPrimary = false;
    /**
     * whether the property is an index
     *
     * @var bool
     */
    public $isIndex = false;
    /**
     * whether the property is unique
     *
     * @var bool
     */
    public $isUnique = false;

    /**
     * Whether this property should display in the list view
     *
     * @var bool
     */
    public $visibleInList = true;

    /**
     * Whether this property should display in the detail view
     *
     * @var bool
     */
    public $visibleInDetail = true;

    /**
     * Whether this property can be modified when the object is being created
     *
     * @var bool
     */
    public $visibleInCreate = true;

    /**
     * Whether this property can be modified when the object is being updated
     *
     * @var bool
     */
    public $visibleInEdit = true;

    /**
     * Whether this property has been added as a custom property or not. non-custom properties cannot be edited in any
     * way.
     *
     * @var bool
     */
    public $isCustom = true;

    /**
     * The ordered position of this property
     *
     * @var int
     */
    public $position = 0;

    /**
     * Find all properties that belong to a class
     *
     * @param int $ownerClassId
     *
     * @return Core_Property[]
     */
    public static function findAllByOwnerClassId($ownerClassId) {
        return Core_Property::findAll(Core_ORM_Finder::newInstance()
            ->where("base.ownerClassId = ".$ownerClassId)
            ->orderBy("base.position")
            ->orderDir("asc")
        );
    }

    /**
     * Find a property by name
     *
     * @param string $propertyName
     *
     * @return Core_Property
     */
    public static function findByName($propertyName) {
        return Core_Property::findOne(Core_ORM_Finder::newInstance()
            ->where("base.name = ('".$propertyName."')")
        );
    }

    protected function beforeCreate() {
        //set the position to the number of properties
        $properties = Core_Property::findAllByOwnerClassId($this->ownerClassId);
        if($this->position == 0) {
            if($this->name == "id") {
                $this->position = 0;
            } else {
                $this->position = count($properties);
            }
        }

        //set the type based on the input type
        $this->setTypeByInputType();

        //sync the property with the table
        $this->syncWithTableField();

        //indexing
        if($this->isIndex) {
            $this->setIndex();
        }
        if($this->isUnique) {
            $this->unsetUnique();
        }
    }

    protected function afterCreate() {
        $this->syncWithModelFiles();
    }

    protected function beforeDelete() {
        //TODO: clean up any non-persisten objects assigned to this property!
    }

    protected function afterDelete() {
        //delete the field
        $this->deleteTableField();
        $this->syncWithModelFiles();

        //update positioning
        $this->fixPositioning();
    }

    protected function beforeUpdate() {
        //if the property was not an index, and now is
        if(!$this->internal_old->isIndex && $this->isIndex) {
            $this->setIndex();
        }
        //if the property was index, and now isn't
        if($this->internal_old->isIndex && !$this->isIndex) {
            $this->unsetIndex();
        }

        //if the property was not unique, and now is
        if(!$this->internal_old->isUnique && $this->isUnique) {
            $this->setUnique();
        }
        //if the property was unique, and now isn't
        if($this->internal_old->isUnique && !$this->isUnique) {
            $this->unsetUnique();
        }

        //if the input type has changed
        if($this->internal_old->inputType != $this->inputType) {
            $this->setTypeByInputType();
        }
    }

    public function fixPositioning() {
        $properties = Core_Property::findAllByOwnerClassId($this->ownerClassId);
        $n=0;
        foreach($properties as $property) {
            $property->position = $n;
            $property->pushThis();
            $n++;
        }
    }

    protected function afterUpdate() {
        //sync the field
        $this->syncWithTableField();
        $this->syncWithModelFiles();
    }

    /**
     * Set this property as an index
     *
     * @throws Core_MVC_Exception
     */
    public function setIndex() {
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $class = Core_Class::findById($this->ownerClassId);
        if($class) {
            $this->updateIndexing();
            if(!$this->isIndex && !$this->isPrimary) {
                $ssql = "ALTER TABLE `".$class->name."` ADD INDEX `".$this->name."_index` (`".$this->name."`)";
                $db->execute($ssql);

                $this->isIndex = true;
            } else {
                throw new Core_MVC_Exception("trying to set ".$this->name." as an index when it is already an index or
                primary");
            }
        }
    }

    /**
     * Unset this property as an index
     *
     * @throws Core_MVC_Exception
     */
    public function unsetIndex() {
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $class = Core_Class::findById($this->ownerClassId);
        if($class) {
            $this->updateIndexing();
            if($this->isIndex) {
                $ssql = "ALTER TABLE `".$class->name."` DROP INDEX `".$this->name."_index`";
                $db->execute($ssql);

                $this->isIndex = false;
            } else {
                throw new Core_MVC_Exception("trying to unset ".$this->name." as an index but it is not currently an
                index.");
            }
        }
    }

    /**
     * Set this property as unique
     *
     * @throws Core_MVC_Exception
     */
    public function setUnique() {
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $class = Core_Class::findById($this->ownerClassId);
        if($class) {
            $this->updateIndexing();
            if(!$this->isUnique && !$this->isPrimary) {
                $ssql = "ALTER TABLE `".$class->name."` ADD UNIQUE `".$this->name."_unique` (`".$this->name."`)";
                $db->execute($ssql);

                $this->isUnique = true;
            } else {
                throw new Core_MVC_Exception("trying to set ".$this->name." as unique but it already is unique or
                primary");
            }
        }
    }

    /**
     * Unset this property as unique
     *
     * @throws Core_MVC_Exception
     */
    public function unsetUnique() {
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $class = Core_Class::findById($this->ownerClassId);
        if($class) {
            $this->updateIndexing();
            if($this->isUnique) {
                $ssql = "ALTER TABLE `".$class->name."` DROP INDEX `".$this->name."_unique`";
                $db->execute($ssql);

                $this->isUnique = false;
            } else {
                throw new Core_MVC_Exception("trying to unset ".$this->name." as unique when it is not unique");
            }
        }
    }

    /**
     * Is the property the primary key
     *
     * @return bool
     */
    public function isPrimary() {
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $class = Core_Class::findById($this->ownerClassId);
        $ssql = "SHOW INDEX FROM `".$class->name."` WHERE Key_name = ('PRIMARY')";
        $data = $db->getRowAssociative($ssql);
        if($data["Column_name"] == $this->name) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Is the property indexed
     *
     * @return bool
     */
    public function isIndex() {
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $class = Core_Class::findById($this->ownerClassId);
        $ssql = "SHOW INDEX FROM `".$class->name."` WHERE Key_name = ('".$this->name."_index')";
        $data = $db->getRowAssociative($ssql);
        if($data["Column_name"] == $this->name) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Is the property unique
     *
     * @return bool
     */
    public function isUnique() {
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $class = Core_Class::findById($this->ownerClassId);
        $ssql = "SHOW INDEX FROM `".$class->name."` WHERE Key_name = ('".$this->name."_unique')";
        $data = $db->getRowAssociative($ssql);
        if($data["Column_name"] == $this->name) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update the properties indexing
     */
    private function updateIndexing() {
        $this->isPrimary = $this->isPrimary();
        $this->isUnique = $this->isUnique();
        $this->isIndex = $this->isIndex();
    }

    /**
     * Sync this property with the table.
     */
    private function syncWithTableField() {
        $db = Core_Extorio::get()->getDbInstanceDefault();
        if($this->phpType != Core_PHP_Types::_OBJECT && $this->phpType != Core_PHP_Types::_ARRAY) {
            $class = Core_Class::findById($this->ownerClassId);
            if($class) {
                $mysqlType = $this->mysqlType;
                if($this->maxLength > 0) {
                    $mysqlType .= "(".$this->maxLength.")";
                }

                if($this->name != "id") {
                    //check if the table has the field or not
                    $ssql = "SHOW COLUMNS FROM `".$class->name."` WHERE `Field` = ('".$this->name."')";
                    if($db->getRowIndices($ssql)) {
                        //updating
                        $ssql = "ALTER TABLE `".$class->name."` CHANGE `".$this->name."` `".$this->name."`
                ".$mysqlType." NOT NULL";
                    } else {
                        //creating
                        $ssql = "ALTER TABLE `".$class->name."` ADD `".$this->name."` ".$mysqlType." NOT NULL";
                    }
                    $db->execute($ssql);
                }
            }
        } else {
            //delete the field
            $this->deleteTableField();
        }
    }

    /**
     * Delete this property from the table.
     */
    private function deleteTableField() {
        $db = Core_Extorio::get()->getDbInstanceDefault();
        $class = Core_Class::findById($this->ownerClassId);
        if($class) {
            $ssql = "ALTER TABLE `".$class->name."` DROP `".$this->name."`";
            try {
                $db->execute($ssql);
            } catch(Core_Db_Exception $ex) {
                //do nothing
            }
        }
    }

    /**
     * Sync the class that this property belongs to with the properties cached in the db
     */
    private function syncWithModelFiles() {
        $class = Core_Class::findById($this->ownerClassId);
        if($class) {
            $class->syncWithClassFile();
        }
    }

    private function setTypeByInputType() {
        switch($this->inputType) {
            case Core_Input_Types::_checkbox :
                $this->phpType = Core_PHP_Types::_BOOLEAN;
                $this->mysqlType = Core_MySQL_Types::_TINYINT;
                $this->maxLength = 1;
                break;
            case Core_Input_Types::_date :
                $this->phpType = Core_PHP_Types::_STRING;
                $this->mysqlType = Core_MySQL_Types::_DATE;
                $this->maxLength = false;
                break;
            case Core_Input_Types::_datetime :
                $this->phpType = Core_PHP_Types::_STRING;
                $this->mysqlType = Core_MySQL_Types::_DATETIME;
                $this->maxLength = false;
                break;
            case Core_Input_Types::_decimalfield :
                $this->phpType = Core_PHP_Types::_FLOAT;
                $this->mysqlType = Core_MySQL_Types::_FLOAT;
                break;
            case Core_Input_Types::_dropdown :
                $this->phpType = Core_PHP_Types::_STRING;
                $this->mysqlType = Core_MySQL_Types::_VARCHAR;
                $this->maxLength = 60;
                break;
            case Core_Input_Types::_html :
                $this->phpType = Core_PHP_Types::_STRING;
                $this->mysqlType = Core_MySQL_Types::_LONGTEXT;
                break;
            case Core_Input_Types::_model :
                $this->phpType = Core_PHP_Types::_OBJECT;
                $this->mysqlType = null;
                break;
            case Core_Input_Types::_model_array :
                $this->phpType = Core_PHP_Types::_ARRAY;
                $this->mysqlType = null;
                break;
            case Core_Input_Types::_numberfield :
                $this->phpType = Core_PHP_Types::_INTEGER;
                $this->mysqlType = Core_MySQL_Types::_INT;
                break;
            case Core_Input_Types::_textarea :
                $this->phpType = Core_PHP_Types::_STRING;
                $this->mysqlType = Core_MySQL_Types::_TEXT;
                break;
            case Core_Input_Types::_textfield :
                $this->phpType = Core_PHP_Types::_STRING;
                $this->mysqlType = Core_MySQL_Types::_VARCHAR;
                if(!$this->maxLength) {
                    $this->maxLength = 60;
                }
                break;
            case Core_Input_Types::_time :
                $this->phpType = Core_PHP_Types::_STRING;
                $this->mysqlType = Core_MySQL_Types::_TIME;
                $this->maxLength = false;
                break;
        }
    }


}