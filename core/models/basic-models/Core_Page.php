<?php
final class Core_Page extends Core_ORM_BasicModel {
    public $id;
    public $name;
    public $title;
    public $requestAddress;
    public $isEnabled;
    public $useTemplate;
    public $templateName;
    public $templateDisplayMode = Core_Template_DisplayMode::full;
    public $templateExtensionName;
    public $targetControllerName;
    public $targetControllerExtensionName;
    public $targetViewName;
    public $targetViewExtensionName;

    public static function findByController($controllerName) {
        return Core_Page::findOne(Core_ORM_Finder::newInstance()
            ->where("base.targetControllerName = ('".$controllerName."')")
        );
    }

    public static function findByView($viewName) {
        return Core_Page::findOne(Core_ORM_Finder::newInstance()
            ->where("base.targetViewName = ('".$viewName."')")
        );
    }

    public static function findByName($name) {
        return Core_Page::findOne(Core_ORM_Finder::newInstance()
            ->where("base.name = ('".$name."')")
        );
    }

    public static function findByAddress($requestAddress) {
        $requestAddress = Core_Utils_String::startsWith($requestAddress,"/");
        $requestAddress = Core_Utils_String::endsWith($requestAddress,"/");
        //check to see if there is an exact match first
        $page = Core_Page::findOne(Core_ORM_Finder::newInstance()
            ->where("base.requestAddress = ('".$requestAddress."')")
        );
        if($page) {
            return $page;
        }

        //keep removing the last field until we have only one left to try and find the closest match
        $fields = Core_Utils_Server::getRequestURLFields();
        while(count($fields) > 0) {
            $url = "/";
            foreach($fields as $field) {
                $url .= $field."/";
            }

            $page = Core_Page::findOne(Core_ORM_Finder::newInstance()
                ->where("base.requestAddress = ('".$url."')")
            );
            if($page) {
                return $page;
            }

            //remove last field and try again
            array_pop($fields);
        }

        return Core_Page::findByName("404 page not found");
    }

    protected function beforeCreate() {
        $this->cleanUpAddress();
        $this->makeSureAddressIsUnique();

        $this->checkControllerViewFileNames();

        $this->createControllerViewFiles();
    }

    protected function beforeUpdate() {
        //if the address has changed
        if($this->requestAddress != $this->internal_old->requestAddress) {
            $this->cleanUpAddress();
            $this->makeSureAddressIsUnique();
        }

        //cannot change request address
        $this->requestAddress = $this->internal_old->requestAddress;
        //cannot change controller/view files associations
        $this->targetViewExtensionName = $this->internal_old->targetViewExtensionName;
        $this->targetViewName = $this->internal_old->targetViewName;
        $this->targetControllerExtensionName = $this->internal_old->targetControllerExtensionName;
        $this->targetControllerName = $this->internal_old->targetControllerName;
    }

    protected function afterCreate() {
        //create the page access events
        $event = new Core_AccessEvent();
        $event->type = "page";
        $event->action = "view";
        $event->target = $this->id;
        $event->pushThis();

        $event = new Core_AccessEvent();
        $event->type = "page";
        $event->action = "edit";
        $event->target = $this->id;
        $event->pushThis();

        //add the page to any blocks that want all pages
        $blocks = Core_LiveBlock::findAll(Core_ORM_Finder::newInstance()
            ->where("base.allPages = 1")
        );
        foreach($blocks as $block) {
            $blockPage = new Core_LiveBlockPage();
            $blockPage->pageId = $this->id;
            $blockPage->liveBlockId = $block->id;
            $blockPage->pushThis();
        }
    }

    protected function afterDelete() {
        //delete the access events
        $events = Core_AccessEvent::findAll(Core_ORM_Finder::newInstance()
            ->where("base.type = ('page') AND base.target = ".$this->id)
        );
        foreach($events as $event) {
            $event->removeThis();
        }

        //delete block pages
        $blockPages = Core_LiveBlockPage::findAll(Core_ORM_Finder::newInstance()
            ->where("base.pageId = ".$this->id)
        );
        foreach($blockPages as $blockPage) {
            $blockPage->removeThis();
        }
    }

    private function cleanUpAddress() {
        $this->requestAddress = preg_replace("/[\s!-.:-@[-`{-~£]/", "-", $this->requestAddress);
        $this->requestAddress = Core_Utils_String::startsWith($this->requestAddress,"/");
        $this->requestAddress = Core_Utils_String::endsWith($this->requestAddress,"/");
    }

    private function makeSureAddressIsUnique() {
        $page = Core_Page::findOne(Core_ORM_Finder::newInstance()
            ->where("base.requestAddress = ('".$this->requestAddress."')")
        );
        if($page) {
            throw new Core_Page_Exception("Trying to change the address of a page to ".$this->requestAddress." but a page with this address already exists.");
        }
    }

    private function checkControllerViewFileNames() {
        //make sure an extension was selected, otherwise use application
        if(!strlen($this->targetViewExtensionName)) {
            $this->targetViewExtensionName = "application";
        }
        if(!strlen($this->targetControllerExtensionName)) {
            $this->targetControllerExtensionName = "application";
        }

        //if no file names assigned, get them from the request address
        if(!strlen($this->targetControllerName)) {
            $this->targetControllerName = Core_Utils_Element::getClassName($this->targetControllerExtensionName,$this->requestAddress,"Controller");
        }
        if(!strlen($this->targetViewName)) {
            $this->targetViewName = Core_Utils_Element::getClassName($this->targetViewExtensionName,$this->requestAddress,"View");
        }
    }

    private function createControllerViewFiles() {
        $fileRoot = "";
        if($this->targetControllerExtensionName == "core" || $this->targetControllerExtensionName == "application") {
            $fileRoot = $this->targetControllerExtensionName;
        } else {
            $fileRoot = "extensions/".$this->targetControllerExtensionName;
        }

        //create the controller file first
        if(!file_exists($fileRoot."/controllers/".$this->targetControllerName.".php")) {
            //make sure the controller path exists
            if(!file_exists($fileRoot."/controllers")) {
                mkdir($fileRoot."/controllers",0775,true);
            }

            $file = Core_Utils_File::createFile($fileRoot."/controllers/".$this->targetControllerName.".php");
            if(!$file) {
                throw new Core_Page_Exception("Permissions are disabling the ability to create the controller file for the page ".$this->name);
            }

            $content = Core_Utils_File::readFromFile("core/assets/file-templates/Core_Controller_Template.txt");
            $content = str_replace("*|CLASS_NAME|*",$this->targetControllerName,$content);

            $file->open("w");
            $file->write($content);
            $file->close();
        }

        //create the view file next
        if(!file_exists($fileRoot."/views/".$this->targetViewName.".php")) {
            //make sure the controller path exists
            if(!file_exists($fileRoot."/views")) {
                mkdir($fileRoot."/views",0775,true);
            }

            $file = Core_Utils_File::createFile($fileRoot."/views/".$this->targetViewName.".php");
            if(!$file) {
                throw new Core_Page_Exception("Permissions are disabling the ability to create the view file for the page ".$this->name);
            }

            $content = Core_Utils_File::readFromFile("core/assets/file-templates/Core_View_Template.txt");
            $content = str_replace("*|CLASS_NAME|*",$this->targetViewName,$content);
            $content = str_replace("*|EXTENDS_CLASS_NAME|*",$this->targetControllerName,$content);

            $file->open("w");
            $file->write($content);
            $file->close();
        }
    }

}