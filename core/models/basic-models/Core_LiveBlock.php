<?php
final class Core_LiveBlock extends Core_ORM_BasicModel {
    public $id;
    public $name;
    public $title;
    public $isEnabled;
    public $blockType;
    public $blockExtensionName;
    public $panelMode;
    public $area;
    public $optionsJson;
    public $allPages;
    public $order;

    public static function findByName($name) {
        return self::findOne(Core_ORM_Finder::newInstance()
            ->where("base.name = ('".$name."')")
        );
    }

    /**
     * Convert the live block model to a core block
     *
     * @return Core_Block
     */
    public function toBlock() {
        $extorio = Core_Extorio::get();
        $extension = $extorio->getExtension($this->blockExtensionName);
        if($extension) {
            $block = $extension->constructBlock($this->blockType);
            $block->id = $this->id;
            $block->title = $this->title;
            $block->panelMode = $this->panelMode;
            $block->options = json_decode($this->optionsJson,true);

            return $block;
        }
        return false;
    }

    protected function afterDelete() {
        $pages = Core_LiveBlockPage::findAll(Core_ORM_Finder::newInstance()
            ->where("base.liveBlockId = ".$this->id)
        );
        foreach($pages as $page) {
            $page->removeThis();
        }
    }

    protected function afterCreate() {
        //create the serve and option events
        $event = new Core_AccessEvent();
        $event->type = "block";
        $event->action = "view";
        $event->target = $this->id;
        $event->pushThis();

        $event = new Core_AccessEvent();
        $event->type = "block";
        $event->action = "edit";
        $event->target = $this->id;
        $event->pushThis();
    }
}