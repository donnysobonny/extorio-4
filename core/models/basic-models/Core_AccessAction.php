<?php
final class Core_AccessAction extends Core_ORM_BasicModel {
    public $id;
    public $eventId;
    public $userType;
    public $accessLevel;
    public $redirectOnSuccess;
    public $redirectOnFailure;
    public $order;

    public function run() {
        //make sure the usertype exists
        $class = Core_Class::findOne(Core_ORM_Finder::newInstance()
            ->where("base.name = ('".$this->userType."') AND base.type = ('user')")
        );
        if($class) {

            $verified = false;

            /** @var $userType Core_BaseModel_Type_User */
            $userType = $this->userType;

            //if there is a user of this type logged in
            $loggedIn = $userType::getLoggedInUser();
            if($loggedIn) {
                //if we need to verify the access level
                if(strlen($this->accessLevel)) {
                    if($loggedIn->accessLevel <= intval($this->accessLevel)) {
                        $verified = true;
                    } else {
                        $verified = false;
                    }
                } else {
                    $verified = true;
                }
            } else {
                $verified = false;
            }

            //verify whether to redirect or not
            if($verified) {
                if(strlen($this->redirectOnSuccess)) {
                    $r = Core_Utils_Server::getRequestURI();
                    header("Location: ".$this->redirectOnSuccess."?r=".urlencode($r));
                    exit;
                } else {
                    return true;
                }
            } else {
                if(strlen($this->redirectOnFailure)) {
                    $r = Core_Utils_Server::getRequestURI();
                    header("Location: ".$this->redirectOnFailure."?r=".urlencode($r));
                    exit;
                } else {
                    return false;
                }
            }

        } else {
            //delete this action, it is useless
            $this->removeThis();
            return false;
        }
    }
}