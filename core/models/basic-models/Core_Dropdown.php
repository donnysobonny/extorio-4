<?php
final class Core_Dropdown extends Core_ORM_BasicModel {
    public $name;
    public $classPath;
    public $isHidden;

    public static function findByName($name) {
        return Core_Dropdown::findOne(Core_ORM_Finder::newInstance()
            ->where("base.name = ('".$name."')")
        );
    }

    protected function beforeDelete() {
        //delete the elements
        $elements = Core_DropdownElement::findByOwnerId($this->id);
        foreach($elements as $element) {
            $element->removeThis();
        }
    }

    protected function afterDelete() {
        //delete the class file
        if(file_exists($this->classPath."/".$this->name.".php")) {
            unlink($this->classPath."/".$this->name.".php");
        }
    }

    public function syncElementsWithClassFile() {

        //create the file if it is not yet created
        if(!file_exists($this->classPath."/".$this->name.".php")) {
            $content = Core_Utils_File::readFromFile("core/assets/file-templates/Core_Dropdown_Template.txt");
            $content = str_replace("*|DROPDOWN_NAME|*",$this->name,$content);
            $content = str_replace("*|DROPDOWN_ELEMENTS|*","",$content);

            $file = Core_Utils_File::createFile($this->classPath."/".$this->name.".php");

            $file->open("w+");
            $file->write($content);
            $file->close();
        }

        $content = Core_Utils_File::readFromFile("core/assets/file-templates/Core_Dropdown_Template.txt");
        $content = $content = str_replace("*|DROPDOWN_NAME|*",$this->name,$content);

        //get all elements
        $constants = "";
        $elements = Core_DropdownElement::findByOwnerId($this->id);
        foreach($elements as $element) {
            $name = Core_Utils_String::replaceSpecialCharacters($element->name,"_");
            $value = "";
            switch(gettype($element->value)) {
                case Core_PHP_Types::_INTEGER :
                    $value = $element->value;
                    break;
                case Core_PHP_Types::_BOOLEAN :
                    $bool = $element->value?"true":"false";
                    break;
                default :
                    $value = '"'.$element->value.'"';
                    break;
            }
            $constants .= "\tconst ".$name." = ".$value.";\n";
        }

        $content = str_replace("*|DROPDOWN_ELEMENTS|*",$constants,$content);

        $file = Core_Utils_File::createFile($this->classPath."/".$this->name.".php");

        $file->open("w+");
        $file->write($content);
        $file->close();

    }

    protected function beforeCreate() {
        $this->cleanUpName();
    }

    protected function beforeUpdate() {
        $this->cleanUpName();
    }


    private function cleanUpName() {
        $this->name = Core_Utils_String::wordsToCamelCase($this->name);
    }
}