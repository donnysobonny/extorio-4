<?php
final class Core_DropdownElement extends Core_ORM_BasicModel {
    public $ownerDropdownId;
    public $value;
    public $name;

    public static function create($ownerDropdownId,$name,$value) {
        $element = new Core_DropdownElement();
        $element->ownerDropdownId = $ownerDropdownId;
        $element->$name = $name;
        $element->value = $value;
        $element->pushThis();

        return $element;
    }

    /**
     * Find all elements owned by a dropdown
     *
     * @param $ownerDropdownId
     *
     * @return Core_DropdownElement[]
     */
    public static function findByOwnerId($ownerDropdownId) {
        return Core_DropdownElement::findAll(Core_ORM_Finder::newInstance()
            ->where("base.ownerDropdownId = ".$ownerDropdownId)
            ->orderBy("base.value")
            ->orderDir("asc")
        );
    }

    /**
     * Find all elements owned by a dropdown
     *
     * @param $ownerDropdownName
     *
     * @return Core_DropdownElement[]
     */
    public static function findByOwnerName($ownerDropdownName) {
        $dropdown = Core_Dropdown::findOne(Core_ORM_Finder::newInstance()->where("base.name = ('".$ownerDropdownName."')"));
        if($dropdown) {
            return Core_DropdownElement::findByOwnerId($dropdown->id);
        } else {
            return array();
        }
    }

    protected function afterCreate() {
        $this->syncElementsWithClassFile();
    }

    protected function afterUpdate() {
        $this->syncElementsWithClassFile();
    }

    protected function afterDelete() {
        $this->syncElementsWithClassFile();
    }

    private function syncElementsWithClassFile() {
        //get the dropdown
        $dropdown = Core_Dropdown::findById($this->ownerDropdownId);
        $dropdown->syncElementsWithClassFile();
    }
}