<?php
final class Core_InstalledExtension extends Core_ORM_BasicModel {
    public $id;
    public $name;
    public $isEnabled;
    public $loadPriority;

    /**
     * @return Core_InstalledExtension[]
     */
    public static function findAllEnabledByPriority() {
        return self::findAll(Core_ORM_Finder::newInstance()
            ->where("base.isEnabled=1")
            ->orderBy("base.loadPriority")
        );
    }
}