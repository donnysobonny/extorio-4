<?php
final class Core_AccessEvent extends Core_ORM_BasicModel {
    public $id;
    public $type;
    public $action;
    public $target;

    public static function runEvent($type,$action,$target) {
        //see if this event was already run
        $extorio = Core_Extorio::get();
        $event = $extorio->getAccessCheck($type,$action,$target);
        if(!is_null($event)) {
            return $event;
        } else {
            $verified = false;
            //non cached event, run the event and cache it
            $event = Core_AccessEvent::find($type,$action,$target);
            if($event) {
                $verified = $event->run();
            } else {
                //no event, no choice but to give access
                $verified = true;
            }

            //cache the event
            $extorio->addAccessCheck($type,$action,$target,$verified);

            return $verified;
        }
    }

    public static function find($type,$action,$target) {
        return Core_AccessEvent::findOne(Core_ORM_Finder::newInstance()
            ->where("
            base.type = ('".$type."') AND
            base.action = ('".$action."') AND
            base.target = ('".$target."')
            ")
        );
    }

    public function run() {
        //get all the actions for this event in priority
        $actions = Core_AccessAction::findAll(Core_ORM_Finder::newInstance()
            ->where("base.eventId = ".$this->id)
            ->orderBy("base.order")
            ->orderDir("asc")
        );
        if(count($actions)) {
            foreach($actions as $action) {
                if($action->run()) {
                    //access granted
                    return true;
                }
            }
            //access not granted
            return false;
        } else {
            //no actions found, allow access!!
            return true;
        }
    }

    protected function beforeCreate() {
        $this->makeSureUnique();
    }

    protected function beforeUpdate() {
        $this->makeSureUnique();
    }

    protected function afterDelete() {
        //delete all actions
        $actions = Core_AccessAction::findAll(Core_ORM_Finder::newInstance()
            ->where("base.eventId = ".$this->id)
        );
        foreach($actions as $action) {
            $action->removeThis();
        }
    }

    private function makeSureUnique() {
        $event = Core_AccessEvent::find($this->type,$this->action,$this->target);
        if($event) {
            throw new Core_Access_Exception("Unable to create event. An event with this type, action and target already exists.");
        }
    }
}