<?php
final class Core_RestRequest extends Core_ORM_BasicModel {
    public $id;
    public $apiKeyId;
    public $session;
    public $sessionUserId;
    public $sessionUserType;
    public $method;
    public $paramJson;
    public $objectDataJson;
    public $responseJson;
}