<?php
final class Core_LiveTask extends Core_ORM_BasicModel {
    public $id;
    public $pid;
    public $taskExtensionName;
    public $taskName;
    public $status;
    public $failedMessage;
    public $dateCreated;
    public $dateUpdated;

    public static function findByPid($pid) {
        return self::findOne(Core_ORM_Finder::newInstance()
            ->where("base.pid = ('".$pid."')")
        );
    }

    public static function findAllByStatus($status) {
        return self::findAll(Core_ORM_Finder::newInstance()
            ->where("base.status = ('".$status."')")
        );
    }

    protected function beforeCreate() {
        $this->status = Core_Task_Status::pending;
        $this->dateCreated = date("Y-m-d H:i:s",time());
    }

    protected function beforeUpdate() {
        $this->dateUpdated = date("Y-m-d H:i:s",time());
    }
}