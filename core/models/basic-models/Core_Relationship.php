<?php

/**
 * A relationship is a link between a parent>child object.
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 * @package Core
 * @subpackage Models
 *
 * Class Core_Relationship
 */
final class Core_Relationship extends Core_ORM_BasicModel {
    public $id;
    public $leftInstanceId;
    public $leftClassName;
    public $rightInstanceId;
    public $rightClassName;
    public $leftToRightProperty;

    /**
     * Find all child relationships for an object's property
     *
     * @param $leftClassName
     * @param $leftInstanceId
     * @param $leftToRightProperty
     *
     * @return Core_Relationship[]
     */
    public static function findChildRelationshipsByPropertyName($leftClassName,$leftInstanceId,$leftToRightProperty) {
        return Core_Relationship::findAll(Core_ORM_Finder::newInstance()
            ->where("base.leftClassName = ('".$leftClassName."') AND base.leftInstanceId = ('"
                .$leftInstanceId."')
             AND
            base.leftToRightProperty = ('".$leftToRightProperty."')")
        );
    }

    /**
     * Find one child relationship for an object's property
     *
     * @param $leftClassName
     * @param $leftInstanceId
     * @param $leftToRightProperty
     *
     * @return Core_Relationship
     */
    public static function findChildRelationshipByPropertyName($leftClassName,$leftInstanceId,$leftToRightProperty) {
        return Core_Relationship::findOne(Core_ORM_Finder::newInstance()
            ->where("base.leftClassName = ('".$leftClassName."') AND base.leftInstanceId = ('"
                .$leftInstanceId."')
         AND
        base.leftToRightProperty = ('".$leftToRightProperty."')")
        );
    }

    public static function findChildRelationshipsByLeftObject($id,$className) {
        return Core_Relationship::findAll(Core_ORM_Finder::newInstance()
            ->where("
            base.leftInstanceId = ('".$id."') AND
            base.leftClassName = ('".$className."')
            ")
        );
    }

    /**
     * Find a child relationship by all
     *
     * @param $leftInstanceId
     * @param $leftClassName
     * @param $rightInstanceId
     * @param $rightClassName
     * @param $leftToRightProperty
     *
     * @return Core_Relationship
     */
    public static function findChildRelationshipByAll(
        $leftInstanceId,
        $leftClassName,
        $rightInstanceId,
        $rightClassName,
        $leftToRightProperty
    ) {
        return Core_Relationship::findOne(Core_ORM_Finder::newInstance()
            ->where("
            base.leftInstanceId = ('".$leftInstanceId."') AND
            base.leftClassName = ('".$leftClassName."') AND
            base.rightInstanceId = ('".$rightInstanceId."') AND
            base.rightClassName = ('".$rightClassName."') AND
            base.leftToRightProperty = ('".$leftToRightProperty."')
            ")
        );
    }

    public static function findAllParentRelationships($id,$className) {
        return Core_Relationship::findAll(Core_ORM_Finder::newInstance()
            ->where("
            base.rightInstanceId = ('".$id."') AND
            base.rightClassName = ('".$className."')
            ")
        );
    }
}