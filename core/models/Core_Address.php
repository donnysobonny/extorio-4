<?php
/**
 * This class extends the internal base class and should be used to add functionality to your model.
 *
 * Class Core_Address
 */
class Core_Address extends Core_BaseModel_Core_Address {
    /**
     * This is called before an object is created.
     */
    protected function beforeCreate() {

    }

    /**
     * This is called after an object is created.
     */
    protected function afterCreate() {

    }

    /**
     * This is called before an object is updated.
     */
    protected function beforeUpdate() {

    }

    /**
     * This is called after an object is updated.
     */
    protected function afterUpdate() {

    }

    /**
     * This is called before an object is deleted.
     */
    protected function beforeDelete() {

    }

    /**
     * This is called after an object is deleted.
     */
    protected function afterDelete() {

    }

    /**
     * This is called before an object is retrieved.
     */
    protected function beforeRetrieve() {

    }

    /**
     * This is called after an object is retrieved.
     */
    protected function afterRetrieve() {

    }

    /**
     * This is called when a property is being displayed. This is useful for changing how a property is displayed in
     * a list.
     *
     * @param string $propertyName
     */
    public function getPropertyValueForDisplay($propertyName) {

    }

    public function canCreate() {
        return true;
    }

    public function canUpdate() {
        return true;
    }

    public function canDelete() {
        return true;
    }

    public function canRetrieve() {
        return true;
    }

    public function canView_Edit() {
        return true;
    }

    public function canView_List() {
        return true;
    }

    public function canView_Detail() {
        return true;
    }

    public function canView_Delete() {
        return true;
    }


}