<?php

session_start();

//$time = microtime();
//$time = explode(' ', $time);
//$time = $time[1] + $time[0];
//$start = $time;

/**
 * Auto load function used to autoload files in the framework
 *
 * @param $className
 */
function autoLoad($className) {

    if(class_exists($className)) {
        return;
    }

    $autoLoadDirs = array(
        "classes",
        "utilities",
        "helpers",
        "models",
        "exceptions"
    );

    $autoLoadExtensions = array(
        "core"
    );

    //add in the loaded modules
    $extensions = Core_Extorio::get()->getExtensions();
    foreach($extensions as $extension) {
        $autoLoadExtensions[] = $extension->root;
    }

    //explode based on \ to support namespaces
    $elements = explode("\\",$className);
    $className = $elements[count($elements)-1];

    foreach($autoLoadExtensions as $extension) {
        foreach($autoLoadDirs as $dir) {
            //get all files in this dir recursively
            $files = Core_Utils_File::findFilesInFolderRecursively($extension."/".$dir);
            foreach($files as $file) {
                if($file->fileName == $className) {
                    include_once($file->path);
                    return;
                }
            }
        }
    }
}

spl_autoload_register("autoLoad");

/**
 * This function logs a system error to file. Only recoverable errors will be logged.
 *
 * @param $number
 * @param $message
 * @param $file
 * @param $line
 */
function systemError($number, $message, $file, $line) {
    if($number <= ini_get("error_reporting")) {
        //log system error
        Core_Logger::systemErrorLog($number,$message,$file,$line);
    }
}

/**
 * This function attempts to log the error to file. If the error is not recoverable,
 * logging is not possible so the error will be displayed if error reporting is on.
 */
function systemShutdown() {
    $error = error_get_last();
    if ($error['type'] != E_ERROR) {
        systemError($error["type"], $error["message"], $error["file"], $error["line"]);
    } else {
        if(ini_get("error_reporting") >= E_ERROR) {
            echo "Unrecoverable error: <br />";
            echo "Error(".$error["type"].") ".$error["message"]." in file ".$error["file"].":".$error["line"];
        }
    }
}

set_error_handler('systemError');
register_shutdown_function("systemShutdown");

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);


###############
# DISPATCHING #
###############
$extorio = false;
try {
    require_once("core/classes/Core_Extorio.php");
    //get extorio
    $extorio = Core_Extorio::get();
    //start extorio and the extensions
    $extorio->start();
    //serve
    $extorio->serve();
} catch(Exception $ex) {

//    echo "<pre>";
//    echo $ex;
//    echo "</pre>";

    //call the on_exception event
    if($extorio) {
        $extorio->createEvent("on_exception")->run($ex);
    } else {
        echo "<pre>";
        echo $ex;
        echo "</pre>";
    }

    //log exception
    Core_Logger::exceptionLog($ex);
}

//echo "<pre>";
//print_r(Core_Extorio::get());
//echo "</pre>";

//echo "<pre>";
//print_r($_SERVER);
//echo "</pre>";

//$time = microtime();
//$time = explode(' ', $time);
//$time = $time[1] + $time[0];
//$finish = $time;
//$total_time = round(($finish - $start), 4);
//echo 'Page generated in '.$total_time.' seconds.';